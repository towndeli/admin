<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function asset_url($path, $isAdmin = true) {
	$URI = '/themes/'.($isAdmin?'quanly/':'frontend/').'assets/' . $path;
	return  base_url($URI);
}
function slug($str, $lowercase = TRUE) {
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/', 'A', $str);
    $str = preg_replace('/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/', 'E', $str);
    $str = preg_replace('/(Ì|Í|Ị|Ỉ|Ĩ)/', 'I', $str);
    $str = preg_replace('/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/', 'O', $str);
    $str = preg_replace('/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/', 'U', $str);
    $str = preg_replace('/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/', 'Y', $str);
    $str = preg_replace('/(Đ)/', 'D', $str);

    $separator   = '-';
    $q_separator = preg_quote($separator);

    $trans = array(
        '&.+?;' => $separator, '[^a-z0-9 _-]' => $separator, '\s+' => $separator,
        '(' . $q_separator . ')+' => $separator
    );

    $str = strip_tags($str);

    foreach ($trans as $key => $val) {
        $str = preg_replace('#' . $key . '#i', $val, $str);
    }

    if ($lowercase === TRUE) {
        $str = strtolower($str);
    }

    $str = str_replace('_', '-', trim($str, $separator));

    return preg_replace('/--/', '-', $str);
}
function slug2($str) {
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/', 'A', $str);
    $str = preg_replace('/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/', 'E', $str);
    $str = preg_replace('/(Ì|Í|Ị|Ỉ|Ĩ)/', 'I', $str);
    $str = preg_replace('/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/', 'O', $str);
    $str = preg_replace('/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/', 'U', $str);
    $str = preg_replace('/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/', 'Y', $str);
    $str = preg_replace('/(Đ)/', 'D', $str);
    //$str = preg_replace('/(,)/', '_', $str);

    $separator   = ' ';
    $q_separator = preg_quote($separator);

    $trans = array(
        '&.+?;' => $separator,
        //'[^a-z0-9 _-]' => $separator,
        '\s+' => $separator,
        '(' . $q_separator . ')+' => $separator
    );

    $str = strip_tags($str);

    foreach ($trans as $key => $val) {
        $str = preg_replace('#' . $key . '#i', $val, $str);
    }

    //$str = str_replace('_', ',', trim($str, $separator));

    return preg_replace('/--/', '-', $str);
}
function randomString($length = 6, $type = 'BOTH') {
    $characters = array_merge(range('a', 'z'), range('A', 'Z'));
    $numbers = range(0, 9);
    $range = '';

    if ($type === 'ONLY_CHAR') {
        $range = $characters;
    } elseif ($type === 'ONLY_NUM') {
        $range = $numbers;
    } else {
        $range = array_merge($characters, $numbers);
    }
    $range = implode('', $range);

    $rangeLength = strlen($range);
    if ($length > $rangeLength) {
        $range = str_repeat($range, ceil($length/$rangeLength));
    }

    return substr(str_shuffle($range), 0, $length);
}
function removeDirectory($path) {
    $path = trim($path, '/');
    $files = glob($path . '/*');
    foreach ($files as $file) {
        is_dir($file) ? removeDirectory($file) : unlink($file);
    }
    @rmdir($path);

    return;
}
function getCoverImageLink($id = null, $table = 'users', $extension = 'png') {
    $file = asset_url('upload/' . $table . '/' . $id . '/coverImage.' . $extension);

    return $file;//is_file($file) ? $file : '';
}
function numberFixedLen($chuoi, $chieudai) {
    return substr(('0000000000'.$chuoi), $chieudai);
}