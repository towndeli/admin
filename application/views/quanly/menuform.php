<script type="text/ng-template" id="myModal.html">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="closeModal()">&times;</button>
        <h4 class="modal-title">Lựa chọn món thêm</h4>
    </div>
    <div class="modal-body">
        <div class="row" style="margin-left: 15px;margin-right: 15px;">
            <div class="col-sm-3" ng-repeat="item in toppings">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="colored-primary" ng-model="item.checked">
                        <span class="text">{{item.name}}</span>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="closeModal()">Close</button>
        <button type="button" class="btn btn-primary" ng-click="capnhattoppingmon()">Save</button>
    </div>
</script>
<div class="form-horizontal Form_">
    <form role="form" ng-submit="luutruregion()">
        <div class="form-body row">

            <div class="col-lg-8">
                <div class="form-group forEdit" ng-if="branchInfo.id">
                    <label class="col-md-4 control-label">ID</label>
                    <div class="col-md-8">
                        <span class="form-control-static idx">{{branchInfo.id}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Danh mục <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <select class="form-control" name="category_id" required ng-model="branchInfo.category_id" ng-options="item.id as item.name for item in categories"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Tên món <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="name" ng-model="branchInfo.name" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Mã sản phẩm</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="product_id" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Mô tả ngắn</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="short_description" cols="30" rows="5" ng-model="branchInfo.description"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Đơn giá <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">

<!--                         <div class="input-group">
								<span class="input-group-addon" style="width: 70px">
								Size 12oz
								</span>
                            <input type="text" class="form-control" name="price" ng-model="branchInfo.price" required>
                        </div> -->
                        <div class="input-group">
								<span class="input-group-addon" style="width: 70px">
								Size M
								</span>
                            <input type="text" class="form-control" name="price_m" ng-model="branchInfo.price_m">
                        </div>
                        <div class="input-group">
								<span class="input-group-addon" style="width: 70px">
								Size L
								</span>
                            <input type="text" class="form-control" name="price_l" ng-model="branchInfo.price_l">
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Dùng ống hút</label>
                    <div class="col-md-8">
                        <select name="use_straw" class="form-control" ng-model="branchInfo.use_straw" style="width: 100px">
                            <option value="0">Không</option>
                            <option value="1">Nhỏ</option>
                            <option value="2">Vừa</option>
                            <option value="3">Lớn</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Dùng muỗng</label>
                    <div class="col-md-8">
                        <select name="use_spoon" class="form-control" ng-model="branchInfo.use_spoon" style="width: 100px">
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Topping/Món thêm</label>
                    <div class="col-md-8">
                        <span class="select2 select2-container select2-container--default select2-container--below select2-container--focus" dir="ltr" style="width: 100%;">
                            <span class="selection">
                                <span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1">
                                    <ul class="select2-selection__rendered">
                                        <li class="pull-right" style="margin: 5px 0 0 5px">
                                            <button type="button" ng-click="openModal('topping')" class="btn btn-xs btn-primary">Bổ sung</button>
                                        </li>
                                        <li class="select2-selection__choice" ng-if="branchInfo.toppings.indexOf(item.id)>=0" ng-repeat="item in toppings">
                                            {{item.name}}
                                        </li>
                                    </ul>
                                </span>
                            </span>
                            <span class="dropdown-wrapper" aria-hidden="true"></span>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Số lượng</label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <input type="text" class="form-control" name="quantity" ng-model="branchInfo.quantity">
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Trạng thái</label>
                    <div class="col-md-8">
                        <div class="md-checkbox has-success">
                            <input type="checkbox" id="status" name="status" class="md-check" ng-model="branchInfo.status" ng-true-value="'1'" ng-false-value="'0'">
                            <label for="status"><span></span><span class="check"></span><span class="box"></span></label>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <span class="btn btn-sm btn-success btnUpload" ngf-select="upload($file)" ngf-pattern="'image/*'" ng-disabled="progressPercentage">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Tải lên</span>
                </span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="{{progressPercentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{progressPercentage}}%"></div>
                </div>
                <a class="thumbnail previewUpload" ngf-select="upload($file)" ngf-pattern="'image/*'" ng-disabled="progressPercentage">
                    <img ng-src="{{myConfig.api_url + 'avatar/menus/' + branchInfo.uniqueId}}" class="img-responsive">
                </a>
            </div>

        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn btn-circle blue btnUpdate">Cập nhật</button>
                    <button type="button" class="btn btn-circle default btnCancel">Hủy</button>
                </div>
            </div>
        </div>
    </form>
</div>