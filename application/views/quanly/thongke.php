<?php

$userId = empty($_SESSION['user_id']) ? 0 : intval($_SESSION['user_id']);
$user = $this->db->where('id', $userId)->get('users')->row();
$role = empty($user->role) ? null : $user->role;
$isAdmin = $role === 'admin' ? true : false;
$isOperator = $role === 'salesman' ? true : false;

if ($isOperator === false):

?>
<div class="row">
    <div class="col-lg-2">
        <table class="table">
            <tr ng-repeat="item in thongke">
                <td style="width: 100px">{{item.table}}</td>
                <td style="text-align: right">{{item.soluong}}</td>
            </tr>
        </table>
    </div>
    <div class="col-lg-10">
        <div class="input-group" style="width: 400px">
            <input type="date" name="reportDate" ng-model="ngayhientai" class="form-control datePicker" placeholder="YYYY-MM-DD">
			<span class="input-group-btn">
				<a class="btn btn-success btnReport" ng-click="thongketinhtrang('theongay')">Xem theo ngày</a>
				<a class="btn btn-danger btnReportAll" ng-click="thongketinhtrang('')">Xem tất cả</a>
			</span>
        </div>
        <div class="reportData">
            <table class="table">
                <thead>
                <tr>
                    <th>Trạng thái</th>
                    <th>Số đơn hàng</th>
                    <th>Số món</th>
                    <th style="text-align: right">Số tiền</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in thongkehoadon.orders">
                    <td>{{item.trangthai}}</td>
                    <td>{{item.soluong}}</td>
                    <td>{{item.total_item}}</td>
                    <td style="text-align: right">{{item.total|number:0}}</td>
                </tr>
                <tr>
                    <th>Tổng cộng</th>
                    <th>{{tongdonhang(thongkehoadon.orders)}}</th>
                    <th>{{tongsomon(thongkehoadon.orders)}}</th>
                    <th style="text-align: right">{{tongsotien2(thongkehoadon.orders)|number:0}}</th>
                </tr>
                </tbody>
            </table>
            <table class="table">
                <thead>
                <tr>
                    <th>Nhân viên</th>
                    <th>Đơn hàng xử lý</th>
                    <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat-start="item3 in danhsach2">
                    <td><b>Chi nhánh {{item3.name}}</b></td>
                    <td colspan="2"><b>{{item3.sodonhang}}</b> đơn hàng: Đang chờ <b>{{item3.tongdangcho}}</b> | Đang xử lý <b>{{item3.tongdangxuly}}</b> | Đang giao <b>{{item3.tongdanggiao}}</b> | Đã giao <b>{{item3.tongdagiao}}</b> | Hoàn tất <b>{{item3.tonghoantat}}</b></td>
                </tr>
                <tr ng-repeat="item in item3.thongtin">
                    <td>{{item.fullname}}</td>
                    <td>
                        <p><b>{{item.sodonhang}}</b> đơn hàng: Đang giao <b>{{item.danggiao}}</b> | Đã giao <b>{{item.dagiao}}</b> | Hoàn tất <b>{{item.hoantat}}</b></p>
                        <p>Tổng tiền: <b>{{item.total|number:0}} đ</b></p>
                        <p>Số tiền NV đang giữ: {{item.tongtiengiu|number:0}} đ</p>
                        <div style="max-height: 105px; overflow: auto;" ng-if="item.danhsachthu.length">
                            <p class="fs11" style="margin-bottom: 0;" ng-repeat="item2 in item.danhsachthu"><i>- Lúc {{item2.ngaythu*1000|date:'HH:mm'}}, ngày {{item2.ngaythu*1000|date:'dd/MM/yyyy'}}, {{item2.nguoithu}} đã hoàn tất thu tiền {{item2.sodonhang}} đơn hàng với số tiền là <b>{{item2.sotienthu|number:0}}</b> đ.</i></p>
                        </div>
                        <span class="btnShowMore" style="font-size: 11px;">Chi tiết >></span>
                        <div>
                            <table class="table table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th>Thông số</th>
                                    <th>Khách hàng</th>
                                    <th>Đơn hàng</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="order in item.orders">
                                    <td>
                                        <b style="text-transform: uppercase">{{order.idv}} [SH: {{order.order_number|numberFixedLen:3}}]</b>
                                        <p>{{order.time_created_text}}<br>{{order.status_text}}</p>
                                    </td>
                                    <td>
                                        {{order.customer_name}} - {{order.customer_phone}}
                                        <p>{{order.shipping_address}}</p>
                                    </td>
                                    <td>{{order.cart_total|number:0}}đ ({{order.cart_total_items}} món)</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td><a class="btn btn-xs btn-info btn-block" ng-if="item.duochoantat" ng-click="updateallstatus(item)">Hoàn tất</a></td>
                </tr>
                <tr ng-repeat-end>
                    <td colspan="3">
                        <b>Tổng tiền: </b>{{item3.total|number:0}} đ (<b>Đang xử lý: </b>{{(item3.total-(item3.tongtiengiu+item3.tongtienthu))|number:0}} đ | <b>NV đang giữ: </b>{{item3.tongtiengiu|number:0}} đ | <b>Đã thu: </b>{{item3.tongtienthu|number:0}} đ)
                    </td>
                </tr>
                </tbody>
            </table>
            <?php /*table class="table">
                <thead>
                <tr>
                    <th>Nhân viên</th>
                    <th>Đơn hàng xử lý</th>
                    <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat-start="(key, tencn) in danhsach | groupBy: 'chinhanh'">
                    <td colspan="3">
                        <b>Chi nhánh {{key}}</b>
                    </td>
                </tr>
                <!--tr ng-repeat-end ng-repeat="child in item" ng-class="child.status_class">
                    <td></td>
                    <td>
                        <a ui-sref="home.branches({id: child.id})">#{{child.id}}</a>
                    </td>
                    <td>
                        <a ui-sref="home.branches({id: child.id})">{{child.name}}</a>
                    </td>
                    <td>
                        <p class="mb0">{{child.status_text}}</p>
                    </td>
                    <td>
                        <p class="mb0">{{child.address}}</p>
                    </td>
                    <td class="text-right"><button class="btn btn-primary btn-xs btn-labeled" ng-if="child.status==0" ng-click="updatestatus(child)"><i class="fa fa-unlock btn-label"></i> Active</button> <button class="btn btn-danger btn-xs btn-labeled" ng-if="child.status" ng-click="updatestatus(child)"><i class="fa fa-lock btn-label"></i> Inactive</button></td>
                </tr-->
                <tr ng-repeat="item in tencn">
                    <td>{{item.fullname}}<!--br>({{item.chinhanh}})--></td>
                    <td>
                        <p><b>{{item.sodonhang}}</b> đơn hàng: Đang giao <b>{{item.danggiao}}</b> | Đã giao <b>{{item.dagiao}}</b> | Hoàn tất <b>{{item.hoantat}}</b></p>
                        <p>Tổng tiền: <b>{{item.total|number:0}} đ</b></p>
                        <p>Số tiền NV đang giữ: {{item.tongtiengiu|number:0}} đ</p>
                        <div style="max-height: 105px; overflow: auto;" ng-if="item.danhsachthu.length">
                            <p class="fs11" style="margin-bottom: 0;" ng-repeat="item2 in item.danhsachthu"><i>- Lúc {{item2.ngaythu*1000|date:'HH:mm'}}, ngày {{item2.ngaythu*1000|date:'dd/MM/yyyy'}}, {{item2.nguoithu}} đã hoàn tất thu tiền {{item2.sodonhang}} đơn hàng với số tiền là <b>{{item2.sotienthu|number:0}}</b> đ.</i></p>
                        </div>
                        <span class="btnShowMore" style="font-size: 11px;">Chi tiết >></span>
                        <div>
                            <table class="table table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th>Thông số</th>
                                    <th>Khách hàng</th>
                                    <th>Đơn hàng</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="order in item.orders">
                                    <td>
                                        <b style="text-transform: uppercase">{{order.idv}} [SH: {{order.order_number|numberFixedLen:3}}]</b>
                                        <p>{{order.time_created_text}}<br>{{order.status_text}}</p>
                                    </td>
                                    <td>
                                        {{order.customer_name}} - {{order.customer_phone}}
                                        <p>{{order.shipping_address}}</p>
                                    </td>
                                    <td>{{order.cart_total|number:0}}đ ({{order.cart_total_items}} món)</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td><a class="btn btn-xs btn-info btn-block" ng-if="item.duochoantat" ng-click="updateallstatus(item)">Hoàn tất</a></td>
                </tr>
                <tr ng-repeat-end>
                    <td colspan="3">
                        <b>Tổng tiền: </b>{{getVolumeSum(tencn)|number:0}} đ
                    </td>
                </tr>
                </tbody>
            </table*/?>
            <!--table class="table">
                <thead>
                    <tr>
                        <th>Tên</th>
                        <th style="width: 120px">Số đơn hàng</th>
                        <th style="width: 120px">Số món</th>
                        <th style="width: 150px">Tổng tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td style="text-align: right"><b>5,917</b></td>
                        <td style="text-align: right"><b>0</b></td>
                        <td style="text-align: right"><b>641,635,000</b></td>
                    </tr>
                </tbody>
            </table-->
        </div>
    </div>
</div>

<?php endif; ?>