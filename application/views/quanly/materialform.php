<div class="form-horizontal Form_">
    <form role="form" ng-submit="luutruregion()">
        <div class="form-body row">

            <div class="col-lg-8">
                <div class="form-group forEdit" ng-if="branchInfo.id">
                    <label class="col-md-4 control-label">ID</label>
                    <div class="col-md-8">
                        <span class="form-control-static idx">{{branchInfo.id}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Tên nguyên liệu <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="name" ng-model="branchInfo.name" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Mô tả</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="description" cols="30" rows="5" ng-model="branchInfo.description"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Trạng thái</label>
                    <div class="col-md-8">
                        <div class="md-checkbox has-success">
                            <input type="checkbox" id="status" name="status" class="md-check" ng-model="branchInfo.status" ng-true-value="'1'" ng-false-value="'0'">
                            <label for="status"><span></span><span class="check"></span><span class="box"></span></label>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <span class="btn btn-sm btn-success btnUpload" ngf-select="upload($file)" ngf-pattern="'image/*'" ng-disabled="progressPercentage">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Tải lên</span>
                </span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="{{progressPercentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{progressPercentage}}%"></div>
                </div>
                <a class="thumbnail previewUpload" ngf-select="upload($file)" ngf-pattern="'image/*'" ng-disabled="progressPercentage">
                    <img ng-src="{{myConfig.api_url + 'avatar/materials/' + branchInfo.uniqueId}}" class="img-responsive">
                </a>
            </div>

        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-circle blue btnUpdate">Cập nhật</button>
                    <button class="btn btn-circle default btnCancel">Hủy</button>
                </div>
            </div>
        </div>
    </form>
</div>