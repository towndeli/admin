<div class="page-header navbar navbar-fixed-top" style="z-index: 100;">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="">
                <img ng-src="{{myConfig.asset_url + 'frontend/images/logo-towndeli.png'}}" alt="logo" class="logo-default"/>
            </a>
            <div class="menu-toggler sidebar-toggler hide">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li>
                    <a ui-sref="home.profile({id: globals.currentUser.userid})"><span class="username username-hide-on-mobile"> {{globals.currentUser.fullname}} </span></a>
                </li>
                <li>
                    <a ui-sref="login"> <i class="icon-logout"></i> </a>
                </li>
            </ul>
        </div>
        <!--a class="btn btn-sm btn-danger" href="http://www.moordy.com/admin/index.php/logout">Thoát</a-->
    </div>
    <!-- END TOP NAVIGATION MENU -->
</div>
<!-- END HEADER INNER -->