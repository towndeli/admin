<style>
    .table > tbody > tr.moitao > td{
        background: #6fcb9f;
        color: #0066FF;
    }
    .table > tbody > tr.dangsua > td{
        background: #666547;
        color: #AACCFF;
    }
    .table > tbody > tr.chuyenchinhanh > td{
        background: #fffeb3;
        color: #0041A3;
    }
    .table > tbody > tr.chuyenlaichinhanh > td{
        background: #fb2e01;
        color: #FFFFFF;
    }
    .table > tbody > tr.dachinhsua > td{
        background: #ff9933;
        color: #3B3B3B;
    }
    .mb0{margin-bottom: 0;}
</style>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> {{titlepage}}
            <a class="btn red btn-xs" ui-sref="home.createorder" target="_blank"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
        </div>
        <div class="tools">
            <a class="reload" title="Làm mới dữ liệu"></a>
        </div>
    </div>

    <div class="row dataListSearch">
        <div class="col-lg-2 col-md-2">
            <select name="branch_id" class="form-control" ng-change="filter_order()" style="margin: 2%;width:96%" ng-model="conditions.branch" ng-options="item.id as item.name for item in branchList2">
                <option value="">Tất cả đơn hàng</option>
                <?php /*foreach($branches as $row){?>
                    <option value="<?php echo $row->id;?>">Xem đ.hàng c.nhánh <?php echo $row->name;?></option>
                <?php }*/ ?>
            </select>
        </div>

        <div class="col-lg-2 col-md-2">
            <select name="orders__status" class="form-control" ng-change="filter_order()" style="margin: 2%;width:96%" ng-model="conditions.status" ng-options="item.id as item.name for item in orderStatus">
                <option value="">Tất cả trạng thái</option>
                <?php /*foreach ($orderStatuses as $Key => $Value): ?>
                    <option value="<?php echo $Key ?>"><?php echo $Value ?></option>
                <?php endforeach;*/ ?>
            </select>
        </div>

        <div class="col-lg-2 col-md-2">
            <input class="form-control" name="orders__idv" ng-keypress="myFunct($event)" placeholder="Mã hóa đơn" style="margin: 2%;" ng-model="conditions.idv">
        </div>

        <div class="col-lg-2 col-md-2">
            <input class="form-control" name="orders__name" ng-keypress="myFunct($event)" placeholder="Tên khách hàng" style="margin: 2%;" ng-model="conditions.name">
        </div>

        <div class="col-lg-2 col-md-2">
            <input class="form-control" name="orders__sdt" ng-keypress="myFunct($event)" placeholder="Số ĐT hoặc Địa chỉ" style="margin: 2%;" ng-model="conditions.phone">
        </div>

        <div class="col-lg-2 col-md-2">
            <a class="btn btn-sm btn-success btnDataListSubmit" ng-click="filter_order()">
                <span class="glyphicon glyphicon-search"></span> Lọc
            </a>
            <a class="btn btn-sm btn-warning" ng-click="reset_order()">
                <span class="glyphicon glyphicon-refresh"></span> Reset
            </a>
        </div>
    </div>

    <div class="portlet-body flip-scroll" style="display: block;">

        <ul uib-pagination ng-if="conditions.loai==0" total-items="pagination.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination.numPerPage" ng-model="pagination.currentPage" ng-change="pageChanged()"></ul>
        <ul uib-pagination ng-if="conditions.loai==1" total-items="pagination2.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination2.numPerPage" ng-model="pagination2.currentPage" ng-change="pageChanged2()"></ul>

        <table class="table table-bordered table-striped table-condensed">
            <thead class="flip-content">
            <tr>
                <!--th style="width:2%"><input type="checkbox" class="selectAllRows"></th-->
                <th style="width:11%">Thông số</th>
                <th style="width:22%">Khách hàng</th>
                <th style="width:26%">Đơn hàng</th>
                <th style="width:24%">Trạng thái</th>
                <th style="width:15%">Bình luận</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="item in filteredOrders | orderBy:['+status', '-shipping_on', '-time_created2']" ng-class="item.status_class">
                <!--td>
                    <i class="fa row-details" ng-class="item.expand?'fa-minus-square-o':'fa-plus-square-o'" ng-click="item.expand=!item.expand"></i>
                </td-->
                <td>
                    <p><a ng-click="chinhsua(item)" style="text-transform: uppercase; font-weight: bold;">#{{item.idv}}</a> <span>SH: <b class="orderNo">{{item.order_number|numberFixedLen:3}}</b></span></p>
                    <p><i>{{item.time_created_text}}</i></p>
                    <p>
                        <a class="btn btn-xs btn-default btnOrderPreview" style="padding: 1px 1px 1px 3px;" ng-if="globals.currentUser.role!='salesman'" ng-click="openModal('print', item)"><span style="padding-top: 4px;" class="badge">{{item.print_count}}</span> <span class="btn btn-xs btn-success" style="margin-right: 0;"><i class="glyphicon glyphicon-print"></i></span></a>
                        <a class="btn btn-xs btn-warning" ng-if="item.chuyenhoadon&&(['admin', 'supregion', 'region', 'salesman', 'branch'].indexOf(globals.currentUser.role)>-1)" ng-click="openModal('chuyenchinhanh', item)"><i class="fa fa-share"></i></a>
                        <a class="btn btn-xs btn-warning" ng-if="item.chuyenlaihoadon&&(['admin', 'supregion', 'region', 'salesman', 'branch'].indexOf(globals.currentUser.role)>-1)" ng-click="xacnhanchuyenlaihoadon(item)"><i class="fa fa-undo"></i></a>
                        <a class="btn btn-xs btn-warning" ng-if="((item.shipping_delay=='0'||item.shipping_delay==0)&&(item.status==1||item.status==2))" ng-click="baotre(item)"><span class="glyphicon glyphicon-alert"></span></a>
                    </p>
                </td>
                <td>
                    <p>{{item.gender=='1'?'Anh':'Chị'}} <a style="font-weight: bold;">{{item.customer_name}}</a> - <b>{{item.customer_phone}}</b></p>
                    <p ng-if="item.shipping_floor" class="mb0">{{item.shipping_floor}}</p>
                    <p>{{item.shipping_address}}</p>
                    <p>Điểm khả dụng: {{(item.user_points - item.user_points_used)|number}}</p>
                    <p>Điểm đã dùng: {{item.user_points_used|number}}</p>
                    <p ng-if="item.branchname"><b>CN:</b> {{item.branchname}}</p>
                </td>
                <td>
                    <p>Giao lúc: <b>{{item.shipping_on_text}}</b></p><!--(getCurrentTime()|date:'HH:mm')-->
                    <p>Tổng cộng: <b>{{item.cart_total|number:0}}đ ({{item.cart_total_items|number:0}} món)</b></p>
                    <p ng-if="item.note">Ghi chú: <b>{{item.note}}</b></p>
                    <p><span ng-repeat="item2 in item.ghichuthuonggap2" ng-if="item.thuonggapnote.indexOf(item2.id) > -1">{{item2.name}}. </span></p>
                    <p>+ Tích điểm: <b>{{item.points|number:0}}</b></p>
                    <p>+ Dùng điểm: <b>{{item.points_used|number:0}}</b></p>
                    <span class="btnShowMore" style="font-size: 11px;">Chi tiết >></span>
                    <div>
                        <table class="table table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>T.T Món</th>
                                <th>SL</th>
                                <th>T.Tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="cart in item.orders">
                                <td>
                                    <b>{{cart.name}} [{{cart.selected_label}}]</b>
                                    <div ng-if="cart.toppings.length">+ <small>{{chuoitoppings(cart.toppings)}}</small></div>
                                    <div ng-if="cart.note||cart.thuonggapnote.length"><small><i style="color:red">** <span ng-repeat="item2 in item.ghichuthuonggap" ng-if="cart.thuonggapnote.indexOf(item2.id) > -1">{{item2.name}}. </span> {{cart.note}}</i></small></div>
                                    <div ng-if="cart.note2">+ <small>{{cart.note2}}</small></div>
                                </td>
                                <td class="text-right">{{cart.qty}}</td>
                                <td class="text-right" ng-class="{'hasDiscount': cart.priceAfter}">
                                    <span>{{(cart.qty * cart.selected_price + (thanhtientoppings(cart.toppings) * cart.qty))|number:0}}</span>
                                    <p ng-show="cart.priceAfter">{{(cart.qty * cart.priceAfter)|number:0}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <p ng-if="item.discount_code">Tạm tính: <b>{{item.cart_subtotal|number:0}}đ</b></p>
                                    <p ng-if="item.discount_code">Mã giảm giá: <b>{{item.discount_code}}</b></p>
                                    <p ng-if="item.points_used > 0">Đổi điểm: <b>-{{item.points_used*1000|number:0}}đ</b></p>
                                    <p>Tổng cộng: <b>{{item.cart_total|number:0}}đ</b></p>
                                    <p ng-if="item.cart_include">** {{item.cart_include}}</p>
                                    <p><a class="btn btn-xs btn-danger btnOrderCancel" ng-if="((item.status<3)&&(item.status>=0)&&(['admin', 'supregion', 'salesman'].indexOf(globals.currentUser.role)>-1))" ng-click="openModal('cancel', item)">Hủy</a></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--p ng-if="item.xacnhanlink&&toigio(item.thoigiangiaohang)"><a ng-href="{{'http://admin.towndeli.vn/index.php/' + item.xacnhanlink}}" target="_blank">Confirm</a></p-->
                    <div style="max-height: 105px; overflow: auto;" ng-if="item.histories.length">
                        <p style="font-style: italic; margin-bottom: 0; font-size: 11px !important;" ng-if="history.action=='edit'||history.action=='print'" ng-repeat="history in item.histories">- {{history.noidung}}.</p>
                    </div>
                </td>
                <td>
                    <p style="font-weight: bold;">{{item.status_text}}</p>
                    <p ng-if="item.shippername">NV giao: <b>{{item.shippername}}</b></p>
                    <p ng-if="(item.status==1||item.status==2)&&(['admin', 'supregion', 'region', 'branch'].indexOf(globals.currentUser.role)>-1)">
                        <select name="shipper_id" style="width: 100%; color: initial;" ng-model="item.shipper_id" ng-options="shipper.id as shipper.last_name disable when shipper.status==0 for shipper in item.shippers">
                            <option value="">-- Chọn NV giao --</option>
                        </select>
                    </p>
                    <a class="btn btn-xs btn-info btn-block" ng-if="item.status==0&&(['admin', 'supregion', 'region', 'branch'].indexOf(globals.currentUser.role)>-1)" ng-click="updatestatus(item, 1)">Chấp nhận</a>
                    <a class="btn btn-xs btn-info btn-block" ng-if="item.status==1&&(['admin', 'supregion', 'region', 'branch'].indexOf(globals.currentUser.role)>-1)" ng-click="updateshipper(item)">Chuyển giao</a>
                    <div class="btn-group btn-group btn-group-justified" ng-if="item.status==2&&(['admin', 'supregion', 'region', 'branch'].indexOf(globals.currentUser.role)>-1)">
                        <a class="btn btn-warning btn-xs" ng-click="updateshipper(item)">Đổi NV giao</a>
                        <a class="btn btn-info btn-xs" ng-show="item.shippername&&toigio(item.thoigiangiaohang)" ng-click="updatestatus(item, 3)">Giao xong</a>
                    </div>
                    <!--a class="btn btn-xs btn-info btn-block" ng-if="item.status==3&&globals.currentUser.role=='branch'" ng-click="updatestatus(item, 4)">Hoàn tất</a-->
                    <div style="max-height: 105px; overflow: auto;" ng-if="item.histories.length">
                        <p style="font-style: italic; margin-bottom: 0; font-size: 11px !important;" ng-repeat="history in item.histories" ng-if="history.action!='edit'&&history.action!='print'">- {{history.noidung}}.</p>
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <input class="form-control" ng-model="item.comment_text" type="text" placeholder="Bình luận" ng-keyup="chuanbiGuiBinhluan(item, $event)">
                            <span class="input-group-btn">
                                <button class="btn btn-success" ng-click="sendComment(item)" type="button" style="margin-right: 0; padding: 7px 10px;">OK</button>
                            </span>
                    </div>
                    <div style="max-height: 105px; overflow: auto;" ng-if="item.comments.length">
                        <p style="font-style: italic; margin-bottom: 0; font-size: 11px !important;" ng-repeat="comment in item.comments"><strong>{{comment.name}} ({{comment.time_created_text2}}): </strong>{{comment.noidung}}</p>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <ul uib-pagination ng-if="conditions.loai==0" total-items="pagination.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination.numPerPage" ng-model="pagination.currentPage" ng-change="pageChanged()"></ul>
        <ul uib-pagination ng-if="conditions.loai==1" total-items="pagination2.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination2.numPerPage" ng-model="pagination2.currentPage" ng-change="pageChanged2()"></ul>

    </div>
</div>
<script type="text/ng-template" id="myModal.html">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="closeModal()">&times;</button>
        <h4 class="modal-title" ng-if="loai=='print'"><b>IN HÓA ĐƠN</b></h4>
        <h4 class="modal-title" ng-if="loai=='cancel'">XÁC NHẬN HỦY HÓA ĐƠN</h4>
        <h4 class="modal-title" ng-if="loai=='chuyenchinhanh'">CHUYỂN HÓA ĐƠN QUA CHI NHÁNH KHÁC</h4>
    </div>
    <div class="modal-body" ng-if="loai=='chuyenchinhanh'">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Chọn chi nhánh cần chuyển tới</label>
                    <select class="form-control" ng-model="chuyenchinhanh.id" ng-options="item.id as item.name for item in danhsachchinhanh"></select>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-body" ng-if="loai=='cancel'">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <select class="form-control" ng-model="lydohuydonhang.loai" ng-options="item.id as (item.id+'. '+item.name) for item in danhsachlydohuy"></select>
                </div>
                <div class="form-group mb0" ng-show="lydohuydonhang.loai=='5'">
                    <textarea ng-model="lydohuydonhang.noidung" class="form-control" placeholder="Nhập lý do khác để xác nhận hủy đơn hàng" rows="5" required></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-body" id="printDiv" ng-if="loai=='print'">
        <table cellspacing="5" cellpadding="5" width="100%">
            <tr>
                <td width="61" class="text-center" style="border: 1px solid #616265;font-weight: bold;">{{selected.order_number|numberFixedLen:3}}</td>
                <td width="8">&nbsp;</td>
                <td width="128" class="print_hidden" style="vertical-align: top;"><img ng-src="{{myConfig.logo_print}}" height="61"></td>
                <td width="8" class="print_hidden">&nbsp;</td>
                <td>
                    <h3 class="chuhoa m0 pb5" style="text-transform: uppercase;margin-top: 0;margin-bottom: 0; font-weight: 700;">HÓA ĐƠN #{{selected.idv}}</h3>
                    <p class="mb0 innghieng" style="margin-bottom: 0;">{{selected.time_created_text}}</p>
                    <p class="mb0" style="font-weight: bold;margin-bottom: 0;">{{myConfig.website}} - {{myConfig.phone}}</p>
                </td>
            </tr>
        </table>
        <table class="table mt5" style="margin-bottom: 3px">
            <thead>
            <tr>
                <th>#</th>
                <th>Thông tin món</th>
                <th class="text-right">SL</th>
                <th class="text-right">Tạm tính</th>
                <th>Ghi chú</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="item in selected.orders">
                <td>{{$index + 1}}</td>
                <td>
                    {{item.name}} [{{item.selected_label}}]
                    <p class="mb0 fs11 lh14" style="margin-bottom: 0;" ng-if="item.toppings.length"><small>+ {{chuoitoppings(item.toppings)}}</small></p>
                    <!--p class="mb0 fs11 lh14" style="margin-bottom: 0;" ng-if="item.note"><small class="danger">*** {{item.note}}</small></p-->
                    <div ng-if="item.note2">+ <small>{{item.note2}}</small></div>
                </td>
                <td class="text-right">{{item.qty}}</td>
                <td ng-class="{'hasDiscount': item.priceAfter}" class="text-right">
                    <span>{{(item.qty * item.selected_price + (thanhtientoppings(item.toppings) * item.qty))|number:0}}đ</span>
                    <p ng-if="item.priceAfter">{{(item.qty * item.priceAfter)|number:0}}đ</p>
                </td>
                <td>
                <span ng-repeat="item2 in selected.ghichuthuonggap" ng-if="item.thuonggapnote.indexOf(item2.id) > -1">{{item2.name}}. </span> {{item.note}}</td>
            </tr>
            </tbody>
            <tfoot>
            <tr ng-if="selected.discount_code">
                <td></td>
                <td>Tạm tính</td>
                <td></td>
                <td class="text-right">{{selected.cart_subtotal|number:0}}đ</td>
                <td></td>
            </tr>
            <tr ng-if="selected.discount_code">
                <td></td>
                <td colspan="2">Mã KM <b>{{selected.discount_code}}</b></td>
                <td class="text-right"><b>-{{(selected.cart_subtotal - selected.cart_total)|number:0}}đ</b></td>
                <td></td>
            </tr>
            <tr ng-if="selected.points_used > 0">
                <td></td>
                <td><b>Dùng điểm</b></td>
                <td class="text-right"><b>{{selected.points_used|number:0}}</b></td>
                <td class="text-right"><b>-{{selected.points_used * 1000|number:0}}đ</b></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td><b style="text-transform: uppercase;">Tổng cộng</b></td>
                <td class="text-right"><b>{{selected.cart_total_items|number:0}}</b></td>
                <td class="text-right"><b>{{selected.cart_total|number:0}}đ</b></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="4">
                    <b>QK tích lũy được {{selected.points}} điểm. 
                    Tổng điểm đang có hiện tại là {{(selected.user_points + selected.points - selected.user_points_used)}} điểm.</b>
                </td>
            </tr>
            </tfoot>
        </table>
        <table class="table mt5" style="margin-bottom: 3px">
            <tbody>
            <tr>
                <td width="50%">
                    {{selected.gender=='1'?'Anh':'Chị'}} {{selected.customer_name}} ({{selected.customer_phone}})
                    <p class="mb0" ng-if="selected.shipping_floor">{{selected.shipping_floor}}</p>
                    <p class="mb0">{{selected.shipping_address}}</p>
                </td>
                <td>
                    <p class="mb0">Giao lúc: <strong>{{selected.shipping_on_text}}</strong></p>
                    <p class="mb0" ng-if="selected.note">{{selected.note}}</p>
                    <p class="mb0" ng-if="selected.cart_include">** {{selected.cart_include}}</p>
                    <p class="mb0"><span ng-repeat="item2 in selected.ghichuthuonggap2" ng-if="selected.thuonggapnote.indexOf(item2.id) > -1">{{item2.name}}. </span></p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <div class="text-center" ng-if="loai=='chuyenchinhanh'"><button type="button" class="btn btn-primary" ng-click="xacnhanchuyenchinhanh()"><i class="fa fa-check"></i> Xác nhận</button></div>
        <div class="text-center" ng-if="loai=='print'"><button type="button" class="btn btn-md btn-success" ng-click="printDiv('printDiv')"><span class="glyphicon glyphicon-print"></span> In hóa đơn này</button></div>
        <div ng-if="loai=='cancel'">
            <button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="closeModal()">Close</button>
            <button type="button" class="btn btn-primary" ng-click="xacnhanhuydonhang()">Save</button>
        </div>
    </div>
</script>
