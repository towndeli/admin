<script type="text/ng-template" id="angular-google-maps-geocoder-item.html">
    <a ng-bind-html='match.model.formatted_address | typeaheadHighlight:query | trustAsHtml'></a>
</script>
<style>
    .google-map{width: 100%; height: 400px;}
</style>
<div class="form-horizontal Form_">
    <form role="form" ng-submit="luutruregion()">
        <div class="form-body row">

            <div class="col-lg-6">
                <div class="form-group forEdit" ng-if="branchInfo.id">
                    <label class="col-md-3 control-label">ID</label>
                    <div class="col-md-9">
                        <span class="form-control-static idx">{{branchInfo.id}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Tên chi nhánh <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="name" ng-model="branchInfo.name" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Khu vực <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-9">
                        <select class="form-control" name="region_id" ng-model="branchInfo.region_id" ng-options="item.id as item.name group by item.groupName disable when item.status==0 for item in rootregions"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Địa chỉ <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" autocomplete="off" name="address" ng-change="thaydoidiachi()" uib-typeahead="address for address in getLocation($viewValue)" typeahead-on-select="luachondiachi($item, $model, $label, $event)" id="inputAddress3" ng-model="branchInfo.address">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Trạng thái</label>
                    <div class="col-md-9">
                        <div class="md-checkbox has-success">
                            <input type="checkbox" id="status" name="status" class="md-check" ng-model="branchInfo.status" ng-true-value="'1'" ng-false-value="'0'">
                            <label for="status"><span></span><span class="check"></span><span class="box"></span></label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <google-map-locator class="google-map"
                                    location="{{location}}"
                                    radius="0"
                                    on-location-initialize="onLocationInitialize(location)"
                                    on-location-change="onLocationChange(location)"
                                    on-map-loaded="onMapLoaded(map)"></google-map-locator>
            </div>

        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn btn-circle blue btnUpdate">Cập nhật</button>
                    <button type="button" class="btn btn-circle default btnCancel">Hủy</button>
                </div>
            </div>
        </div>

    </form>
</div>