<div class="form-horizontal Form_">
    <form role="form" ng-submit="luutruregion()">
        <div class="form-body row">

            <!-- BLOCK LEFT -->
            <div class="col-lg-8">
                <div class="form-group forEdit" ng-if="branchInfo.id">
                    <label class="col-md-4 control-label">ID</label>
                    <div class="col-md-8">
                        <span class="form-control-static idx">{{branchInfo.id}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Họ tên <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <input type="text" class="form-control" name="first_name" ng-model="branchInfo.first_name" placeholder="Họ và tên lót">
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <input type="text" class="form-control" name="last_name" required ng-model="branchInfo.last_name" placeholder="Tên gọi">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Số ĐT <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="phone" required ng-model="branchInfo.phone">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Username <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="username" required ng-model="branchInfo.username">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Email <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="email" class="form-control" name="email" required ng-model="branchInfo.email">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Mật khẩu <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <input type="password" class="form-control" ng-model="branchInfo.password" name="password">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                    </div>
                </div>

                <div class="form-group" ng-if="branchInfo.role=='admin'">
                    <label class="col-md-4 control-label">Loại tài khoản</label>
                    <div class="col-md-8">
                        <input type="text" disabled readonly class="form-control" value="Administrator">
                    </div>
                </div>

                <div class="form-group" ng-if="branchInfo.role!='admin'&&branchInfo.role!='customer'">
                    <label for="inputEmail17" class="col-md-4 control-label">Khu vực / Chi nhánh <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <div class="btn-group">
                            <a class="btn btn-default" href="javascript:void(0);">{{tenRegion()}}</a>
                            <a class="btn btn-default  dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li ng-repeat="item in regions" ng-style="{'background-color': item.id2==branchInfo.role_tbl?'#f5f5f5':''}">
                                    <a href="javascript:void(0);" ng-click="chonRegion(item.id2)" ng-style="{'padding-left': item.paddingleft + 'px'}">{{item.name}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="form-group" ng-if="branchInfo.role!='admin'&&branchInfo.role!='customer'">
                    <label for="inputEmail19" class="col-md-4 control-label">Loại tài khoản <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-8">
                        <select class="form-control" id="inputEmail19" placeholder="Loại tài khoản" ng-model="branchInfo.role" ng-options="item.id as item.name for item in account_types"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Ngày sinh</label>
                    <div class="col-md-8">
                        <input type="date" class="form-control datePicker" ng-model="branchInfo.birthday" name="ngaysinh">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Giới tính</label>
                    <div class="col-md-8">
                        <!--input type="hidden" id="gender" name="gender" value="<?php //echo @$record->gender ?>" /-->
                        <div class="md-radio-inline">
                            <div class="md-radio" data-value="1">
                                <input type="radio" id="gender1" name="gender" class="md-radiobtn" ng-model="branchInfo.gender" value="1">
                                <label for="gender1">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    Nam </label>
                            </div>
                            <div class="md-radio" data-value="0">
                                <input type="radio" id="gender2" name="gender" class="md-radiobtn" ng-model="branchInfo.gender" value="0">
                                <label for="gender2">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    Nữ </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Địa chỉ nhà</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="home_address" ng-model="branchInfo.home_address">
                    </div>
                </div>

                <?php /*if(isset($danhsach)&&count($danhsach)): ?>
                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-8">
                            <table class="table table-bordered table-striped table-condensed">
                                <?php foreach($danhsach as $row): ?>
                                    <tr>
                                        <td><?php echo $row->shipping_address; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                <?php endif;*/ ?>

                <div class="form-group">
                    <label class="col-md-4 control-label">Địa chỉ cty</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="company_address" ng-model="branchInfo.company_address">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Facebook</label>
                    <div class="col-md-8">
                        <input type="url" class="form-control" name="page_facebook" ng-model="branchInfo.page_facebook">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Twitter</label>
                    <div class="col-md-8">
                        <input type="url" class="form-control" name="page_twitter" ng-model="branchInfo.page_twitter">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Trạng thái</label>
                    <div class="col-md-8">
                        <div class="md-checkbox has-success">
                            <input type="checkbox" id="status" name="status" class="md-check" ng-model="branchInfo.status" ng-true-value="'1'" ng-false-value="'0'">
                            <label for="status"><span></span><span class="check"></span><span class="box"></span></label>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END: BLOCK LEFT -->

            <div class="col-lg-4">
                <span class="btn btn-sm btn-success btnUpload" ngf-select="upload($file)" ngf-pattern="'image/*'" ng-disabled="progressPercentage">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Tải lên</span>
                </span>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="{{progressPercentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{progressPercentage}}%"></div>
                </div>
                <a class="thumbnail previewUpload" ngf-select="upload($file)" ngf-pattern="'image/*'" ng-disabled="progressPercentage">
                    <img ng-src="{{myConfig.api_url + 'avatar/users/' + branchInfo.uniqueId}}" class="img-responsive">
                </a>
            </div>
        </div>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-circle blue btnUpdate">Cập nhật</button>
                    <button class="btn btn-circle default btnCancel">Hủy</button>
                </div>
            </div>
        </div>

    </form>
</div>