
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo asset_url('global/plugins/respond.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/excanvas.min.js');?>"></script>
<![endif]-->
<script src="<?php echo asset_url('global/plugins/jquery.min.js');?>"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo asset_url('global/plugins/jquery-ui/jquery-ui.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.blockui.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.cokie.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/uniform/jquery.uniform.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo asset_url('global/plugins/flot/jquery.flot.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/flot/jquery.flot.resize.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/flot/jquery.flot.categories.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.pulsate.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-daterangepicker/moment.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-daterangepicker/daterangepicker.js');?>"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="<?php echo asset_url('global/plugins/fullcalendar/fullcalendar.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery-easypiechart/jquery.easypiechart.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.sparkline.min.js');?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo asset_url('global/scripts/metronic.js');?>"></script>
<script src="<?php echo asset_url('admin/layout/scripts/layout.js');?>"></script>
<script src="<?php echo asset_url('admin/layout/scripts/quick-sidebar.js');?>"></script>
<script src="<?php echo asset_url('admin/layout/scripts/demo.js');?>"></script>
<script src="<?php echo asset_url('admin/pages/scripts/index.js');?>"></script>
<script src="<?php echo asset_url('admin/pages/scripts/tasks.js');?>"></script>

<script src="<?php echo asset_url('global/plugins/fileupload/jquery.ui.widget.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/fileupload/jquery.iframe-transport.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/fileupload/jquery.fileupload.js');?>"></script>

<script src="<?php echo asset_url('global/plugins/select2/js/select2.full.min.js');?>"></script>

<script src="<?php echo asset_url('global/plugins/datepicker/js/bootstrap-datepicker.js');?>"></script>

<script src="<?php echo asset_url('js/jQuery.print.js');?>"></script>

<script>
    var PARAMS = {
        siteURL: "admin/index.php",
        ajaxURL: "admin/index.php/ajax",
        moduleURL: "admin/index.php",
        model: "index",
        method: "",
        action: "",
        uploadURL: ""
    };

    var API = {
        cart: "admin/index.php/api/cart"
    };

    var URL = {
        site: "admin/index.php",
        ajax: "admin/index.php/ajax",
        upload: "admin/index.php/upload",
        module: "admin/index.php"
    };
</script>
<script src="<?php echo asset_url('global/scripts/common.js');?>"></script>
<script src="<?php echo asset_url('global/scripts/functions.js');?>"></script>
<script src="<?php echo asset_url('admin/Notification.min.js');?>"></script>

<!--<script src="<?php echo asset_url('global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>-->

<!-- END PAGE LEVEL SCRIPTS -->
<script type="javascript">
    jQuery(document).ready(function() {

        if (Notification.permission !== 'denied') {
            Notification.requestPermission();
        }

        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        Index.init();
        Index.initDashboardDaterange();
        //Index.initJQVMAP(); // init index page's custom scripts
        Index.initCalendar(); // init index page's custom scripts
        Index.initCharts(); // init index page's custom scripts
        Index.initChat();
        Index.initMiniCharts();
        Tasks.initDashboardWidget();


        x = $('.page-sidebar-menu >li >a');


        $('.page-sidebar-menu >li >a').each(function(i,v){
            /*
             regex = '/admin$/gi';
             eval('var regex = /'+ $(this).attr('href').replace('//', '\/\/') + '/;');

             if (regex.test(url)) {
             $(this).parent().addClass('active');
             }
             */
        });

    });
</script>
<!-- END JAVASCRIPTS -->

<script src="<?php echo asset_url('angular-1.5.8/angular.min.js');?>"></script>
<!--script src="<?php echo asset_url('angular-1.5.8/angular-route.min.js');?>"></script-->
<script src="<?php echo asset_url('angular-1.5.8/angular-cookies.min.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular-sanitize.min.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/ui-bootstrap-2.2.0.min.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular-ui-router.min.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular-filter.min.js');?>"></script>
<!-- Now is <?php echo date('d/m/Y H:i') ?> -->
<?php if (date('H') >= 12): ?>
<script src="//maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDHAxm7KVtIWZWC4SEYmtMXVYRLmDdCjcY&libraries=places"></script><!--O: <12h-->
<?php else: ?>
<script src="//maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyB956FEe0uRCrbMXBEqvgH_pXSPBXHcvX8&libraries=places"></script><!--N: >=12h-->
<?php endif; ?>
<script src="<?php echo asset_url('js/locationpicker.jquery.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular-googlemap-location-picker.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular-google-maps-geocoder.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular.audio.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/socket.io-1.4.5.js');?>"></script>
<!--script src="https://cdn.socket.io/socket.io-1.4.5.js"></script-->
<script src="<?php echo asset_url('angular-1.5.8/angular-socket-io/socket.min.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/select2.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/ng-file-upload.min.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular-bootstrap-switch.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular-notification.min.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/autocomplete.js');?>"></script>
<script src="<?php echo asset_url('js/myLibraries.js');?>"></script>
<script src="//bennadel.github.io/JavaScript-Demos/vendor/stacktrace/stacktrace-min-0.4.js"></script>
<script>
    var asset_url = "<?php echo asset_url('');?>";
    var site_url = "<?php echo site_url('quanly');?>/";
    var base_url = "<?php echo base_url();?>";
    var api_url = "<?php echo site_url('quanly/api');?>/";
    var socket_url = '//<?php echo $_SERVER['HTTP_HOST'] ?>:3000';
    //var socket_url = "http://localhost:3000";
    var logo_print_url = "<?php echo asset_url('frontend/images/logo-towndeli.png');?>";
    //console.re.error('remote log test');
</script>
<script src="<?php echo asset_url('scripts/app.js');?>"></script>
<script src="<?php echo asset_url('modules/authentication/services.js');?>"></script>
<script src="<?php echo asset_url('modules/authentication/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/home/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/orders/controllers.js?v='.time());?>"></script>
<script src="<?php echo asset_url('modules/regions/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/branches/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/categories/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/toppings/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/materials/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/menus/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/users/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/customers/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/feedbacks/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/sms/controllers.js');?>"></script>
<script src="<?php echo asset_url('modules/bugs/controllers.js');?>"></script>
<!--<script src="https://uhchat.net/code.php?f=2ca29c"></script>-->
</body>
</html>