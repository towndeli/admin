
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="vi" ng-app="BasicHttpAuthExample">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <title>{{titlepage}} | TownDeli.VN</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content="Quốc Hưng Nguyễn"/>

    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"><![endif]--><!-- Force IE to use the latest rendering engine -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><!-- Optimize mobile viewport -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!-- icons and icons and icons and icons and icons and a tile -->
    <meta name="application-name" content="Stencil"><!-- Windows 8 Tile Name -->
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>favicon.ico"><!-- Windows 8 Tile Image -->
    <meta name="msapplication-TileColor" content="#4eb4e5"><!-- Windows 8 Tile Color -->
    <link rel="icon" href="<?php echo asset_url('img/icons/favicon-32.png');?>" type="image/png"><!-- default favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico"><!-- legacy default favicon (in root, 32x32) -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo asset_url('img/icons/favicon-57.png');?>"><!-- iPhone low-res and Android -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo asset_url('img/icons/favicon-57.png');?>"><!-- legacy Android -->
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo asset_url('img/icons/favicon-72.png');?>"><!-- iPad -->
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo asset_url('img/icons/favicon-114.png');?>"><!-- iPhone 4 -->
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo asset_url('img/icons/favicon-144.png');?>"><!-- iPad hi-res -->

    <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <link href="<?php echo asset_url('global/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/simple-line-icons/simple-line-icons.min.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/uniform/css/uniform.default.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');?>" rel="stylesheet" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo asset_url('global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/fullcalendar/fullcalendar.min.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/jqvmap/jqvmap/jqvmap.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');?>" rel="stylesheet" />
    <!-- END PAGE LEVEL PLUGIN STYLES -->
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <!--link href="<?php echo asset_url('admin/pages/css/login3.css');?>" ng-if="page=='login'" rel="stylesheet"/-->
    <!-- BEGIN PAGE STYLES -->
    <link href="<?php echo asset_url('admin/pages/css/tasks.css');?>" rel="stylesheet" />
    <!-- END PAGE STYLES -->
    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css') ?>' stylesheet instead of 'components.css' in the below style tag -->
    <link href="<?php echo asset_url('global/css/components.css');?>" id="style_components" rel="stylesheet" />
    <link href="<?php echo asset_url('admin/layout/css/layout.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('admin/layout/css/themes/darkblue.css');?>" rel="stylesheet" id="style_color"/>
    <link href="<?php echo asset_url('admin/layout/css/custom.css');?>" rel="stylesheet" />

    <link href="<?php echo asset_url('global/plugins/select2/css/select2.min.css');?>" rel="stylesheet" />


    <link href="<?php echo asset_url('global/css/common.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('css/mystyle.css');?>" rel="stylesheet" />

    <link href="<?php echo asset_url('angular-1.5.8/autocomplete.css');?>" rel="stylesheet" />
    <!-- END THEME STYLES -->
    <!--<script src="//console.re/connector.js" data-channel="try-e493-688d-df37" id="consolerescript"></script>-->
</head>
<body ng-class="page=='login'?'login':('page-admin index page-header-fixed page-quick-sidebar-over-content page-style-square'+(thunho?' page-sidebar-closed':''))">