<div class="portlet box green" ng-if="regionInfo.id">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Thông tin chi tiết
            <!--a class="btn red btn-xs" ui-sref="home.createregion"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a-->
        </div>
        <div class="tools">
            <a class="reload" title="Làm mới dữ liệu"></a>
            <a class="remove" title="Xóa"></a>
        </div>
    </div>
    <div class="portlet-body form">
        <ng-include src="myConfig.api_url + 'pages/regionform'" include-replace></ng-include>
    </div>
</div>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> {{titlepage}}
            <a class="btn red btn-xs" ui-sref="home.createregion"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
        </div>
        <div class="tools">
            <a class="reload" title="Làm mới dữ liệu"></a>
        </div>
    </div>
    <div class="portlet-body flip-scroll" style="display: block;">

        <ul uib-pagination total-items="pagination.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination.numPerPage" ng-model="pagination.currentPage" ng-change="pageChanged()"></ul>

        <table class="table table-bordered table-striped table-condensed">
            <thead class="flip-content">
            <tr>
                <th style="width: 25px"><input type="checkbox" class="selectAllRows"></th>
                <th>ID</th>
                <th>Tên khu vực</th>
                <th>Trạng thái</th>
                <th>Người quản lý</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat-start="item in filteredOrders">
                <td>
                    <input type="checkbox" class="selectRow">
                </td>
                <td class="text-right">
                    {{item.id}}
                </td>
                <td>
                    <a ui-sref="home.regions({id: item.id})">{{item.name}}</a>
                </td>
                <td>
                    <div class="md-checkbox has-success">
                        <input type="checkbox" id="checkbox_{{item.id}}" class="md-check" ng-checked="item.status">
                        <label for="checkbox_{{item.id}}" ng-click="updatestatus(item)"><span></span><span class="check"></span><span class="box"></span></label>
                    </div>
                </td>
                <td>{{item.quanly}}</td>
            </tr>
            <tr ng-repeat-end ng-if="item.children.length" ng-repeat="child in item.children">
                <td>
                    <input type="checkbox" class="selectRow">
                </td>
                <td class="text-right">
                    {{child.id}}
                </td>
                <td style="padding-left: 25px;">
                    <a ui-sref="home.regions({id: child.id})">{{child.name}}</a>
                </td>
                <td>
                    <div class="md-checkbox has-success">
                        <input type="checkbox" id="checkbox_{{child.id}}" class="md-check" ng-checked="child.status">
                        <label for="checkbox_{{child.id}}" ng-click="updatestatus(child)"><span></span><span class="check"></span><span class="box"></span></label>
                    </div>
                </td>
                <td>{{child.quanly}}</td>
            </tr>
            </tbody>
        </table>

        <ul uib-pagination total-items="pagination.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination.numPerPage" ng-model="pagination.currentPage" ng-change="pageChanged()"></ul>

    </div>
</div>