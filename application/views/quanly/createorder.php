
<script type="text/ng-template" id="myModal.html">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="closeModal()">&times;</button>
        <h4 class="modal-title">{{selected.name}}</h4>
    </div>
    <div class="modal-body">
        <div data-id="{{selected.id}}" data-rowid="">
            <div class="row">
                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon">Số lượng</span>
                        <span class="input-group-btn"><button class="btn btn-default" type="button" ng-click="giamsoluong(selected, 1)">-</button></span>
                        <input type="text" class="form-control" id="ordersMenuQuantity" ng-model="selected.qty" min="1">
                        <span class="input-group-btn"><button class="btn btn-default" type="button" ng-click="tangsoluong(selected)">+</button></span>
                    </div>
                </div>
                <div class="col-sm-8">
                    <p class="ordersMenuSizes" style="margin-bottom: 5px;">
                        Chọn size:
<!--                         <a href="" class="btn btn-md btn-success" ng-click="selected.selected_size='sm'" ng-class="selected.selected_size=='sm'?'selected':''">{{selected.size.sm.price/1000|number:0}}K [{{selected.size.sm.label}}]</a> -->
                        <a href="" class="btn btn-md btn-success" ng-click="selected.selected_size='md'" ng-class="selected.selected_size=='md'?'selected':''">{{selected.size.md.price/1000|number:0}}K [{{selected.size.md.label}}]</a>
                        <a href="" class="btn btn-md btn-success" ng-click="selected.selected_size='lg'" ng-class="selected.selected_size=='lg'?'selected':''">{{selected.size.lg.price/1000|number:0}}K [{{selected.size.lg.label}}]</a>
                    </p>
                </div>
            </div>
            <div class="ordersMenuTopping">
                <a ng-repeat="item in selected.toppings" ng-click="tangsoluong(item)" style="margin-bottom: 0;font-size: 12px;" class="btn btn-sm btn-default" data-id="{{item.id}}">
                    <span class="badge">{{item.qty}}</span>
                    {{item.name}}
                    <span style="font-size: 11px;" class="btn btn-xs btn-danger btnOrdersToppingDecrease" ng-click="giamsoluong(item);$event.stopPropagation()"><i class="glyphicon glyphicon-minus" aria-hidden="true"></i></span>
                </a>
            </div>
            <div style="margin-top: 5px;">
                <p ng-repeat="(key, item) in selected.ghichuthuonggap | groupBy: 'nhom'">
                    <label ng-repeat="subitem in item" style="margin-right: 10px; margin-bottom: 0;"><input type="checkbox" ng-click="toggleSelection(subitem.id, subitem.name)" ng-checked="selected.thuonggapnote.indexOf(subitem.id) > -1" value="{{subitem.id}}"> {{subitem.name}}</label>
                </p>
            </div>
            <div style="margin-top: 5px;">
                <label for="ordersMenuNote"><b>Ghi chú khác</b></label>
                <textarea id="ordersMenuNote" class="form-control" cols="30" rows="3" ng-model="selected.note"></textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <!--button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="closeModal()">Close</button-->
        <button type="button" class="btn btn-primary" ng-click="additemtocart()">Thêm</button>
    </div>
</script>
<script type="text/ng-template" id="customTemplate.html">
    <a>
        <span ng-bind-html="(match.model.first_name + ' ' + match.model.last_name + ' (' + match.model.customer_phone + ')' + (match.model.shipping_address?(' - ' + match.model.shipping_address):'') + (match.model.shipping_floor?(' (' + match.model.shipping_floor + ')'):'')) | uibTypeaheadHighlight:query"></span>
    </a>
</script>
<script type="text/ng-template" id="customTemplate2.html">
    <a>
        <span ng-bind-html="match.model.label | uibTypeaheadHighlight:query"></span>
        <p ng-bind-html="match.model.description"></p>
    </a>
</script>
<style>
    .sdt .dropdown-menu {
        overflow: scroll;
        float: none;
        height: 340px;/*102px;*/
        top: -340px !important;/*102px;*/
    }
    .sdt2 .dropdown-menu {
        overflow: scroll;
        float: none;
        height: 340px;/*136px;*/
        top: -340px !important;/*102px;*/
    }
    .fs12{font-size: 12px;}
</style>
<div class="row">
    <div class="col-lg-8 col-md-7 col-sm-6 orderMenu">
        <div class="row orderMenuItems" ng-repeat="item in danhsach" ng-if="item.menus.length">
            <div><strong style="font-size: 12px;">{{item.name}}</strong></div>
            <div data-id="{{item.id}}" class="col-lg-4 col-md-6" style="border-bottom: 1px solid rgb(221, 221, 221);" ng-repeat="menu in item.menus">
                <span style="font-size: 12px;">{{menu.name}}</span>
                <a class="btn btn-xs btn-success" style="font-size: 11px;" data-size="price_l" ng-click="openModal('additem', menu, 'lg')">{{(menu.size.lg.price/1000)|number:0}}K</a>
                <a class="btn btn-xs btn-success" style="font-size: 11px;" data-size="price_m" ng-click="openModal('additem', menu, 'md')">{{(menu.size.md.price/1000)|number:0}}K</a>
<!--                 <a class="btn btn-xs btn-success" style="font-size: 11px;" data-size="price" ng-click="openModal('additem', menu, 'sm')">{{(menu.size.sm.price/1000)|number:0}}K</a> -->
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-5 col-sm-6 orderCustomer">
        <h3 style="font-size: 17px;">Thông tin đơn hàng</h3>
        <div class="orderCartInfo">
            <style>
                .orderCartInfo table thead tr th {
                    font-size: 13px;
                }
                .orderCartInfo table tbody tr td, .orderCartInfo table tbody tr th {
                    font-size: 12px;
                }
            </style>
            <table class="table">
                <thead>
                <tr>
                    <th class="fs12">#</th>
                    <th class="fs12">Thông tin món</th>
                    <th class="fs12">SL</th>
                    <th class="fs12">T.Tính</th>
                    <th style="width: 80px;"></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-if="orders.length==0">
                    <td colspan="5">Chưa có hàng!</td>
                </tr>
                <tr ng-repeat="item in orders">
                    <td>{{$index + 1}}</td>
                    <td>
                        <b>{{item.name}} [{{item.selected_label}}]</b>
                        <div ng-if="chuoitoppings(item.toppings)"><small>+ {{chuoitoppings(item.toppings)}}</small></div>
                        <div ng-if="item.note||item.thuonggapnote.length"><small><i style="color:red">** <span ng-repeat="item2 in item.ghichuthuonggap" ng-if="item.thuonggapnote.indexOf(item2.id) > -1">{{item2.name}}. </span> {{item.note}}</i></small></div>
                    </td>
                    <td class="text-right">
                        <input type="text" ng-change="apdungkhuyenmai()" style="width: 50px;" ng-model="item.qty">
                    </td>
                    <th class="text-right" ng-class="{'hasDiscount': item.priceAfter}">
                        <span>{{(item.qty*item.selected_price+(thanhtientoppings(item.toppings)*item.qty))/1000|number:0}}K</span>
                        <p ng-show="item.priceAfter">{{(item.qty*item.priceAfter)/1000|number:0}}K</p>
                    </th>
                    <td>
                        <a class="btn btn-xs btn-success btnOrdersCartEdit" ng-click="openModal('edititem', item, item.selected_size)"><span class="glyphicon glyphicon-edit"></span></a>
                        <a class="btn btn-xs btn-danger btnOrdersCartRemove" ng-click="deleteitem(item)"><span class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
                </tbody>
                <tfoot ng-if="orders.length">
                <tr ng-if="orders.length&&chuongtrinhkhuyenmai.length">
                    <td colspan="3" class="fs12">Tạm tính</td>
                    <th colspan="2" class="fs12">{{tamtinh()|number:0}}K đ</th>
                </tr>
                <tr ng-if="orders.length&&chuongtrinhkhuyenmai.length">
                    <td colspan="5">
                        <div class="orderCartDiscounts">
                            <a class="btn btn-xs btn-warning fs12" ng-click="dienkhuyenmai(item.code)" ng-repeat="item in chuongtrinhkhuyenmai" title="{{item.name}}{{item.description?(' ('+item.description+')'):''}}">{{item.code}}</a>
                        </div>
                    </td>
                </tr>
                <tr ng-if="orders.length&&chuongtrinhkhuyenmai.length">
                    <td colspan="5" class="fs12">
                        Mã khuyến mãi <input type="text" class="form-control orderCartDiscountCode" ng-keyup="kiemtrakhuyenmai($event)" ng-model="shippingInfo.discount_code" placeholder="Nhập mã rồi Enter">
                        <div style="font-weight:700;font-size:0.95em">{{khuyenmai.description}}</div>
                    </td>
                </tr>
                <tr ng-if="orders.length" class="hidden">
                    <td colspan="5" class="fs12">
                        Điểm tích lũy x2 <input type="checkbox" ng-model="shippingInfo.points_x2">
                    </td>
                </tr>
                <tr class="orderUsePoint fs12" ng-if="orders.length">
                    <td colspan="5">Dùng điểm thưởng <input type="text" class="form-control" ng-model="shippingInfo.points_used"></td>
                </tr>
                <tr ng-if="orders.length">
                    <td colspan="2" class="fs12">Tổng cộng</td>
                    <th class="fs12">{{tongmonhoadon()}}</th>
                    <th colspan="2" class="fs12">{{tongtien()|number:0}} đ</th>
                </tr>
                </tfoot>
            </table>
            <div class="text-center"><!-- ng-if="orders.length"-->
                <a class="btn btn-md btn-primary btnCustomerInfoToggle" style="font-size: 12px; padding: 7px;"><span class="glyphicon glyphicon-user" style="font-size: 12px;"></span> T.T K.Hàng</a>
                <a class="btn btn-md btn-success btnOrderCreate" style="font-size: 12px; padding: 7px;" ng-disabled="orders.length==0" ng-click="taomoiorder()"><span class="glyphicon glyphicon-send" style="font-size: 12px;"></span> Tạo/Sửa Đ.Hàng</a>
                <a class="btn btn-md btn-danger btnOrderCancel" style="font-size: 12px; padding: 7px;" ng-click="xacnhanthoat()"><span class="glyphicon glyphicon-trash" style="font-size: 12px;"></span> Hủy</a>
            </div>
        </div>
    </div>
</div>
<div class="orderCustomerInfo">

    <span class="idx hidden">{{orderInfo.id}}</span>

    <a class="orderCustomerToggle btnCustomerInfoToggle">
        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
    </a>
    <br />
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <label>T.Tin K.Hàng <span id="orderCustomerNotification"></span>
                    <b>Cấp độ: {{customerInfo.level}} | Điểm số: {{customerInfo.points}}</b>
                </label>
                <input type="hidden" id="orderCustomerId" name="customer_id">

                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="orderCustomerName" class="col-lg-3 col-md-3 control-label fs12">KH <span style="color: #cb5a5e;">*</span></label>
                        <div class="col-lg-4 col-md-4 sdt2" style="padding-right: 0;">
                            <input type="text" class="form-control" autocomplete="off" id="orderCustomerName" name="customer_name" ng-model="customerInfo.first_name" typeahead-template-url="customTemplate.html" typeahead-on-select="choncustomer($item, $model, $label, $event)" typeahead-should-select="choncustomer2($event, 'orderCustomerName2')" tabindex="1" uib-typeahead="state as state.first_name for state in getCustomer($viewValue, 'name')" typeahead-min-length="2" placeholder="Họ và chữ lót khách hàng">
                        </div>
                        <div class="col-lg-5 col-md-5 sdt2" style="padding-left: 0;">
                            <input type="text" style="border-left: 0;" class="form-control" autocomplete="off" id="orderCustomerName2" name="customer_name2" ng-model="customerInfo.last_name" typeahead-template-url="customTemplate.html" typeahead-on-select="choncustomer($item, $model, $label, $event)" typeahead-should-select="choncustomer2($event, 'orderCustomerPhone')" tabindex="2" uib-typeahead="state as state.last_name for state in getCustomer($viewValue, 'name')" typeahead-min-length="2" placeholder="Tên khách hàng" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="orderCustomerPhone" class="col-lg-3 col-md-3 control-label fs12">SĐT <span style="color: #cb5a5e;">*</span></label>
                        <div class="col-lg-9 col-md-9 sdt">
                            <input type="text" class="form-control" autocomplete="off" id="orderCustomerPhone" name="customer_phone" ng-model="customerInfo.phone" typeahead-template-url="customTemplate.html" typeahead-on-select="choncustomer($item, $model, $label, $event)" typeahead-should-select="choncustomer2($event, 'shipping_floor')" tabindex="3" uib-typeahead="state as state.customer_name for state in getCustomer($viewValue, 'phone')" typeahead-min-length="4" placeholder="Số ĐT khách hàng" required="" style="color:red">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="orderCustomerEmail" class="col-lg-3 col-md-3 control-label fs12">GT</label>
                        <div class="col-lg-9 col-md-9">

                            <div class="md-radio-inline" style="margin-bottom: 0;">
                                <?php foreach ([1 => 'Nam', 0 => 'Nữ'] as $key => $label): ?>
                                    <div class="md-radio" data-value="<?php echo $key ?>">
                                        <input type="radio" id="orderCustomerGender<?php echo $key ?>" value="<?php echo $key ?>" name="orderCustomerGender" ng-model="customerInfo.gender" class="md-radiobtn">
                                        <label for="orderCustomerGender<?php echo $key ?>">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            <?php echo $label ?> </label>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div for="orderBranch" class="col-lg-3 col-md-3 fs12"><input
                                bs-switch
                                ng-model="isSelected"
                                type="checkbox"
                                switch-size="{{'mini'}}"
                                switch-on-text="{{ onLabel }}"
                                switch-off-text="{{ offLabel }}"
                                switch-change="onChange()"></div>
                        <div class="col-lg-9 col-md-9">
                            <select name="branch_id" id="orderBranch" class="form-control" ng-disabled="isSelected" required ng-model="shippingInfo.branchid" ng-options="item.id as (item.name + (item.distance?(' ('+item.distance+')'):'')) for item in branches | orderBy:'+distance2'">
                                <option value="">Chọn chi nhánh (*)</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <label>Thông tin giao hàng</label>

                <div class="form-horizontal">

                    <div id="orderCustomerAddresses"></div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <input type="text" class="form-control" name="shipping_floor" id="shipping_floor" placeholder="Hẻm / Tòa nhà / Lầu / Phòng" ng-model="shippingInfo.floor" value="<?php echo @$record->shipping_floor ?>">
                        </div>
                    </div>
                    <style>
                        .orderCustomerAddress .dropdown-menu {
                            height: 310px;
                            top: -310px !important;
                        }
                    </style>
                    <div class="form-group">
                        <div class="col-lg-12 orderCustomerAddress">
                            <!--input type="text" class="form-control" autocomplete="off" id="orderCustomerAddress" name="shipping_address" typeahead-template-url="customTemplate2.html" typeahead-on-select="choncustomeraddress($item, $model, $label, $event)" uib-typeahead="state.label for state in getCustomerAddress($viewValue)" placeholder="Địa chỉ (*)" ng-model="shippingInfo.address"-->
                            <input type="text" class="form-control" g-places-autocomplete data-placeholder="Địa chỉ (*)" options="autocompleteOptions" ng-model="shippingInfo.address">
                            <!--input type="text" class="form-control" autocomplete="off" id="orderCustomerAddress" name="shipping_address" typeahead-on-select="choncustomeraddress($item, $model, $label, $event)" ng-keypress="keypress()" uib-typeahead="state as state.formatted_address for state in danhsachdiachi" placeholder="Địa chỉ (*)" ng-model="shippingInfo.address"-->
                        </div><!-- ng-change="kiemtracngannhat()"-->
                    </div>

                    <!--div class="form-group">
                        <label for="orderReceiverMessage" class="col-lg-3 col-md-3 control-label">Lời nhắn</label>
                        <div class="col-lg-12">
                            <textarea name="receiver_message" id="orderReceiverMessage" class="form-control" cols="30" rows="4" placeholder="Lời nhắn tới người nhận"></textarea>
                        </div>
                    </div-->

                    <div class="form-group">
                        <div class="col-lg-12">
                            <textarea name="note" id="orderNote" placeholder="Ghi chú" class="form-control" cols="30" rows="3" ng-model="shippingInfo.note"></textarea>
                        </div>
                    </div>

                </div>

                <!-- LẤY ĐỊA CHỈ GIAO HÀNG GẦN ĐÂY (LÀM TẠM 1 THỜI GIAN RỒI XÓA) -->
                <div class="orderLastShipping"></div>

            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 hidden">
                <label>Người nhận (nếu là biếu tặng)</label>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="orderReceiverName" class="col-lg-3 col-md-3 control-label">Họ tên</label>
                        <div class="col-lg-9 col-md-9">
                            <input type="text" class="form-control" id="orderReceiverName" name="receiver_name" placeholder="Họ tên người nhận">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="orderReceiverPhone" class="col-lg-3 col-md-3 control-label">Số ĐT</label>
                        <div class="col-lg-9 col-md-9">
                            <input type="text" class="form-control" id="orderReceiverPhone" name="receiver_phone" placeholder="Số ĐT người nhận">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <!--label>Ghi chú giao hàng</label-->
                <label>Hẹn giao hàng</label>
                <div class="form-horizontal">
                    <!--div class="form-group">
                        <label for="orderShippingSchedule" class="col-lg-3 col-md-3 control-label">Giao lúc</label>
                        <div class="col-lg-9 col-md-9">
                            <select class="form-control" id="orderShippingSchedule" name="shipping_on" ng-model="shippingInfo.delivery_on" ng-options="item.id as item.name for item in schedules">
                            </select>
                        </div>
                    </div-->
                    <div class="form-group" style="margin-bottom: 0;">
                        <!--label for="orderShippingSchedule" class="col-lg-3 col-md-3 control-label"></label-->
                        <div class="col-lg-12 col-md-12">
                            <select class="form-control" id="orderShippingSchedule" name="shipping_on" ng-model="shippingInfo.delivery_on_loai" ng-options="item.id as item.name for item in schedules"></select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0;">
                        <div class="col-lg-6 col-md-6">
                            <div uib-timepicker ng-model="shippingInfo.delivery_on" ng-change="changed()" hour-step="1" minute-step="5" show-meridian="false"></div>
                        </div>
                        <div class="col-lg-6 col-md-6" ng-if="shippingInfo.delivery_on_loai=='2'">
                            <div uib-timepicker ng-model="shippingInfo.delivery_on2" ng-change="changed()" hour-step="1" minute-step="5" show-meridian="false"></div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0;">
                        <div class="col-lg-12 col-md-12">
                            <p ng-repeat="subitem in shippingInfo.ghichuthuonggap">
                                <label style="margin-bottom: 0;"><input type="checkbox" ng-click="toggleSelection2(subitem.id, subitem.name)" ng-checked="shippingInfo.thuonggapnote.indexOf(subitem.id) > -1" value="{{subitem.id}}"> {{subitem.name}}</label>
                            </p>
                            <!--p>
                                <label style="margin-bottom: 0;"><input type="checkbox"> THỐI TIỀN 500K</label>
                            </p-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <a class="btn btn-md btn-success btn-block btnOrderCreate" style="font-size: 12px; padding: 7px; white-space: pre-wrap;" ng-disabled="orders.length==0" ng-click="taomoiorder()"><span style="font-size: 12px;" class="glyphicon glyphicon-send"></span> Tạo/Sửa Đ.Hàng</a>
                <a class="btn btn-md btn-danger btn-block btnOrderCancel" style="font-size: 12px; padding: 7px; margin-left: 0;" ng-click="xacnhanthoat()"><span class="glyphicon glyphicon-trash" style="font-size: 12px;"></span> Hủy</a>
            </div>
        </div>
    </div>
</div>
