<div class="form-horizontal Form_">
    <form role="form" ng-submit="luutruregion()">
        <div class="form-body row">

            <div class="col-lg-8">
                <div class="form-group forEdit" ng-if="regionInfo.id">
                    <label class="col-md-3 control-label">ID</label>
                    <div class="col-md-9">
                        <span class="form-control-static idx">{{regionInfo.id}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Tên khu vực <span style="color: #cb5a5e;">*</span></label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" name="name" ng-model="regionInfo.name" required>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" name="phamvi" ng-model="regionInfo.phamvi" ng-options="item.id as item.name for item in phamviregions"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Thuộc nhóm khu vực</label>
                    <div class="col-md-9">
                        <select class="form-control" name="parent_id" ng-disabled="regionInfo.parent_id==0&&regionInfo.children_total!=0" ng-model="regionInfo.parent_id" ng-options="item.id as item.name for item in rootregions"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Trạng thái</label>
                    <div class="col-md-9">
                        <div class="md-checkbox has-success">
                            <input type="checkbox" id="status" name="status" class="md-check" ng-model="regionInfo.status" ng-true-value="'1'" ng-false-value="'0'">
                            <label for="status"><span></span><span class="check"></span><span class="box"></span></label>
                        </div>
                    </div>
                </div>
            </div>

            <!--div class="col-lg-4">
                <?php /*if (@$record->manager_name): ?><fieldset class="form-group">
                        <label>Quản lý: </label>
                        <?php echo @$record->manager_name ? (@$record->manager_name . ' ('.@$record->phone.')') : ' -- None -- ';?>
                    </fieldset><?php endif*/ ?>
                <?php /*if(count(@$record->nguoidung)): ?>
                    <table class="table table-bordered table-striped table-condensed">
                        <thead class="flip-content">
                        <tr>
                            <th>ID</th>
                            <th>Họ tên</th>
                            <th>Số điện thoại</th>
                            <th>Loại T.Khoản</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach (@$record->nguoidung as $row): ?>
                            <tr data-id="<?php echo $row->id ?>">
                                <td class="text-right"><?php echo $row->id ?></td>
                                <td><a href="#"><?php echo $row->first_name.' '.$row->last_name ?></a></td>
                                <td><?php echo $row->phone ?></td>
                                <td><?php echo $row->loai ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif*/ ?>
            </div-->

        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn btn-circle blue btnUpdate">Cập nhật</button>
                    <button type="button" class="btn btn-circle default btnCancel">Hủy</button>
                </div>
            </div>
        </div>

    </form>
</div>