<div class="portlet box green" ng-if="branchInfo.id">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Thông tin chi tiết
            <!--a class="btn red btn-xs" ui-sref="home.createregion"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a-->
        </div>
        <div class="tools">
            <a class="reload" title="Làm mới dữ liệu"></a>
            <a class="remove" title="Xóa"></a>
        </div>
    </div>
    <div class="portlet-body form">
        <ng-include src="myConfig.api_url + 'pages/menuform'" include-replace></ng-include>
    </div>
</div>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> {{titlepage}}
            <a class="btn red btn-xs" ui-sref="home.createmenu"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
        </div>
        <div class="tools">
            <a class="reload" title="Làm mới dữ liệu"></a>
        </div>
    </div>
    <div class="portlet-body flip-scroll" style="display: block;">

        <ul uib-pagination total-items="pagination.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination.numPerPage" ng-model="pagination.currentPage" ng-change="pageChanged()"></ul>

        <table class="table table-bordered table-striped table-condensed">
            <thead class="flip-content">
            <tr>
                <th style="width: 25px"><input type="checkbox" class="selectAllRows"></th>
                <th>ID</th>
                <th>Tên thức uống</th>
                <th>Đơn giá</th>
                <th>Số lượng</th>
                <th>GIảm giá</th>
                <th>Lượt xem</th>
                <th>Toppings</th>
                <th>Trạng thái</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="item in filteredOrders">
                <td>
                    <input type="checkbox" class="selectRow">
                </td>
                <td class="text-right">
                    {{item.id}}
                </td>
                <td>
                    <a ui-sref="home.menus({id: item.id, page: pagination.currentPage})">{{item.name}}</a>
                </td>
                <td>
                    S: <b>{{item.dongia['S']|number:0}}đ</b>;
                    M: <b>{{item.dongia['M']|number:0}}đ</b>;
                    L: <b>{{item.dongia['L']|number:0}}đ</b>
                </td>
                <td>{{item.quantity|number:0}}</td>
                <td></td>
                <td>{{item.views|number:0}}</td>
                <td><span class="glyphicon glyphicon-ok" ng-if="item.toppings"></span></td>
                <td>
                    <div class="md-checkbox has-success">
                        <input type="checkbox" id="checkbox_{{item.id}}" class="md-check" ng-checked="item.status">
                        <label for="checkbox_{{item.id}}" ng-click="updatestatus(item)"><span></span><span class="check"></span><span class="box"></span></label>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <ul uib-pagination total-items="pagination.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination.numPerPage" ng-model="pagination.currentPage" ng-change="pageChanged()"></ul>

    </div>
</div>