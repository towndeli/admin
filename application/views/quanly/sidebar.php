
<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu" ng-class="thunho?'page-sidebar-menu-closed':''" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <li class="sidebar-toggler-wrapper">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler" ng-click="thunhosidebar()">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
    </li>
    <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
    <li class="sidebar-search-wrapper">
        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
        <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
        <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
        <form class="sidebar-search " action="" method="POST">
            <a href="javascript:;" class="remove">
                <i class="icon-close"></i>
            </a>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Tìm kiếm...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
            </div>
        </form>
        <!-- END RESPONSIVE QUICK SEARCH FORM -->
    </li>
    <li class="start active open">
        <a ui-sref="home.dashboard">
            <i class="icon-home"></i>
            <span class="title">Thống kê</span>
            <span class="selected"></span>
            <span class="arrow open"></span>
        </a>
        <ul class="sub-menu">
            <li class="active">
                <a ui-sref="home.dashboard">
                    <i class="icon-bar-chart"></i>
                    Tổng quan</a>
            </li>
            <li>
                <a href="">
                    <i class="icon-bulb"></i>
                    Thức uống</a>
            </li>
            <li>
                <a href="">
                    <i class="icon-graph"></i>
                    Khách hàng</a>
            </li>
        </ul>
    </li>
    <li ng-if="['admin', 'supregion'].indexOf(globals.currentUser.role)>=0">
        <a ui-sref="home.regions">
            <i class="icon-basket"></i>
            <span class="title">Khu vực</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="home.createregion">
                    <i class="icon-plus"></i>
                    Thêm mới</a>
            </li>
        </ul>
    </li>
    <li ng-if="['admin', 'supregion', 'region'].indexOf(globals.currentUser.role)>=0">
        <a ui-sref="home.branches">
            <i class="icon-basket"></i>
            <span class="title">Chi nhánh</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="home.createbranch">
                    <i class="icon-plus"></i>
                    Thêm mới</a>
            </li>
        </ul>
    </li>
    <li ng-if="['admin'].indexOf(globals.currentUser.role)>=0">
        <a ui-sref="home.categories">
            <i class="icon-rocket"></i>
            <span class="title">Danh mục</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="home.createcategory">
                    <i class="icon-plus"></i>
                    Thêm mới</a>
            </li>
        </ul>
    </li>
    <li ng-if="['admin'].indexOf(globals.currentUser.role)>=0">
        <a ui-sref="home.menus">
            <i class="icon-diamond"></i>
            <span class="title">Thức uống</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="home.createmenu">
                    <i class="icon-plus"></i>
                    Thêm mới</a>
            </li>
        </ul>
    </li>
    <li ng-if="['admin'].indexOf(globals.currentUser.role)>=0">
        <a ui-sref="home.toppings">
            <i class="icon-diamond"></i>
            <span class="title">Món thêm</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="home.createtopping">
                    <i class="icon-plus"></i>
                    Thêm mới</a>
            </li>
        </ul>
    </li>
    <li ng-if="['admin'].indexOf(globals.currentUser.role)>=0">
        <a ui-sref="home.materials">
            <i class="icon-diamond"></i>
            <span class="title">Nguyên liệu</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="home.creatematerial">
                    <i class="icon-plus"></i>
                    Thêm mới</a>
            </li>
        </ul>
    </li>
    <li>
        <a ui-sref="home.orders">
            <i class="icon-puzzle"></i>
            <span class="title">Đơn hàng</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="home.createorder" target="_blank">
                    <i class="icon-plus"></i>
                    Thêm mới</a>
            </li>
        </ul>
    </li>
    <li ng-if="['admin', 'region', 'branch', 'supregion'].indexOf(globals.currentUser.role)>=0">
        <a ui-sref="home.users">
            <i class="icon-user"></i>
            <span class="title">Thành viên</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="home.createuser">
                    <i class="icon-plus"></i>
                    Thêm mới</a>
            </li>
        </ul>
    </li>				<li>
        <a ui-sref="home.customers">
            <i class="icon-user"></i>
            <span class="title">Khách hàng</span>
            <span class="arrow "></span>
        </a>
    </li>
    <li>
        <a ui-sref="home.sms">
            <i class="icon-user"></i>
            <span class="title">Tin SMS</span>
            <span class="arrow "></span>
        </a>
    </li>
    <li>
        <a ui-sref="home.feedbacks">
            <i class="icon-puzzle"></i>
            <span class="title">Phản hồi</span>
            <span class="arrow "></span>
        </a>
    </li>
    <li>
        <a ui-sref="home.bugs">
            <i class="icon-puzzle"></i>
            <span class="title">Báo lỗi</span>
            <span class="arrow "></span>
        </a>
    </li>

    <li class="heading">
        <h3 class="uppercase">Chức năng</h3>
    </li>
    <li>
        <a href="javascript:;">
            <i class="icon-settings"></i>
            <span class="title">Cấu hình</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="">
                    <span class="badge badge-roundless badge-danger">mới</span>
                    Mục 1</a>
            </li>
            <li>
                <a href="">
                    Mục 2</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:;">
            <i class="icon-briefcase"></i>
            <span class="title">Data Tables</span>
            <span class="arrow "></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="">
                    Mục 3</a>
            </li>
            <li>
                <a href="">
                    Mục 4</a>
            </li>
        </ul>
    </li>
</ul>