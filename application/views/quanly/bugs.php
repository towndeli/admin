<div class="portlet box green" ng-if="branchInfo.id">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Thông tin chi tiết
            <!--a class="btn red btn-xs" ui-sref="home.createregion"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a-->
        </div>
        <div class="tools">
            <a class="reload" title="Làm mới dữ liệu"></a>
            <a class="remove" title="Xóa"></a>
        </div>
    </div>
    <div class="portlet-body form">
        <ng-include src="myConfig.api_url + 'pages/bugform'" include-replace></ng-include>
    </div>
</div>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> {{titlepage}}
            <!--a class="btn red btn-xs" ui-sref="home.createregion"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a-->
        </div>
        <div class="tools">
            <a class="reload" title="Làm mới dữ liệu"></a>
        </div>
    </div>
    <div class="portlet-body flip-scroll" style="display: block;">

        <ul uib-pagination total-items="pagination.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination.numPerPage" ng-model="pagination.currentPage" ng-change="pageChanged()"></ul>

        <table class="table table-bordered table-striped table-condensed">
            <thead class="flip-content">
            <tr>
                <th style="width: 25px"><input type="checkbox" class="selectAllRows"></th>
                <th>ID</th>
                <th>Loại báo lỗi</th>
                <th>Mô tả</th>
                <th>Thao tác</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="item in filteredOrders">
                <td>
                    <input type="checkbox" class="selectRow">
                </td>
                <td class="text-right">
                    {{item.id}}
                </td>
                <td>
                    <a ui-sref="home.bugs({id: item.id, page: pagination.currentPage})">{{item.type}}</a>
                </td>
                <td>{{item.message}}</td>
                <td class="actions">
                    <button type="button" class="btn btn-xs red btnDelete"><span class="glyphicon glyphicon-trash"></span></button>
                </td>
            </tr>
            </tbody>
        </table>

        <ul uib-pagination total-items="pagination.totalItems" max-size="5" boundary-link-numbers="true" rotate="true" force-ellipses="true" items-per-page="pagination.numPerPage" ng-model="pagination.currentPage" ng-change="pageChanged()"></ul>

    </div>
</div>