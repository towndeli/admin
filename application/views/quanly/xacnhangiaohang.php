
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="vi" ng-app="BasicHttpAuthExample">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <title>TownDeli.VN</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content="Quốc Hưng Nguyễn"/>

    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"><![endif]--><!-- Force IE to use the latest rendering engine -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><!-- Optimize mobile viewport -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!-- icons and icons and icons and icons and icons and a tile -->
    <meta name="application-name" content="Stencil"><!-- Windows 8 Tile Name -->
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>favicon.ico"><!-- Windows 8 Tile Image -->
    <meta name="msapplication-TileColor" content="#4eb4e5"><!-- Windows 8 Tile Color -->
    <link rel="icon" href="<?php echo asset_url('img/icons/favicon-32.png');?>" type="image/png"><!-- default favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico"><!-- legacy default favicon (in root, 32x32) -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo asset_url('img/icons/favicon-57.png');?>"><!-- iPhone low-res and Android -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo asset_url('img/icons/favicon-57.png');?>"><!-- legacy Android -->
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo asset_url('img/icons/favicon-72.png');?>"><!-- iPad -->
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo asset_url('img/icons/favicon-114.png');?>"><!-- iPhone 4 -->
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo asset_url('img/icons/favicon-144.png');?>"><!-- iPad hi-res -->

    <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <link href="<?php echo asset_url('global/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/simple-line-icons/simple-line-icons.min.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/uniform/css/uniform.default.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');?>" rel="stylesheet" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo asset_url('global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/fullcalendar/fullcalendar.min.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/jqvmap/jqvmap/jqvmap.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');?>" rel="stylesheet" />
    <!-- END PAGE LEVEL PLUGIN STYLES -->
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <!--link href="<?php echo asset_url('admin/pages/css/login3.css');?>" ng-if="page=='login'" rel="stylesheet"/-->
    <!-- BEGIN PAGE STYLES -->
    <link href="<?php echo asset_url('admin/pages/css/tasks.css');?>" rel="stylesheet" />
    <!-- END PAGE STYLES -->
    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css') ?>' stylesheet instead of 'components.css' in the below style tag -->
    <link href="<?php echo asset_url('global/css/components.css');?>" id="style_components" rel="stylesheet" />
    <link href="<?php echo asset_url('admin/layout/css/layout.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('admin/layout/css/themes/darkblue.css');?>" rel="stylesheet" id="style_color"/>
    <link href="<?php echo asset_url('admin/layout/css/custom.css');?>" rel="stylesheet" />

    <link href="<?php echo asset_url('global/plugins/select2/css/select2.min.css');?>" rel="stylesheet" />


    <link href="<?php echo asset_url('global/css/common.css');?>" rel="stylesheet" />
    <link href="<?php echo asset_url('css/mystyle.css');?>" rel="stylesheet" />
    <!-- END THEME STYLES -->
</head>
<body ng-controller="HomeController">
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content" style="margin-left: 0;">
            <table cellspacing="5" cellpadding="5" width="100%" style="margin-bottom: 20px;">
                <tr>
                    <td width="61" class="text-center" style="border: 1px solid #616265;font-weight: bold;"><?php echo $order->order_number; ?></td>
                    <td width="8">&nbsp;</td>
                    <td>
                        <h3 class="chuhoa m0 pb5" style="text-transform: uppercase;margin-top: 0;margin-bottom: 0; font-weight: 700;">HÓA ĐƠN #<?php echo $order->idv; ?></h3>
                        <p class="mb0" style="margin-bottom: 0;"><?php echo $order->time_created_text; ?></p>
                        <p class="mb0" style="margin-bottom: 0;"><?php echo ($order->gender?'Anh':'Chị') . ' ' . $order->customer_name . ' - ' . $order->customer_phone; ?></p>
                        <p class="mb0" style="margin-bottom: 0;"><?php echo ($order->shipping_floor.' '.$order->shipping_address); ?></p>
                    </td>
                </tr>
            </table>
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Thông tin món</th>
                    <th class="text-center">SL</th>
                    <th>Tạm tính</th>
                    <th>Ghi chú</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($order->orders as $key => $value) { ?>
                    <tr>
                        <td><?php echo ($key + 1); ?></td>
                        <td>
                            <?php echo $value->name; ?> [<?php echo $value->selected_label; ?>]
                            <?php $toppings = array(); $toppingstotal = 0; foreach ($value->toppings as $value2) {if($value2->qty){$toppingstotal += ($value2->qty*$value2->price*$value->qty);$toppings[] = $value2->name . ' ('.$value2->qty.')';}} if (count($toppings)) { ?>
                                <p class="mb0 fs11 lh14" style="margin-bottom: 0;"><small>+ <?php echo implode(', ', $toppings); ?></small></p>
                            <?php } ?>
                        </td>
                        <td class="text-center"><?php echo $value->qty; ?></td>
                        <td><?php echo number_format(($value->qty * $value->selected_price) + $toppingstotal); ?>đ</td>
                        <td>
                            <?php foreach($value->ghichuthuonggap as $row){if(in_array($row['id'], $value->thuonggapnote)){
                                echo $row['name'].'. ';
                            }} ?>
                            <?php echo $value->note; ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <?php if ($order->discount_code) {?>
                    <tr>
                        <td></td>
                        <td>Tạm tính</td>
                        <td></td>
                        <td colspan="2"><?php echo number_format($order->cart_subtotal); ?>đ</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">Mã KM <b><?php echo $order->discount_code; ?></b></td>
                        <td></td>
                        <td></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td></td>
                    <td><b style="text-transform: uppercase;">Tổng cộng</b></td>
                    <td class="text-center"><b><?php echo $order->cart_total_items; ?></b></td>
                    <td colspan="2"><b><?php echo number_format($order->cart_total); ?>đ</b></td>
                </tr>
                <tr ng-if="position" class="ng-cloak"><!---->
                    <td class="text-center" colspan="5">
                        <button type="button" ng-click="xacnhangiaohang()" class="btn btn-md btn-success" ng-if="position">Xác nhận</button><!---->
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo asset_url('global/plugins/respond.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/excanvas.min.js');?>"></script>
<![endif]-->
<script src="<?php echo asset_url('global/plugins/jquery.min.js');?>"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo asset_url('global/plugins/jquery-ui/jquery-ui.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.blockui.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.cokie.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/uniform/jquery.uniform.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo asset_url('global/plugins/flot/jquery.flot.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/flot/jquery.flot.resize.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/flot/jquery.flot.categories.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.pulsate.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-daterangepicker/moment.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/bootstrap-daterangepicker/daterangepicker.js');?>"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="<?php echo asset_url('global/plugins/fullcalendar/fullcalendar.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery-easypiechart/jquery.easypiechart.min.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/jquery.sparkline.min.js');?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo asset_url('global/scripts/metronic.js');?>"></script>
<script src="<?php echo asset_url('admin/layout/scripts/layout.js');?>"></script>
<script src="<?php echo asset_url('admin/layout/scripts/quick-sidebar.js');?>"></script>
<script src="<?php echo asset_url('admin/layout/scripts/demo.js');?>"></script>
<script src="<?php echo asset_url('admin/pages/scripts/index.js');?>"></script>
<script src="<?php echo asset_url('admin/pages/scripts/tasks.js');?>"></script>

<script src="<?php echo asset_url('global/plugins/fileupload/jquery.ui.widget.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/fileupload/jquery.iframe-transport.js');?>"></script>
<script src="<?php echo asset_url('global/plugins/fileupload/jquery.fileupload.js');?>"></script>

<script src="<?php echo asset_url('global/plugins/select2/js/select2.full.min.js');?>"></script>

<script src="<?php echo asset_url('global/plugins/datepicker/js/bootstrap-datepicker.js');?>"></script>

<script src="<?php echo asset_url('js/jQuery.print.js');?>"></script>

<script>
    var PARAMS = {
        siteURL: "index.php",
        ajaxURL: "index.php/ajax",
        moduleURL: "index.php",
        model: "index",
        method: "",
        action: "",
        uploadURL: ""
    };

    var API = {
        cart: "index.php/api/cart"
    };

    var URL = {
        site: "index.php",
        ajax: "index.php/ajax",
        upload: "index.php/upload",
        module: "index.php"
    };
</script>
<script src="<?php echo asset_url('global/scripts/common.js');?>"></script>
<script src="<?php echo asset_url('global/scripts/functions.js');?>"></script>
<script src="<?php echo asset_url('admin/Notification.min.js');?>"></script>

<!--<script src="<?php echo asset_url('global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>-->

<!-- END PAGE LEVEL SCRIPTS -->
<script type="javascript">
    jQuery(document).ready(function() {

        if (Notification.permission !== 'denied') {
            Notification.requestPermission();
        }

        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        Index.init();
        Index.initDashboardDaterange();
        //Index.initJQVMAP(); // init index page's custom scripts
        Index.initCalendar(); // init index page's custom scripts
        Index.initCharts(); // init index page's custom scripts
        Index.initChat();
        Index.initMiniCharts();
        Tasks.initDashboardWidget();


        x = $('.page-sidebar-menu >li >a');


        $('.page-sidebar-menu >li >a').each(function(i,v){
            /*
             regex = '/admin$/gi';
             eval('var regex = /'+ $(this).attr('href').replace('//', '\/\/') + '/;');

             if (regex.test(url)) {
             $(this).parent().addClass('active');
             }
             */
        });

    });
</script>
<!-- END JAVASCRIPTS -->

<script src="<?php echo asset_url('angular-1.5.8/angular.min.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular.audio.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/socket.io-1.4.5.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular-socket-io/socket.min.js');?>"></script>
<script src="<?php echo asset_url('angular-1.5.8/angular-notification.min.js');?>"></script>
<script src="<?php echo asset_url('js/myLibraries.js');?>"></script>
<script>
    var asset_url = "<?php echo asset_url('');?>";
    var site_url = "<?php echo site_url();?>/";
    var base_url = "<?php echo base_url();?>";
    var api_url = "<?php echo site_url();?>/";
    var socket_url = "<?php echo $_SERVER['HTTP_HOST'] ?>:3000";
    //var socket_url = "http://103.195.239.189:3000";
    //var socket_url = "http://localhost:3000";
    var logo_print_url = "<?php echo asset_url('frontend/images/logo-towndeli.png');?>";
</script>
<script>
    'use strict';
    angular.module('BasicHttpAuthExample', [
        'notification',
        'btford.socket-io',
        'ngAudio',
        'myLibrary'
    ])
        .constant('myConfig', {
            asset_url: asset_url,
            site_url: site_url,
            base_url: base_url,
            api_url: api_url,
            socket_url: socket_url,
            website: 'http://towndeli.vn',
            phone: '(08) 3826350',
            logo_print: logo_print_url,
            danhsachsoluongitems: ['05', '20', '35', '50'],
            danhsachtrangthai: {0: 'Inactive', 1: 'Active'},
            danhsachgioitinh: {0: 'Nữ', 1: 'Nam'}
        })
        .directive('includeReplace',
        function () {
            return {
                require: 'ngInclude',
                restrict: 'A', /* optional */
                link: function (scope, el, attrs) {
                    el.replaceWith(el.children());
                }
            };
        })
        .factory('geolocationSvc', ['$q', '$window', function ($q, $window) {
            function getCurrentPosition() {
                var deferred = $q.defer();

                if (!$window.navigator.geolocation) {
                    deferred.reject('Geolocation not supported.');
                } else {
                    $window.navigator.geolocation.getCurrentPosition(
                        function (position) {
                            deferred.resolve(position);
                        },
                        function (err) {
                            deferred.reject(err);
                        });
                }

                return deferred.promise;
            }

            return {
                getCurrentPosition: getCurrentPosition
            };
        }])
        .controller('HomeController', function ($scope, $rootScope, myConfig, $http, mySocket, myAudio, geolocationSvc, $notification) {
            mySocket.forward('messageReceived');
            $scope.myConfig = myConfig;
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position){
                    $scope.$apply(function(){
                        $scope.position = position;
                        console.log($scope.position)
                    });
                });
            }
            /*geolocationSvc.getCurrentPosition().then(function(position){
                console.log(position)
            });*/
            $scope.position = true;
            $scope.dangclick = 0;
            $scope.xacnhangiaohang = function () {
                if (!$scope.dangclick) {
                    //if ($scope.position) {
                        if (confirm('Are you sure?')) {
                            $scope.dangclick = 1;
                            //console.log($scope.position)
                            $http.post($scope.myConfig.site_url + 'xacnhandagiao', {
                                id: <?php echo $order->id; ?>,
                                token: '<?php echo $token; ?>',
                                //position: {lat: 0, lng: 0}//{lat:$scope.position.coords.latitude, lng: $scope.position.coords.longitude}
                            }).success(function (response) {
                                $scope.dangclick = 0;
                                if (response.status=='success') {
                                    $scope.position = null;
                                    mySocket.emit('sendMessage', {action: 'updateStatus', data: {id: <?php echo $order->id; ?>}, users: []});
                                } else {
                                    alert('Có thật không? Đừng thông báo dối nhé!');
                                }
                            });
                        }
                    //}
                }
            };
        });
</script>

</body>
</html>