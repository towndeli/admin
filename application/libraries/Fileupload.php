<?php

include dirname(__FILE__) . '/FileUpload/UploadHandler.php';


class Fileupload {

	private $uploadDir, $uploadUrl, $uploadTmpUrl;


	function __construct()
	{
		$ci =& get_instance();
		$uniqueId = $ci->input->post('uniqueId');

		$this->uploadDir = 'themes/quanly/assets/upload';

		$this->uploadTmp = $this->uploadDir . '/tmp/' . $uniqueId . '/';
		$this->uploadTmpUrl = site_url($this->uploadTmp) . '/';
	}


	public function handler()
	{
		$options = array(
				'upload_dir' => $this->uploadTmp,
				'upload_url' => $this->uploadTmpUrl,
			);

		return new UploadHandler($options);
	}


	public function saveImage($path)
	{
		include_once APPPATH . '/third_party/ImageResize.php';
		
		$files = glob($this->uploadTmp . '*.{jpg,jpeg,gif,png}', GLOB_BRACE);
		$dir = $this->uploadDir . '/' . $path;

		if (!is_dir($dir)) {
			$old = umask(0);
			mkdir($dir, 0777, TRUE);
			umask($old);
		}
		$exts = array('jpg', 'jpeg', 'gif', 'png');
		foreach ($files as $file) {
			$filePath = $dir .  '/' . basename($file);
			$info = new SplFileInfo($file);
			$ext = $info->getExtension();

			if (in_array($ext, $exts)) {
				$image = new Eventviva\ImageResize($file);
				$image->resizeToWidth(200);
				$image->save($dir . '/coverImage.png', IMAGETYPE_PNG);
			}

			rename($file, $filePath);
		}

		removeDirectory($this->uploadTmp);
	}

}