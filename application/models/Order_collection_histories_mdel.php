<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_collection_histories_mdel extends MY_Model {
    public $table = 'order_collection_histories';
    public function __construct() {
        parent::__construct();
    }
}