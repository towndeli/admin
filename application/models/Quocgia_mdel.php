<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quocgia_mdel extends MY_Model {
    public $table = 'quocgia';
    public function __construct() {
        parent::__construct();
    }
}