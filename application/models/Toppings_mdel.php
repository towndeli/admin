<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Toppings_mdel extends MY_Model {
    public $table = 'toppings';
    public function __construct() {
        parent::__construct();
    }
    public function getList($wheres = array(), $options = array()) {
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            foreach ($danhsach as &$value) {
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
            }
        }
        return $danhsach;
    }
    public function getDetail($id, $selector = '*', $escape = true) {
        $result = parent::getDetail($id, $selector, $escape);
        if (isset($result->id)) {
            $result->status = (int)$result->status;
            $result->status_class = $this->orderStatusClasses[$result->status];
            $result->status_text = $this->orderStatuses[$result->status];
        }
        return $result;
    }
    public function danhsach($data, $thongtin) {
        $id = @$data->id;
        $region = array();
        if ($id) {
            $region = parent::getDetail($id);
            $region->uniqueId = $region->id;
        }
        return array('data' => $this->getList(array(), array('getAll' => true, 'order_by' => array('status' => 'desc'))), 'uniqueId' => md5(time() . randomString(10)), 'branchInfo' => $region);
    }
    public function chuanbi($data, $thongtin) {
        $region = array(
            'name' => '',
            'description' => '',
            'status' => '1',
            'price' => 0
        );
        $regions = array();
        return array('branchInfo' => $region, 'uniqueId' => md5(time() . randomString(10)));
    }
    public function luutru($data = array(), $thongtin) {
        $regionInfo = @$data->thongtin;
        $region = array(
            'name' => $regionInfo->name,
            'description' => $regionInfo->description,
            'price' => (int)$regionInfo->price,
            'use_straw' => (int) $regionInfo->use_straw,
            'use_spoon' => (int) $regionInfo->use_spoon,
            'status' => (int)$regionInfo->status
        );
        $uniqueId = @$regionInfo->uniqueId;
        if (isset($regionInfo->id)) {
            $region['id'] = $regionInfo->id;
            $id = parent::edit($region);
            if ($id) {
                $id = $region['id'];
            }
        } else {
            $id = parent::create($region);
            if ($id) {
                if ($uniqueId && $uniqueId!=$id) {
                    $dir = FCPATH . 'themes/quanly/assets/upload/' . $this->table;
                    $olddir = $dir . '/' . $uniqueId;
                    $newdir = $dir . '/' . $id;
                    @rename($olddir, $newdir);
                }
            }
        }
        return $id;
    }
    public function capnhat($data = array(), $thongtin) {
        $data = (array)$data;
        $id = parent::edit($data);
        if ($id) {
            $id = $data['id'];
        }
        return $id;
    }
}