<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_mdel extends MY_Model
{
    public $table = 'users';
    public $rolesStyle = array(
        'admin' => 'Administrator',
        'supregion' => 'Quản lý khu vực quốc gia',
        'region' => 'Quản lý khu vực miền',
        'salesman' => 'Tổng đài viên',
        'branch' => 'Quản lý chi nhánh',
        'members' => 'Nhân viên'
    );
    public $account_supper_regions = array(
        array(
            'id' => 'supregion',
            'name' => 'Quản lý khu vực quốc gia'
        )
    );
    public $account_regions = array(
        array(
            'id' => 'region',
            'name' => 'Quản lý khu vực miền'
        ),
        array(
            'id' => 'salesman',
            'name' => 'Tổng đài viên'
        )
    );
    public $account_branches = array(
        array(
            'id' => 'branch',
            'name' => 'Quản lý chi nhánh'
        ),
        array(
            'id' => 'members',
            'name' => 'Nhân viên'
        )
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function getNameList($wheres = array(), $options = array())
    {
        $danhsach2 = [];
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            foreach ($danhsach as $value) {
                /*$value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
                $value->fullname = $value->first_name.' '.$value->last_name;
                $value->password = '';*/
                $danhsach2[] = $value->first_name . ' ' . $value->last_name;
            }
        }
        return $danhsach2;
    }

    public function getList($wheres = array(), $options = array())
    {
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            foreach ($danhsach as &$value) {
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
                $value->fullname = $value->first_name . ' ' . $value->last_name;
                $value->password = '';
            }
        }
        return $danhsach;
    }

    public function getDetail($id, $selector = '*', $escape = true)
    {
        $result = parent::getDetail($id, $selector, $escape);
        if (isset($result->id)) {
            $result->status = (int)$result->status;
            $result->status_class = $this->orderStatusClasses[$result->status];
            $result->status_text = $this->orderStatuses[$result->status];
            $result->fullname = $result->first_name . ' ' . $result->last_name;
            $result->password = '';
        }
        return $result;
    }

    public function danhsach($data, $thongtin)
    {
        $id = @$data->id;
        $region = $account_types = $regions = array();
        if ($id) {
            $region = parent::getDetail($id);
            $region->uniqueId = $region->id;
            $region->password = '';
            $region->birthday = strtotime($region->birthday);
            $region->role_tbl = $region->role_tbl . '_' . $region->role_tbl_id;
        }
        $this->load->model('Regions_mdel');
        $this->load->model('Branches_mdel');
        $regions = $this->Regions_mdel->getList(array(), array('getAll' => true));
        $branches = $this->Branches_mdel->getList(array(), array('getAll' => true));
        $tmp = $this->chuanbiDulieu($thongtin);
        if ($data->loai == 'customer') {
            $danhsach = $this->getList(array('role' => 'customer'), array('getAll' => true, 'order_by' => array('id' => 'desc')));
        } else {
            $user = $this->getDetail($thongtin->uid);
            $user_role = $user->role;
            if ($user_role == 'admin') {
                $danhsach = $this->getList(array('role' => array('members', 'region', 'supregion', 'branch', 'salesman'), 'id != ' => $thongtin->uid), array('getAll' => true, 'order_by' => array('status' => 'desc')));
            } elseif ($user_role == 'supregion') {
                $tmpregions = $tmpbranches = array();
                if (count($regions)) {
                    foreach ($regions as $value) {
                        if ($value->parent_id == $user->role_tbl_id) {
                            $tmpregions[] = $value->id;
                            if (count($branches)) {
                                foreach ($branches as $value2) {
                                    if ($value2->region_id == $value->id) {
                                        $tmpbranches[] = $value2->id;
                                    }
                                }
                            }
                        }
                    }
                }
                $tmpregions = implode(', ', $tmpregions);
                $tmpbranches = implode(', ', $tmpbranches);
                $sql = 'select * from ' . $this->table . ' where ((role="supregion" and role_tbl_id=' . $user->role_tbl_id . ')';
                if ($tmpregions) {
                    $sql .= ' or (role="region" and role_tbl_id in (' . $tmpregions . ')) or ' .
                        '(role="salesman" and role_tbl_id in (' . $tmpregions . '))';
                }
                if ($tmpbranches) {
                    $sql .= ' or (role="branch" and role_tbl_id in (' . $tmpbranches . ')) or ' .
                        '(role="members" and role_tbl_id in (' . $tmpbranches . '))';
                }
                $sql .= ') and id!=' . $thongtin->uid;
                $danhsach = parent::getListBySQL($sql);
            } elseif ($user_role == 'branch') {
                $sql = 'select * from ' . $this->table . ' where ((role="branch" and role_tbl_id=' . $user->role_tbl_id . ') or (role="members" and role_tbl_id=' . $user->role_tbl_id . ')) and id!=' . $thongtin->uid;
                $danhsach = parent::getListBySQL($sql);
            } elseif ($user_role == 'region') {
                $tmpbranches = array();
                if (count($branches)) {
                    foreach ($branches as $value2) {
                        if ($value2->region_id == $user->role_tbl_id) {
                            $tmpbranches[] = $value2->id;
                        }
                    }
                }
                $tmpbranches = implode(', ', $tmpbranches);
                $sql = 'select * from ' . $this->table . ' where ((role="region" and role_tbl_id=' . $user->role_tbl_id . ') or (role="salesman" and role_tbl_id=' . $user->role_tbl_id . ')';
                if ($tmpbranches) {
                    $sql .= ' or (role="branch" and role_tbl_id in (' . $tmpbranches . ')) or ' .
                        '(role="members" and role_tbl_id in (' . $tmpbranches . '))';
                }
                $sql .= ') and id!=' . $thongtin->uid;
                $danhsach = parent::getListBySQL($sql);
            }
            //$danhsach = $this->getList(array('role' => array('members', 'region', 'supregion', 'branch', 'salesman'), 'id != ' => $thongtin->uid), array('getAll' => true, 'order_by' => array('status' => 'desc')));
            if (count($danhsach)) {
                foreach ($danhsach as &$value) {
                    $value->status = (int)$value->status;
                    $value->status_class = $this->orderStatusClasses[$value->status];
                    $value->status_text = $this->orderStatuses[$value->status];
                    $value->fullname = $value->first_name . ' ' . $value->last_name;
                    $value->rolename = $this->rolesStyle[$value->role];
                    unset($value->password, $value->salt, $value->points, $value->activation_code, $value->forgotten_password_code, $value->forgotten_password_time, $value->remember_code);

                    if ($value->role_tbl == 'region') {
                        $value->table = '';
                        foreach ($regions as $item) {
                            if ($item->id == $value->role_tbl_id) {
                                $value->table = $item->name;
                            }
                        }
                    } elseif ($value->role_tbl == 'branch') {
                        $value->table = '';
                        foreach ($branches as $item) {
                            if ($item->id == $value->role_tbl_id) {
                                $value->table = $item->name;
                            }
                        }
                    } else {
                        $value->table = '';
                    }
                    unset($value->role_tbl, $value->role_tbl_id, $value->verify_email, $value->verify_phone);
                }
            }
        }
        return array('data' => $danhsach, 'uniqueId' => md5(time() . randomString(10)), 'branchInfo' => $region, 'account_types' => $account_types, 'regions' => $tmp);
    }

    public function chuanbiDulieu($thongtin)
    {
        $user = $this->getDetail($thongtin->uid);
        $role = $user->role;
        $tmp = array();
        $this->load->model('Groups_mdel');
        $this->load->model('Regions_mdel');
        $this->load->model('Branches_mdel');
        $branches = $this->Branches_mdel->getList(array(), array('getAll' => true));
        $regions = $this->Regions_mdel->getList(array(), array('getAll' => true));
        if ($role == 'admin') {
            if (count($regions)) {
                foreach ($regions as $row) {
                    if ($row->parent_id == 0) {
                        $row->paddingleft = 11;
                        $row->id2 = 'region_' . $row->id;
                        $row->account_types = $this->account_supper_regions;
                        $tmp[] = $row;
                        foreach ($regions as $row2) {
                            if ($row2->parent_id == $row->id) {
                                $row2->paddingleft = 20;
                                $row2->id2 = 'region_' . $row2->id;
                                $row2->account_types = $this->account_regions;
                                $tmp[] = $row2;
                                if (count($branches)) {
                                    foreach ($branches as $row3) {
                                        if ($row3->region_id == $row2->id) {
                                            $row3->paddingleft = 40;
                                            $row3->id2 = 'branch_' . $row3->id;
                                            $row3->account_types = $this->account_branches;
                                            $tmp[] = $row3;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } elseif ($role == 'supregion') {
            if (count($regions)) {
                foreach ($regions as $row) {
                    if ($row->parent_id == 0 && $row->id == $user->role_tbl_id) {
                        foreach ($regions as $row2) {
                            if ($row2->parent_id == $row->id) {
                                $row2->paddingleft = 11;
                                $row2->id2 = 'region_' . $row2->id;
                                $row2->account_types = $this->account_regions;
                                $tmp[] = $row2;
                                if (count($branches)) {
                                    foreach ($branches as $row3) {
                                        if ($row3->region_id == $row2->id) {
                                            $row3->paddingleft = 20;
                                            $row3->id2 = 'branch_' . $row3->id;
                                            $row3->account_types = $this->account_branches;
                                            $tmp[] = $row3;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } elseif ($role == 'region') {
            if (count($regions)) {
                foreach ($regions as $row) {
                    if ($row->parent_id != 0 && $row->id == $user->role_tbl_id) {
                        $row->paddingleft = 11;
                        $row->id2 = 'region_' . $row->id;
                        $row->account_types = array($this->account_regions[1]);
                        $tmp[] = $row;
                        if (count($branches)) {
                            foreach ($branches as $row2) {
                                if ($row2->region_id == $row->id) {
                                    $row2->paddingleft = 20;
                                    $row2->id2 = 'branch_' . $row2->id;
                                    $row2->account_types = $this->account_branches;
                                    $tmp[] = $row2;
                                }
                            }
                        }
                    }
                }
            }
        } elseif ($role == 'branch') {
            if (count($branches)) {
                foreach ($branches as $row) {
                    if ($row->id == $user->role_tbl_id) {
                        $row->paddingleft = 11;
                        $row->id2 = 'branch_' . $row->id;
                        $row->account_types = array($this->account_branches[1]);
                        $tmp[] = $row;
                    }
                }
            }
        }
        return $tmp;
    }

    public function chuanbi($data, $thongtin)
    {
        $region = array(
            'first_name' => '',
            'last_name' => '',
            'username' => '',
            'phone' => '',
            'email' => '',
            'password' => '',
            'birthday' => strtotime(date('1984-m-d H:i:s')),
            'role_tbl' => '',
            'role' => '',
            'gender' => '1',
            'status' => '1',
            'page_facebook' => '',
            'page_twitter' => '',
            'home_address' => '',
            'company_address' => '',
            'uniqueId' => ''
        );
        $account_types = array();
        //print_r($thongtin);
        $tmp = $this->chuanbiDulieu($thongtin);
        return array('branchInfo' => $region, 'uniqueId' => md5(time() . randomString(10)), 'account_types' => $account_types, 'regions' => $tmp);
    }

    public function luutru($data = array(), $thongtin)
    {
        $regionInfo = @$data->thongtin;
        $birthday = date('Y-m-d', strtotime($regionInfo->birthday));
        $role_tbl = explode('_', $regionInfo->role_tbl);
        $region = array(
            'first_name' => $regionInfo->first_name,
            'last_name' => $regionInfo->last_name,
            'status' => (int)$regionInfo->status,
            'active' => (int)$regionInfo->status,
            'username' => $regionInfo->username,
            'phone' => $regionInfo->phone,
            'password' => $regionInfo->password,
            'birthday' => $birthday,
            'page_facebook' => $regionInfo->page_facebook,
            'page_twitter' => $regionInfo->page_twitter,
            'role_tbl' => @$role_tbl[0],
            'role_tbl_id' => @$role_tbl[1],
            'home_address' => $regionInfo->home_address,
            'company_address' => $regionInfo->company_address,
            'role' => @$regionInfo->role,
            'gender' => (int)$regionInfo->gender,
            'email' => $regionInfo->email
        );
        //print_r($data);
        $uniqueId = @$regionInfo->uniqueId;
        if (isset($regionInfo->id)) {
            $region['id'] = $regionInfo->id;
            unset($region['username'], $region['phone'], $region['email']);
            if ($region['role'] == 'admin' || $region['role'] == 'customer') {
                unset($region['role'], $region['role_tbl'], $region['role_tbl_id']);
            }
            $id = parent::edit($region);
            if ($id) {
                $id = $region['id'];
            }
        } else {
            if ($region['password']) {
                $user = parent::getDetailByField('email', $region['email']);
                if (isset($user->id)) {
                    $id = false;
                } else {
                    $user = parent::getDetailByField('phone', $region['phone']);
                    if (isset($user->id)) {
                        $id = false;
                    } else {
                        $user = parent::getDetailByField('username', $region['username']);
                        if (isset($user->id)) {
                            $id = false;
                        } else {
                            $id = parent::create($region);
                            if ($id) {
                                if ($uniqueId && $uniqueId != $id) {
                                    $dir = FCPATH . 'themes/quanly/assets/upload/' . $this->table;
                                    $olddir = $dir . '/' . $uniqueId;
                                    $newdir = $dir . '/' . $id;
                                    @rename($olddir, $newdir);
                                }
                            }
                        }
                    }
                }
            } else {
                $id = false;
            }
        }
        return $id;
    }

    public function luutru2($data = array(), $thongtin)
    {
        $regionInfo = @$data->thongtin;
        $birthday = date('Y-m-d', strtotime($regionInfo->birthday));
        $role_tbl = explode('_', $regionInfo->role_tbl);
        $region = array(
            'first_name' => $regionInfo->first_name,
            'last_name' => $regionInfo->last_name,
            //'status' => (int)$regionInfo->status,
            //'active' => (int)$regionInfo->status,
            'username' => $regionInfo->username,
            'phone' => $regionInfo->phone,
            'password' => $regionInfo->password,
            'birthday' => $birthday,
            'page_facebook' => $regionInfo->page_facebook,
            'page_twitter' => $regionInfo->page_twitter,
            //'role_tbl' => @$role_tbl[0],
            //'role_tbl_id' => @$role_tbl[1],
            'home_address' => $regionInfo->home_address,
            'company_address' => $regionInfo->company_address,
            //'role' => @$regionInfo->role,
            'gender' => (int)$regionInfo->gender,
            'email' => $regionInfo->email
        );
        //print_r($data);
        $uniqueId = @$regionInfo->uniqueId;
        if (isset($regionInfo->id)) {
            $region['id'] = $regionInfo->id;
            unset($region['username'], $region['phone'], $region['email']);
            if ($region['role'] == 'admin' || $region['role'] == 'customer') {
                unset($region['role'], $region['role_tbl'], $region['role_tbl_id']);
            }
            $id = parent::edit($region);
            if ($id) {
                $id = $region['id'];
            }
        } else {
            if ($region['password']) {
                $user = parent::getDetailByField('email', $region['email']);
                if (isset($user->id)) {
                    $id = false;
                } else {
                    $user = parent::getDetailByField('phone', $region['phone']);
                    if (isset($user->id)) {
                        $id = false;
                    } else {
                        $user = parent::getDetailByField('username', $region['username']);
                        if (isset($user->id)) {
                            $id = false;
                        } else {
                            $id = parent::create($region);
                            if ($id) {
                                if ($uniqueId && $uniqueId != $id) {
                                    $dir = FCPATH . 'themes/quanly/assets/upload/' . $this->table;
                                    $olddir = $dir . '/' . $uniqueId;
                                    $newdir = $dir . '/' . $id;
                                    @rename($olddir, $newdir);
                                }
                            }
                        }
                    }
                }
            } else {
                $id = false;
            }
        }
        return $id;
    }

    public function capnhat($data = array(), $thongtin)
    {
        $data = (array)$data;
        if (isset($data['status'])) {
            $data['active'] = $data['status'];
        }
        $id = parent::edit($data);
        if ($id) {
            $id = $data['id'];
        }
        return $id;
    }


    public function getPoint($userId = null, $getAll = false)
    {
        if (is_null($userId )) {
            $userId = $this->auth->userId();
        }

        // select sum(CASE WHEN `status` IN(3,4) THEN points ELSE 0 END) as p, sum(CASE WHEN `status` < 5 THEN points_used ELSE 0 END) as u from orders where customer_id = 9729

        $resPoint = $this->db
            ->select('sum(points) as total_points', false)
            ->where('customer_id', $userId)
            ->where_in('status', [3, 4])
            ->get('orders')
            ->row();

        $pointBonus = empty($resPoint) ? 0 : intval($resPoint->total_points);

        $resPointUsed = $this->db
            ->select('sum(points_used) as total_points_used', false)
            ->where('customer_id', $userId)
            ->where('status <=', 4)
            ->get('orders')
            ->row();

        $pointUsed = empty($resPointUsed) ? 0 : intval($resPointUsed->total_points_used);

        $point = $pointBonus - $pointUsed;
        if ($point < 0) {
            $point = 0;
        }


        if ($getAll) {
            return [
                'total' => $pointBonus,
                'used' => $pointUsed,
                'available' => $point
            ];
        } else {
            return $point;
        }
    }


    public function calculateLevel($userId, $upgrade = true, $point = null)
    {
        if (is_null($point)) {
            $res = $this->db
                ->select_sum('points')
                ->select_sum('points_used')
                ->where('customer_id', $userId)
                ->where('status', 4)
                ->get('orders')
                ->row();

            $point = (int) $res->points;
            $pointsUsed = (int) $res->points_used;
        }

        if ($point <= 50) {
            $level = 1;
        } elseif ($point <= 700) {
            $level = 2;
        } elseif ($point <= 2000) {
            $level = 3;
        } else {
            $level = 4;
        }

        $this->db->where('id', $userId)->update('users', ['points' => $point, 'points_used' => $pointsUsed]);

        if ($upgrade) {
            $user = $this->db
                ->select('level')
                ->where('id', $userId)
                ->get($this->table)
                ->row();
            $currentLevel = (int) $user->level;

            if ($currentLevel != $level) {
                $this->update(['level' => $level], $userId);
            }
        }

        return $level;
    }
}