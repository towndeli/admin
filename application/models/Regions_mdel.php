<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Regions_mdel extends MY_Model {
    public $table = 'regions';
    public function __construct() {
        parent::__construct();
    }
    public function getList($wheres = array(), $options = array()) {
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            $this->load->model('Users_mdel');
            foreach ($danhsach as &$value) {
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
                $quanly = $this->Users_mdel->getNameList(array('role' => 'supregion', 'role_tbl_id' => $value->id), array('getAll' => true));
                $value->quanly = implode(', ', $quanly);
                $children = parent::getList(array('parent_id' => $value->id), array('getAll' => true, 'order_by' => array('status' => 'desc')));
                if (count($children)) {
                    foreach ($children as &$value2) {
                        $value2->status = (int)$value2->status;
                        $value2->status_class = $this->orderStatusClasses[$value2->status];
                        $value2->status_text = $this->orderStatuses[$value2->status];
                        $quanly = $this->Users_mdel->getNameList(array('role' => 'region', 'role_tbl_id' => $value2->id), array('getAll' => true));
                        $value2->quanly = implode(', ', $quanly);
                    }
                }
                $value->children = $children;
                //$value->children_total = count($children);
            }
        }
        return $danhsach;
    }
    public function getDetail($id, $selector = '*', $escape = true) {
        $result = parent::getDetail($id, $selector, $escape);
        if (isset($result->id)) {
            $result->status = (int)$result->status;
            $result->status_class = $this->orderStatusClasses[$result->status];
            $result->status_text = $this->orderStatuses[$result->status];
            $children = parent::getList(array('parent_id' => $result->id), array('getAll' => true, 'order_by' => array('status' => 'desc')));
            if (count($children)) {
                foreach ($children as &$value2) {
                    $value2->status = (int)$value2->status;
                    $value2->status_class = $this->orderStatusClasses[$value2->status];
                    $value2->status_text = $this->orderStatuses[$value2->status];
                }
            }
            $result->children = $children;
            //$result->children_total = count($children);
        }
        return $result;
    }
    public function getList4Branch($wheres = array(), $options = array()) {
        $danhsach = parent::getList($wheres, $options);
        $tmp = array();
        if (count($danhsach)) {
            foreach ($danhsach as $value) {
                if ($value->parent_id) {
                    $value->status = (int)$value->status;
                    $value->status_class = $this->orderStatusClasses[$value->status];
                    $value->status_text = $this->orderStatuses[$value->status];
                    $value->groupName = $this->getParentName($danhsach, $value->parent_id);
                    if ($value->groupName) {
                        $tmp[] = $value;
                    }
                }
                /*$children = parent::getList(array('parent_id' => $value->id), array('getAll' => true, 'order_by' => array('status' => 'desc')));
                if (count($children)) {
                    foreach ($children as &$value2) {
                        $value2->status = (int)$value2->status;
                        $value2->status_class = $this->orderStatusClasses[$value2->status];
                        $value2->status_text = $this->orderStatuses[$value2->status];
                    }
                }
                $value->children = $children;
                //$value->children_total = count($children);*/
            }
        }
        return $tmp;
    }
    private function getParentName($danhsach, $parent_id) {
        $tmp = '';
        if (count($danhsach)) {
            foreach ($danhsach as $value) {
                if ($value->id==$parent_id) {
                    $tmp = $value->name;
                    break;
                }
            }
        }
        return $tmp;
    }
    public function danhsach($data, $thongtin) {
        $parentid = @$data->id;
        $region = $regions = array();
        if ($parentid) {
            $region = parent::getDetail($parentid);
            $regions = parent::getList(array('parent_id' => 0, 'id != ' => $region->id), array('getAll' => true));
        }
        $danhsach = [];
        $this->load->model(array('Users_mdel', 'Quocgia_mdel'));
        $user = $this->Users_mdel->getDetail($thongtin->uid);
        if ($user->role=='admin') {
            $danhsach = $this->getList(array('parent_id' => 0), array('getAll' => true, 'order_by' => array('status' => 'desc')));
        } elseif ($user->role=='supregion') {
            $danhsach = $this->getList(array('id' => $user->role_tbl_id), array('getAll' => true, 'order_by' => array('status' => 'desc')));
        }
        return array('data' => $danhsach, 'regionInfo' => $region, 'regions' => $regions, 'phamviregions' => $this->Quocgia_mdel->getList(array(), array('getAll' => true)));
    }
    public function chuanbi($data, $thongtin) {
        $result = $this->mauchung();
        /*if (@$data->id) {
            $order = parent::getDetail(@$data->id);
            if (isset($order->id)) {
                $orders = json_decode($order->cart_data);
                if (count($orders)) {
                    $this->load->model('Menus_mdel');
                    $this->load->model('Toppings_mdel');
                    foreach ($orders as $key => &$value) {
                        $menu = $this->Menus_mdel->getDetail($value->id);
                        if (isset($menu->id)) {
                            $value->name = $menu->name;
                            //$value->hinhanh = isset($menu->hinhanh)?$menu->hinhanh:'http://downloadicons.net/sites/default/files/product-icon-27962.png';
                            $tmptoppings = $menu->toppings;
                            $tmptoppings = explode(',', $tmptoppings);
                            $toppings = $this->Toppings_mdel->getList(array('id' => $tmptoppings), array('getAll' => true));
                            if (count($toppings)) {
                                foreach ($toppings as &$value2) {
                                    $key2 = array_search($value2->id, $this->array_column2($value->toppings, 'id'));
                                    if ($key2 !== false) {
                                        $value2->qty = $value->toppings[$key2]->qty;
                                        $value2->price = $value->toppings[$key2]->price;
                                    } else {
                                        $value2->qty = 0;
                                    }
                                }
                            }
                            $value->toppings = $toppings;
                            $value->size = array(
                                'sm' => array(
                                    'label' => $this->menuSizes['sm'].'oz',
                                    'price' => $menu->price
                                ),
                                'md' => array(
                                    'label' => $this->menuSizes['md'].'oz',
                                    'price' => $menu->price_m
                                ),
                                'lg' => array(
                                    'label' => $this->menuSizes['lg'].'oz',
                                    'price' => $menu->price_l
                                )
                            );
                        } else {
                            unset($orders[$key]);
                        }
                    }
                }
                $orderInfo = array(
                    'id' => (int)$order->id,
                    'idv' => $order->idv,
                    'order_number' => $order->order_number,
                    'edit_count' => $order->edit_count
                );
                $customerInfo = array(
                    'name' => $order->customer_name,
                    'phone' => $order->customer_phone,
                    'gender' => (int)$order->gender
                );
                $shippingInfo = array(
                    'floor' => $order->shipping_floor,
                    'address' => $order->shipping_address,
                    'branchid' => (int)$order->branch_id,
                    'delivery_on' => $order->shipping_on,
                    'note' => $order->note,
                    'discount_code' => $order->discount_code
                );
                $result['orders'] = $orders;
                $result['orderInfo'] = $orderInfo;
                $result['customerInfo'] = $customerInfo;
                $result['shippingInfo'] = $shippingInfo;
            }
        }*/
        return $result;
    }
    /*private function array_column2($arr, $col) {
        $tmp = array();
        foreach ($arr as $value) {
            $tmp[] = $value->{$col};
        }
        return $tmp;
    }*/
    public function mauchung() {
        $this->load->model('Quocgia_mdel');
        $regions = parent::getList(array('parent_id' => 0), array('getAll' => true));
        return array('data' => $regions, 'phamviregions' => $this->Quocgia_mdel->getList(array(), array('getAll' => true)));
    }
    public function luutru($data = array(), $thongtin) {
        $regionInfo = @$data->thongtin;
        $region = array(
            'name' => $regionInfo->name,
            'parent_id' => $regionInfo->parent_id,
            'phamvi' => $regionInfo->phamvi,
            'status' => (int)$regionInfo->status
        );
        if (isset($regionInfo->id)) {
            $region['id'] = $regionInfo->id;
            $id = parent::edit($region);
            if ($id) {
                $id = $region['id'];
            }
        } else {
            $id = parent::create($region);
        }
        return $id;
    }
    public function capnhat($data = array(), $thongtin) {
        $data = (array)$data;
        $id = parent::edit($data);
        if ($id) {
            $id = $data['id'];
        }
        return $id;
    }
    /*public function binhluan($data, $thongtin) {
        $this->load->model('Order_comments_mdel');
        $id = $data->id;
        $comment = $data->comment;
        $userid = $thongtin->uid;
        $data2 = array(
            'order_id' => $id,
            'user_id' => $userid,
            'time_created' => date('Y-m-d H:i:s'),
            'noidung' => trim($comment)
        );
        $this->Order_comments_mdel->create($data2);
        return $id;
    }
    public function lastOrderNumber() {
        $lastOrderToday = $this->db
            ->select('order_number as no')
            ->where('DATE(`time_created`)', date('Y-m-d'))
            ->order_by('order_number', 'DESC')
            ->get($this->table)
            ->row();
        return is_null($lastOrderToday) ? 1 : intval($lastOrderToday->no) + 1;
    }*/
}