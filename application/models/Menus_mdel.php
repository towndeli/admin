<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menus_mdel extends MY_Model {
    public $table = 'menus';
    public function __construct() {
        parent::__construct();
    }
    public function getList($wheres = array(), $options = array()) {
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            foreach ($danhsach as &$value) {
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
                $value->dongia = array(
                    'S' => $value->price,
                    'M' => $value->price_m,
                    'L' => $value->price_l
                );
            }
        }
        return $danhsach;
    }
    public function getDetail($id, $selector = '*', $escape = true) {
        $result = parent::getDetail($id, $selector, $escape);
        if (isset($result->id)) {
            $result->status = (int)$result->status;
            $result->status_class = $this->orderStatusClasses[$result->status];
            $result->status_text = $this->orderStatuses[$result->status];
        }
        return $result;
    }
    public function danhsach($data, $thongtin) {
        $id = @$data->id;
        $region = $regions = $toppings = array();
        if ($id) {
            $region = parent::getDetail($id);
            $toppings = $region->toppings;
            $toppings = explode(',', $toppings);
            if (count($toppings)) {
                foreach ($toppings as $key => $value) {
                    if (!$value) {
                        unset($toppings[$key]);
                    }
                }
            }
            $region->toppings = $toppings;
            $region->uniqueId = $region->id;
        }
        $this->load->model('Categories_mdel');
        $regions = $this->Categories_mdel->getList(array(), array('getAll' => true));
        $this->load->model('Toppings_mdel');
        $toppings = $this->Toppings_mdel->getList(array(), array('getAll' => true));
        return array('data' => $this->getList(array(), array('getAll' => true, 'order_by' => array('status' => 'desc'))), 'uniqueId' => md5(time() . randomString(10)), 'branchInfo' => $region, 'categories' => $regions, 'toppings' => $toppings);
    }
    public function chuanbi($data, $thongtin) {
        $region = array(
            'name' => '',
            'description' => '',
            'category_id' => '',
            'price' => 0,
            'price_m' => 0,
            'price_l' => 0,
            'toppings' => array(),
            'quantity' => 0,
            'uniqueId' => '',
            'status' => '1'
        );
        $this->load->model('Categories_mdel');
        $regions = $this->Categories_mdel->getList(array(), array('getAll' => true));
        $this->load->model('Toppings_mdel');
        $toppings = $this->Toppings_mdel->getList(array(), array('getAll' => true));
        return array('branchInfo' => $region, 'uniqueId' => md5(time() . randomString(10)), 'categories' => $regions, 'toppings' => $toppings);
    }
    public function luutru($data = array(), $thongtin) {
        $regionInfo = @$data->thongtin;
        $toppings = implode(',', $regionInfo->toppings);
        $region = array(
            'name' => $regionInfo->name,
            'description' => $regionInfo->description,
            'category_id' => (int)$regionInfo->category_id,
            'price' => (int)$regionInfo->price,
            'price_m' => (int)$regionInfo->price_m,
            'price_l' => (int)$regionInfo->price_l,
            'toppings' => $toppings,
            'quantity' => (int)$regionInfo->quantity,
            'use_straw' => (int) $regionInfo->use_straw,
            'use_spoon' => (int) $regionInfo->use_spoon,
            'status' => (int)$regionInfo->status
        );
        $uniqueId = @$regionInfo->uniqueId;
        if (isset($regionInfo->id)) {
            $region['id'] = $regionInfo->id;
            $id = parent::update($region, 'id');
            if ($id) {
                $id = $region['id'];
            }
        } else {
            $id = parent::create($region);
            if ($id) {
                if ($uniqueId && $uniqueId!=$id) {
                    $dir = FCPATH . 'themes/quanly/assets/upload/' . $this->table;
                    $olddir = $dir . '/' . $uniqueId;
                    $newdir = $dir . '/' . $id;
                    @rename($olddir, $newdir);
                }
            }
        }
        return $id;
    }
    public function capnhat($data = array(), $thongtin) {
        $data = (array)$data;
        $id = parent::edit($data);
        if ($id) {
            $id = $data['id'];
        }
        return $id;
    }
}