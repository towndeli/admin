<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Messages_mdel extends MY_Model {

    public $table = 'messages'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    private $apiKey, $secretKey;
    private $apiBaseUrl = 'http://restapi.esms.vn/MainService.svc/json';


    function __construct()
    {
        parent::__construct();

        /*$config = $this->load->config('esms', TRUE);
        $this->apiKey = $config['ApiKey'];
        $this->secretKey = $config['SecretKey'];*/

        $ci =& get_instance();
        $ci->config->load('esms', true);
        $this->apiKey = $ci->config->item('ApiKey', 'esms');
        $this->secretKey = $ci->config->item('SecretKey', 'esms');
    }


    public function sendVerify($phoneNumber)
    {
        $activeCode = strtoupper(randomString());
        $content = '[TownDeli.vn] Hay dien Ma Xac Thuc ' . $activeCode . ' de xac thuc so dien thoai cua ban. Xin cam on!';

        return $this->send($phoneNumber, $content);
    }


    public function sendConfirmOrder($phoneNumber, $orderId)
    {
        $this->load->model('Orders');
        $order = $this->Orders->getDetail($orderId);
        $phoneNumber = $order->customer_phone;
        // var_dump($order);

        $deliveryTime = time() + 2100;
        if (!empty($order->shipping_on)) {
            $deliveryTime = strtotime($order->shipping_on);
        }

        // $content = 'TOWNDELI da nhan duoc Order ' . strtoupper($order->idv) . ' va du kien se giao tan noi luc ' . date('H:i', $deliveryTime) . '. Quy Khach co the theo doi Order tai http://tr.towndeli.vn/' . $order->idv . '. Xin cam on!';

        $content = 'TOWNDELI da nhan duoc Order ' . strtoupper($order->idv) . ' cua Quy Khach va du kien se giao tan noi luc ' . date('H:i', $deliveryTime) . '. Neu co yeu cau khac, vui long goi (08)73073777. Xin cam on!';

        return $this->send($phoneNumber, $content);
    }


    public function sendGiftOrder($phoneNumber, $orderId)
    {
        $this->load->model('Orders');
        $order = $this->Orders->getDetail($orderId);
        $phoneNumber = $order->customer_phone;

        $shipping_on = empty($order->shipping_on) ? time() + 2100 : strtotime($order->shipping_on);

        $content = '[TownDeli.vn] Chuc mung! Quy khach vua duoc tang order ' . strtoupper($order->idv) . ' duoc giao tan noi luc ' . date('H:i (d/m/Y)', $shipping_on)  . '. Quy khach co the theo doi don hang tai ' . site_url('/tracking/order/' . $order->idv) . '. Xin cam on!';

        return $this->send($phoneNumber, $content);
    }


    public function sendDelay($phoneNumber, $orderId)
    {
        $this->load->model('Orders');
        $order = $this->Orders->getDetail($orderId);
        $phoneNumber = $order->customer_phone;

        $content = 'TOWNDELI thanh that xin loi Quy Khach. Vi mot so ly do ngoai y muon, Order ' . strtoupper($order->idv) . ' se duoc giao tre hon du kien khoang 10-15 phut. Mong Quy Khach thong cam!';

        return $this->send($phoneNumber, $content);
    }


    public function sendConfirmAccount($phoneNumber, $password)
    {
        $content = '[TownDeli.vn] Quy khach co the dat mon truc tuyen tai TownDeli.vn voi tai khoan:\nUsername: ' . $phoneNumber . '\nPassword: ' . $password . '\nXin cam on!';

        return $this->send($phoneNumber, $content);
    }


    public function getBalance()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiBaseUrl . '/GetBalance/' . $this->apiKey . '/' . $this->secretKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($result);

        return empty($data->Balance) ? 0 : (int) $data->Balance;
    }


    public function getSMSStatus($smsId)
    {
        $xml = '<RQST><APIKEY>' . $this->apiKey . '</APIKEY><SECRETKEY>' . $this->secretKey . '</SECRETKEY><SMSID>' . $smsId . '</SMSID></RQST>';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiBaseUrl . '/GetSendStatus/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/xml',
            'Content-length: ' . strlen($xml)
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $result = curl_exec($ch);
        curl_close($ch);
        var_dump($xml);

        echo($result);
    }


    public function send($phoneNumber, $content)
    {
        if (ENVIRONMENT === 'production') {
            $params = [
                'ApiKey' => $this->apiKey,
                'SecretKey' => $this->secretKey,
                'SmsType' => 8,
                'Phone' => $phoneNumber,
                'Content' => $content,
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->apiBaseUrl . '/SendMultipleMessage_V4_get?' . http_build_query($params));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            $result = null;
        }

        if (empty($result)) {
            $codeResult = 0;
            $smsId = 0;
        } else {
            $data = json_decode($result);

            $codeResult = (int) @$data->CodeResult;
            $smsId = (int) @$data->SMSID;
        }

        $data = [
            'sms_id' => $smsId,
            'code_result' => $codeResult,
            'phone' => $phoneNumber,
            'content' => $content,
            'time_created' => date('Y-m-d H:i:s')
        ];
        if ($codeResult < 100) {
            $data['status'] = -1;
        } elseif ($codeResult === 100) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        $id = parent::create($data);
        $data['id'] = $id;

        return $data;
    }
}