<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Discounts_mdel extends MY_Model
{
    public $table = 'discounts';

    public function __construct()
    {
        parent::__construct();
    }

    public function getList($wheres = array(), $options = array())
    {
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            foreach ($danhsach as &$value) {
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
            }
        }
        return $danhsach;
    }

    public function getDetail($id, $selector = '*', $escape = true)
    {
        $result = parent::getDetail($id, $selector, $escape);
        if (isset($result->id)) {
            $result->status = (int)$result->status;
            $result->status_class = $this->orderStatusClasses[$result->status];
            $result->status_text = $this->orderStatuses[$result->status];
        }
        return $result;
    }

    public function getAvailable()
    {
        return $this->db
            ->where('type', 2)
            ->where('time_start <= NOW()', null, false)
            ->where('time_end >= NOW()', null, false)
            ->where('status', 1)
            ->get($this->table)
            ->result();
    }

    public function apply($code = '', $carts = [])
    {
        $code = strtolower($code);//print_r($carts);
        $total = $total2 = 0;//$this->cart->total();

        $discount = 0;

        if (count($carts)) {
            foreach ($carts as $idx => $value) {
                $tongtientoppings = 0;
                foreach ($value->toppings as $value2) {
                    if ($value2->qty) {
                        $tongtientoppings += ($value2->qty * $value2->price * $value->qty);
                    }
                }
                $total += (($value->selected_price * $value->qty) + $tongtientoppings);
                $total2 += ($value->selected_price * $value->qty);

                // Clear priceAfter
                unset($value->priceAfter);
                $carts[$idx] = $value;
            }
        }
        $row = $this->getDetailByField('code', $code);
        if (!isset($row->id)) {
            return [
                'data' => 0,
                'description' => '',
                'newCartData' => $carts
            ];
        }

        //print_r($row);
        switch ($row->type_value) {

            // Giảm giá theo số tiền
            case 1:
                $discount = (int) $row->value;
                break;

            // Giảm giá theo phần trăm
            case 2:
                $discount = round($total * intval($row->value) / 100);
                break;

            // Giảm số theo chương trình
            case 3:
                $moduleName = 'module_' . $row->code;
                if (method_exists($this, $moduleName)) {

                    $cartTotal = $this->cartTotal($carts);

                    $result = $this->{$moduleName}($carts);

                    if (is_array($result)) {
                        $discount = $result['amount'];
                        $carts = $result['cart'];
                    } else {
                        $discount = $result;
                    }

                    $discount = ($discount && $cartTotal > $discount) ? round($cartTotal - $discount, -3, PHP_ROUND_HALF_UP) : 0;
                } else {
                    $discount = 0;
                }
				
				if ($discount === 0) {
                    // Reset price discount
                    foreach ($carts as $idx => $item) {
                        unset($item->priceAfter);
                        $carts[$idx] = $item;
                    }
				}

                break;
        }

        /*if ($isSave) {
            $discounts_used = [
                'code' => $code,
                'discount_id' => $row->id,
                'user_id' => 1,
                'amount' => $discount,
                'time_add' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('discounts_used', $discounts_used);
        }*/


        $response = [
            'data' => $discount,
            'description' => $row->description,
            'newCartData' => $carts
        ];

        return $response;
    }

    // Chương trình combo 4 ly
    public function module_combo4($carts = [])
    {
        if (count($carts)) {
            $total_items = 0;
            foreach ($carts as $value) {
                $total_items += $value->qty;
            }
            if ($total_items < 4) {
                return 0;
            }
        } else {
            return 0;
        }

        $comboPrice = 119000;
        $comboSize = 4;

        $price16oz = [];
        $priceOther = [];
        $totalToppingAmount = 0;

        foreach ($carts as &$values) {
            $size = $values->selected_size;
            $price = (int)$values->selected_price;
            $qty = (int)$values->qty;            

            if ($size === 'md') {
                for ($i = 1; $i <= $qty; $i++) {
                    $price16oz[] = $price;
                }
            } else {
                $priceOther[] = $values->selected_price * $values->qty;
            }

            if (!empty($values->toppings)) {
                foreach ($values->toppings as $topping) {
                    if ($topping->qty > 0) {
                        $totalToppingAmount += ($topping->price * $topping->qty) * $qty;
                    }
                }
            }

        }
        rsort($price16oz);

        $n = floor(count($price16oz) / $comboSize);
        $comboSizeApply = $comboSize * $n;
        $sodu = (count($price16oz) % $comboSize);
        $comboTotal = array_sum(array_slice($price16oz, 0, $comboSizeApply));
        $combosPrice = $comboPrice * $n;
        $soduTotal = $sodu ? array_sum(array_slice($price16oz, $comboSizeApply, $sodu)) : 0;

        /*if ($comboTotal > $combosPrice) {
            return $comboTotal - $combosPrice;
        } else {
            return 0;
        }*/

        return $combosPrice + $soduTotal + array_sum($priceOther) + $totalToppingAmount;
    }


    // Chương trình combo 4 ly 99k
    public function module_combo99($carts = [])
    {
        if (count($carts)) {
            $total_items = 0;
            foreach ($carts as $value) {
                $total_items += $value->qty;
            }
            if ($total_items < 4) {
                return 0;
            }
        } else {
            return 0;
        }

        $comboPrice = 99000;
        $comboSize = 4;

        $price16oz = [];
        $priceOther = [];
        $totalToppingAmount = 0;

        foreach ($carts as &$values) {
            $size = $values->selected_size;
            $price = (int)$values->selected_price;
            $qty = (int)$values->qty;            

            if ($size === 'md') {
                for ($i = 1; $i <= $qty; $i++) {
                    $price16oz[] = $price;
                }
            } else {
                $priceOther[] = $values->selected_price * $values->qty;
            }

            if (!empty($values->toppings)) {
                foreach ($values->toppings as $topping) {
                    if ($topping->qty > 0) {
                        $totalToppingAmount += ($topping->price * $topping->qty) * $qty;
                    }
                }
            }

        }
        rsort($price16oz);

        $n = floor(count($price16oz) / $comboSize);
        $comboSizeApply = $comboSize * $n;
        $sodu = (count($price16oz) % $comboSize);
        $comboTotal = array_sum(array_slice($price16oz, 0, $comboSizeApply));
        $combosPrice = $comboPrice * $n;
        $soduTotal = $sodu ? array_sum(array_slice($price16oz, $comboSizeApply, $sodu)) : 0;

        return $combosPrice + $soduTotal + array_sum($priceOther) + $totalToppingAmount;
    }


    // Chương trình combo 3 ly lớn giá 119k (combo big 3)
    public function module_combobig3($carts = [])
    {
        $comboPrice = 119000;
        $comboSize = 3;

        if (count($carts)) {
            $total_items = 0;
            foreach ($carts as $value) {
                $total_items += $value->qty;
            }
            if ($total_items < $comboSize) {
                return 0;
            }
        } else {
            return 0;
        }

        $priceL = [];
        $priceOther = [];
        $totalToppingAmount = 0;

        foreach ($carts as &$values) {
            $size = $values->selected_size;
            $price = (int) $values->selected_price;
            $qty = (int) $values->qty;

            if ($size === 'lg') {
                for ($i = 1; $i <= $qty; $i++) {
                    $priceL[] = $price;
                }
            } else {
                $priceOther[] = $values->selected_price * $values->qty;
            }

            if (!empty($values->toppings)) {
                foreach ($values->toppings as $topping) {
                    if ($topping->qty > 0) {
                        $totalToppingAmount += ($topping->price * $topping->qty) * $qty;
                    }
                }
            }

        }

        if (count($priceL) < $comboSize) {
            return 0;
        }

        rsort($priceL);

        $n = floor(count($priceL) / $comboSize);
        $comboSizeApply = $comboSize * $n;
        $sodu = (count($priceL) % $comboSize);
        $comboTotal = array_sum(array_slice($priceL, 0, $comboSizeApply));
        $combosPrice = $comboPrice * $n;
        $soduTotal = $sodu ? array_sum(array_slice($priceL, $comboSizeApply, $sodu)) : 0;

        return $combosPrice + $soduTotal + array_sum($priceOther) + $totalToppingAmount;
    }


    // Chương trình combo 7 ly lớn giá 119k (combo big 7)
    public function module_combobig7($carts = [])
    {
        $comboPrice = 369000;
        $comboSize = 7;

        if (count($carts)) {
            $total_items = 0;
            foreach ($carts as $value) {
                $total_items += $value->qty;
            }
            if ($total_items < $comboSize) {
                return 0;
            }
        } else {
            return 0;
        }

        $priceL = [];
        $priceOther = [];
        $totalToppingAmount = 0;

        foreach ($carts as &$values) {
            $size = $values->selected_size;
            $price = (int) $values->selected_price;
            $qty = (int) $values->qty;

            if ($size === 'lg') {
                for ($i = 1; $i <= $qty; $i++) {
                    $priceL[] = $price;
                }
            } else {
                $priceOther[] = $values->selected_price * $values->qty;
            }

            if (!empty($values->toppings)) {
                foreach ($values->toppings as $topping) {
                    if ($topping->qty > 0) {
                        $totalToppingAmount += ($topping->price * $topping->qty) * $qty;
                    }
                }
            }

        }

        if (count($priceL) < $comboSize) {
            return 0;
        }
        
        rsort($priceL);

        $n = floor(count($priceL) / $comboSize);
        $comboSizeApply = $comboSize * $n;
        $sodu = (count($priceL) % $comboSize);
        $comboTotal = array_sum(array_slice($priceL, 0, $comboSizeApply));
        $combosPrice = $comboPrice * $n;
        $soduTotal = $sodu ? array_sum(array_slice($priceL, $comboSizeApply, $sodu)) : 0;

        return $combosPrice + $soduTotal + array_sum($priceOther) + $totalToppingAmount;
    }


    private function module_big33($carts = [])
    {
        return $this->ModuleBigx($carts, 3, 33);
    }


    private function module_big22($carts = [])
    {
        return $this->ModuleBigx($carts, 2, 22);
    }


    private function ModuleBigx($carts, $min, $discount)
    {
        $percentRemain = (100 - $discount) / 100;  // Discount x%

        $count22oz = 0;
        $total = $this->cartTotal($carts);
        $totalBalance = 0;

        foreach ($carts as &$values) {
            $size = $values->selected_size;
            $price = (int)$values->selected_price;
            $qty = (int)$values->qty;

            if ($size === 'lg') {
                $count22oz += $qty;

                if (!empty($values->toppings)) {
                    foreach ($values->toppings as $topping) {
                        if ($topping->qty > 0) {
                            $price += $topping->price * $topping->qty;
                        }
                    }
                }

                // Update data cart
                $values->priceAfter = round($price * $percentRemain, -3, PHP_ROUND_HALF_UP); // ceil(($price * $percentRemain)/1000)*1000
                $totalBalance += ($price - $values->priceAfter) * $qty;
            }
        }

        if ($count22oz >= $min) {
            return [
                'cart' => $carts,
                'amount' => $total - $totalBalance
            ];
        } else {
            return 0;
        }
    }


    private function module_freeupsize($carts = [])
    {
        $min22oz = 3;
        $count22oz = 0;

        $min16oz = 5;
        $count16oz = 0;

        if (count($carts)) {
            $this->load->model('Menus_mdel', 'Menus');
            foreach ($carts as $values) {
                $size = isset($values->selected_size) ? $values->selected_size : null;
                $qty = isset($values->qty) ? intval($values->qty) : 0;

                if ($size === 'lg') {
                    $count22oz += $qty;
                } elseif ($size === 'md') {
                    $count16oz += $qty;
                }
            }

            $total = $this->cartTotal($carts);
            $decrease = 0;

            foreach ($carts as &$values) {
                $size = isset($values->selected_size) ? $values->selected_size : null;
                $price = isset($values->selected_price) ? intval($values->selected_price) : 0;
                $qty = isset($values->qty) ? intval($values->qty) : 0;

                $toppingAmount = 0;
                if (!empty($values->toppings)) {
                    foreach ($values->toppings as $topping) {
                        if ($topping->qty > 0) {
                            $toppingAmount += $topping->price * $topping->qty;
                        }
                    }
                }

                if ($size === 'lg' && $count22oz >= $min22oz) {
                    $menu = $this->Menus->getDetail($values->id);
                    if ($menu) {
                        $priceMd = (int)$menu->price_m;
                        $decrease += ($price - $priceMd) * $qty;

                        // Update data cart item
                        $values->priceAfter = $priceMd + $toppingAmount;
                    }
                } elseif ($size === 'md' && $count16oz >= $min16oz) {
                    $menu = $this->Menus->getDetail($values->id);
                    if ($menu) {
                        $priceSm = (int)$menu->price;
                        $decrease += ($price - $priceSm) * $qty;

                        // Update data cart item
                        $values->priceAfter = $priceSm + $toppingAmount;
                    }
                }
            }

            if ($decrease > 0) {
                return [
                    'cart' => $carts,
                    'amount' => $total - $decrease
                ];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    private function module_lylongialyvua($carts = [])
    {
        $minBig = 3;
        $count = 0;
        if (count($carts)) {
            foreach ($carts as $values) {
                $size = isset($values->selected_size) ? $values->selected_size : null;
                $qty = isset($values->qty) ? intval($values->qty) : 0;

                if ($size === 'lg') {
                    $count += $qty;
                }
            }
            if ($count >= $minBig) {
                $total = 0;
                $decrease = 0;
                foreach ($carts as $rowID => $values) {
                    $size = isset($values->selected_size) ? $values->selected_size : null;
                    $price = isset($values->selected_price) ? intval($values->selected_price) : 0;
                    $qty = isset($values->qty) ? intval($values->qty) : 0;

                    if ($size === 'lg') {
                        $menu = $this->Menus->getDetail($values['id']);
                        if ($menu) {
                            $priceMd = (int)$menu->price_m;
                            $decrease += ($price - $priceMd) * $qty;
                        }
                    }

                    $total += $price * $qty; // !!!
                }

                return $total - $decrease;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }


    public function cartTotal($carts)
    {
        $total = 0;

        foreach ($carts as &$values) {
            $price = (int)$values->selected_price;
            $qty = (int)$values->qty;

            if (!empty($values->toppings)) {
                foreach ($values->toppings as $topping) {
                    if ($topping->qty > 0) {
                        $price += $topping->price * $topping->qty;
                    }
                }
            }

            $total += $price * $qty;
        }

        return $total;
    }
}