<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branches_mdel extends MY_Model {
    public $table = 'branches';
    private $default_lat = '12.2421914';
    private $default_lng = '109.19642599999997';
    public function __construct() {
        parent::__construct();
    }
    public function getList($wheres = array(), $options = array()) {
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            $this->load->model('Regions_mdel');
            $this->load->model('Users_mdel');
            foreach ($danhsach as &$value) {
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
                $region = $this->Regions_mdel->getDetail($value->region_id);
                $value->regionName = isset($region->id)?$region->name:'';
                $quanly = $this->Users_mdel->getNameList(array('role' => 'branch', 'role_tbl_id' => $value->id), array('getAll' => true));
                $value->quanly = implode(', ', $quanly);
            }
        }
        return $danhsach;
    }
    public function getDetail($id, $selector = '*', $escape = true) {
        $result = parent::getDetail($id, $selector, $escape);
        if (isset($result->id)) {
            $this->load->model('Regions_mdel');
            $result->status = (int)$result->status;
            $result->status_class = $this->orderStatusClasses[$result->status];
            $result->status_text = $this->orderStatuses[$result->status];
            $region = $this->Regions_mdel->getDetail($result->region_id);
            $result->regionName = isset($region->id)?$region->name:'';
        }
        return $result;
    }
    public function danhsach($data, $thongtin) {
        $id = @$data->id;
        $region = $regions = array();
        if ($id) {
            $region = parent::getDetail($id);
        }
        $this->load->model('Users_mdel');
        $this->load->model('Regions_mdel');
        $tmpbranches = $tmpregions = $branches = array();
        $regions = $this->Regions_mdel->getList4Branch(array(), array('getAll' => true));

        $tmpregions = $this->Regions_mdel->getList(array(), array('getAll' => true));
        $tmpbranches = $this->getList(array(), array('getAll' => true));
        $user = $this->Users_mdel->getDetail($thongtin->uid);
        $user_role = $user->role;
        if ($user_role=='admin') {
            if (count($tmpbranches)) {
                foreach ($tmpbranches as $value) {
                    $branches[] = $value->id;
                }
            }
        } elseif ($user_role=='supregion') {
            if (count($tmpregions)) {
                foreach ($tmpregions as $value) {
                    if ($value->parent_id==$user->role_tbl_id) {
                        if (count($tmpbranches)) {
                            foreach ($tmpbranches as $value2) {
                                if ($value2->region_id==$value->id) {
                                    $branches[] = $value2->id;
                                }
                            }
                        }
                    }
                }
            }
        } elseif ($user_role=='region') {
            if (count($tmpbranches)) {
                foreach ($tmpbranches as $value) {
                    if ($value->region_id==$user->role_tbl_id) {
                        $branches[] = $value->id;
                    }
                }
            }
        }
        if (!count($branches)) {
            $branches[] = 0;
        }

        return array('data' => $this->getList(array('id' => $branches), array('getAll' => true, 'order_by' => array('status' => 'desc'))), 'branchInfo' => $region, 'regions' => $regions);
    }
    public function chuanbi($data, $thongtin) {
        $this->load->model('Regions_mdel');
        $regions = $this->Regions_mdel->getList4Branch(array('status' => 1), array('getAll' => true));
        $region = array(
            'name' => '',
            'address' => '',
            'geo_latitude' => $this->default_lat,//'12.2421914',
            'geo_longitude' => $this->default_lng,//'109.19642599999997',
            'region_id' => '0',
            'status' => '1'
        );
        return array('regions' => $regions, 'branchInfo' => $region);
    }
    public function luutru($data = array(), $thongtin) {
        $regionInfo = @$data->thongtin;
        $region = array(
            'name' => $regionInfo->name,
            'region_id' => $regionInfo->region_id,
            'address' => $regionInfo->address,
            'geo_latitude' => $regionInfo->geo_latitude,
            'geo_longitude' => $regionInfo->geo_longitude,
            'status' => (int)$regionInfo->status
        );
        if (isset($regionInfo->id)) {
            $region['id'] = $regionInfo->id;
            $id = parent::edit($region);
            if ($id) {
                $id = $region['id'];
            }
        } else {
            $id = parent::create($region);
        }
        return $id;
    }
    public function capnhat($data = array(), $thongtin) {
        $data = (array)$data;
        $id = parent::edit($data);
        if ($id) {
            $id = $data['id'];
        }
        return $id;
    }
}