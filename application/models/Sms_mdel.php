<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms_mdel extends MY_Model {
    public $table = 'sms';
    public function __construct() {
        parent::__construct();
    }
    public function getList($wheres = array(), $options = array()) {
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            foreach ($danhsach as &$value) {
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
            }
        }
        return $danhsach;
    }
    public function getDetail($id, $selector = '*', $escape = true) {
        $result = parent::getDetail($id, $selector, $escape);
        if (isset($result->id)) {
            $result->status = (int)$result->status;
            $result->status_class = $this->orderStatusClasses[$result->status];
            $result->status_text = $this->orderStatuses[$result->status];
        }
        return $result;
    }
    public function danhsach($data, $thongtin) {
        $result = [];
        $result = parent::getList(array(), array('limit' => $data->numPerPage, 'page' => $data->page, 'order_by' => array('status' => 'asc', 'id' => 'desc')));
        $batdau = ($result['paging']['page'] - 1) * $data->numPerPage;
        $ketthuc = $batdau + $data->numPerPage;
        $ketthuc = $ketthuc>$result['paging']['total']?$result['paging']['total']:$ketthuc;
        if (count($result['records'])) {
            foreach ($result['records'] as &$value) {
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
            }
        }
        return array('data' => $result['records'], 'totalItems' => $result['paging']['total'], 'currentPage' => $result['paging']['page'], 'begin' => $batdau + 1, 'end' => $ketthuc);
    }
}