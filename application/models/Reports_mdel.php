<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_mdel extends MY_Model {
    public $table = 'order_delivery_histories';
    public function __construct() {
        parent::__construct();
    }
    public function danhsachNhanviengiaotrongngay($userid, $ngayhientai) {
        $tmp = $tmp2 = [];
        $this->load->model(array('Users_mdel', 'Regions_mdel', 'Branches_mdel'));
        $user = $this->Users_mdel->getDetail($userid);
        $user_role = $user->role;
        $regions = $this->Regions_mdel->getList(array('status' => 1), array('getAll' => true));
        $branches = $this->Branches_mdel->getList(array('status' => 1), array('getAll' => true));
        if ($user_role=='admin') {
            if (count($branches)) {
                foreach ($branches as $value) {
                    $tmp2[] = array(
                        'id' => $value->id,
                        'name' => $value->name
                    );
                }
            }
        } elseif ($user_role=='supregion') {
            if (count($regions)) {
                foreach ($regions as $value) {
                    if ($value->parent_id==$user->role_tbl_id) {
                        if (count($branches)) {
                            foreach ($branches as $value2) {
                                if ($value2->region_id==$value->id) {
                                    $tmp2[] = array(
                                        'id' => $value2->id,
                                        'name' => $value2->name
                                    );
                                }
                            }
                        }
                    }
                }
            }
        } elseif ($user_role=='branch') {
            if (count($branches)) {
                foreach ($branches as $value) {
                    if ($value->id==$user->role_tbl_id) {
                        $tmp2[] = array(
                            'id' => $value->id,
                            'name' => $value->name
                        );
                    }
                }
            }
        } elseif ($user_role=='region') {
            if (count($branches)) {
                foreach ($branches as $value) {
                    if ($value->region_id==$user->role_tbl_id) {
                        $tmp2[] = array(
                            'id' => $value->id,
                            'name' => $value->name
                        );
                    }
                }
            }
        }
        if (count($tmp2)) {
            foreach ($tmp2 as &$value) {
                $tmp3 = $this->danhsachNhanviengiaoChinhanhtrongngay($value['id'], $ngayhientai);
                if (count($tmp3)) {
                    foreach ($tmp3 as &$value2) {
                        $value2['chinhanh'] = $value['name'];
                    }
                    $tmp = array_merge($tmp, $tmp3);
                }
                $query = $this->db->select('sum(cart_total) as total, sum(CASE WHEN status = 3 THEN cart_total END) as tongtiengiu, sum(CASE WHEN status = 4 THEN cart_total END) as tongtienthu, count(id) as tongdonhang, count(CASE WHEN status = 0 THEN 1 END) as tongdangcho, count(CASE WHEN status = 1 THEN 1 END) as tongdangxuly, count(CASE WHEN status = 2 THEN 1 END) as tongdanggiao, count(CASE WHEN status = 3 THEN 1 END) as tongdagiao, count(CASE WHEN status = 4 THEN 1 END) as tonghoantat', false)->where(array('branch_id' => $value['id'], 'DATE(time_created)' => $ngayhientai, 'status <' => 5))->get('orders')->row();
                $value['sodonhang'] = $query->tongdonhang;
                $value['tongdangcho'] = $query->tongdangcho;
                $value['tongdangxuly'] = $query->tongdangxuly;
                $value['tongdanggiao'] = $query->tongdanggiao;
                $value['tongdagiao'] = $query->tongdagiao;
                $value['tonghoantat'] = $query->tonghoantat;
                $value['total'] = (int)$query->total;
                $value['tongtiengiu'] = (int)$query->tongtiengiu;
                $value['tongtienthu'] = (int)$query->tongtienthu;
                $value['thongtin'] = $tmp3;
            }
        }
        return array($tmp, $tmp2);
    }
    public function danhsachNhanviengiaoChinhanhtrongngay($branchid, $ngayhientai) {
        $tmp = $tmp2 = [];
        $this->load->model(array('Users_mdel', 'Orders_mdel', 'Order_collection_histories_mdel'));
        $employees = $this->Users_mdel->getList(array('role_tbl_id' => $branchid, 'role' => 'members'), array('getAll' => true));
        if (count($employees)) {
            $tmpemployees = array();
            foreach ($employees as $value) {
                $tmpemployees[] = $value->id;
            }
            $danhsach = parent::getList(array('employee_id' => $tmpemployees, 'DATE(time_created)' => $ngayhientai, 'status <' => 5), array('getAll' => true));
            if (count($danhsach)) {
                foreach ($danhsach as $value) {
                    if (!in_array($value->employee_id, $tmp2)) {
                        $tmp2[] = $value->employee_id;
                        $nhanviengiao = $this->Users_mdel->getDetail($value->employee_id);
                        $order = $this->Orders_mdel->getDetail($value->order_id);
                        $tongtien = 0;
                        if ($order->status==3) {
                            $tongtien = $order->cart_total;
                        }
                        $tongtienthu = $this->Order_collection_histories_mdel->getList(array('DATE(ngaythu)' => $ngayhientai, 'shipper_id' => $value->employee_id), array('getAll' => true, 'order_by' => array('id' => 'desc')));
                        if (count($tongtienthu)) {
                            foreach ($tongtienthu as &$value2) {
                                $value2->ngaythu = strtotime($value2->time_created);//strtotime($value2->ngaythu);
                                $nguoithu = $this->Users_mdel->getDetail($value2->user_id);
                                $value2->nguoithu = (isset($nguoithu->id)?$nguoithu->last_name:'');
                            }
                        }
                        $tmp3 = array(
                            'employee_id' => $value->employee_id,
                            'fullname' => $nhanviengiao->first_name.' '.$nhanviengiao->last_name,
                            'lastname' => $nhanviengiao->last_name,
                            'phone' => $nhanviengiao->phone,
                            'sodonhang' => 1,
                            'tongsomon' => $order->cart_total_items,
                            'total' => $value->cart_subtotal,
                            'danggiao' => $value->status==2?1:0,
                            'dagiao' => $value->status==3?1:0,
                            'duochoantat' => $value->status==3?true:false,
                            'hoantat' => $value->status==4?1:0,
                            'tongtiengiu' => $tongtien,
                            'danhsachthu' => $tongtienthu,
                            'orders' => array($order)
                        );
                        $tmp[$value->employee_id] = $tmp3;
                    } else {
                        $order = $this->Orders_mdel->getDetail($value->order_id);
                        $tmp3 = $tmp[$value->employee_id];
                        $tmp3['sodonhang'] = $tmp3['sodonhang'] + 1;
                        $tmp3['tongsomon'] = $tmp3['tongsomon'] + $order->cart_total_items;
                        $tmp3['total'] = $tmp3['total'] + $value->cart_subtotal;
                        $tongtien = 0;
                        if ($order->status==3) {
                            $tongtien = $order->cart_total;
                        }
                        $tmp3['tongtiengiu'] = $tmp3['tongtiengiu'] + $tongtien;
                        if ($value->status==2) {
                            $tmp3['danggiao'] = $tmp3['danggiao'] + 1;
                        } elseif ($value->status==3) {
                            $tmp3['dagiao'] = $tmp3['dagiao'] + 1;
                            $tmp3['duochoantat'] = true;
                        } elseif ($value->status==4) {
                            $tmp3['hoantat'] = $tmp3['hoantat'] + 1;
                        }
                        $tmp3['orders'][] = $order;
                        $tmp[$value->employee_id] = $tmp3;
                    }
                }
            }
        }
        return $tmp;
    }
}