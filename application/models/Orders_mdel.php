<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders_mdel extends MY_Model {
    public $table = 'orders';
    public $orderStatuses = array(
        0 => 'Đang chờ',
        1 => 'Đang xử lý',
        2 => 'Đang giao',
        3 => 'Đã giao',
        4 => 'Hoàn tất',
        5 => 'Hủy',
        -1 => 'Đang sửa'
    );
    public $orderStatusClasses = array(
        0 => 'moitao',
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => '',
        -1 => 'dangsua'
    );
    public $menuSizes = array(
        'sm' => 12,
        'md' => 16,
        'lg' => 22
    );
    public $mySchedules = array(
        0 => 'Sớm nhất có thể trước',
        1 => 'Đúng vào lúc',
        2 => 'Trong vòng'
    );
    public $ghichuthuonggap2 = array(
        array(
            'id' => 1,
            'name' => 'ĐÃ THANH TOÁN'
        ),
        array(
            'id' => 2,
            'name' => 'THỐI TIỀN 500K'
        )
    );
    public $ghichuthuonggap = array(
        array(
            'id' => 1,
            'nhom' => 'dong1',
            'name' => 'ÍT ĐÁ'
        ),
        array(
            'id' => 12,
            'nhom' => 'dong1',
            'name' => 'ÍT ĐÁ NHẤT CÓ THỂ'
        ),
        array(
            'id' => 2,
            'nhom' => 'dong1',
            'name' => 'KHÔNG ĐÁ'
        ),
        array(
            'id' => 3,
            'nhom' => 'dong1',
            'name' => 'ĐÁ RIÊNG'
        ),
        array(
            'id' => 4,
            'nhom' => 'dong2',
            'name' => '30% ĐƯỜNG'
        ),
        array(
            'id' => 5,
            'nhom' => 'dong2',
            'name' => '50% ĐƯỜNG'
        ),
        array(
            'id' => 6,
            'nhom' => 'dong2',
            'name' => '70% ĐƯỜNG'
        ),
        array(
            'id' => 7,
            'nhom' => 'dong2',
            'name' => 'KHÔNG ĐƯỜNG'
        ),
        array(
            'id' => 8,
            'nhom' => 'dong3',
            'name' => 'PHA NGỌT'
        ),
        array(
            'id' => 9,
            'nhom' => 'dong3',
            'name' => 'PHA ĐẮNG'
        ),
        array(
            'id' => 10,
            'nhom' => 'dong3',
            'name' => 'PHA CHUA'
        ),
        array(
            'id' => 11,
            'nhom' => 'dong3',
            'name' => 'PHA ĐẬM VỊ'
        )
    );
    public function __construct() {
        parent::__construct();
    }
    public function getList2($wheres = array(), $options = array()) {
        return parent::getList($wheres, $options);
    }
    public function getList($wheres = array(), $options = array()) {
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            $this->load->model('Users_mdel');
            $this->load->model('Menus_mdel');
            $this->load->model('Toppings_mdel');
            $this->load->model('Order_comments_mdel');
            $this->load->model('Order_histories_mdel');
            $this->load->model('Branches_mdel');
            foreach ($danhsach as &$value) {
                $value->print_count = (int)$value->print_count;
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
                $value->time_created = strtotime($value->time_created);
                $value->time_created_text = date('d/m/Y, H:i', $value->time_created);
                $value->points = (int) $value->points;
                $value->points_used = (int) $value->points_used;
                /*if ($value->shipping_on=='') {
                    $value->shipping_on = 'Sớm nhất có thể ('.date('H:i', $value->time_created + 1950).')';
                }*/
                $value->shipping_on_text = $this->mySchedules[$value->shipping_on_loai].' '.date('H:i', strtotime($value->shipping_on)).($value->shipping_on_loai==2?(' - '.date('H:i', strtotime($value->shipping_on2))):'');
                $value->chuyenhoadon = false;
                $value->chuyenlaihoadon = false;
                if ($value->original_branch_id==0&&($value->status==0||$value->status==1)) {
                    $value->chuyenhoadon = true;
                } elseif ($value->original_branch_id>0&&($value->status==0||$value->status==1)) {
                    $value->chuyenlaihoadon = true;
                }
                if ($value->original_branch_id>0) {
                    $value->status_class = 'chuyenchinhanh';
                } elseif ($value->original_branch_id==-1) {
                    $value->status_class = 'chuyenlaichinhanh';
                }
                if ($value->editted) {
                    $value->status_class = 'dachinhsua';
                }
                //$dukien = strtotime($value->shipping_on);
                $thoigiantao = $value->time_created;
                //$value->shipping_on = date('Y-m-d ', $thoigiantao).date('H:i:s', $dukien);
                $value->shipping_on = (int)strtotime($value->shipping_on);
                $sodu = floor(($value->shipping_on - $thoigiantao)*70/100);
                $dukien2 = $thoigiantao + $sodu;
                $value->thoigiangiaohang = array(
                    'year' => date('Y', $dukien2),
                    'month' => date('m', $dukien2),
                    'day' => date('d', $dukien2),
                    'hour' => date('H', $dukien2),
                    'minute' => date('i', $dukien2),
                );
                //$value->shipping_on = (int)strtotime(date('Y-m-d H:i:00', $value->shipping_on));
                $value->time_created2 = (int)$value->time_created;
                //
                $branch = $this->Branches_mdel->getDetail($value->branch_id);
                $value->branchname = isset($branch->id)?$branch->name:'';
                $shipper = $this->Users_mdel->getDetail($value->shipper_id);
                $value->shippername = (isset($shipper->id)?($shipper->first_name.' '.$shipper->last_name):'');
                $shippers = $this->Users_mdel->getList(array('role_tbl' => 'branch', 'role_tbl_id' => $value->branch_id, 'role' => 'members', 'status' => 1), array('getAll' => true));
                if ($value->shipper_id) {
                    $exist = -1;
                    if (count($shippers)) {
                        foreach ($shippers as $key => $tmpshipper) {
                            if ($tmpshipper->id == $value->shipper_id) {
                                $exist = $key;
                            }
                        }
                    }
                    if ($exist==-1) {
                        $shippers[] = $shipper;
                    }
                }
                $value->shippers = $shippers;
                $orders = json_decode($value->cart_data);
                if (count($orders)) {
                    foreach ($orders as &$value2) {
                        $menu = $this->Menus_mdel->getDetail($value2->id);
                        $value2->name = $menu->name;
                        $toppings = $value2->toppings;
                        if (count($toppings)) {
                            foreach ($toppings as &$value3) {
                                $topping = $this->Toppings_mdel->getDetail($value3->id);
                                $value3->name = $topping->name;
                            }
                            $value2->toppings = $toppings;
                        }
                    }
                }
                $value->orders = $orders;
                $value->ghichuthuonggap = $this->ghichuthuonggap;
                $value->ghichuthuonggap2 = $this->ghichuthuonggap2;
                $value->thuonggapnote = json_decode($value->thuonggapnote);
                $value->comments = $this->Order_comments_mdel->getList(array('order_id' => $value->id), array('getAll' => true, 'order_by' => array('time_created' => 'desc')));
                $value->histories = $this->Order_histories_mdel->getList(array('order_id' => $value->id), array('getAll' => true, 'order_by' => array('time_created' => 'desc')));
            }
        }
        return $danhsach;
    }
    public function getDetail($id, $selector = '*', $escape = true) {
        $result = parent::getDetail($id, $selector, $escape);
        if (isset($result->id)) {
            $this->load->model('Users_mdel');
            $this->load->model('Menus_mdel');
            $this->load->model('Toppings_mdel');
            $this->load->model('Order_comments_mdel');
            $this->load->model('Order_histories_mdel');
            $this->load->model('Branches_mdel');
            $result->print_count = (int)$result->print_count;
            $result->status = (int)$result->status;
            $result->status_class = $this->orderStatusClasses[$result->status];
            $result->status_text = $this->orderStatuses[$result->status];
            $result->time_created = strtotime($result->time_created);
            $result->time_created_text = date('d/m/Y, H:i', $result->time_created);
            $result->time_created_text2 = date('H:i', $result->time_created);

            $result->points = (int) $result->points;
            $result->points_used = (int) $result->points_used;
            
            /*if ($result->shipping_on=='') {
                $result->shipping_on = 'Sớm nhất có thể ('.date('H:i', $result->time_created + 1950).')';
            }*/
            $result->shipping_on_text = $this->mySchedules[$result->shipping_on_loai].' '.date('H:i', strtotime($result->shipping_on)).($result->shipping_on_loai==2?(' - '.date('H:i', strtotime($result->shipping_on2))):'');
            $result->chuyenhoadon = false;
            $result->chuyenlaihoadon = false;
            if ($result->original_branch_id==0&&($result->status==0||$result->status==1)) {
                $result->chuyenhoadon = true;
            } elseif ($result->original_branch_id>0&&($result->status==0||$result->status==1)) {
                $result->chuyenlaihoadon = true;
            }
            if ($result->original_branch_id>0) {
                $result->status_class = 'chuyenchinhanh';
            } elseif ($result->original_branch_id==-1) {
                $result->status_class = 'chuyenlaichinhanh';
            }
            if ($result->editted) {
                $result->status_class = 'dachinhsua';
            }
            //$dukien = strtotime($result->shipping_on);
            $thoigiantao = $result->time_created;
            //$result->shipping_on = date('Y-m-d ', $thoigiantao).date('H:i:s', $dukien);
            $result->shipping_on = (int)strtotime($result->shipping_on);
            $sodu = floor(($result->shipping_on - $thoigiantao)*70/100);
            $dukien2 = $thoigiantao + $sodu;
            $result->thoigiangiaohang = array(
                'year' => date('Y', $dukien2),
                'month' => date('m', $dukien2),
                'day' => date('d', $dukien2),
                'hour' => date('H', $dukien2),
                'minute' => date('i', $dukien2),
            );
            //$result->shipping_on = (int)strtotime(date('Y-m-d H:i:00', $result->shipping_on));
            $result->time_created2 = (int)$result->time_created;
            //
            $branch = $this->Branches_mdel->getDetail($result->branch_id);
            $result->branchname = isset($branch->id)?$branch->name:'';
            $shipper = $this->Users_mdel->getDetail($result->shipper_id);
            $result->shippername = (isset($shipper->id)?($shipper->first_name.' '.$shipper->last_name):'');
            $shippers = $this->Users_mdel->getList(array('role_tbl' => 'branch', 'role_tbl_id' => $result->branch_id, 'role' => 'members', 'status' => 1), array('getAll' => true));
            if ($result->shipper_id) {
                $exist = -1;
                if (count($shippers)) {
                    foreach ($shippers as $key => $tmpshipper) {
                        if ($tmpshipper->id == $result->shipper_id) {
                            $exist = $key;
                        }
                    }
                }
                if ($exist==-1) {
                    $shippers[] = $shipper;
                }
            }
            $result->shippers = $shippers;
            $orders = json_decode($result->cart_data);
            if (count($orders)) {
                foreach ($orders as &$value2) {
                    $menu = $this->Menus_mdel->getDetail($value2->id);
                    $value2->name = $menu->name;
                    $toppings = $value2->toppings;
                    if (count($toppings)) {
                        foreach ($toppings as &$value3) {
                            $topping = $this->Toppings_mdel->getDetail($value3->id);
                            $value3->name = $topping->name;
                        }
                        $value2->toppings = $toppings;
                    }
                }
            }
            $result->orders = $orders;
            $result->ghichuthuonggap = $this->ghichuthuonggap;
            $result->ghichuthuonggap2 = $this->ghichuthuonggap2;
            $result->thuonggapnote = json_decode($result->thuonggapnote);
            $result->comments = $this->Order_comments_mdel->getList(array('order_id' => $result->id), array('getAll' => true, 'order_by' => array('time_created' => 'desc')));
            $result->histories = $this->Order_histories_mdel->getList(array('order_id' => $result->id), array('getAll' => true, 'order_by' => array('time_created' => 'desc')));


            $customer = $this->Users_mdel->getDetail($result->customer_id);
            if ($customer) {
                $result->user_points = (int) $customer->points;
                $result->user_points_used = (int) $customer->points_used;
            } 

        }
        return $result;
    }
    public function danhsach($data, $thongtin) {
        $result = [];
        $this->load->model('Users_mdel');
        $this->load->model('Branches_mdel');
        $this->load->model('Regions_mdel');
        $user = $this->Users_mdel->getDetail($thongtin->uid);
        $user_role = $user->role;
        $tmp = $branchList = [];
        $regions = $this->Regions_mdel->getList(array('status' => 1), array('getAll' => true));
        $branches = $this->Branches_mdel->getList(array('status' => 1), array('getAll' => true));
        $branchList = $branchList2 = [];//$branches;
        if ($user_role=='admin') {
            if (count($branches)) {
                foreach ($branches as $value) {
                    $tmp[] = $value->id;
                    $branchList[] = $value;
                    $branchList2[] = $value;
                }
            }
        } elseif ($user_role=='supregion') {
            if (count($regions)) {
                foreach ($regions as $value) {
                    if ($value->parent_id==$user->role_tbl_id) {
                        if (count($branches)) {
                            foreach ($branches as $value2) {
                                if ($value2->region_id==$value->id) {
                                    $tmp[] = $value2->id;
                                    $branchList[] = $value2;
                                    $branchList2[] = $value2;
                                }
                            }
                        }
                    }
                }
            }
        } elseif ($user_role=='branch') {
            $tmp[] = $user->role_tbl_id;
            if (count($branches)) {
                foreach ($branches as $value) {
                    if ($value->id == $user->role_tbl_id) {
                        $mybranch = $value;
                        $branchList2[] = $value;
                    }
                }
            }
            if (count($regions)) {
                foreach ($regions as $value) {
                    if ($value->id==$mybranch->region_id) {
                        if (count($branches)) {
                            foreach ($branches as $value2) {
                                if ($value2->region_id==$value->id) {
                                    //$tmp[] = $value2->id;
                                    $branchList[] = $value2;
                                }
                            }
                        }
                    }
                }
            }
        } elseif ($user_role=='region'||$user_role=='salesman') {
            if (count($branches)) {
                foreach ($branches as $value) {
                    if ($value->region_id==$user->role_tbl_id) {
                        $tmp[] = $value->id;
                        $branchList[] = $value;
                        $branchList2[] = $value;
                    }
                }
            }
        }
        $dieukien = array('branch_id' => $tmp);
        if (isset($data->dieukien)) {
            $dieukientim = $data->dieukien;
            if ((int)$dieukientim->branch) {
                $dieukien['branch_id'] = (int)$dieukientim->branch;
            }
            if ($dieukientim->status!='') {
                $dieukien['status'] = $dieukientim->status;
            }
            if ($dieukientim->idv!='') {
                $dieukien[] = '(idv like "%'.$dieukientim->idv.'%" or order_number like "%'.$dieukientim->idv.'%")';
            }
            if ($dieukientim->name!='') {
                $dieukien[] = 'customer_name like "%'.$dieukientim->name.'%"';
            }
            if ($dieukientim->phone!='') {
                $dieukien[] = '(customer_phone like "%'.$dieukientim->phone.'%" or shipping_floor like "%'.$dieukientim->phone.'%" or shipping_address like "%'.$dieukientim->phone.'%")';
            }
        }
        $result = parent::getList($dieukien, array('limit' => $data->numPerPage, 'page' => $data->page, 'order_by' => array('status' => 'asc', 'shipping_on' => 'desc', 'time_created' => 'desc', 'id' => 'desc')));
        $batdau = ($result['paging']['page'] - 1) * $data->numPerPage;
        $ketthuc = $batdau + $data->numPerPage;
        $ketthuc = $ketthuc>$result['paging']['total']?$result['paging']['total']:$ketthuc;
        if (count($result['records'])) {
            $this->load->model('Menus_mdel');
            $this->load->model('Toppings_mdel');
            $this->load->model('Order_comments_mdel');
            $this->load->model('Order_histories_mdel');

            $customers = [];
            foreach ($result['records'] as &$value) {
                if (empty($customers[$value->customer_id])) {
                    $customer = $this->Users_mdel->getDetail($value->customer_id);
                    if ($customer) {
                        $customers[$value->customer_id] = $customer;
                    }
                } else {
                    $customer = $customers[$value->customer_id];
                }

                if ($customer) {
                    $value->user_points = (int) $customer->points;
                    $value->user_points_used = (int) $customer->points_used;
                }

                $value->print_count = (int)$value->print_count;
                $value->status = (int)$value->status;
                $value->status_class = $this->orderStatusClasses[$value->status];
                $value->status_text = $this->orderStatuses[$value->status];
                $value->time_created = strtotime($value->time_created);
                $value->time_created_text = date('d/m/Y, H:i', $value->time_created);
                $value->points = (int) $value->points;
                $value->points_used = (int) $value->points_used;
                /*if ($value->shipping_on=='') {
                    $value->shipping_on = 'Sớm nhất có thể ('.date('H:i', $value->time_created + 1950).')';
                }*/
                $value->shipping_on_text = $this->mySchedules[$value->shipping_on_loai].' '.date('H:i', strtotime($value->shipping_on)).($value->shipping_on_loai==2?(' - '.date('H:i', strtotime($value->shipping_on2))):'');
                $value->chuyenhoadon = false;
                $value->chuyenlaihoadon = false;
                if ($value->original_branch_id==0&&($value->status==0||$value->status==1)) {
                    $value->chuyenhoadon = true;
                } elseif ($value->original_branch_id>0&&($value->status==0||$value->status==1)) {
                    $value->chuyenlaihoadon = true;
                }
                if ($value->original_branch_id>0) {
                    $value->status_class = 'chuyenchinhanh';
                } elseif ($value->original_branch_id==-1) {
                    $value->status_class = 'chuyenlaichinhanh';
                }
                if ($value->editted) {
                    $value->status_class = 'dachinhsua';
                }
                //$value->duocthaotac = false;
                //$dukien = strtotime($value->shipping_on);
                $thoigiantao = $value->time_created;
                //$value->shipping_on = date('Y-m-d ', $thoigiantao).date('H:i:s', $dukien);
                $value->shipping_on = (int)strtotime($value->shipping_on);
                $sodu = floor(($value->shipping_on - $thoigiantao)*70/100);
                $dukien2 = $thoigiantao + $sodu;
                $value->thoigiangiaohang = array(
                    'year' => date('Y', $dukien2),
                    'month' => date('m', $dukien2),
                    'day' => date('d', $dukien2),
                    'hour' => date('H', $dukien2),
                    'minute' => date('i', $dukien2),
                );
                //$value->shipping_on = (int)strtotime(date('Y-m-d H:i:00', $value->shipping_on));
                $value->time_created2 = (int)$value->time_created;
                //
                $branch = $this->Branches_mdel->getDetail($value->branch_id);
                $value->branchname = isset($branch->id)?$branch->name:'';
                $shipper = $this->Users_mdel->getDetail($value->shipper_id);
                $value->shippername = (isset($shipper->id)?($shipper->first_name.' '.$shipper->last_name):'');
                $shippers = $this->Users_mdel->getList(array('role_tbl' => 'branch', 'role_tbl_id' => $value->branch_id, 'role' => 'members', 'status' => 1), array('getAll' => true));
                if ($value->shipper_id) {
                    $exist = -1;
                    if (count($shippers)) {
                        foreach ($shippers as $key => $tmpshipper) {
                            if ($tmpshipper->id == $value->shipper_id) {
                                $exist = $key;
                            }
                        }
                    }
                    if ($exist==-1) {
                        $shippers[] = $shipper;
                    }
                }
                $value->shippers = $shippers;
                $orders = json_decode($value->cart_data);
                if (count($orders)) {
                    foreach ($orders as &$value2) {
                        $menu = $this->Menus_mdel->getDetail($value2->id);
                        $value2->name = $menu->name;
                        $toppings = $value2->toppings;
                        if (count($toppings)) {
                            foreach ($toppings as &$value3) {
                                $topping = $this->Toppings_mdel->getDetail($value3->id);
                                $value3->name = $topping->name;
                            }
                            $value2->toppings = $toppings;
                        }
                    }
                }
                $value->orders = $orders;
                $value->ghichuthuonggap = $this->ghichuthuonggap;
                $value->ghichuthuonggap2 = $this->ghichuthuonggap2;
                $value->thuonggapnote = json_decode($value->thuonggapnote);
                $value->comments = $this->Order_comments_mdel->getList(array('order_id' => $value->id), array('getAll' => true, 'order_by' => array('time_created' => 'desc')));
                $value->histories = $this->Order_histories_mdel->getList(array('order_id' => $value->id), array('getAll' => true, 'order_by' => array('time_created' => 'desc')));
            }
        }
        return array('branchList' => $branchList, 'branchList2' => $branchList2, 'data' => $result['records'], 'totalItems' => $result['paging']['total'], 'currentPage' => $result['paging']['page'], 'begin' => $batdau + 1, 'end' => $ketthuc);
    }
    public function chuanbi($data, $thongtin) {
        $result = $this->mauchung();
        if (@$data->id) {
            $order = parent::getDetail(@$data->id);
            if (isset($order->id)) {
                /*if ($order->status==0 || $order->status==1) {
                    $order->status = -1;
                    parent::edit(array('id' => $order->id, 'status' => -1));
                }*/
                $orders = json_decode($order->cart_data);
                if (count($orders)) {
                    $this->load->model('Menus_mdel');
                    $this->load->model('Toppings_mdel');
                    foreach ($orders as $key => &$value) {
                        $menu = $this->Menus_mdel->getDetail($value->id);
                        if (isset($menu->id)) {
                            $value->name = $menu->name;
                            //$value->hinhanh = isset($menu->hinhanh)?$menu->hinhanh:'http://downloadicons.net/sites/default/files/product-icon-27962.png';
                            $tmptoppings = $menu->toppings;
                            $tmptoppings = explode(',', $tmptoppings);
                            $toppings = $this->Toppings_mdel->getList(array('id' => $tmptoppings), array('getAll' => true, 'order_by' => array('id' => 'asc')));
                            if (count($toppings)) {
                                foreach ($toppings as &$value2) {
                                    $key2 = array_search($value2->id, $this->array_column2($value->toppings, 'id'));
                                    if ($key2 !== false) {
                                        $value2->qty = $value->toppings[$key2]->qty;
                                        $value2->price = $value->toppings[$key2]->price;
                                    } else {
                                        $value2->qty = 0;
                                    }
                                }
                            }
                            $value->toppings = $toppings;
                            $value->size = array(
                                'sm' => array(
                                    'label' => $this->menuSizes['sm'].'oz',
                                    'price' => $menu->price
                                ),
                                'md' => array(
                                    // 'label' => $this->menuSizes['md'].'oz',
                                    'label' => 'M',
                                    'price' => $menu->price_m
                                ),
                                'lg' => array(
                                    // 'label' => $this->menuSizes['lg'].'oz',
                                    'label' => 'L',
                                    'price' => $menu->price_l
                                )
                            );
                            $value->ghichuthuonggap = $this->ghichuthuonggap;
                        } else {
                            unset($orders[$key]);
                        }
                    }
                }
                $orderInfo = array(
                    'id' => (int)$order->id,
                    'idv' => $order->idv,
                    'order_number' => $order->order_number,
                    'edit_count' => $order->edit_count,
                    'branch_id' => $order->branch_id,
                    'status' => $order->status
                );
                $tmp = explode(' ', $order->customer_name);
                $lastname = array_pop($tmp);
                $firstname = implode(' ', $tmp);
                $customerInfo = array(
                    'first_name' => $firstname,
                    'last_name' => $lastname,
                    'phone' => $order->customer_phone,
                    'gender' => (int)$order->gender
                );
                $shippingInfo = array(
                    'floor' => $order->shipping_floor,
                    'address' => $order->shipping_address,
                    'branchid' => $order->branch_id,
                    'delivery_on' => strtotime($order->shipping_on),
                    'delivery_on2' => strtotime($order->shipping_on2),
                    'delivery_on_loai' => $order->shipping_on_loai,
                    'shipping_on_text' => $this->mySchedules[$order->shipping_on_loai].' '.date('H:i', strtotime($order->shipping_on)).($order->shipping_on_loai==2?(' - '.date('H:i', strtotime($order->shipping_on2))):''),
                    'note' => $order->note,
                    'discount_code' => $order->discount_code,
                    'thuonggapnote' => json_decode($order->thuonggapnote),
                    'points_used' => (int) $order->points_used
                );
                $result['orders'] = $orders;
                $result['orderInfo'] = $orderInfo;
                $result['customerInfo'] = $customerInfo;
                $result['shippingInfo'] = $shippingInfo;
            }
        }
        return $result;
    }
    private function array_column2($arr, $col) {
        $tmp = array();
        foreach ($arr as $value) {
            $tmp[] = $value->{$col};
        }
        return $tmp;
    }
    public function mauchung() {
        /*$schedules = [array(
            'id' => '',
            'name' => 'Sớm nhất có thể ('.date('H:i', strtotime(date('H:i:s')) + 1950).')'
        )];
        $openHour = strtotime('09:00:00');
        $closeHour = strtotime('20:00:00');
        $nextTime = $openHour;//strtotime(date('H:00:00')) + ceil(date('i')/15)*15*60;
        if ($nextTime < $openHour) {
            $nextTime = $openHour;
        }
        while ($nextTime < $closeHour) {
            $nextTime += 900;
            $schedules[] = array(
                'id' => date('H:i', $nextTime),
                'name' => date('H:i', $nextTime)
            );
        }*/
        $schedules = [
            array(
                'id' => '0',
                'name' => $this->mySchedules[0]//'Sớm nhất có thể trước'
            ),
            array(
                'id' => '1',
                'name' => $this->mySchedules[1]//'Đúng vào lúc'
            ),
            array(
                'id' => '2',
                'name' => $this->mySchedules[2]//'Đúng vào lúc'
            )
        ];
        $this->load->model('Branches_mdel');
        $this->load->model('Categories_mdel');
        $this->load->model('Menus_mdel');
        $this->load->model('Toppings_mdel');
        $this->load->model('Discounts_mdel');
        $categories = $this->Categories_mdel->getList(array(), array('getAll' => true, 'order_by' => array('id' => 'asc')));
        if (count($categories)) {
            foreach ($categories as &$value) {
                $menus = $this->Menus_mdel->getList(array('category_id' => $value->id, 'status' => 1), array('getAll' => true, 'order_by' => array('id' => 'asc')));
                if (count($menus)) {
                    foreach ($menus as &$value2) {
                        $tmptoppings = $value2->toppings;
                        $tmptoppings = explode(',', $tmptoppings);
                        $toppings = $this->Toppings_mdel->getList(array('id' => $tmptoppings), array('getAll' => true, 'order_by' => array('id' => 'asc')));
                        if (count($toppings)) {
                            foreach ($toppings as &$value3) {
                                $value3->qty = 0;
                            }
                        }
                        $value2->toppings = $toppings;
                        $value2->size = array(
                            'sm' => array(
                                'label' => $this->menuSizes['sm'].'oz',
                                'price' => $value2->price
                            ),
                            'md' => array(
                                // 'label' => $this->menuSizes['md'].'oz',
                                'label' => 'M',
                                'price' => $value2->price_m
                            ),
                            'lg' => array(
                                // 'label' => $this->menuSizes['lg'].'oz',
                                'label' => 'L',
                                'price' => $value2->price_l
                            )
                        );
                        $value2->qty = 1;
                        $value2->ghichuthuonggap = $this->ghichuthuonggap;
                        $value2->thuonggapnote = array();
                    }
                }
                $value->menus = $menus;
            }
        }
        $branches = $this->Branches_mdel->getList(array(), array('getAll' => true));
        $chuongtrinhkhuyenmai = $this->Discounts_mdel->getAvailable();
        return array('data' => $categories, 'ghichuthuonggap' => $this->ghichuthuonggap2, 'branches' => $branches, 'schedules' => $schedules, 'chuongtrinhkhuyenmai' => $chuongtrinhkhuyenmai);
    }


    private function trimData($order)
    {
        if (is_array($order)) {
            foreach ($order as $key => &$value) {
                if (!empty($value) && !is_null($value)) {
                    $value = trim($value);
                }
            }
        }

        return $order;
    }

    /**
    * Calcute number of straws and spoons for the order
    */
    public function calculate4StrawAndSpoon(&$cart, $applyToCart = false)
    {
        $include = [];
        $notes = [];
        $strawSizes = [
            1 => 'nhỏ',
            2 => 'vừa',
            3 => 'lớn'
        ];

        if (!empty($cart) && is_array($cart)) {
            // Load models
            $this->load->model('Menus_mdel', 'Menu');
            $this->load->model('Toppings_mdel', 'Toppings');

            // Loop cart items
            foreach ($cart as &$item) {
                $item = (object) $item;

                if (!empty($item->id)) {
                    // Get menu info
                    $menuInfo = $this->Menu->getDetail($item->id);
                    if ($menuInfo) {
                        $qty = (int) $item->qty;
                        $strawSize = (int) $menuInfo->use_straw;
                        $spoon = $menuInfo->use_spoon == 1 ? true : false;

                        // Loop topping items
                        if (!empty($item->toppings) && is_array($item->toppings)) {
                            foreach ($item->toppings as $topping) {
                                $topping = (object) $topping;

                                if (isset($topping->id)) {
                                    // Get topping info
                                    $toppingInfo = $this->Toppings->getDetail($topping->id);
                                    if ($toppingInfo) {
                                        // Get straw size for topping
                                        if ($toppingInfo->use_straw > $strawSize) {
                                            $strawSize = (int) $toppingInfo->use_straw;
                                        }

                                        // Get spoon for topping
                                        if ($toppingInfo->use_spoon == 1) {
                                            $spoon = true;
                                        }
                                    }
                                }
                            }
                        }

                        $_include = [];

                        // Calculate straw
                        if ($strawSize > 0 && isset($strawSizes[$strawSize])) {
                            if (empty($include['straws'][$strawSize])) {
                                $include['straws'][$strawSize] = 0;
                            }
                            $include['straws'][$strawSize] += $qty;

                            $_include['straws'][$strawSize] = $qty;
                        }

                        // Calculate spoon
                        if ($spoon) {
                            if (empty($include['spoons'])) {
                                $include['spoons'] = 0;
                            }
                            $include['spoons'] += $qty;

                            $_include['spoons'] = $qty;
                        }

                        // Update StrawAndSpoon into cart
                        if (!empty($_include) && $applyToCart) {
                            $item->note2 = $this->calculate4StrawAndSpoon_Human_Readable($_include);
                        }
                    }
                }
            }

            // Convert to human-readable
            $note = $this->calculate4StrawAndSpoon_Human_Readable($include);
            if ($note) {
                $include['note'] = $note;
            }


            return $include;
        }
    }

    private function calculate4StrawAndSpoon_Human_Readable($include)
    {
        $strawSizes = [
            1 => 'nhỏ',
            2 => 'vừa',
            3 => 'lớn'
        ];
        $notes = [];

        if (!empty($include['straws'])) {
            ksort($include['straws']);
            foreach ($include['straws'] as $strawKey => $strawQty) {
                $notes[] = 'Ống hút ' . $strawSizes[$strawKey] . ': ' . $strawQty;
            }
        }

        if (!empty($include['spoons'])) {
            $notes[] = 'Muỗng: ' . $include['spoons'];
        }

        if (!empty($notes)) {
            $include['note'] = implode('; ', $notes);
        }

        return $include['note'];
    }


    public function luutru($data = array(), $thongtin) {
        $orderInfo = @$data->order;
        $items = $data->items;
        $customer = $data->customer;
        $shipping = $data->shipping;
        $tmpcart = array();
        $cart_subtotal = $cart_total = $cart_total_items = $customer_id = 0;
        $this->load->model('Users_mdel');
        $tmpcustomer = $this->Users_mdel->getDetailByField('phone', $customer->phone);
        if (isset($tmpcustomer->id)) {
            $customer_id = $tmpcustomer->id;
        } else {
            $data2 = array(
                'username' => $customer->phone,
                'password' => $customer->phone,
                'role' => 'customer',
                'active' => 0,
                'first_name' => $customer->first_name,
                'last_name' => $customer->last_name,
                'phone' => $customer->phone,
                'gender' => $customer->gender
            );
            $customer_id = $this->Users_mdel->create($data2);
        }
        if (count($items)) {
            foreach ($items as $value) {
                $itemQty = (int) $value->qty;
                $tmp = array(
                    'id' => $value->id,
                    //'name' => $value->name,
                    'qty' => $itemQty,
                    'selected_label' => $value->selected_label,
                    'selected_price' => $value->selected_price,
                    'selected_size' => $value->selected_size,
                    'note' => @$value->note,
                    'thuonggapnote' => @$value->thuonggapnote,
                    'toppings' => array()
                );

                // Add price after appy discount
                if (!empty($value->priceAfter)) {
                    $tmp['priceAfter'] = (int) $value->priceAfter;
                }

                $cart_total_items += $itemQty;
                $cart_subtotal += ($itemQty * $value->selected_price);
                foreach ($value->toppings as $item) {
                    if ($item->qty) {
                        $cart_subtotal += (($item->qty * $item->price) * $itemQty);
                        unset($item->name, $item->description, $item->category_id, $item->slug, $item->price_m, $item->timeout, $item->time_created, $item->time_updated, $item->status);
                        $tmp['toppings'][] = $item;
                    }
                }
                $tmpcart[] = $tmp;
            }
        }
        $cart_total = $cart_subtotal;
        if ($shipping->discount_code) {
            //$cart_total = $cart_subtotal - (($cart_subtotal * 10) / 100);
            $this->load->model('Discounts_mdel');
            $tmpcart2 = json_encode($tmpcart);
            $tmpcart2 = json_decode($tmpcart2);
            $khuyenmai = $this->Discounts_mdel->apply($shipping->discount_code, $tmpcart2);
            if ($khuyenmai['data']) {
                $cart_total = $cart_subtotal - $khuyenmai['data'];
            }
        }
        $shipping_on = date('Y-m-d ').date('H:i:00', strtotime($shipping->delivery_on));
        $shipping_on2 = date('Y-m-d ').date('H:i:00', strtotime($shipping->delivery_on2));
        $calculate4StrawAndSpoon = $this->calculate4StrawAndSpoon($tmpcart, true);

        $order = array(
            'idv' => parent::getIdv(),
            'order_number' => $this->lastOrderNumber(),
            'time_created' => date('Y-m-d H:i:s'),
            'branch_id' => $shipping->branchid,
            'customer_id' => $customer_id,
            'customer_name' => $customer->first_name.' '.$customer->last_name,
            'customer_phone' => $customer->phone,
            'gender' => $customer->gender,
            'shipping_address' => $shipping->address,
            'shipping_floor' => $shipping->floor,
            'shipping_on_loai' => $shipping->delivery_on_loai,
            'shipping_on' => $shipping_on,
            'shipping_on2' => $shipping_on2,
            'note' => $shipping->note,
            'discount_code' => $shipping->discount_code,
            'cart_total' => $cart_total,//da tinh discount
            'cart_subtotal' => $cart_subtotal,//chua co discount
            'cart_total_items' => $cart_total_items,
            'cart_data' => json_encode($tmpcart),
            'cart_include' => empty($calculate4StrawAndSpoon) ? null : $calculate4StrawAndSpoon['note'],
            'thuonggapnote' => json_encode($shipping->thuonggapnote),
            'points_used' => empty($shipping->points_used) ? 0 : abs(intval($shipping->points_used)),
            'print_count' => 0,
            'time_updated' => '',
            'status' => 0
        );


        $order = $this->trimData($order);


        // Apply double for bonus points from 2017-06-16 to 2017-07-01
        $now = time();
        if ($now >= strtotime('2017-06-16') && $now < strtotime('2017-07-01')) {
            $order['points_x2'] = 1;
        }


        $order = $this->usePoint($order);

        $this->load->model('Order_histories_mdel');
        if (@$orderInfo->id) {
            $order['id'] = $orderInfo->id;
            unset($order['idv'], $order['order_number'], $order['time_created'], $order['print_count'], $order['status']);
            $thanhcong = parent::edit($order);
            $id = $orderInfo->id;
            if ($thanhcong) {
                $order['time_updated'] = date('Y-m-d H:i:s');
                $order['edit_count'] = $orderInfo->edit_count + 1;
                parent::edit(array(
                    'id' => $id,
                    'time_updated' => date('Y-m-d H:i:s'),
                    'edit_count' => $orderInfo->edit_count + 1,
                    'editted' => 1
                ));
                $noidung = 'Chỉnh sửa thông tin lần thứ '.$order['edit_count'].' lúc %s bởi %s';
                $this->Order_histories_mdel->create(array('order_id' => $id, 'user_id' => $thongtin->uid,  'time_created' => date('Y-m-d H:i:s'), 'action' => 'edit', 'noidung' => $noidung));
            }
        } else {
            $id = parent::create($order);
            if ($id) {
                $noidung = 'Khởi tạo đơn hàng lúc %s bởi %s';
                $this->Order_histories_mdel->create(array('order_id' => $id, 'user_id' => $thongtin->uid,  'time_created' => date('Y-m-d H:i:s'), 'action' => 'create', 'noidung' => $noidung));
                switch($order['shipping_on_loai']) {
                    case 0:
                        $loaigiogiao = 'SOM NHAT TRUOC '.date('H:i', strtotime($order['shipping_on']));
                        break;
                    case 1:
                        $loaigiogiao = 'DUNG VAO LUC '.date('H:i', strtotime($order['shipping_on']));
                        break;
                    case 2:
                        $loaigiogiao = 'TU '.date('H:i', strtotime($order['shipping_on'])).' DEN '.date('H:i', strtotime($order['shipping_on2']));
                        break;
                }
                // $content = 'TOWNDELI: Order #'.strtoupper($order['idv']).' (SH: '.numberFixedLen($order['order_number'], -3).') dang duoc xu ly. Du kien giao '.$loaigiogiao.'. Neu co yeu cau khac, vui long goi (08)73073777. Xin cam on.';


                $oId = strtoupper($order['idv']);
                $oTotal = number_format($order['cart_total']);
                $oPoint = number_format($order['points']);
                $content = 'TOWNDELI: DH #' . $oId . ' t/c ' . $oTotal . 'd (+' . $oPoint . ' diem) du kien giao ' . $loaigiogiao . '. Xem Tong Diem cua QK: http://town.vn/KH. Hotline: (08)73073777. Xin cam on!';

                $this->load->model('Messages_mdel');
                $this->Messages_mdel->send($order['customer_phone'], $content);
            }
        }
        return $id;
    }
    public function capnhattatca($data = array(), $thongtin) {
        $ids = $data->ids;
        $giatri = $data->status;
        $shipper = $data->shipper;
        $tongtienthu = $data->tongtienthu;
        $ngaythu = $data->ngaythu;
        if (count($ids)) {
            foreach ($ids as $value) {
                $tmp = array(
                    'id' => $value,
                    'status' => $giatri
                );
                $this->capnhat($tmp, $thongtin);
            }
            $this->load->model('Order_collection_histories_mdel');
            $data2 = array(
                'user_id' => $thongtin->uid,
                'shipper_id' => $shipper,
                'time_created' => date('Y-m-d H:i:s'),
                'sotienthu' => $tongtienthu,
                'sodonhang' => count($ids),
                'ngaythu' => date(date('Y-m-d ', strtotime($ngaythu)).'H:i:s')
            );
            $this->Order_collection_histories_mdel->create($data2);
            return true;
        } else {
            return false;
        }
    }
    public function capnhat($data = array(), $thongtin) {
        $data = (array)$data;
        $this->load->model('Users_mdel');
        $this->load->model('Order_histories_mdel');
        $id = $data['id'];
        $action2 = '';
        if (isset($data['action'])) {
            $action2 = $data['action'];
            unset($data['action']);
            $order = $this->getDetail($id);
        }
        if (isset($data['status'])) {
            $order = $this->getDetail($id);
            if ($order->original_branch_id>0||$order->original_branch_id==-1) {
                $data['original_branch_id'] = -2;
            }
            
            // Update point for customer
            $userPoints = $this->Users_mdel->getPoint($order->customer_id, true);
            $this->Users_mdel->update([
                    'points' => (int) $userPoints['total'],
                    'points_used' => (int) $userPoints['used']
                ], $order->customer_id);
        }
        $thanhcong = parent::edit($data);
        if ($thanhcong) {
            $action = '';
            if (isset($data['status'])) {
                $action = 'update';
                if ($data['status']==1) {
                    $action = 'accept';
                    $noidung = 'Nhận xử lý lúc %s bởi %s';
                } elseif ($data['status']==3) {
                    $user = $this->Users_mdel->getDetail($thongtin->uid);
                    $action = 'delivered';
                    $noidung = (isset($user->id)?$user->last_name:'').' thông báo Đã giao lúc %s';
                    $this->load->model('Order_delivery_histories_mdel');
                    $order = $this->getDetail($id);
                    $shipperid = $order->shipper_id;
                    $delivery_history = $this->Order_delivery_histories_mdel->getList(array('order_id' => $id), array('getAll' => true));
                    if (count($delivery_history)) {
                        $delivery_history = $delivery_history[0];
                        $this->Order_delivery_histories_mdel->edit(array(
                            'id' => $delivery_history->id,
                            'status' => 3
                        ));
                    } else {
                        $this->Order_delivery_histories_mdel->create(array(
                            'order_id' => $id,
                            'employee_id' => $shipperid,
                            'time_created' => date('Y-m-d H:i:s'),
                            'cart_subtotal' => $order->cart_total,
                            'status' => 3
                        ));
                    }
                } elseif ($data['status']==4) {
                    $action = 'completed';
                    $noidung = 'Hoàn tất lúc %s bởi %s';
                    $this->load->model('Order_delivery_histories_mdel');
                    $order = $this->getDetail($id);
                    $shipperid = $order->shipper_id;
                    $delivery_history = $this->Order_delivery_histories_mdel->getList(array('order_id' => $id), array('getAll' => true));
                    if (count($delivery_history)) {
                        $delivery_history = $delivery_history[0];
                        $this->Order_delivery_histories_mdel->edit(array(
                            'id' => $delivery_history->id,
                            'status' => 4
                        ));
                    } else {
                        $this->Order_delivery_histories_mdel->create(array(
                            'order_id' => $id,
                            'employee_id' => $shipperid,
                            'time_created' => date('Y-m-d H:i:s'),
                            'cart_subtotal' => $order->cart_total,
                            'status' => 4
                        ));
                    }

                    // Update user level
                    $this->load->model('Users_mdel', 'Users');
                    $this->Users->calculateLevel($order->customer_id);

                } elseif ($data['status']==5) {
                    $action = 'cancel';
                    $noidung = 'Hủy hóa đơn lúc %s bởi %s với lý do: "'.$data['lydo'].'"';
                    $this->load->model('Order_delivery_histories_mdel');
                    $order = $this->getDetail($id);
                    $shipperid = $order->shipper_id;
                    $delivery_history = $this->Order_delivery_histories_mdel->getList(array('order_id' => $id), array('getAll' => true));
                    if (count($delivery_history)) {
                        $delivery_history = $delivery_history[0];
                        $this->Order_delivery_histories_mdel->edit(array(
                            'id' => $delivery_history->id,
                            'status' => 5
                        ));
                    } else {
                        $this->Order_delivery_histories_mdel->create(array(
                            'order_id' => $id,
                            'employee_id' => $shipperid,
                            'time_created' => date('Y-m-d H:i:s'),
                            'cart_subtotal' => $order->cart_total,
                            'status' => 5
                        ));
                    }
                } elseif ($data['status']==2) {
                    $this->load->model('Users_mdel');
                    $action = 'assign';
                    $user = $this->Users_mdel->getDetail($data['shipper_id']);
                    $noidung = 'Lúc %s, %s chuyển cho '.(isset($user->id)?$user->last_name:'').' đi giao';
                    $this->load->model('Order_delivery_histories_mdel');
                    $order = $this->getDetail($id);
                    $delivery_history = $this->Order_delivery_histories_mdel->getList(array('order_id' => $id), array('getAll' => true));
                    if (count($delivery_history)) {
                        $delivery_history = $delivery_history[0];
                        $this->Order_delivery_histories_mdel->edit(array(
                            'id' => $delivery_history->id,
                            'employee_id' => $data['shipper_id'],
                            'status' => 2
                        ));
                    } else {
                        $this->Order_delivery_histories_mdel->create(array(
                            'order_id' => $id,
                            'employee_id' => $data['shipper_id'],
                            'time_created' => date('Y-m-d H:i:s'),
                            'cart_subtotal' => $order->cart_total,
                            'status' => 2
                        ));
                    }
                    /*$this->Order_delivery_histories_mdel->create(array(
                        'order_id' => $id,
                        'employee_id' => $data['shipper_id'],
                        'time_created' => date('Y-m-d H:i:s'),
                        'cart_subtotal' => $order->cart_total,
                        'status' => 2
                    ));*/
                    $ci =& get_instance();
                    $ci->config->load('mysecretcode', true);
                    $secretkey = $ci->config->item('SecretCode', 'mysecretcode');
                    /*$token = $this->jwt->encode(array(
                        'uid' => $data['shipper_id'],
                        'time' => time()
                    ), $secretkey);*/
                    $hientai = time();
                    $shipperid = $data['shipper_id'];
                    $token = sha1($shipperid.$hientai.$secretkey);
                    $linkxn = ('orderconfirm/'.$order->idv.'/'.$shipperid.'/'.$hientai.'/'.$token);
                    //$content = '#'.strtoupper($order->idv).' ('.$order->order_number.') - '.($order->gender?'Anh':'Chi').' '.slug2($order->customer_name).' - ('.$order->customer_phone.') - '.slug2($order->shipping_floor).' '.$this->laydiachikhongdau($order->shipping_address).' '.number_format($order->cart_total, 0, '', '.').' - '.$order->cart_total_items.' mon - '.date('H:i', strtotime($order->shipping_on));
                    //$content .= ' - http://town.vn/'.$order->idv;
                    $content = '#'.strtoupper($order->idv).' (SH: '.numberFixedLen($order->order_number, -3).') - THU TIEN '.number_format($order->cart_total, 0, '', '.').' - SDT KH: '.$order->customer_phone.'. Vui long XAC NHAN DA GIAO khi giao den cho KH: http://town.vn/'.$order->idv;
                    $this->load->model('Messages_mdel');
                    //$this->Messages_mdel->send($user->phone, $content);
                    parent::edit(array('id' => $id, 'xacnhanlink' => $linkxn));
                }
            } elseif (isset($data['print_count'])) {
                $action = 'print';
                $noidung = 'In lần thứ '.$data['print_count'].' lúc %s';
            } elseif (isset($data['original_branch_id'])) {
                if ($action2=='move') {
                    $action = $action2;
                    $fromBranch = $data['original_branch_id'];
                    $toBranch = $data['branch_id'];
                    $fromBranchItem = $this->Branches_mdel->getDetail($fromBranch);
                    $toBranchItem = $this->Branches_mdel->getDetail($toBranch);
                    $noidung = 'CN "'.(isset($fromBranchItem->id)?$fromBranchItem->name:'').'" chuyển hóa đơn qua CN "'.(isset($toBranchItem->id)?$toBranchItem->name:'').'" lúc %s bởi %s';
                } elseif ($action2=='moveback') {
                    $action = $action2;
                    $fromBranch = $order->branch_id;
                    $toBranch = $data['branch_id'];
                    $fromBranchItem = $this->Branches_mdel->getDetail($fromBranch);
                    $toBranchItem = $this->Branches_mdel->getDetail($toBranch);
                    $noidung = 'CN "'.(isset($fromBranchItem->id)?$fromBranchItem->name:'').'" chuyển hóa đơn về lại CN "'.(isset($toBranchItem->id)?$toBranchItem->name:'').'" lúc %s bởi %s';
                }
            }
            if ($action) {
                $this->Order_histories_mdel->create(array('order_id' => $id, 'user_id' => $thongtin->uid, 'time_created' => date('Y-m-d H:i:s'), 'action' => $action, 'noidung' => $noidung));
            }
        }
        return $id;
    }
    public function laydiachikhongdau($str) {
        $data = array(
            'address' => $str
        );
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?'.http_build_query($data);
        $result = file_get_contents($url);
        $result = json_decode($result);
        $tmp = '';
        if (isset($result->results)&&count($result->results[0]->address_components)) {
            foreach ($result->results[0]->address_components as $row) {
                $loaithanhpho = $row->types;
                if (in_array('administrative_area_level_1', $loaithanhpho)) {
                    $tmp = $row->long_name;
                }
            }
        }
        $vitri = strpos($data['address'], $tmp);
        $result2 = substr($data['address'], 0, $vitri);
        $result2 = slug2(trim($result2, ' ,'));
        return $result2;
    }
    public function baotre($data = array(), $thongtin) {
        $id = @$data->id;
        $order = $this->getDetail($id);
        if (!$order->shipping_delay) {
            $data2 = array(
                'id' => $id,
                'shipping_delay' => 15
            );
            $thanhcong = parent::edit($data2);
            if ($thanhcong) {
                // $content = 'TOWNDELI: Vi mot so ly do ngoai y muon, Order #' . strtoupper($order->idv) . ' (SH: ' . numberFixedLen($order->order_number, -3) . ') se duoc giao tre hon du kien khoang 10-15 phut. Xin loi Quy Khach ve su bat tien nay!';

                $oId = strtoupper($order->idv);
                $content = 'TOWNDELI: Vi mot so ly do ngoai y muon, DH #' . $oId . ' se giao tre hon du kien khoang 10-15 phut. Thanh that xin loi QK vi su bat tien nay! Hotline: (08)73073777.';

                $this->load->model('Messages_mdel');
                $this->load->model('Order_histories_mdel');
                $this->Messages_mdel->send($order->customer_phone, $content);
                $this->Order_histories_mdel->create(array('order_id' => $id, 'user_id' => $thongtin->uid, 'time_created' => date('Y-m-d H:i:s'), 'action' => 'delay', 'noidung' => 'Báo giao trễ lúc %s bởi %s'));
            }
        }
        return $id;
    }
    public function binhluan($data, $thongtin) {
        $this->load->model('Order_comments_mdel');
        $comment = $data->comment;
        $userid = $thongtin->uid;
        $data2 = array(
            'order_id' => $data->id,
            'user_id' => $userid,
            'time_created' => date('Y-m-d H:i:s'),
            'noidung' => trim($comment)
        );
        $id = $this->Order_comments_mdel->create($data2);
        if ($id) {
            $this->load->model('Users_mdel');
            $user = $this->Users_mdel->getDetail($userid);
            return array(
                trim($comment),
                $user
            );
        } else {
            return false;
        }
    }
    public function lastOrderNumber() {
        $lastOrderToday = $this->db
            ->select('order_number as no')
            ->where('DATE(`time_created`)', date('Y-m-d'))
            ->order_by('order_number', 'DESC')
            ->get($this->table)
            ->row();
        return is_null($lastOrderToday) ? 1 : intval($lastOrderToday->no) + 1;
    }


    private function usePoint($data)
    {
        if ( !empty($data['cart_total']) && !empty($data['customer_id']) ) {
            $this->load->model('Users_mdel', 'Users');
            $userId = $data['customer_id'];
            $currentPoint = $this->Users->getPoint($userId);
            $pointUsed = empty($data['points_used']) ? 0 : abs(intval($data['points_used']));
            $charge = intval($data['cart_total']);
            $total2Point = ceil($charge/1000);

            if ($pointUsed >= $currentPoint) {
                $pointUsed = $currentPoint;
            }

            if ($pointUsed >= $total2Point) {
                $pointUsed = $total2Point;
                $charge = 0;
            } else {
                $charge -= $pointUsed*1000;
            }

            $data['points'] = $this->calculatePoint($userId, $charge);
            $data['points_used'] = $pointUsed;
            $data['cart_total'] = $charge;

            // Update point used into cart data
            $data['points_used'] = $pointUsed;

            if ($data['points_x2'] == 1) {
                $data['points'] *= 2;
            }
        }

        return $data;
    }


    public function calculatePoint($userId, $amount, $level = null)
    {
        if (is_null($level)) {
            $user = $this->db
                ->select('level')
                ->where('id', $userId)
                ->get('users')
                ->row();

            if (is_null($user)) {
                return 0;
            }

            $level = (int) $user->level;
        }

        if ($level <= 2) {
            $point = floor($amount/30000);
        } elseif ($level === 3) {
            $point = floor($amount/25000);
        } else {
            $point = floor($amount/20000);
        }

        return intval($point);
    }
}