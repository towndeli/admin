<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_delivery_histories_mdel extends MY_Model {
    public $table = 'order_delivery_histories';
    public function __construct() {
        parent::__construct();
    }
}