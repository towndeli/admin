<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_comments_mdel extends MY_Model {
    public $table = 'order_comments';
    public function __construct() {
        parent::__construct();
    }
    public function getList($wheres = array(), $options = array()) {
        $danhsach = parent::getList($wheres, $options);
        if (count($danhsach)) {
            $this->load->model('Users_mdel');
            foreach ($danhsach as &$value) {
                $value->time_created = strtotime($value->time_created);
                $value->time_created_text = date('d/m/Y, H:i', $value->time_created);
                $value->time_created_text2 = date('H:i', $value->time_created);
                $user = $this->Users_mdel->getDetail($value->user_id);
                $value->name = isset($user->id)?$user->last_name:'';
            }
        }
        return $danhsach;
    }
}