<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    function index() {
        // $this->recalcutePoint();
    }


    public function update_include($page = 1) {
        $limit = 100;
        if (!is_numeric($page) OR $page < 1) {
            $page = 1;
        }

        $this->load->model('Orders_mdel', 'Orders');
        $orders = $this->db->select('id, cart_data')->limit($limit, ($page-1)*$limit)->get('orders')->result();
        foreach ($orders as $order) {
            $cart = json_decode($order->cart_data);
            if ($cart) {
                $res = $this->Orders->calculate4StrawAndSpoon($cart, true);
                if (!empty($res)) {
                    $dataToUpdate = [
                        'cart_include' => $res['note']
                    ];
                    if (!empty($cart)) {
                        $dataToUpdate['cart_data'] = json_encode($cart);
                    }
                    
                    $this->db->where('id', $order->id)->update('orders', $dataToUpdate);
                }
            }
        }

        if (count($orders) === $limit) {
            // Next page
            $nextPageUrl = site_url('quanly/test/update_include/' . ++$page);
            echo 'Auto redirect to ' . $nextPageUrl;
            echo '<meta http-equiv="refresh" content="1; url=' . $nextPageUrl . '">';
        } else {
            // Done
            echo 'Done';
        }
    }



    private function recalcutePoint()
    {
        $this->load->helper('url');

        $limit = 100;
        $page = empty($_GET['page']) ? 1 : intval($_GET['page']);
        if ($page === 1) {
            echo '<p>** Reset users: level and point!</p>';
            $this->db->set('level', 1)->set('points', 0)->update('users');

            echo '<p>** Reset orders: points and points_used!</p>';
            $this->db->set('points', 0)->set('points_used', 0)->update('orders');
        }

        $this->load->model('Orders_mdel', 'Orders');
        $this->load->model('Users_mdel', 'Users');

        $orders = $this->db->query('SELECT * FROM orders LIMIT ' . ( ($page - 1) * $limit ) . ',' . $limit)->result();
        $users = [];

        $orderPoints = [];

        foreach ($orders as $order) {
            $userId = (int) $order->customer_id;
            $amount = (int) $order->cart_total;
            if (empty($users[$userId])) {
                $userInfo = $this->db->query('SELECT level, points FROM users WHERE id = ' . $userId)->row();
                if (empty($userInfo)) {
                    echo '<p>ERROR: This user is not exist!</p>';
                    die();
                }
                $users[$userId] = [
                    'level' => (int) $userInfo->level,
                    'points' => (int) $userInfo->points
                ];
            }
            $currentUserLevel = $users[$userId]['level'];
            $currentUserPoint = $users[$userId]['points'];

            $orderPoint = $this->Orders->calculatePoint($userId, $amount, $currentUserLevel);
            $currentUserPoint += $orderPoint;
            $newUserLevel = $this->Users->calculateLevel($userId, false, $currentUserPoint);

            $users[$userId] = [
                'level' => $newUserLevel,
                'points' => $currentUserPoint
            ];

            $orderPoints[$order->id] = $orderPoint;

            $infos = [
                'OrderId: ' . $order->id,
                'UserId: ' . $userId,
                'Level: ' . $currentUserLevel . ' -> ' . $newUserLevel,
                'Amount: ' . number_format($amount),
                'User Point: ' . $currentUserPoint
            ];
//            var_dump(implode(' | ', $infos));
        }

        // Update point for orders
        $sqlUpdateOrderPoint = 'UPDATE `orders` SET `points` = CASE';
        foreach ($orderPoints as $orderId => $orderPoint) {
            $sqlUpdateOrderPoint .= ' WHEN `id` = ' . $orderId . ' THEN ' . $orderPoint;
        }
        $sqlUpdateOrderPoint .= ' ELSE `points` END';
        $this->db->query($sqlUpdateOrderPoint);

        // Update level for users
        foreach ($users as $userId => $userInfo) {
            $this->db->where('id', $userId)->update('users', $userInfo);
        }

        $nextUrl = current_url() . '?page=' . ($page + 1) . '&limit=' . $limit;

        if (count($orders) >= $limit) {
            echo '<a href="' . $nextUrl . '">' . $nextUrl . '</a>';
            echo '<meta http-equiv="REFRESH" content="1; url=' . $nextUrl . '">';
        } else {
            echo 'Done';
        }

//        var_dump($users);
    }
}