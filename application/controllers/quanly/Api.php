<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	private $secretkey = 'bimatquocgia00L;';
	private $googlekey = 'AIzaSyBcE95p6HSipudA0s0mI3d0FYff8Mlk21U';
	function __construct() {
		parent::__construct();

		$this->config->load('mysecretcode', true);
		$this->secretkey = $this->config->item('SecretCode', 'mysecretcode');
	}

	public function calc($orderId = null)
	{
		if (is_numeric($orderId)) {
			$this->load->model('Orders_mdel', 'Orders');
			$order = $this->Orders->getDetail($orderId);
			if ($order) {
				$cart = json_decode($order->cart_data);
				if ($cart) {
					$calc = $this->Orders->calculate4StrawAndSpoon($cart);
					if (!empty($calc)) {
						echo $calc['note'];
					}
				}
			}
		}
	}

	public function timkiem($qg='', $data) {
		/*$data = $this->input->raw_input_stream;
		$data = json_decode($data);
		$data = $data->address;*/
		$data = urldecode($data);
		$danhsach = array();
		$result = file_get_contents('https://maps.googleapis.com/maps/api/place/autocomplete/json?input='.urlencode($data).'&types=geocode'.(($qg!=''&&$qg!='undefined')?('&components=country:'.$qg):'').'&key='.$this->googlekey);
		$result = json_decode($result);
		if (isset($result->predictions)) {
			foreach ($result->predictions as $row) {
				$fruit = array_shift($row->terms);
				$label = $fruit->value;
				$description = array();
				if (count($row->terms)) {
					foreach ($row->terms as $item) {
						$description[] = $item->value;
					}
				}
				$result2 = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?place_id='.$row->place_id.'&key='.$this->googlekey);
				$result2 = json_decode($result2);
				$vitri = array('lat' => 0, 'lng' => 0);
				if (isset($result2->status)&&$result2->status=='OK') {
					$vitri['lat'] = $result2->results[0]->geometry->location->lat;
					$vitri['lng'] = $result2->results[0]->geometry->location->lng;
				}
				$danhsach[] = array('label' => $label, 'description' => implode(', ', $description), 'vitri' => $vitri);
			}
		}
		//header('Cache-Control: max-age=290304000, public');
		//header('Pragma: cache');
		echo json_encode(array('results' => $danhsach));
	}
	public function index2($page=1) {
		$limit = 500;
		$query = $this->db->query('select * from orders where shipping_floor!="" limit '.(($page-1)*$limit).', '.$limit);
		$results = $query->result();
		if (count($results)) {
			foreach ($results as $row) {
				/*$user = $this->Users_mdel->getDetailByField('phone', $row->customer_phone);
				if (isset($user->id)&&($user->gender!=$row->gender)) {
					$this->db->update('orders', array('gender' => $user->gender), array('id' => $row->id));
				}*/
				$tmp = explode($row->shipping_floor.', ', $row->shipping_address);
				/*echo $row->id;
				print_r($tmp);echo '<br>';*/
				$this->db->update('orders', array('shipping_address' => $tmp[1]), array('id' => $row->id));
			}
			echo '<a href="' . site_url('quanly/api/index2/' . ($page + 1)) . '">Next ' . ($page + 1) . '</a>';
		}
	}
	public function postdulieuuser() {
		$data = $_POST['field1'];
		$data = json_decode($data);
		foreach($data as $row) {
			$row = (array)$row;
			$this->db->insert('users', $row);
		}
	}
	public function postdulieuorder() {
		$data = $_POST['field1'];
		$data = json_decode($data);
		foreach($data as $row) {
			$row = (array)$row;
			$this->db->insert('orders', $row);
		}
	}
	public function postupdatedulieuorder() {
		$data = $_POST['field1'];
		$data = json_decode($data);
		foreach($data as $row) {
			$row = (array)$row;
			$this->db->update('orders', array('shipping_on' => $row['thoigian']), array('id' => $row['id']));
			//print_r($row);
		}
	}
	/*public function index2() {
		$data = $_POST['field1'];
		$data = json_decode($data);
		foreach($data as $row) {
			$row = (array)$row;
			//$this->db->insert('users', $row);
		}
	}
	public function index5($page=1) {
		$this->load->model('Users_mdel');
		$limit = 500;
		$query = $this->db->query('select * from orders limit '.(($page-1)*$limit).', '.$limit);
		$results = $query->result();
		if (count($results)) {
			foreach ($results as $row) {
				$user = $this->Users_mdel->getDetailByField('phone', $row->customer_phone);
				if (isset($user->id)&&($user->gender!=$row->gender)) {
					$this->db->update('orders', array('gender' => $user->gender), array('id' => $row->id));
				}
			}
			echo '<a href="' . site_url('quanly/api/index5/' . ($page + 1)) . '">Next ' . ($page + 1) . '</a>';
		}
	}*/
	public function shortenxacnhan($ma = '') {
		$this->load->model('Orders_mdel');
		$order = $this->Orders_mdel->getDetailByField('idv', $ma);
		if (isset($order->id) && $order->xacnhanlink) {
			header("Location: ".site_url()."/".$order->xacnhanlink);
			exit();
		}
	}
	public function xacnhangiaohang($ma = '', $shipper = 0, $thoigian = 0, $mahash = '') {
		$tmphash = sha1($shipper.$thoigian.$this->secretkey);
		if ($tmphash==$mahash) {
			$this->load->model('Orders_mdel');
			$order = $this->Orders_mdel->getDetailByField('idv', $ma);
			if (isset($order->id) && $order->status == 2) {
				$shipperid = (int)$shipper;
				if ($shipperid == $order->shipper_id) {
					$dukien = strtotime($order->shipping_on);
					$thoigiantao = strtotime($order->time_created);//
					$sodu = floor(($dukien - $thoigiantao)*70/100);//
					$dukien3 = $thoigiantao + $sodu;//
					$hientai = time();
					$dukien2 = $hientai + 600;
					if ($hientai>=$dukien3) {//if ($dukien2 >= $dukien) {
						$order->time_created = strtotime($order->time_created);
						$order->time_created_text = date('d/m/Y, H:i', $order->time_created);
						$orders = json_decode($order->cart_data);
						if (count($orders)) {
							$this->load->model('Menus_mdel');
							$this->load->model('Toppings_mdel');
							foreach ($orders as &$value2) {
								$menu = $this->Menus_mdel->getDetail($value2->id);
								$value2->name = $menu->name;
								$toppings = $value2->toppings;
								if (count($toppings)) {
									foreach ($toppings as &$value3) {
										$topping = $this->Toppings_mdel->getDetail($value3->id);
										$value3->name = $topping->name;
									}
									$value2->toppings = $toppings;
								}
								$value2->ghichuthuonggap = $this->Orders_mdel->ghichuthuonggap;
							}
						}
						$order->orders = $orders;
						$thongtin = array(
							'order' => $order,
							'token' => $shipper.'/'.$thoigian.'/'.$mahash
						);
						$this->render_page('xacnhangiaohang', $thongtin);
					}
				}
			}
		}
	}
	public function dagiao() {
		$data = $this->input->raw_input_stream;
		$data = json_decode($data);
		$id = (int)$data->id;
		$token = $data->token;
		$token = explode('/', $token);
		if (count($token)==3) {
			$tmphash = sha1($token[0].$token[1].$this->secretkey);
			if ($tmphash==$token[2]) {
				$this->load->model('Orders_mdel');
				$order = $this->Orders_mdel->getDetailByField('id', $id);
				if (isset($order->id) && $order->status == 2) {
					$dukien = strtotime($order->shipping_on);
					$thoigiantao = strtotime($order->time_created);//
					$sodu = floor(($dukien - $thoigiantao)*70/100);//
					$dukien3 = $thoigiantao + $sodu;//
					$hientai = time();
					$dukien2 = $hientai + 600;
					if ($hientai>=$dukien3) {//if ($dukien2 >= $dukien) {
						$shipperid = (int)$token[0];
						if ($shipperid && $shipperid == $order->shipper_id) {//$data->position->lat &&
						$thanhcong = $this->Orders_mdel->edit(array(
								'id' => $id,
								'status' => 3,
								/*'lat' => $data->position->lat,
								'lng' => $data->position->lng,*/
								'time_updated' => date('Y-m-d H:i:s')
							));
							if ($thanhcong) {
								$this->load->model('Users_mdel');
								$user = $this->Users_mdel->getDetailByField('id', $shipperid);
								$action = 'delivered';
								$noidung = (isset($user->id) ? $user->last_name : '') . ' thông báo Đã giao lúc %s';
								$this->load->model('Order_delivery_histories_mdel');
								$this->load->model('Order_histories_mdel');
								$this->Order_histories_mdel->create(array('order_id' => $id, 'user_id' => $user->id, 'time_created' => date('Y-m-d H:i:s'), 'action' => $action, 'noidung' => $noidung));
								$delivery_history = $this->Order_delivery_histories_mdel->getList(array('order_id' => $id), array('getAll' => true));
								if (count($delivery_history)) {
									$delivery_history = $delivery_history[0];
									$this->Order_delivery_histories_mdel->edit(array(
										'id' => $delivery_history->id,
										'status' => 3,
										/*'lat' => $data->position->lat,
										'lng' => $data->position->lng,*/
										'time_updated' => date('Y-m-d H:i:s')
									));
								} else {
									$this->Order_delivery_histories_mdel->create(array(
										'order_id' => $id,
										'employee_id' => $shipperid,
										'time_created' => date('Y-m-d H:i:s'),
										'cart_subtotal' => $order->cart_subtotal,
										'status' => 3,
										/*'lat' => $data->position->lat,
										'lng' => $data->position->lng,*/
										'time_updated' => date('Y-m-d H:i:s')
									));
								}
								echo json_encode(array('status' => 'success'));
							}
						}
					}
				}
			}
		}
	}
	public function index() {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$thongke = $this->total();
			echo json_encode(array('ngayhientai' => time(), 'thongke' => $thongke));
		}
	}
	public function thongke() {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$loai = $data->loai;
			$ngayhientai = date('Y-m-d', strtotime($data->ngayhientai));
			//
			$tmp = $tmp2 = [];
			$this->load->model(array('Users_mdel', 'Regions_mdel', 'Branches_mdel'));
			$user = $this->Users_mdel->getDetail($thongtin->uid);
			$user_role = $user->role;
			$regions = $this->Regions_mdel->getList(array('status' => 1), array('getAll' => true));
			$branches = $this->Branches_mdel->getList(array('status' => 1), array('getAll' => true));
			if ($user_role=='admin') {
				if (count($branches)) {
					foreach ($branches as $value) {
						$tmp2[] = $value->id;
					}
				}
			} elseif ($user_role=='supregion') {
				if (count($regions)) {
					foreach ($regions as $value) {
						if ($value->parent_id==$user->role_tbl_id) {
							if (count($branches)) {
								foreach ($branches as $value2) {
									if ($value2->region_id==$value->id) {
										$tmp2[] = $value2->id;
									}
								}
							}
						}
					}
				}
			} elseif ($user_role=='branch') {
				if (count($branches)) {
					foreach ($branches as $value) {
						if ($value->id==$user->role_tbl_id) {
							$tmp2[] = $value->id;
						}
					}
				}
			} elseif ($user_role=='region') {
				if (count($branches)) {
					foreach ($branches as $value) {
						if ($value->region_id==$user->role_tbl_id) {
							$tmp2[] = $value->id;
						}
					}
				}
			}
			/*if (count($tmp2)) {
				foreach ($tmp2 as $value) {
					$tmp3 = $this->danhsachNhanviengiaoChinhanhtrongngay($value['id'], $ngayhientai);
					if (count($tmp3)) {
						foreach ($tmp3 as &$value2) {
							$value2['chinhanh'] = $value['name'];
						}
						$tmp = array_merge($tmp, $tmp3);
					}
				}
			}*/
			//
			$thongkehoadon = $this->totalByDate(($loai!=''?$ngayhientai:null), $tmp2);
			$danhsach = [];
			$this->load->model('Reports_mdel');
			$danhsach = $this->Reports_mdel->danhsachNhanviengiaotrongngay($thongtin->uid, $ngayhientai);
			echo json_encode(array('thongke' => $thongkehoadon, 'danhsach' => $danhsach[0], 'danhsach2' => $danhsach[1]));
		}
	}
	public function totalByDate($range, $branches) {
		$this->load->model('Orders_mdel');

		$wheres = [];
		if (!is_null($range)) {
			$wheres = ['DATE(time_created)' => $range];
		}

		$orders = [];
		$orderStatuses = $this->Orders_mdel->orderStatuses;
		foreach ($orderStatuses as $key => $value) {
			if (!count($branches)) {
				$query = $this->db->select('count(id) as count, sum(cart_total_items) as total_item, sum(cart_total) as total', false)->where($wheres)->where('status', $key)->get('orders')->row();
				//$orders[] = array('trangthai' => $value, 'soluong' => $this->db->where($wheres)->where('status', $key)->count_all_results('orders'));
				$orders[] = array('trangthai' => $value, 'loai' => (int)$key, 'soluong' => $query->count, 'total_item' => $query->total_item, 'total' => $query->total);
			} else {
				$query = $this->db->select('count(id) as count, sum(cart_total_items) as total_item, sum(cart_total) as total', false)->where($wheres)->where('status', $key)->where_in('branch_id', $branches)->get('orders')->row();
				//$orders[] = array('trangthai' => $value, 'soluong' => $this->db->where($wheres)->where('status', $key)->where_in('branch_id', $branches)->count_all_results('orders'));
				$orders[] = array('trangthai' => $value, 'loai' => (int)$key, 'soluong' => $query->count, 'total_item' => $query->total_item, 'total' => $query->total);
			}
		}

		$shippers = $this->db->select('id, first_name, last_name, phone')->where('role', 4)->get('users')->result();
		foreach ($shippers as $key => $row) {
			$res = $this->db->select('count(id) as count, sum(cart_total_items) as total_item, sum(cart_total) as total', false)->where($wheres)->where('shipper_id', $row->id)->where('status', 4)->get('orders')->row();
			$row->order_count = (int) $res->count;
			$row->total_item = (int) $res->total_item;
			$row->order_total = (int) $res->total;
			$shippers[$key] = $row;
		}

		$total = $this->db->select('count(id) as count, sum(cart_total_items) as total_item, sum(cart_total) as total', false)->where($wheres)->where('status', 4)->get('orders')->row();

		return [
			'orders' => $orders,
			'orderFinish' => (int) $total->count,
			'orderItem' => (int) $total->total_item,
			'revenue' => (int) $total->total,
			'shippers' => $shippers
		];
	}
	private function total() {
		$data = [];
		$objs = ['regions', 'branches', 'categories', 'menus', 'toppings', 'users', 'orders'];
		foreach ($objs as $key => $value) {
			$data[] = array('table' => $value, 'soluong' => $this->db->count_all_results($value));
		}
		return $data;
	}
	public function geocode($loai) {
		$data = $this->input->raw_input_stream;
		$data = json_decode($data, true);
		$data = http_build_query($data);
		$dulieu = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?'.$data);
		echo $dulieu;
	}
	public function luutru_baoloi() {
		$data = $this->input->raw_input_stream;
		var_dump($data);
		$this->load->model('Baoloi_mdel');
		//$data = json_decode($data);
		$this->Baoloi_mdel->luutru(array('error_message' => $data, 'time_created' => date('Y-m-d H:i:s')));
	}
	public function danhsach($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model($loai.'_mdel');
			$danhsach = $this->{$loai.'_mdel'}->danhsach($data, $thongtin);
			echo json_encode($danhsach);
		}
	}
	public function chuanbi($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model($loai.'_mdel');
			$result = $this->{$loai.'_mdel'}->chuanbi($data, $thongtin);
			echo json_encode($result);
		}
	}
	public function luutru($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model($loai.'_mdel');
			$result = $this->{$loai.'_mdel'}->luutru($data, $thongtin);
			
			if ($result) {
				$data2 = $this->{$loai.'_mdel'}->getDetail($result);
				$this->load->model('Users_mdel');
				$createuser = $this->Users_mdel->getDetail($thongtin->uid);
				echo json_encode(array('status' => 'success', 'data' => $data2, 'users' => array(), 'createuser' => $createuser));
			} else {
				echo json_encode(array('status' => 'error', 'msg' => 'Lỗi không tạo được mẫu tin!'));
			}
		}
	}
	public function capnhat($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model($loai . '_mdel');
			$result = $this->{$loai . '_mdel'}->capnhat($data, $thongtin);
			if ($result) {
				$data2 = $this->{$loai . '_mdel'}->getDetail($result);
				$this->load->model('Users_mdel');
				$createuser = $this->Users_mdel->getDetail($thongtin->uid);
				echo json_encode(array('status' => 'success', 'data' => $data2, 'users' => array(), 'createuser' => $createuser));
			} else {
				echo json_encode(array('status' => 'error', 'msg' => 'Lỗi không tạo được mẫu tin!'));
			}
		}
	}
	public function capnhattatca($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model($loai . '_mdel');
			$result = $this->{$loai . '_mdel'}->capnhattatca($data, $thongtin);
			if ($result) {
				echo json_encode(array('status' => 'success'));
			} else {
				echo json_encode(array('status' => 'error', 'msg' => 'Lỗi không tạo được mẫu tin!'));
			}
		}
	}
	public function baotre($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model($loai . '_mdel');
			$result = $this->{$loai . '_mdel'}->baotre($data, $thongtin);
			if ($result) {
				$data2 = $this->{$loai . '_mdel'}->getDetail($result);
				/*$this->load->model('Users_mdel');
				$createuser = $this->Users_mdel->getDetail($thongtin->uid);*/
				echo json_encode(array('status' => 'success', 'data' => $data2, 'users' => array()));
			} else {
				echo json_encode(array('status' => 'error', 'msg' => 'Lỗi không tạo được mẫu tin!'));
			}
		}
	}
	public function avatar($loai, $id) {
		$this->load->model($loai . '_mdel');
		$result = $this->{$loai . '_mdel'}->avatar($id);
		/*$mine = 'application/force-download';
		//header('Pragma: public');
		//header('Expires: 0');
		//header('Cache-Control: max-age=290304000');
		//header('Cache-Control: public', false);
		header('Content-Type: ' . $mine);
		header('Content-Disposition: attachment; filename="hinhanh.png"');
		header('Content-Transfer-Encoding: binary');
		header('Connection: close');
		echo $result;
		exit();*/
		//$this->load->helper('download');
		//force_download('hinhanh.png', $result);
		header('Content-type: image/png');
		echo $result;
	}
	public function binhluan($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model($loai.'_mdel');
			$result = $this->{$loai.'_mdel'}->binhluan($data, $thongtin);
			if (!empty($result)) {
				$data2 = $this->{$loai.'_mdel'}->getDetail($data->id);
				echo json_encode(array('status' => 'success', 'data' => $data2, 'users' => array(), 'comment' => $result[0], 'createuser' => $result[1]));
			} else {
				echo json_encode(array('status' => 'error', 'msg' => 'Lỗi không tạo được mẫu tin!'));
			}
		}
	}
	public function refresh($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model($loai.'_mdel');
			$result = $this->{$loai.'_mdel'}->getDetail($data->id);
			echo json_encode(array('status' => 'success', 'data' => $result, 'users' => array()));
		}
	}
	public function discount_apply() {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model('Discounts_mdel');
			$code = $data->data;
			$items = $data->items;

			$result = $this->Discounts_mdel->apply($code, $items);

			echo json_encode($result);
		}
	}
	public function search_customer() {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model('Users_mdel');
			$this->load->model('Orders_mdel');
			$loai = $data->loai;
			$data = $data->data;
			if ($loai=='name') {
				$ketqua = $this->Users_mdel->getListBySQL('SELECT distinct customer_name, customer_id, customer_phone, shipping_address, shipping_floor, gender FROM orders WHERE (shipping_address!="") AND customer_name LIKE ? ORDER BY time_created DESC', array('%'.$data.'%'));
				if (count($ketqua)) {
					foreach ($ketqua as &$value) {
						$userRes = $this->Users_mdel->getListBySQL('SELECT * FROM `users` WHERE `id`=' . $value->customer_id);
						$user = $userRes[0];
						
						$tmp = explode(' ', $value->customer_name);
						$lastname = array_pop($tmp);
						$firstname = implode(' ', $tmp);
						$value->last_name = $lastname;
						$value->first_name = $firstname;
						
						$value->level = (int) $user->level;
						$value->points = (int) ($user->points - $user->points_used);
					}
				}
			} elseif ($loai=='phone') {
				$ketqua = $this->Users_mdel->getListBySQL('SELECT distinct customer_name, customer_id, customer_phone, shipping_address, shipping_floor, gender FROM orders WHERE (shipping_address!="") AND customer_phone LIKE ? ORDER BY time_created DESC', array('%'.$data.'%'));
				if (count($ketqua)) {
					foreach ($ketqua as &$value) {
						$userRes = $this->Users_mdel->getListBySQL('SELECT * FROM `users` WHERE `id`=' . $value->customer_id);
						$user = $userRes[0];
						
						$tmp = explode(' ', $value->customer_name);
						$lastname = array_pop($tmp);
						$firstname = implode(' ', $tmp);
						$value->last_name = $lastname;
						$value->first_name = $firstname;
						
						$value->level = (int) $user->level;
						$value->points = (int) ($user->points - $user->points_used);
					}
				}
			}
			echo json_encode(array('results' => $ketqua));
		}
	}
	public function getInfo($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->raw_input_stream;
			$data = json_decode($data);
			$this->load->model('Branches_mdel');
			$this->load->model('Users_mdel');
			$fromBranchItem = $this->Branches_mdel->getDetail($data->fromBranch);
			$toBranchItem = $this->Branches_mdel->getDetail($data->toBranch);
			$createuser = $this->Users_mdel->getDetail($data->userId);
			echo json_encode(array('status' => 'success', 'createuser' => $createuser, 'fromBranch' => $fromBranchItem->name, 'toBranch' => $toBranchItem->name));
		}
	}
	public function upload($loai) {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)) {
			$data = $this->input->post('id', true);
			$this->load->model($loai.'_mdel');
			$result = $this->{$loai.'_mdel'}->uploadHandler($data, $thongtin);
			if (!empty($result)) {
				echo json_encode(array('status' => 'success', 'data' => $result));
			} else {
				echo json_encode(array('status' => 'error', 'msg' => 'Lỗi không tạo được mẫu tin!'));
			}
		}
	}
	public function pages($page='') {
		$thongtin = $this->getDataFromHeader();
		if (!empty($thongtin)&&$page) {
			$this->render_page($page);
		}
	}
	private function getDataFromHeader() {
		$token = '';
		//$headers = apache_request_headers();
		//$headers = $headers['Authorization'];
		$headers = @$_SERVER["HTTP_AUTHORIZATION"];
		if(isset($headers)){
			$token = explode(' ', $headers);
			if (count($token) == 2 && $token[1] != '') {
				$token = $token[1];
				$token = base64_decode($token);
			}
		}
		if ($token) {
			$thongtin = $this->getDataFromToken($token);
			return $thongtin;
		} else {
			return false;
		}
	}
	private function getDataFromToken($token) {
		$thongtin = $this->jwt->decode($token, $this->secretkey);
		return $thongtin;
	}
	public function authenticate() {
		$data = $this->input->raw_input_stream;
		$data = json_decode($data);
		if (!empty($data)) {
			$this->load->model('ion_auth_model');
			if ($this->ion_auth_model->login($data->username, $data->password)) {
				$this->load->model('Users_mdel');
				$user = $this->Users_mdel->getDetailByField($this->config->item('identity', 'ion_auth'), $data->username);
				if ($user->role!='members'&&$user->role!='customer'&&$user->active) {
					$branches = $regions = $tmp = $tmp2 = array();
					$quocgia = '';
					$userid = $user->id;
					$useremail = $user->email;
					$useraccount = $data->username;
					$user_role = $user->role;
					$username = $user->first_name . ' ' . $user->last_name;
					$token = $this->jwt->encode(array(
						'uid' => $userid,
						'email' => $useremail,
						'user' => $useraccount,
						'name' => $username
					), $this->secretkey);
					$this->load->model('Regions_mdel');
					$this->load->model('Branches_mdel');
					$regions = $this->Regions_mdel->getList(array('status' => 1), array('getAll' => true));
					$branches = $this->Branches_mdel->getList(array('status' => 1), array('getAll' => true));
					if ($user_role=='admin') {
						if (count($branches)) {
							foreach ($branches as $value) {
								$tmp[] = $value->id;
								$tmp2[] = array('id' => $value->id, 'name' => $value->name);
							}
						}
					} elseif ($user_role=='supregion') {
						if (count($regions)) {
							foreach ($regions as $value) {
								if ($value->parent_id==$user->role_tbl_id) {
									if (count($branches)) {
										foreach ($branches as $value2) {
											if ($value2->region_id==$value->id) {
												$tmp[] = $value2->id;
												$tmp2[] = array('id' => $value2->id, 'name' => $value2->name);
											}
										}
									}
								}
								if ($value->id==$user->role_tbl_id) {
									$quocgia = $value->phamvi;
								}
							}
						}
					} elseif ($user_role=='branch') {
						$tmp[] = $user->role_tbl_id;
						$mybranch = $myregion = null;
						if (count($branches)) {
							foreach ($branches as $value) {
								if ($value->id==$user->role_tbl_id) {
									$tmp2[] = array('id' => $value->id, 'name' => $value->name);
									$mybranch = $value;
								}
							}
						}
						if (count($regions)&&!is_null($mybranch)) {
							foreach ($regions as $value) {
								if ($value->id==$mybranch->region_id) {
									$myregion = $value;
								}
							}
							if (!is_null($myregion)) {
								foreach ($regions as $value) {
									if ($value->id==$myregion->parent_id) {
										$quocgia = $value->phamvi;
									}
								}
							}
						}
					} elseif ($user_role=='region'||$user_role=='salesman') {
						$myregion = null;
						if (count($branches)) {
							foreach ($branches as $value) {
								if ($value->region_id==$user->role_tbl_id) {
									$tmp[] = $value->id;
									$tmp2[] = array('id' => $value->id, 'name' => $value->name);
								}
							}
						}
						if (count($regions)) {
							foreach ($regions as $value) {
								if ($value->id==$user->role_tbl_id) {
									$myregion = $value;
								}
							}
							if (!is_null($myregion)) {
								foreach ($regions as $value) {
									if ($value->id==$myregion->parent_id) {
										$quocgia = $value->phamvi;
									}
								}
							}
						}
					}
					echo json_encode(array('status' => 'success', 'currentUser' => array(
						'authdata' => $token,
						'branches' => $tmp,
						'branchList' => array(),//$tmp2,
						'userid' => $userid,
						'role' => $user_role,
						'username' => $useraccount,
						'fullname' => $username,
						'quocgia' => $quocgia,
						'email' => $useremail
					)));
				} else {
					echo json_encode(array('status' => 'error', 'msg' => 'Bạn không có quyền truy cập!'));
				}
			} else {
				echo json_encode(array('status' => 'error', 'msg' => 'Bạn đăng nhập sai mật khẩu hoặc tên đăng nhập!'));
			}
		}
	}
	private function render_page($page, $data = array()) {
		$this->load->view('quanly/'.$page, $data);
	}
}
