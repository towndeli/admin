<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index() {
		$this->render_page('home');
	}
	public function kiemtra() {
		$data = array(
			'address' => '20 Đồng Dậu, Phước Mỹ, tp. Phan Rang - Tháp Chàm, Ninh Thuận, Việt Nam'
			//'address' => '28/3/12 Hoa Lan, phường 2, Phú Nhuận, Hồ Chí Minh, Việt Nam'
		);
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?'.http_build_query($data);
		$result = file_get_contents($url);
		$result = json_decode($result);
		$tmp = '';
		if (isset($result->results)&&count($result->results[0]->address_components)) {
			foreach ($result->results[0]->address_components as $row) {
				$loaithanhpho = $row->types;
				if (in_array('administrative_area_level_1', $loaithanhpho)) {
					$tmp = $row->long_name;
				}
			}
		}
		$vitri = strpos($data['address'], $tmp);
		$result2 = substr($data['address'], 0, $vitri);
		$result2 = slug2(trim($result2, ' ,'));
		echo $result2;
	}
	public function navbar() {
		$token = '';
		$headers = apache_request_headers();
		if(isset($headers['Authorization'])){
			$token = explode(' ', $headers['Authorization']);
			if (count($token) == 2 && $token[1] != '') {
				$token = $token[1];
				$token = base64_decode($token);
			}
		}
		if ($token) {
			$this->render_page('navbar');
		}
	}
	public function sidebar() {
		$token = '';
		$headers = apache_request_headers();
		if(isset($headers['Authorization'])){
			$token = explode(' ', $headers['Authorization']);
			if (count($token) == 2 && $token[1] != '') {
				$token = $token[1];
				$token = base64_decode($token);
			}
		}
		if ($token) {
			$this->render_page('sidebar');
		}
	}
	public function pages($page = '') {
		$token = '';
		$headers = apache_request_headers();
		if(isset($headers['Authorization'])){
			$token = explode(' ', $headers['Authorization']);
			if (count($token) == 2 && $token[1] != '') {
				$token = $token[1];
				$token = base64_decode($token);
			}
		}
		if ($token && $page) {
			$this->render_page($page);
		}
	}
	private function render_page($page) {
		$this->load->view('quanly/'.$page);
	}
}
