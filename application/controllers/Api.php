<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
    public function users($userId = null, $action = null) {
        if (is_null($userId)) {
            $this->listUsers();
        } else {
            if (is_numeric($userId)) {
                switch ($action) {
                    case 'points':
                        $this->load->model('Users_mdel');
                        $userPoints = $this->Users_mdel->getPoint($userId, true);
                        echo json_encode($userPoints);
                        break;

                    case 'calculate':
                        $page = abs($this->input->get('page'));
                        $limit = abs($this->input->get('limit'));
                        if ($page === 0) {
                            $page = 1;
                        }
                        if ($limit === 0) {
                            $limit = 50;
                        }

                        echo "Page: $page, limit: $limit</br>";

                        $this->load->model('Users_mdel');
                        $users = $this->db->select('id')->limit($limit, ($page - 1) * $limit)->get('users')->result();
                        foreach ($users as $u) {
                            $points = $this->Users_mdel->getPoint($u->id, true);
                            $this->db->where('id', $u->id)->set([
                                    'points' => $points['total'],
                                    'points_used' => $points['used']
                                ])->update('users');
                        }

                        if (count($users) === $limit) {
                            $nextPageUrl = site_url('api/users/0/calculate?page=' . ++$page);
                            echo 'Auto redirect to ' . $nextPageUrl;
                            echo '<meta http-equiv="refresh" content="1; url=' . $nextPageUrl . '">';
                        } else {
                            echo 'Done';
                        }
                        break;
                    
                    default:
                        # code...
                        break;
                }
            }
        }
    }


    private function listUsers()
    {
        header("Content-type: application/json; charset=utf-8");

        $this->load->model('Users_mdel', 'Users');
        if (!empty($_POST['phone'])) {
            $phone = $_POST['phone'];
        } elseif (!empty($_GET['phone'])) {
            $phone = $_GET['phone'];
        } else {
            $phone = '';
        }
        $phone = preg_replace('/[^0-9]/', '', $phone);

        $user = $this->Users->getDetailbyField('phone', $phone);

        if (empty($user)) {
            $response = [
                'success' => false
            ];
        } else {
            $levels = [
                1 => 'NEW TOWNIE',
                2 => 'SILVER TOWNIE',
                3 => 'GOLDEN TOWNIE',
                4 => 'DIAMOND TOWNIE'
            ];
            $response = [
                'success' => true,
                'code' => 200,
                'data' => [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'fullname' => trim($user->first_name . ' ' . $user->last_name),
                    'gender' => ($user->gender == 1 ? 'Anh' : 'Chị'),
                    'phone' => $phone,
                    'level' => intval($user->level),
                    'level_text' => $levels[$user->level],
                    'points' => $this->Users->getPoint($user->id, true)
                ]
            ];
        }

        echo json_encode($response);
    }


    public function sms($phone = null)
    {
        if (preg_match('#(\d+)#', $phone)) {
            $this->load->model('Messages_mdel');
            $content = 'TOWNDELI: DH #XXXXXX t/c 100.000d (+3 diem) du kien giao SOM NHAT TRUOC 12:00. Xem Tong Diem cua QK: http://town.vn/KH. Hotline: (08)73073777. Xin cam on!';

            $res = $this->Messages_mdel->send($phone, $content);
            echo '<pre>';
            print_r($res);
            echo '</pre>';
        } else {
            echo 'Phone number is invalid!';
        }
    }

    public function sms_balance()
    {
        $this->load->model('Messages_mdel');
        $res = $this->Messages_mdel->getBalance();
        var_dump($res);
    }
}