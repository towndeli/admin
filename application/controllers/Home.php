<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('ALLOWED_CHARS', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

class Home extends CI_Controller {
	public function index() {
		redirect('quanly#/home/dashboard');
	}
	public function index3($page=1) {
		$loai = array(
			'price' => '12oz',
			'price_m' => 'M',
			'price_l' => 'L'
		);
		$kichco = array(
			'price' => 'sm',
			'price_m' => 'md',
			'price_l' => 'lg'
		);
		$danhsach = [];
		//print_r($_REQUEST);
		//$this->load->model('Messages_mdel');
		//$this->Messages_mdel->send('0902855377', 'Hello');
		$limit = 500;
		$query = $this->db->query('select * from toppings');
		$toppings = $query->result();
		$query = $this->db->query('select * from orders limit '.(($page-1)*$limit).', '.$limit);
		$results = $query->result();
		foreach ($results as $row) {
			$cart_data = json_decode($row->cart_data);
			if (count($cart_data)) {
				$cart_data_tmp = array();
				foreach ($cart_data as $value) {
					$tmp2 = array();
					$selected_label = $loai[$value->options->size];
					$selected_size = $kichco[$value->options->size];
					$note = @$value->options->note;
					if (isset($value->options->toppings)) {
						foreach ($value->options->toppings as $key => $value2) {
							$price = $this->timkiem($key, $toppings);
							$tmp2[] = array(
								'id' => $key,
								'qty' => $value2,
								'price' => $price
							);
						}
					}
					$cart_data_tmp[] = array(
						'id' => $value->id,
						'qty' => $value->qty,
						'selected_label' => $selected_label,
						'selected_price' => $value->price,
						'selected_size' => $selected_size,
						'note' => $note,
						'toppings' => $tmp2
					);
				}
				//print_r($cart_data);
				//echo '///-///';
				//print_r($cart_data_tmp);
				$gender = 0;
				//$shipping_address = '';
				$shipping_on_loai = 0;
				$shipping_on = 0;
				$shipping_on2 = 0;
				$edit_count = 0;
				$order_tmp = array(
					'id' => $row->id,
					'idv' => $row->idv,
					'branch_id' => $row->branch_id,
					'employee_id' => $row->employee_id,
					'customer_id' => $row->customer_id,
					'cart_data' => json_encode($cart_data_tmp),
					'order_number' => $row->order_number,
					'discount_code' => $row->discount_code?$row->discount_code:'',
					'cart_subtotal' => $row->cart_subtotal,
					'cart_total' => $row->cart_total,
					'cart_total_items' => $row->cart_total_items,
					'customer_name' => $row->customer_name,
					'customer_phone' => $row->customer_phone,
					'gender' => $gender,
					'shipping_address' => ($row->shipping_fulladdress?$row->shipping_fulladdress:($row->address?$row->address:'')),//$shipping_address,
					'shipping_floor' => $row->shipping_floor?$row->shipping_floor:'',
					'shipping_on_loai' => $shipping_on_loai,
					'shipping_on' => $shipping_on,
					'shipping_on2' => $shipping_on2,
					'shipping_delay' => $row->shipping_delay?$row->shipping_delay:'',
					'shipper_id' => $row->shipper_id,
					'note' => $row->note,
					'print_count' => $row->print_count,
					'edit_count' => $edit_count,
					'time_created' => $row->time_created,
					'time_updated' => $row->time_updated?$row->time_updated:'',
					'status' => $row->status,
				);
				$danhsach[] = $order_tmp;
			}
			//echo '<br>';
		}
		//print_r($danhsach);
		if (count($danhsach)) {
			print_r($this->postdulieu(json_encode($danhsach)));
			echo '<a href="' . site_url('home/index/' . ($page + 1)) . '">Next ' . ($page + 1) . '</a>';
		}
		//$this->render_page('welcome_message');
	}
	function postdulieu($field1) {
		$url = 'http://admin.towndeli.vn/index.php/quanly/api/index2';
		$fields = array(
			'field1' => $field1
		);
		$postvars = http_build_query($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	private function timkiem($id, $data) {
		$tmp = 0;
		foreach ($data as $value) {
			if ($value->id==$id) {
				$tmp = $value->price;
			}
		}
		return $tmp;
	}
	function convertIntToShortCode($id = 0) {
		$id = intval($id);
		//echo PHP_INT_MAX . '<br>';
		$id=time()/10000;//rand(10000,999999);
		echo number_format($id).'-'.base_convert($id,20,36) . '<br>';
		//$id = PHP_INT_MAX;
		if ($id < 1) {
			throw new Exception(
				"The ID is not a valid integer");
		}

		$chars = "123456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";

		$length = strlen($chars);
		// make sure length of available characters is at
		// least a reasonable minimum - there should be at
		// least 10 characters
		if ($length < 10) {
			throw new Exception("Length of chars is too small");
		}

		$code = "";
		while ($id > $length - 1) {
			// determine the value of the next higher character
			// in the short code should be and prepend
			$code = substr($chars, ($id % $length), 1) . //fmod($id, $length)] .
				$code;
			// reset $id to remaining value to be converted
			$id = floor($id / $length);
		}

		// remaining value of $id is less than the length of
		// self::$chars
		$code = substr($chars, $id, 1) . $code;

		echo $code;
	}
	function index2() {
		$this->render_page('welcome_message');
	}
	private function render_page($page, $data=array()) {
		$this->load->view($page, $data);
	}
}
