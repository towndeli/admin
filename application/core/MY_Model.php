<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {
    public $table = null;
    public $options = null;
    public $primary_key = 'id';
    public $limit = 10;
    public $idv_length = 6;
    public $orderStatuses = array(
        0 => 'Inactive',
        1 => 'Active'
    );
    public $orderStatusClasses = array(
        0 => 'danger',
        1 => ''
    );
    public function __construct() {
        parent::__construct();
    }
    public function avatar($id) {
        $result = @file_get_contents(getCoverImageLink($id, $this->table));
        if (!$result) {
            $file = asset_url('img/default-'.$this->table.'.png');
            $result = file_get_contents($file);
            return $result;
        }
        return $result;
    }
    public function getNested($records) {
        return $records;
    }
    public function getIdv() {
        if (!isset($this->idv_length)) {
            $this->idv_length = 6;
        }
        $idv = '';
        while ($idv === '') {
            $randomString = strtolower(randomString($this->idv_length));
            $chk = $this->db->where('idv', $randomString)->count_all_results($this->table);
            if ($chk === 0) {
                $idv = $randomString;
            }
        }
        return $idv;
    }
    public function uploadHandler($id, $thongtin) {
    	/*$ci =& get_instance();
        $uniqueId = $ci->input->post('uniqueId');
        if (!empty($uniqueId)) {
            $this->load->library('Fileupload');
            $this->fileupload->saveImage($this->table . '/' . $id);
        }

        return !empty($uniqueId);*/
        $uniqueId = md5(time() . randomString(10));
        $dir = 'themes/quanly/assets/upload/' . $this->table . '/' . ($id?$id:$uniqueId);
        $target = FCPATH . $dir;
        @mkdir($target, 0777, true);
        $mconfig['upload_path'] = $target;
        $mconfig['allowed_types'] = '*';
        $mconfig['file_ext_tolower'] = true;
        $mconfig['file_name'] = 'coverImage';
        $mconfig['overwrite'] = true;
        //$mconfig['encrypt_name'] = true;
        $ci =& get_instance();
        $ci->load->library('upload', $mconfig);
        if ($ci->upload->do_upload('file')) {
            $myupload = $ci->upload->data();
            return $id?$id:$uniqueId;
        } else {
            return false;
        }
    }
    public function create($data = array()) {
        if (!empty($data)) {
            if (isset($data['password'])&&$data['password']) {
                $this->load->model('ion_auth_model');
                $salt = $this->ion_auth_model->store_salt ? $this->ion_auth_model->salt() : FALSE;
                $data['password'] = $this->ion_auth_model->hash_password($data['password'], $salt);
                if ($this->ion_auth_model->store_salt) {
                    $data['salt'] = $salt;
                }
            }
            $this->db->insert($this->table, $data);
            if ($this->db->affected_rows() > 0) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getAllActive() {
        $result = $this->getList(array('status' => 1), array('getAll' => true));
        if ($result === false) {
            $result = [];
        }

        return $result;
    }
    public function getList($wheres = array(), $options = array()) {
        $result = array();
        if (empty($options) && !empty($this->options)) {
            $options = $this->options;
        }
        /* START CACHE QUERY */
        $this->db->start_cache();
        // Selector
        if (!empty($options['selector'])) {
            $this->db->select($options['selector'], false);
        }
        // Group by
        if (!empty($options['group_by'])) {
            foreach ($options['group_by'] as $key) {
                $this->db->group_by($key);
            }
        }
        // Order by
        if (!empty($options['order_by'])) {
            foreach ($options['order_by'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        } else {
            // Sort default
            $this->db->order_by($this->table . '.id', 'DESC');
        }
        // Where
        if (!empty($wheres)) {
            foreach ($wheres as $key => $value) {
                if (is_array($value)) {
                    if (!empty($value)) {
                        $this->db->where_in($key, $value);
                    }
                } elseif(is_int($key)) {
                    $this->db->where($value);
                } else {
                    $this->db->where($key, $value);
                }
            }
        }
        // Filter
        // $this->dataFilters();
        $total = $this->db->count_all_results($this->table);
        /* STOP CACHE QUERY */
        $this->db->stop_cache();
        /* PAGING */
        if (!empty($options['limit']) && $options['limit'] > 0 && $options['limit'] <= 100) {
            $limit = (int) $options['limit'];
        } else {
            $limit = $this->limit;
        }
        if (!empty($options['offset'])) {
            $offset = (int) $options['offset'];
        } else {
            $offset = 0;
        }
        if (!empty($options['page']) && $options['page'] > 0) {
            $page = (int) $options['page'];
            $offset = ($page - 1) * $limit;
        } else {
            $page = ceil(($offset+1)/$limit);
        }
        if (isset($options['getAll'])) {
            $result = $this->db->get($this->table)->result();
        } else {
            $this->db->limit($limit, $offset);
            $result = array(
                'records' => $this->db->get($this->table)->result(),
                'paging' => array(
                    'total' => $total,
                    'limit' => $limit,
                    'offset' => $offset,
                    'page' => $page,
                    'pages' => ceil($total/$limit)
                ),
                'sql' => $this->db->last_query()
            );
        }
        /* FLUSH CACHE QUERY */
        $this->db->flush_cache();
        if (!isset($options['getAll'])) {
            if ($result['paging']['total'] > 0) {
                $result['records'] = $this->getNested($result['records']);
            }
        }
        return $result;
    }
    // Get list by field value
    public function getListBySQL($sql = '', $wheres = array()) {
        $result = $this->db->query($sql, $wheres)->result();
        return $result;
    }
    // Get list by field value
    public function getListBySQL2($sql = '', $options = array()) {
        $offset = 0;
        $limit = 10;
        $query = $this->db->query($sql);
        $total = $query->num_rows();
        if (!empty($options['page']) && $options['page'] > 0) {
            $page = (int) $options['page'];
            $offset = ($page - 1) * $limit;
        } else {
            $page = ceil(($offset+1)/$limit);
        }
        if (isset($options['getAll'])) {
            $result = $query->result();
        } else {
            //$this->db->limit($limit, $offset);
            $query = $this->db->query($sql.' limit '.$offset.', '.$limit);
            $result = array(
                'records' => $query->result(),
                'paging' => array(
                    'total' => $total,
                    'limit' => $limit,
                    'offset' => $offset,
                    'page' => $page,
                    'pages' => ceil($total/$limit)
                )
            );
        }
        if (!isset($options['getAll'])) {
            if ($result['paging']['total'] > 0) {
                $result['records'] = $this->getNested($result['records']);
            }
        }
        return $result;
    }
    // Get list by field value
    public function getDetailByField($field, $value, $selector = '*', $escape = true, $options = array()) {
        $this->db->select($selector, $escape)->where($field, $value);
        if (!empty($options['order_by'])) {
            foreach ($options['order_by'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        } else {
            // Sort default
            $this->db->order_by($this->table . '.id', 'DESC');
        }
        $result = $this->db->get($this->table)->row();
        if ($result) {
            $getNested = $this->getNested([$result]);
            $result = $getNested[0];
        }
        return $result;
    }
    // Get detail
    public function getDetail($id, $selector = '*', $escape = true) {
        return $this->getDetailByField('id', $id, $selector, $escape);
    }
    public function edit($data = array()) {
        $id = isset($data['id']) ? $data['id'] : '';
        unset($data['id']);
        if (!empty($data)) {
            if (isset($data['password'])&&$data['password']) {
                $this->load->model('ion_auth_model');
                $salt = $this->ion_auth_model->store_salt ? $this->ion_auth_model->salt() : FALSE;
                $data['password'] = $this->ion_auth_model->hash_password($data['password'], $salt);
                if ($this->ion_auth_model->store_salt) {
                    $data['salt'] = $salt;
                }
            } elseif (isset($data['password'])&&!$data['password']) {
                unset($data['password']);
            }
            return $this->update($data, $id);
        }
        return false;
    }
    public function update($data = NULL, $column_name_where = NULL) {
        if (isset($column_name_where)) {
            if (is_array($column_name_where)) {
                $this->db->where($column_name_where);
            } elseif (is_numeric($column_name_where)) {
                $this->db->where($this->primary_key, $column_name_where);
            } else {
                $column_value = (is_object($data)) ? $data->{$column_name_where} : $data[$column_name_where];
                $this->db->where($column_name_where, $column_value);
            }
        }

        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}