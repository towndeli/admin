var express = require('express');
var app = express();
var _ = require('lodash');
var sock = require('socket.io');
var server = app.listen(3000, function(){});
var io = sock.listen(server);
var users = [];
var orders = [];
io.on('connection', function(socket){
	socket.on('login', function(data){
		users.push({'id': data.id, 'socket': socket.id});
	});
	socket.on('logout', function(data){
		_.remove(users, function(user){
			return user.id == data.id;
		});
	});
	socket.on('getOrderEditLists', function(data){
		users.forEach(function(user, index){
			if (user.socket==socket.id) {
				io.to(user.socket).emit('messageReceived', {'action': 'orderEditLists', data: orders});
			}
		});
	});
	socket.on('removeEditLists', function(data){
		_.remove(orders, function(order){
			return order.id == data.id;
		});
		users.forEach(function(user, index){
			//if (user.socket!=socket.id) {
				io.to(user.socket).emit('messageReceived', {'action': 'removeEditLists', data: orders, id: data.id, order: data.order});
			//}
		});
	});
	socket.on('addEditLists', function(data){
		orders.push({id: data.id, user: data.user});
		users.forEach(function(user, index){
			//if (user.socket!=socket.id) {
				io.to(user.socket).emit('messageReceived', {'action': 'addEditLists', data: orders, id: data.id, order: data.order});
			//}
		});
	});
	socket.on('sendMessage', function(message){
		users.forEach(function(user, index){
			//if (user.socket!=socket.id) {
				io.to(user.socket).emit('messageReceived', message);
			//}
		});
	});
	/*socket.on('chinhsuahoadon', function(message){
		var tmp = -1;
		var tmp2;
		orders.forEach(function(order, index){
			if (order.id==message.order) {
				tmp = index;
			}
		});
		if (tmp==-1) {
			orders.push({id: message.order, user: message.user, item: message.item});
		} else {

		}
		users.forEach(function(user, index){
			io.to(user.socket).emit('chinhsuahoadon', {user: message.user, order: message.order, status: -1});
		});
	});*/
	socket.on('disconnect', function(){
		var myuser, myorder = [];
		users.forEach(function(user){
			if (user.socket == socket.id) {
				myuser = user.id;
				orders.forEach(function(order){
					if (order.user == myuser) {
						myorder.push(order.id);
					}
				});
			}
		});
		myorder.forEach(function(order){
			users.forEach(function(user){
				//if (user.socket!=socket.id) {
					io.to(user.socket).emit('messageReceived', {'action': 'removeEditLists', data: orders, id: order});
				//}
			});
		});
		_.remove(orders, function(order){
			return order.user == myuser;
		});

		_.remove(users, function(user){
			return user.socket == socket.id;
		});
	});
});
//modules: lodash, express, request, socket.io
