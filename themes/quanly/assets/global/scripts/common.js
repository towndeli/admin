var _mydata = [];



$(document).ready(function(){

    
    $('*').on('touchstart', function () {
        $(this).trigger('hover');
    }).on('touchend', function () {
        $(this).trigger('hover');
    });
    
    
    $(this).on('click', 'a[rel="nofollow"], a._blank, ._blank a', function(){
        $(this).attr('target', '_blank');
    });
    
    $(this).on('keyup', function(e){
        k = e.keyCode;
        
        if (k === 13) {
            $(':visible[accesskey="e"]').last().click();
            $(this.activeElement).next().children('button:first').click();
        }
    });
    
    /*
    $('body').append('<div class="dialogs"></div><a id="back-to-top"></>');
    $('#back-to-top').click(function(){$('html, body').animate({scrollTop:0},800);});
    $(window).scroll(function(){
        if($(window).scrollTop() != 0) {
            $('#back-to-top').fadeIn();
        }
    	else $('#back-to-top').fadeOut();
    });
    */
    
    /*
    $(this).on('mouseover', 'input[class^="date-"]', function(){
        if ($(this).hasClass('date-picker') === false) {
            
            $(this).addClass('date-picker').datepicker({
                format: 'dd-mm-yyyy'
            }).on('changeDate', function(e){
                $('#' + e.currentTarget.id).attr('data-change', my_data('temp:' + e.currentTarget.id));
            });
        }
    });
    
    $(this).datepicker({
        format: 'dd-mm-yyyy'
    }).on('changeDate', function(e){
        $('#' + e.currentTarget.id).attr('data-change', my_data('temp:' + e.currentTarget.id));
    });
    */


    // Captcha
    $(this).on('click', '.captcha', function(){
        time = new Date().getTime();
        if ($(this)[0].tagName === 'IMG') {
            _captcha = $(this);
        }
        else {
            _captcha = $(this).parent().siblings('.input-group-addon').children('.captcha');
        }
		_src = _captcha.attr('src') + '?';
		_src = _src.split('?');
		_captcha.attr('src', _src[0] + '?' + time);
		$('#captcha').val('');
    });


    // Handle event for radio
    $(this).on('click', '.md-radio', function(){
        var radio = $('#' + $(this).children('input:eq(0)').attr('name'));
        var value = $(this).data('value');
        radio.val(value);
        radio.data('origin', value);
    });

    // Set flag for data changes
    $(this).on('focus', 'input, select, textarea', function(){
        if (typeof $(this).data('origin') === 'undefined') {
            $(this).data('origin', getValue($(this)));
        }
    });

    /*$(this).on('change', 'input, select, textarea', function(){
        if ($(this).data('origin') != getValue($(this))) {
            $(this).addClass('dataChanged');
        } else {
            $(this).removeData('origin');
            $(this).removeClass('dataChanged');
        }
    });*/
    
});


function fileUploader()
{
    $('.btnUpload, .previewUpload').click(function(){
        $('.fileupload').trigger('click');
    });
    $('.fileupload').fileupload({
        url: URL.upload,
        dataType: 'json',
        add: function (e, data) {
            $(this).next('.progress').children('.progress-bar').css('width', '0%').text('0%');
            data.formData = {
                'unique': $(this).data('unique'),
                'multiple': $(this).is('[multiple]') ? 1 : 0,
            };
            data.submit();
        },
        done: function (e, data) {
            var thumbnail = $('.previewUpload');
            $.each(data.result.files, function (index, file) {
                thumbnail.html('<img class="img-responsive" src="' + file.url + '"/>');
            });
            $(this).data('origin', true);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            var progressText = progress + '%';
            $(this).next('.progress').children('.progress-bar').css('width', progressText).text(progressText);
        }
    });
}


var Data = {
    set: function(key, value) {
        _mydata[key] = value;
    },
    get: function(key) {
        return (typeof _mydata[key] !== 'undefined') ? _mydata[key] : null;
    },
    remove: function(key) {
        delete _mydata[key];
    }
};


function getValue(obj)
{
    if (typeof obj === 'string') {
        obj = $(obj);
    }
    
    val = obj.val();
    
    if (obj.is('input:checkbox')) {
        val = obj.is(':checked') ? 1 : 0;
    } else if (obj.prop('tagName') === 'SELECT') {
         if (typeof obj.children('option:selected').attr('value') === 'undefined') {
            val = '';
         } else {
            val = obj.val();
            if (typeof obj.attr('multiple') !== 'undefined') {
                val = obj.val().join(',');
            }
         }
    }
    
    return val;
}


function getForm(form, act, model, getAll)
{
    var F = $(form);
    var params = {};
    
	var i = 0; // Total input data
    var required = 0; // Total input required data which empty

    F.find('input, select, textarea').each(function() {
        var name = $(this).prop('name');
        if (name !== '') {
            var value = getValue($(this));
            // Check required
            if ($(this).prop('required') === true && value === '') {
                required = 1;
            }
            // Get data input
            if ((typeof $(this).data('origin') !== 'undefined' && $(this).data('origin') !== value) || (getAll === true && value !== '')) {
                params[name] = $.trim(value);
                i++;
            }
        }
    });

    var uploader = F.find('.fileupload:first');
    if (uploader.length === 1) {
        if (typeof uploader.data('origin') !== 'undefined' && typeof uploader.data('unique') !== 'undefined') {
            params.uniqueId = uploader.data('unique');
            i++;
        }
    }
    
    if (required > 0) {
        return false;
    }
    else if (i === 0) {
        return null;
    }
    else {
        var idx = F.find('.idx:first');
        if (idx.length === 1) {
            var num = parseInt(idx.text());
            if (num > 0) {
                params.id = num;
            }
        }
        params['useAction'] = act;
        params['useModel'] = model;

        return params;
    }
}


function resetForm(form, keepData)
{
    if (typeof keepData === 'undefined') {
        keepData = false;
    }
    var F = $(form);
    F.find('input, select, textarea').each(function() {
        if (keepData === false) {
            var tagName = this.tagName;
            this.value = '';
            if (tagName === 'SELECT') {
                $(this).children('option[value=""]').prop('selected', true);
            } else {
                $(this).prop('checked', false);
            }
        }
        $(this).removeData('origin');
        $(this).removeClass('dataChanged');
    });

    var uploader = F.find('.fileupload:first');
    if (uploader.length === 1) {
        uploader.removeData('origin');
    }
}


// Chuyển mã màu dạng RGB sang HEX
function rgb2hex(rgb) {
    
    if (typeof rgb === 'undefined' || rgb === 'transparent' || rgb == 'rgba(0, 0, 0, 0)') {
        return '';
    }
    
    if (  rgb.search('rgb') == -1 ) {
        return rgb;
    }
    else {
        rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
        function hex(x) {
            return ('0' + parseInt(x).toString(16)).slice(-2);
        }
        return hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]); 
    }
}


function url_title(str)
{
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/gi, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/gi, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/gi, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/gi, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/gi, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/gi, 'y');
    str = str.replace(/đ/gi, 'd');
    
    str = str.replace(/[^a-zA-Z0-9]/g, '-');
    str = str.replace(/--/g, '');
    
    return str;
}


function getToken()
{
    return '&csrf_token=' + $.cookie('csrf_cookie');
}


function cut_str(str, pos1, pos2)
{
	str		= str.toString();
	strlen 	= str.length;
    if (typeof pos2 === 'undefined') {
        pos2 = strlen;
    }
	if (pos1 > strlen) {
	   return false;
	}
	pos2 = ((pos1 + pos2) > strlen) ? (strlen - pos1 + 1) : pos2;
	return (str.substring(0, pos1 - 1) + str.substring(pos1 + pos2 - 1, strlen));
}


function validateURL(value)
{
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
}


function thumb(uri, width, height, is_cached)
{
    w = (typeof width !== 'undefined') ? '&amp;w=' + width : '';
    h = (typeof height !== 'undefined') ? '&amp;h=' + height : '';
    if (typeof is_cached === 'undefined')
    {
        t = new Date();
        cache = '&amp;t=' + t.getTime()
    }
    else cache = '';
    
    return (typeof controller !== 'undefined' ? controller : '/') + 'thumb.php?path=' + encodeURIComponent(uri) + w + h + cache;
}


function img_path(input)
{
    if (input.indexOf('thumb.php?path=') > 0)
    {
        path = input.split('thumb.php?path=')[1];
        path = path.split('&')[0];
    }
    else if (input.indexOf('?') > 0) path = input.split('?')[0];
    else path = input;
    
    return path;
}


function parseNum(input)
{
    input   = input + '';
    input   = input.replace(',', '');
    input   = input.replace('.', '');
    strlen  = input.length;
    number  = '';
    
    for (i = 0; i < strlen; i++)
    {
        num = parseInt(input.charAt(i));
        if (num >= 0 && num <= 9) number += num;
        else number += '';
    }
    
    if (number === '') number = false;
    else if (input.charAt(0) === '-') number = '-' + number;
    
    return number === false ? 0 : parseInt(number);
}


function scroll(el, speed)
{
    if (typeof el !== 'object') {
        el = $(el);
    }
	
	if (typeof speed == 'undefined') {
	   speed = 0;
	}
    /*
	$('html,body').animate({
			scrollTop: el.offset().top
		}, speed);*/
}


function modalBox(content, options)
{
    var params = $.extend(true, {
        title: null,
        callback: null,
        button: true
    }, options);
    
    $('.myDialogBox').remove();

    var button = '';
    if (params.button) {
        button = `
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>`;
    }
    var modal = `
            <div class="modal fade myDialogBox" tabindex="-1" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">` + (params.title !== null ? params.title : '') + `</h4>
                  </div>
                  <div class="modal-body">
                    ` + content + `
                  </div>
                  ` + button + `
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->`;

    $('body').append(modal);
    $('.myDialogBox').modal();

    if (params.callback !== null) {
        $('.myDialogBox button').one('click', function(){
            params.callback();
        });
    }
}


function confirmBox(content, callbacks)
{
    $('.myConfirmBox').remove();
    var modal = `
            <div class="modal fade myConfirmBox" tabindex="-1" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Xác nhận thông tin</h4>
                  </div>
                  <div class="modal-body">
                    ` + content + `
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default btnConfirmBoxCancel" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Hủy</button>
                    <button type="button" class="btn btn-success btnConfirmBoxOk"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Đồng ý</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
    `;
    $('body').append(modal);
    $('.myConfirmBox').modal();

    if (typeof callbacks !== 'undefined') {
        // on OK
        if (typeof callbacks.onOK !== 'undefined') {
            $('.myConfirmBox .btnConfirmBoxOk').one('click', function(){
                callbacks.onOK();
                $('.myConfirmBox').modal('hide');
            });
        } 
        // on Cancel
        if (typeof callbacks.onCancel !== 'undefined') {
            $('.myConfirmBox .btnConfirmBoxCancel').one('click', function(){
                callbacks.onCancel();
                $('.myConfirmBox').modal('hide');
            });
        } 
    } else {
        $('.myConfirmBox').modal('hide');
    }
}


function user_changepw(pos)
{
    if ($('#pw_current').val() === '')
    {
        msgBox('Hãy nhập mật khẩu hiện thời của bạn!');
        return false;
    }
    
    if ($('#password').val() === '')
    {
        msgBox('Bạn chưa nhập mật khẩu mới!');
        return false;
    }
    
    if ($('.pw_confirm').val() === '')
    {
        msgBox('Bạn chưa nhập mật khẩu xác nhận!');
        return false;
    }
    
    if ($('#password').val() !== $('.pw_confirm').val())
    {
        msgBox('Mật khẩu xác nhận không trùng khớp!');
        return false;
    }
    
    $.ajax({
        data: getData(pos, 'modify', 'user', true),
        success: function(response)
        {
            res = parse_json(response);
            
            if (res.result === 'OK')
            {
                msgBox('Đã thay đổi mật khẩu mới!');
                $('#pw_current').removeClass('changed').val('');
                $('#password').removeClass('changed').val('');
                $('.pw_confirm').removeClass('changed').val('');
                //$('a[href="#tab_info"]').click();
            }
            else
            {
                msgBox(res.alert);
            }
        }
    });
}
