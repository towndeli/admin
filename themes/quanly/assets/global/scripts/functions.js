$(document).ready(function(){

    $.ajaxSetup({
		url: PARAMS.ajaxURL,
		type: 'POST',
    });

	fileUploader();

    $(this).on('keyup', function(e) {
        if (e.keyCode === 27) {
        	$('.orderCustomerInfo').removeClass('show');
        }
    });


    if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true
        });
        //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
    }

	$('.datePicker').datepicker({
		format: 'yyyy-mm-dd'
	});


    $('.select2').select2();


    /*$(this).on('click', '.btnReport', function(e){
    	var wrapper = '.reportData';
    	var reportDate = $('[name="reportDate"]').val();
    	$.ajax({
    		type: 'POST',
    		dataType: 'html',
    		url: '',
    		data: {range: reportDate},
    		success: function(res) {
    			$(wrapper).html($(res).find(wrapper).html());
    			$('script:not([src])').each(function(){
					$(this).remove();
					$('body').append('<script>' + $(this).text() + '</script>');
    			});
    		}
    	});
    });


    $(this).on('click', '.btnReportAll', function(e){
    	$('[name="reportDate"]').val('');
    	$('.btnReport').trigger('click');
    });*/


    /*$(this).on('click', '.btnDataListSubmit, .btnDataListReset', function(){
    	var wrapper = '.dataList';
    	var dataListSearch;
    	if ($(this).hasClass('btnDataListSubmit')) {
    		dataListSearch = getForm('.dataListSearch');
    	} else {
    		resetForm('.dataListSearch');
    		dataListSearch = {};
    	}

    	$.ajax({
    		type: 'POST',
    		dataType: 'html',
    		url: '',
    		data: {filters: dataListSearch},
    		success: function(res) {
    			$(wrapper).html($(res).find(wrapper).html());
    			$('script:not([src])').each(function(){
					$(this).remove();
					$('body').append('<script>' + $(this).text() + '</script>');
    			});
    		}
    	});
    });*/


	// Check/Uncheck multiple checkbox
	$(this).on('click', '.selectAllRows', function(){
		var status = $(this).is(':checked');
		var rows = $(this).closest('thead').next('tbody').find('.selectRow');
		rows.prop('checked', status);
		if (status) {
			rows.parent().addClass('checked');
		} else {
			rows.parent().removeClass('checked');
		}
	});


	// Fix select row on load
	$('.selectRow').prop('checked', false).parent().removeClass('checked');


    /*$(this).on('click', '.md-checkbox span', function(){
        var cbox = $(this).closest('.md-checkbox').children('input:eq(0)');
        var oldStatus = cbox.is(':checked') ? 1 : 0;
        var status = oldStatus ? 0 : 1;
        
        if (typeof cbox.data('origin') === 'undefined') {
            cbox.data('origin', oldStatus);
        }

		var id = $('.form .idx:eq(0)');
		var inList = true;
		if (id.length === 1) {
			id = id.text();
			inList = false;
		} else {
			var row = $(this).closest('[data-id]');
			id = row.data('id');
		}

		if (inList) {
			$.ajax({
				data: {
					useModel: PARAMS.model,
					useAction: 'update',
					id: id,
					status: status
				},
				success: function(){}
			});
		}
    });*/


	// Create OR Update event
	/*$(this).on('click', '.btnUpdate', function(){
		var isCreateNew = $('.idx').text() === '' ? true : false;
		var action = $('.idx').text() === '' ? 'create' : 'edit';

		var data = getForm('.form', action, PARAMS.model, isCreateNew);

		if (data === false) {
			modalBox('<h4>Hãy nhập đủ thông tin!</h4>');
			return;
		} else if (data === null) {
			modalBox('<h4>Không có dữ liệu nào thay đổi!</h4>');
			return;
		} else {
			$.ajax({
				data: data,
				success: function(res) {
					if (isCreateNew) {
						modalBox('Đã tạo mới!', {
							title: 'Tạo mới thông tin',
							callback: function(){
								window.location.href = window.location.href;
							}
						});
					} else {
						modalBox('<h4>Đã cập nhật!</h4>', {
							title: 'Cập nhật thông tin',
							callback: function(){
								resetForm('.form', !isCreateNew);
							}
						});
					}

					if (isCreateNew) {
						$('[name="status"]').prop('checked', true);
					}
				}
			});
		}
	});*/


	// Delete item (hide)
	$(this).on('click', '.btnDelete', function(){
		var id = $('.form .idx:eq(0)');
		var inList = true;
		if (id.length === 1) {
			id = id.text();
			inList = false;
		} else {
			var row = $(this).closest('[data-id]');
			id = row.data('id');
		}

		if (id > 0) {
			confirmBox('<h4 style="color:red">Bạn muốn xóa dữ liệu này?</h4>', {
				onOK: function(){
					$.ajax({
						data: {useModel: PARAMS.model, useAction: 'remove', id: id},
						success: function(res) {
							if (inList) {
								row.remove();
							} else {
								
							}
						}
					});
				}
			});
		}
	});


	/*$(this).on('change', '.locationCity', function(){
		var selected = $(this).children('option:selected');
		var forId = $(this).data('for');
		var districtBox = $('#' + forId);
		if (districtBox.length === 0) {
			return;
		}

		districtBox.children('option:gt(0)').remove();

		if (selected.length === 1) {
			$.ajax({
				dataType: 'json',
				data: {useModel: 'cities', useAction: 'getListDistrict', city_id: selected.val()},
				success: function(res) {
					$.each(res.data, function(i, r){
						districtBox.append('<option value="' + r.id + '">' + r.name + '</option>');
					});
				}
			});
		}
	});*/


	$(this).on('click', '.btnAddToppings', function(){
		if ($('.selectRow:checked').length === 0) {
			alert('Chọn thức uống/bánh trước khi thêm topping');
			return;
		}
		$('#modal_addToppings').modal();
	});
	$(this).on('click', '.listTopping li', function(){
		$(this).toggleClass('selected');
	});
	$(this).on('click', '.btn_addToppings_save', function(){
		var _items = $('.selectRow:checked');
		var _toppings = $('.listTopping li.selected');
		if (_toppings.length === 0) {
			alert('Chọn topping');
			return;
		}
		var items = [];
		var toppings = [];
		$.each(_items, function(i,v){
			items.push($(v).closest('[data-id]').data('id'));
		});
		$.each(_toppings, function(i,v){
			toppings.push($(v).val());
		});
		$.ajax({
			dataType: 'json',
			data: {useModel: 'menu', useAction: 'setToppings', items: items, toppings: toppings},
			success: function(res) {
				if (res.result === true) {
					alert('Cập nhật xong');
					$('#modal_addToppings').modal('hide');
					_items.prop('checked', false);
					_items.parent().removeClass('checked');
					_toppings.removeClass('selected');
				}
			}
		});
	});


	$('select.select2.tags').select2({
		tags: true,
	}).on('select2:select select2:unselect', function(e){
		$(this).data('origin', 1);
	});

	$('select.select2').select2({
		placeholder: 'Vui lòng chọn',
	}).on('select2:select', function(e){
		$(this).data('origin', 1);
	});


	$(this).on('change', '[name="branch_manage"]', function(){
		var value = $(this).val();
		var action = $(this).val() == 1 ? 'open' : 'close';
		$.ajax({
			dataType: 'json',
			data: {useModel: 'branch', useAction: action, within: value},
			success: function(res) {
				
			}
		})
	});







	/* ADD/UPDATE ORDER */
	/*$(this).on('click', '.orderMenuItems a, .btnOrdersCartEdit', function(){
		if ($(this).hasClass('btnOrdersCartEdit')) {
			data = {
				rowid: $(this).closest('[data-id]').data('id')
			};
		} else {
			data = {
				id: $(this).parent().data('id'),
				size:  $(this).data('size')
			};
		}

		$.ajax({
			url: PARAMS.moduleURL + '/orders-menu',
			type: 'POST',
			data: data,
			success: function(res) {
				$('#modal_addMenu .modal-body').html(res);
				$('#modal_addMenu').modal();
			}
		});
	});*/


	// Chọn size
	$(this).on('click', '.ordersMenuSizes >a', function(){
		$(this).siblings('a').removeClass('selected');
		$(this).addClass('selected');
	});


	// Thêm topping
	/*$(this).on('click', '.ordersMenuTopping >a', function(){
		var countObj = $(this).children('.badge');
		var count = countObj.text() !== '' ? parseInt(countObj.text()) : 0;
		countObj.text(count+1);
	});

	// Giảm topping
	$(this).on('click', '.ordersMenuTopping >a >.btnOrdersToppingDecrease', function(e){
		e.stopPropagation();
		var countObj = $(this).siblings('.badge');
		var count = countObj.text() !== '' ? parseInt(countObj.text()) : 0;
		if (count > 0) {
			count--;
		}

		countObj.text(count);
	});*/


	// Add item
	/*$(this).on('click', '.btn_addMenuSave', function(){
		var rowid = $('#modal_addMenu [data-rowid]').data('rowid');
        var menu_id = $('#modal_addMenu [data-id]').data('id');
        var size = $('#modal_addMenu .ordersMenuSizes a.selected').data('field');
        var note = $('#modal_addMenu #ordersMenuNote').val();

        var quantity = parseInt($('#ordersMenuQuantity').val());
        var toppingData = {};
        var toppings = $('#modal_addMenu .ordersMenuTopping >a');
        if (toppings.length > 0) {
            $.each(toppings, function(i, val){
                var t_id = $(this).data('id');
                var t_qty = $(this).children('.badge').text();
                if (t_qty > 0) {
                	toppingData[t_id] = t_qty;
                }
            });
        }

        var data = {
                id: menu_id,
                qty: quantity,
                options: {
                	size: size,
                	toppings: toppingData
                }
        };
        if (note != '') {
        	data.options.note = note;
        }

        // Edit item
        var ajaxType = 'POST';
        if (rowid !== '') {
        	data.rowid = rowid;
        	ajaxType = 'PUT';
        }

        $.ajax({
            url: API.cart,
            type: ajaxType,
            data: data,
            success: function(res) {
            	loadOrderCart();
            	$('#modal_addMenu').modal('hide');
            }
        });
	});*/


	// Change quantity
	$(this).on('change', '[name="orderItemQty"]', function(){
		var rowid = $(this).closest('[data-id]').data('id');
		var qty = parseInt($(this).val());

		$.ajax({
			url: API.cart,
			type: 'PUT',
			data: {
				rowid: rowid,
				qty: qty
			},
			success: function(res) {
				loadOrderCart();
			}
		});
	});


	// Remove item
    /*$(this).on('click', '.btnOrdersCartRemove', function(){
        if (!confirm('Bạn muốn bỏ món này?')) {
            return false;
        }
        var row = $(this).closest('[data-id]');

	    $.ajax({
	        url: API.cart,
	        type: 'DELETE',
	        data: {
	        	rowid: row.data('id')
	        },
	        success: function(res) {
	        	if (res === true) {
	        		loadOrderCart();
	        	}
	        }
	    });
    });*/


    // Clean cart
    $(this).on('click', '.btnOrderClean', function(){
    	if (!confirm('Bạn có muốn hủy giỏ hàng?')) {
    		return false;
    	}

    	$.ajax({
    		url: API.cart,
    		type: 'DELETE',
    		success: function() {
				// resetForm('.orderCustomerInfo');
				// $('.orderCustomerInfo').removeClass('show');
				// loadOrderCart();
				window.location.href = URL.module + '/orders';
    		}
    	});
    });



    // Discount code
    /*$(this).on('click', '.orderCartDiscounts a', function(){
    	var obj = $('.orderCartDiscountCode');
    	obj.val($(this).text());
		var e = jQuery.Event('keypress');
		e.which = 13
		e.keyCode = 13;
		obj.trigger(e);
    });

    $(this).on('keypress', '.orderCartDiscountCode', function(e){
    	if (e.keyCode === 13) {
    		var slf = $(this);
    		var code = slf.val();
    		$.ajax({
    			url: API.cart + '/discount',
    			type: 'POST',
    			data: {discount_code: code},
    			success: function(res) {
    				if (res.result == true) {
    					loadOrderCart();
    				} else {
    					alert('Mã giảm giá không thể sử dụng!');
    					slf.val('');
    				}
    			}
    		});
    	}
    });*/


    // Show/Hide customer information
    $(this).on('click', '.btnCustomerInfoToggle', function(){
    	$('.orderCustomerInfo').toggleClass('show');
    });
    // Auto show on create order page
	if ((window.location.href).indexOf('createorder') > -1) {
		window.setTimeout(function () {
            $('.orderCustomerInfo').toggleClass('show');
        }, 1000);
	}



    // Create order
    /*$(this).on('click', '.btnOrderCreate', function(){

    	if ($('.orderCartInfo tr[data-id]').length === 0) {
    		modalBox('Vui lòng đặt món!');
    		return;
    	}

		var isCreateNew = $('.idx').text() === '' ? true : false;
		var action = $('.idx').text() === '' ? 'create' : 'edit';
		var data = getForm('.orderCustomerInfo', action, PARAMS.model, true);

    	if (data === null || data === false) {
    		var customerInfo = $('.orderCustomerInfo');
    		if (customerInfo.hasClass('show')) {
    			alert('Vui lòng nhập đủ thông tin!');
    		}
    		$('.orderCustomerInfo').addClass('show');
    		return false;
    	}

    	var discount_code = $('.orderCartDiscountCode').val();
    	if (discount_code !== '') {
    		data.discount_code = discount_code;
    	}

    	var pointUsed = parseInt($('.orderUsePoint input').val()) || 0;
    	if (pointUsed > 0) {
    		data.points_used = pointUsed;
    	}

    	var confirmText = 'Xác nhận ' +  (isCreateNew ? 'tạo' : 'cập nhật') + ' đơn hàng này?';
    	if (!confirm(confirmText)) {
    		return false;
    	}

    	if (typeof data === 'object') {
	    	var fulladdress = [];
	    	['shipping_floor', 'shipping_address', 'shipping_ward', 'shipping_district'].forEach(function(field){
	    		if (typeof data[field] !== 'undefined' && data[field] !== '') {
	    			var value = data[field];
	    			if (field === 'shipping_district') {
	    				value = $('.orderCustomerInfo [name="' + field + '"] option:selected').text();
	    			}
	    			fulladdress.push(value);
	    		}
	    	});

	    	if (fulladdress.length > 0) {
	    		data.shipping_fulladdress = fulladdress.join(', ');
	    	}
    	}

    	$.ajax({
    		data: data,
    		dataType: 'json',
    		success: function(res) {
    			if (res.result == true) {
    				window.location.href = URL.module + '/orders';
    			} else {
    				alert('Có lỗi! Vui lòng thử lại!');
    			}
    		}
    	});
    });*/


    // Edit order number
    $(this).on('click', '.dataList b.orderNo', function(){
    	if (this.nodeName === 'B' && $(this).closest('[data-id]').find('[name="status"] >option:selected').val() == 1) {
    		var value = $(this).text();
    		$(this).replaceWith('<input type="text" class="orderNo" style="width:40px;height:20px;padding:2px" value="' + value + '" />');
    	}
    });
    $(this).on('keyup', '.dataList input.orderNo', function(e){
    	if (e.keyCode === 13) {
    		var slf = $(this);
    		var id = $(this).closest('[data-id]').data('id');
			var value = $(this).val();
			$.ajax({
				data: {
					useModel: 'orders',
					useAction: 'edit',
					id: id,
					order_number: value
				},
				success: function(){
					slf.replaceWith('<b class="orderNo" >' + value + '</b>');
				}
			});
    	}
    });


    // Cancel order
	$(this).on('click', '.btnOrderCancel', function(){
		var slf = $(this);
		var row = $(this).closest('[data-id]');
		var id = $('.form .idx:eq(0)');
		var inList = true;
		if (id.length === 1) {
			id = id.text();
			inList = false;
		} else {
			id = row.data('id');
		}

		if (id > 0) {
			confirmBox('<h4 style="color:red">Bạn muốn hủy đơn hàng này?</h4>', {
				onOK: function(){
					$.ajax({
						data: {useModel: PARAMS.model, useAction: 'cancel', id: id},
						success: function(res) {
							if (inList) {
								row.find('[name="status"] >option[value="5"]').prop('selected', true);
								row.find('[name="status"]').prop('disabled', true);
								slf.remove();
							} else {}
						}
					});
				}
			});
		}
	});


    // Autofill customer info
    $(this).on('keyup', '#orderCustomerPhone', function(e){
    	if (e.keyCode === 13) {

	    	var phone = $(this).val();
	    	if (/(\d{10,11})/.test(phone)) {
	    		$.ajax({
	    			url: URL.site + 'api/users/find_by_phone',
	    			data: {phone: phone},
	    			success: function(res) {

	    				$('#orderCustomerNotification').html('');
    					// $('#orderCustomerId').val('');
    					// $('#orderCustomerName').val('');
    					$('#orderCustomerAddresses').html('');
    					$('#orderCustomerGender +div >div[data-value="1"] >label').trigger('click');
    					resetForm('.orderCustomerInfo');
    					$('#orderCustomerPhone').val(phone);

    					$('.orderLastShipping').html('');

	    				if (res.result === true) {
	    					var user = res.data;
	    					$('#orderCustomerId').val(user.id);
	    					$('#orderCustomerName').val(user.firstname);
	    					$('#orderCustomerGender').val(user.gender);
	    					$('#orderCustomerGender +div >div[data-value="' + user.gender + '"] >label').trigger('click');

	    					$('#orderCustomerNotification').html('<b style="color:red">[ID KH: ' + user.id + ']' + (user.points > 0 ? ' (Điểm: <span class="userPoint">' + user.points + '</span>)' : '') + '</b>');

	    					var addresses = [];
	    					if (user.addresses.length > 0) {
	    						addresses.push('<option value="">-- Chọn địa chỉ giao hàng --</option>');
	    						user.addresses.forEach(function(address){
	    							var dataAttr = [
	    								'data-floor="' + address.shipping_floor + '"',
	    								'data-address="' + address.shipping_address + '"',
	    								'data-ward="' + address.shipping_ward + '"',
	    								'data-district="' + address.shipping_district + '"',
	    							];
	    							addresses.push('<option ' + dataAttr.join(' ') + '">' + address.shipping_fulladdress + '</option>');
	    						});
	    					}
	    					if (addresses.length > 0) {
	    						$('#orderCustomerAddresses').html('<div class="form-group"><div class="col-lg-12"><select class="form-control select2">' + addresses.join('') + '</select></div></div>');
	    						// $('.select2').select2();
	    					}


	    					if (user.last_shipping.length > 0) {
	    						addresses = [];
	    						user.last_shipping.forEach(function(i){
	    							addresses.push('<option>' + i + '</option>');
	    						});
	    						$('.orderLastShipping').html('<select class="form-control" style="background-color:#E6E6E6">' + addresses.join('') + '</select>');
	    					}
	    					
	    				} else {
	    					$('#orderCustomerNotification').html('<b style="color:red">[Khách hàng mới]</b>');
	    				}
	    			}
	    		});
	    	} else {
	    		alert('Vui lòng nhập số ĐT bao gồm 10 hoặc 11 số!');
	    	}
    	}
    });


    $(this).on('change', '#orderCustomerAddresses select', function(){
    	var selected = $(this).children('option:selected');

    	if (selected.val() === '') {
	    	var floor = '';
	    	var address = '';
	    	var ward = '';
	    	var district = '';
    	} else {
	    	var floor = selected.data('floor');
	    	var address = selected.data('address');
	    	var ward = selected.data('ward');;
	    	var district = selected.data('district');
    	}
    	
    	$('.orderCustomerInfo [name="shipping_floor"]').val(floor);
    	$('.orderCustomerInfo [name="shipping_address"]').val(address);
    	$('.orderCustomerInfo [name="shipping_ward"]').val(ward);
    	$('.orderCustomerInfo [name="shipping_district"] >option[value="' + district + '"]').prop('selected', true);
    });


    // button show more
    $(this).on('click', '.btnShowMore', function(){
    	var content = $(this).next();
    	if (content.is(':visible')) {
    		content.fadeOut();
    	} else {
    		content.fadeIn();
    	}
    });


    var selectBoxValue;
    $(this).on('focus', 'select', function(){
    	selectBoxValue = $(this).val();
    });
    // update selectbox data in list
    /*$(this).on('change', '.dataList td select[name]', function(){
    	var id = $(this).closest('[data-id]').data('id');
    	var obj = $(this).children('option:selected');
    	var prev = obj.prev();
    	var next = obj.next();
    	var isStep = $(this).hasClass('changeByStep');

    	var name = $(this).attr('name');
    	var value = obj.val();

    	var data = {
				useModel: 'orders',
				useAction: 'edit',
				id: id
			};
		data[name] = value;

		var prevObj = $(this).children('option[value="' + selectBoxValue + '"]');
    	
    	confirmBox('<h4>Bạn có muốn đổi thông tin "' + prevObj.text() + '" thành "' + obj.text() + '" ?</h4>', {
    		onOK: function(){
    			$.ajax({
    				dataType: 'json',
    				data: data,
    				success: function(res) {
    					if (res.result === true) {
    						if (isStep) {
	    						prevObj.prop('disabled', true);
	    						next.prop('disabled', false);
	    						if (next.length === 0) {
	    							obj.parent().prop('disabled', true);
	    						}
    						}
    					} else {
    						prevObj.prop('selected', true);
    					}
    				}
    			});
    		},
    		onCancel: function(){
    			prevObj.prop('selected', true);
    		}
    	});
    });*/



    /* PRINT ORDER */
    /*$(this).on('click', '.btnOrderPreview', function(){
    	var id = $(this).closest('[data-id]').data('id');

    	$.ajax({
    		url: PARAMS.moduleURL + '/orders-print',
    		data: {id: id},
    		success: function(res) {
    			modalBox(res, {
    				title: '<b>IN ĐƠN HÀNG</b>',
    				button: false
    			});
    		}
    	});
    });*/

    /*$(this).on('click', '.btnOrderPrint', function(){
    	var id = $(this).data('id');
    	$('#orderPrintBLock').print();
    	// Cập nhật số lần đã in đơn hàng /
    	$.ajax({
    		data: {
    			id: id,
    			useModel: 'orders',
    			useAction: 'printing'
    		}
    	});
    });*/



    /* ORDER DELAY NOTIFICATION */
    $(this).on('click', '.btnOrderDelay', function(){
    	var slf = $(this);
    	var row = slf.closest('[data-id]');
    	var id = row.data('id');
    	var statusObj = row.find('[name="status"]');
    	var status = statusObj.children('option:selected').val();

    	if (status == 0) {
    		alert('Đơn hàng phải ở trạng thái [Đang xử lý]!');
    		return false;
    	}

    	confirmBox('<h4>Bạn muốn báo trễ cho đơn hàng này?</h4>', {
    		onOK: function(){
    			$.ajax({
    				dataType: 'json',
    				data: {useModel: 'orders', useAction: 'delay', id: id},
    				success: function(res) {
    					if (res.result === true) {
    						alert('Đã báo trễ!');
    						slf.remove();
    					} else {
							alert('Gửi sms thông báo không thành công!');
    					}
    				}
    			});
    		}
    	});
    });


	$(this).on('click', '.listTopping li', function(){
		$(this).toggleClass('selected');
	});


});


function loadOrderCart()
{
	$.ajax({
		url: PARAMS.moduleURL + '/orders-cart',
		success: function(res) {
			$('.orderCartInfo').html(res);
		}
	});
}