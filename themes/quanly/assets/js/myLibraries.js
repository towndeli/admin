'use strict';
angular.module('myLibrary', [])
    .factory('mySocket', function (socketFactory, myConfig) {
        var myIoSocket = io.connect(myConfig.socket_url);
        var mySocket = socketFactory({
            ioSocket: myIoSocket
        });
        return mySocket;
    })
    .factory('myAudio', function (ngAudio, myConfig) {
        var sound_new = ngAudio.load(myConfig.asset_url + "sounds/new-order.mp3");
        var sound_transfer = ngAudio.load(myConfig.asset_url + "sounds/transfer.mp3");
        var sound_transfer_back = ngAudio.load(myConfig.asset_url + "sounds/transfer-back.mp3");
        var sound_cancel = ngAudio.load(myConfig.asset_url + "sounds/huy.mp3");
        var sound_edit_order = ngAudio.load(myConfig.asset_url + "sounds/edit-order.mp3");
        var sound_edit_done = ngAudio.load(myConfig.asset_url + "sounds/edit-done.mp3");
        var sound_comment = ngAudio.load(myConfig.asset_url + "sounds/new-comment.mp3");
        return {
            play: function(loai) {
                switch (loai) {
                    case 'new':
                        sound_new.play();
                        break;
                    case 'transfer':
                        sound_transfer.play();
                        break;
                    case 'disagree':
                        sound_transfer_back.play();
                        break;
                    case 'cancel':
                        sound_cancel.play();
                        break;
                    case 'edit':
                        sound_edit_order.play();
                        break;
                    case 'xong':
                        sound_edit_done.play();
                        break;
                    case 'comment':
                        sound_comment.play();
                        break;
                }
            },
            stop: function(loai) {
                switch (loai) {
                    case 'new':
                        sound_new.stop();
                        break;
                    case 'transfer':
                        sound_transfer.stop();
                        break;
                    case 'disagree':
                        sound_transfer_back.stop();
                        break;
                    case 'cancel':
                        sound_cancel.stop();
                        break;
                    case 'edit':
                        sound_edit_order.stop();
                        break;
                    case 'xong':
                        sound_edit_done.stop();
                        break;
                    case 'comment':
                        sound_comment.stop();
                        break;
                }
            }
        };
    })
    .filter('numberFixedLen', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    });
