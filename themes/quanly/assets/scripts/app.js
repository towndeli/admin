'use strict';

// declare modules
angular.module('Authentication', []);
angular.module('Home', []);
angular.module('Orders', []);
angular.module('Regions', []);
angular.module('Branches', []);
angular.module('Categories', []);
angular.module('Toppings', []);
angular.module('Materials', []);
angular.module('Menus', []);
angular.module('Users', []);
angular.module('Customers', []);
angular.module('Feedbacks', []);
angular.module('Sms', []);
angular.module('Bugs', []);

angular.module('BasicHttpAuthExample', [
    'Authentication',
    'Home',
    'Orders',
    'Regions',
    'Branches',
    'Categories',
    'Toppings',
    'Materials',
    'Menus',
    'Users',
    'Customers',
    'Feedbacks',
    'Sms',
    'Bugs',
    'notification',
    'ui.router',
    'ngCookies',
    'angular.filter',
    'angular-location-picker',
    'angular-google-maps-geocoder',
    'ui.bootstrap',
    'frapontillo.bootstrap-switch',
    'ui.select2',
    'ngFileUpload',
    'ngSanitize',
    'btford.socket-io',
    'ngAudio',
    'myLibrary',
    'google.places'
])
 
.constant('myConfig', {
	asset_url: asset_url,
	site_url: site_url,
	base_url: base_url,
	api_url: api_url,
    socket_url: socket_url,
    website: 'http://towndeli.vn',
    phone: '(08) 73073777',
    logo_print: logo_print_url,
    danhsachsoluongitems: ['05', '20', '35', '50'],
    danhsachtrangthai: {0: 'Inactive', 1: 'Active'},
    danhsachgioitinh: {0: 'Nữ', 1: 'Nam'}
})
 
.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider
        .otherwise('/login');
    $stateProvider
        // HOME STATES AND NESTED VIEWS ========================================
        .state('login', {
            url: '/login',
            controller: 'LoginController',
            templateUrl: asset_url + 'modules/authentication/views/login.html'
        })
        .state('home', {
            url: '/home',
            controller: 'HomeController',
            templateUrl: asset_url + 'modules/home/views/home2.html'
        });
    /*$routeProvider
        .when('/login', {
            controller: 'LoginController',
            templateUrl: asset_url + 'modules/authentication/views/login.html'
        })

        .otherwise({ redirectTo: '/login' });*/
})
 
.run(function ($rootScope, $location, $cookieStore, $http, $state, mySocket) {
        //$rootScope.$state = $state;
        //$rootScope.$stateParams = $stateParams;
        /*$rootScope.$on('socket:loginStatus', function (ev, data) {
            console.log(ev, data)
        });*/

        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
            /*$rootScope.$on('socket:messageReceived', function (ev, data) {
                console.log(ev, data)
            });*/
            mySocket.emit('login', {id: $rootScope.globals.currentUser.userid});
        }
 
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            //$rootScope.appLoaded = false;
            /*console.log($state)
            if ($state.current.name=='home.editorder') {
                $rootScope.$broadcast('editorder', {id: $state.params.id});
            }*/
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });

        /*$rootScope.$on('$locationChangeSuccess', function (event, next, current, nextParams, currentParams) {
            console.log($state, nextParams, currentParams)
            if ($state.current.name=='home.editorder') {
                $rootScope.$broadcast('editorder', {id: $state.params.id});
                console.log('Go')
            }
        });*/
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            //console.log(toState, toParams, fromState, fromParams)
            if (fromState.name=='home.editorder') {
                $rootScope.$broadcast('editorderend', {id: fromParams.id});
            }
        });
    })
.directive('includeReplace',
    function () {
        return {
            require: 'ngInclude',
            restrict: 'A', /* optional */
            link: function (scope, el, attrs) {
                el.replaceWith(el.children());
            }
        };
    })
    .factory(
    "stacktraceService",
    function() {

        // "printStackTrace" is a global object.
        return({
            print: printStackTrace
        });

    })
    .provider(
    "$exceptionHandler",
    {
        $get: function( errorLogService ) {

            return( errorLogService );

        }
    })
    .factory(
    "errorLogService",
    function( $log, $window, stacktraceService, myConfig ) {

        // I log the given error to the remote server.
        function log( exception, cause ) {

            // Pass off the error to the default error handler
            // on the AngualrJS logger. This will output the
            // error to the console (and let the application
            // keep running normally for the user).
            $log.error.apply( $log, arguments );

            // Now, we need to try and log the error the server.
            // --
            // NOTE: In production, I have some debouncing
            // logic here to prevent the same client from
            // logging the same error over and over again! All
            // that would do is add noise to the log.
            try {

                var errorMessage = exception.toString();
                var stackTrace = stacktraceService.print({ e: exception });

                // Log the JavaScript error to the server.
                // --
                // NOTE: In this demo, the POST URL doesn't
                // exists and will simply return a 404.
                $.ajax({
                    type: "POST",
                    url: myConfig.api_url + 'luutru_baoloi',
                    contentType: "application/json",
                    data: angular.toJson({
                        errorUrl: $window.location.href,
                        errorMessage: errorMessage,
                        stackTrace: stackTrace,
                        cause: ( cause || "" )
                    })
                });
                //console.log($rootScopeProvider)
                /*$http.post(myConfig.api_url + 'luutru_baoloi', {data: angular.toJson({
                    errorUrl: $window.location.href,
                    errorMessage: errorMessage,
                    stackTrace: stackTrace,
                    cause: ( cause || "" )
                })});*/
                console.re.error($window.location.href);
                console.re.error(errorMessage);
                console.re.error(stackTrace);
                console.re.error(cause);

            } catch ( loggingError ) {

                // For Developers - log the log-failure.
                $log.warn( "Error logging failed" );
                $log.log( loggingError );

            }

        }


        // Return the logging function.
        return( log );

    });