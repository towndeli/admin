'use strict';
angular.module('myLibrary', [])
    .constant('base_url', 'http://admin.towndeli.vn/')
    .factory('mySocket', function (socketFactory) {
        var myIoSocket = io.connect('http://103.195.239.189:3000');
        var mySocket = socketFactory({
            ioSocket: myIoSocket
        });
        return mySocket;
    })
    .factory('myAudio', function (ngAudio, base_url) {
        var sound_new = ngAudio.load(base_url + "assets/sounds/new-order.mp3");
        var sound_transfer = ngAudio.load(base_url + "assets/sounds/transfer.mp3");
        var sound_transfer_back = ngAudio.load(base_url + "assets/sounds/transfer-back.mp3");
        var sound_cancel = ngAudio.load(base_url + "assets/sounds/huy.mp3");
        var sound_edit_order = ngAudio.load(base_url + "assets/sounds/edit-order.mp3");
        var sound_edit_done = ngAudio.load(base_url + "assets/sounds/edit-done.mp3");
        return function(loai) {
            switch (loai) {
                case 'new':
                    sound_new.play();
                    break;
                case 'transfer':
                    sound_transfer.play();
                    break;
                case 'disagree':
                    sound_transfer_back.play();
                    break;
                case 'cancel':
                    sound_cancel.play();
                    break;
                case 'edit':
                    sound_edit_order.play();
                    break;
                case 'xong':
                    sound_edit_done.play();
                    break;
            }
        };
    })
    .filter('numberFixedLen', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    });
