'use strict';

angular.module('Branches', ['ui.router'])
    .config(function ($stateProvider) {
        $stateProvider
            // HOME STATES AND NESTED VIEWS ========================================
            .state('home.branches', {
                url: '/branches/:id',
                controller: 'BranchesController',
                templateUrl: api_url + 'pages/branches'
            })
            .state('home.createbranch', {
                url: '/createbranch',
                controller: 'CreateBranchesController',
                templateUrl: api_url + 'pages/createbranch'
            });
    })
    .controller('BranchesController', function ($scope, $rootScope, $http, mySocket, myAudio, $stateParams, geocoder) {
        $scope.id = $stateParams.id || 0;
        $rootScope.page = 'branches';
        $rootScope.titlepage = 'Chi nhánh';
        $scope.pagination = {
            currentPage: 1,
            numPerPage: '20',
            totalItems: 0,
            begin: 0,
            end: 0
        };
        $scope.danhsach = [];
        $scope.filteredOrders = [];
        $scope.branchInfo = {};
        $scope.rootregions = [];
        $scope.onLocationInitialize = function (location) {
            console.log("=====from controller 1=======");
            console.log(location);
        };
        $scope.onLocationChange = function (location) {
            console.log("=====from controller 2=======")
            console.log(location);
            $scope.branchInfo.geo_latitude = location.latitude;
            $scope.branchInfo.geo_longitude = location.longitude;
        };
        $scope.onMapLoaded = function (map) {
            console.log("=====from controller 3=======")
            console.log(map);
            //var mapContext = $(this).locationpicker('map');
            //console.log(mapContext);
            $scope.mapContext = map;
        };
        $scope.thaydoidiachi = function () {
            geocoder.geocode_by_query($scope.branchInfo.address).then(function (values) {
                $scope.latlngList = values;
                if ($scope.latlngList.length) {
                    var lat = $scope.latlngList[0].geometry.location.lat();
                    var lng = $scope.latlngList[0].geometry.location.lng();
                    $scope.location = {latitude: lat, longitude: lng};
                    $scope.mapContext.map.panTo($scope.latlngList[0].geometry.location);
                    $scope.mapContext.marker.setPosition($scope.latlngList[0].geometry.location);
                    $scope.branchInfo.geo_latitude = lat;
                    $scope.branchInfo.geo_longitude = lng;
                }
            });
        };
        $scope.luachondiachi = function ($item, $model, $label, $event) {
            geocoder.geocode_by_query($item).then(function (values) {
                $scope.latlngList = values;
                if ($scope.latlngList.length) {
                    var lat = $scope.latlngList[0].geometry.location.lat();
                    var lng = $scope.latlngList[0].geometry.location.lng();
                    var myLatlng = new google.maps.LatLng(lat, lng);
                    $scope.location = {latitude: lat, longitude: lng};
                    $scope.mapContext.map.panTo($scope.latlngList[0].geometry.location);
                    $scope.mapContext.marker.setPosition($scope.latlngList[0].geometry.location);
                    $scope.branchInfo.geo_latitude = lat;
                    $scope.branchInfo.geo_longitude = lng;
                    //console.log(lat, lng, $scope.branchInfo)
                }
            });
        };
        $scope.getLocation = function(val) {
            /*return $http.post($scope.myConfig.api_url + 'geocode/json', {
                address: val,
                sensor: false
            }).then(function(response){
                return response.data.results.map(function(item){
                    return item.formatted_address;
                });
            });*/
            return geocoder.geocode_by_query(val).then(function (response) {
                return response.map(function(item){
                    return item.formatted_address;
                });
            });
        };
        $scope.init = function () {
            $http.post($scope.myConfig.api_url + 'danhsach/branches', {id: $scope.id}).success(function (response) {
                $scope.danhsach = response.data;
                $scope.rootregions = response.regions;
                $scope.branchInfo = response.branchInfo;
                $scope.location = {latitude: $scope.branchInfo.geo_latitude, longitude: $scope.branchInfo.geo_longitude};
                //$scope.regionInfo.status = String($scope.regionInfo.status);
                $scope.pagination.totalItems = $scope.danhsach.length;
                $scope.pagination.currentPage = 1;
                $scope.pageChanged();
                $rootScope.appLoaded = true;
            });
        };
        $scope.init();
        $scope.pageChanged = function () {
            var numPerPage = parseInt($scope.pagination.numPerPage, 10),
                begin = (($scope.pagination.currentPage - 1) * numPerPage),
                end = begin + numPerPage;
            $scope.pagination.begin = begin + 1;
            $scope.pagination.end = end>$scope.pagination.totalItems?$scope.pagination.totalItems:end;
            $scope.filteredOrders = $scope.danhsach.slice(begin, end);
        };
        $scope.dangclick = 0;
        $scope.luutruregion = function () {
            if (!$scope.dangclick) {
                if (confirm('Are you sure?')) {
                    $rootScope.appLoaded = false;
                    $scope.dangclick = 1;
                    $http.post($scope.myConfig.api_url + 'luutru/branches', {
                        thongtin: $scope.branchInfo
                    }).success(function (response) {
                        $rootScope.appLoaded = true;
                        $scope.dangclick = 0;
                        if (response.status == 'success') {
                            $scope.init();
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            }
        };
        $scope.dangclick2 = 0;
        $scope.updatestatus = function (item) {
            if (!$scope.dangclick2) {
                if (confirm('Are you sure?')) {
                    $scope.dangclick2 = 1;
                    $http.post($scope.myConfig.api_url + 'capnhat/branches', {
                        id: item.id,
                        status: !item.status
                    }).success(function (response) {
                        $scope.dangclick2 = 0;
                        if (response.status == 'success') {
                            item = response.data;
                            angular.forEach($scope.danhsach, function (value, index) {
                                if (value.id == item.id) {
                                    for (var prop in item) {
                                        if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                        $scope.danhsach[index][prop] = item[prop];
                                    }
                                }
                            });
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            }
        };
        $scope.$watch("pagination.numPerPage", function() {
            $scope.pageChanged();
        });
    })
    .controller('CreateBranchesController', function ($scope, $rootScope, $http, mySocket, myAudio, $state, geocoder) {
            $rootScope.page = 'createbranch';
            $rootScope.titlepage = 'Tạo chi nhánh';
            $scope.branchInfo = {
                name: '',
                address: '',
                geo_latitude: '',
                geo_longitude: '',
                region_id: '0',
                status: '1'
            };
            $scope.location = {latitude: "12.2421914", longitude: "109.19642599999997"};
            $scope.rootregions = [];
            $scope.onLocationInitialize = function (location) {
                console.log("=====from controller 1=======");
                console.log(location);
            };
            $scope.onLocationChange = function (location) {
                console.log("=====from controller 2=======")
                console.log(location);
                $scope.branchInfo.geo_latitude = location.latitude;
                $scope.branchInfo.geo_longitude = location.longitude;
            };
            $scope.onMapLoaded = function (map) {
                console.log("=====from controller 3=======")
                console.log(map);
                //var mapContext = $(this).locationpicker('map');
                //console.log(mapContext);
                $scope.mapContext = map;
            };
            $scope.thaydoidiachi = function () {
                geocoder.geocode_by_query($scope.branchInfo.address).then(function (values) {
                    $scope.latlngList = values;
                    if ($scope.latlngList.length) {
                        var lat = $scope.latlngList[0].geometry.location.lat();
                        var lng = $scope.latlngList[0].geometry.location.lng();
                        $scope.location = {latitude: lat, longitude: lng};
                        $scope.mapContext.map.panTo($scope.latlngList[0].geometry.location);
                        $scope.mapContext.marker.setPosition($scope.latlngList[0].geometry.location);
                        $scope.branchInfo.geo_latitude = lat;
                        $scope.branchInfo.geo_longitude = lng;
                    }
                });
            };
            $scope.luachondiachi = function ($item, $model, $label, $event) {
                geocoder.geocode_by_query($item).then(function (values) {
                    $scope.latlngList = values;
                    if ($scope.latlngList.length) {
                        var lat = $scope.latlngList[0].geometry.location.lat();
                        var lng = $scope.latlngList[0].geometry.location.lng();
                        $scope.location = {latitude: lat, longitude: lng};
                        $scope.mapContext.map.panTo($scope.latlngList[0].geometry.location);
                        $scope.mapContext.marker.setPosition($scope.latlngList[0].geometry.location);
                        $scope.branchInfo.geo_latitude = lat;
                        $scope.branchInfo.geo_longitude = lng;
                        console.log($scope.latlngList)
                    }
                });
            };
            $scope.getLocation = function(val) {
                return geocoder.geocode_by_query(val).then(function (response) {
                    return response.map(function(item){
                        return item.formatted_address;
                    });
                });
            };
            $scope.init = function () {
                $http.post($scope.myConfig.api_url + 'chuanbi/branches', {}).success(function (response) {
                    $scope.rootregions = response.regions;
                    $scope.branchInfo = response.branchInfo;
                    //$scope.location = {latitude: $scope.branchInfo.geo_latitude, longitude: $scope.branchInfo.geo_longitude};
                    //$scope.location = {latitude: "12.2421914", longitude: "109.19642599999997"};
                    $rootScope.appLoaded = true;
                });
            };
            $scope.init();
            $scope.dangclick = 0;
            $scope.luutruregion = function () {
                if (!$scope.dangclick) {
                    if (confirm('Are you sure?')) {
                        $rootScope.appLoaded = false;
                        $scope.dangclick = 1;
                        $http.post($scope.myConfig.api_url + 'luutru/branches', {
                            thongtin: $scope.branchInfo
                        }).success(function (response) {
                            $rootScope.appLoaded = true;
                            $scope.dangclick = 0;
                            if (response.status == 'success') {
                                $state.go('home.branches');
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            };
        });