'use strict';
 
angular.module('Authentication')

.controller('LoginController', function ($scope, $rootScope, $state, AuthenticationService, mySocket) {
        $rootScope.page = 'login';
        $rootScope.titlepage = 'Login';
        // reset login status
        AuthenticationService.ClearCredentials();
 
        $scope.login = function () {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function(response) {
                if(response.status == 'success') {
                    AuthenticationService.SetCredentials(response.currentUser);
                    mySocket.emit('login', {id: response.currentUser.userid});
                    $state.go('home.dashboard');
                } else {
                    $scope.error = response.msg;
                    $scope.dataLoading = false;
                }
            });
        };
        $scope.dangnhap = function ($event) {
            if ($event.which === 13) {
                $scope.login();
            }
        };
        $rootScope.appLoaded = true;
    });