'use strict';

angular.module('Orders', ['ui.router'])
    .config(function ($stateProvider) {
        $stateProvider
            // HOME STATES AND NESTED VIEWS ========================================
            .state('home.orders', {
                url: '/orders/:page',
                controller: 'OrdersController',
                templateUrl: api_url + 'pages/orders'
            })
            .state('home.editorder', {
                url: '/editorder/:id',
                controller: 'EditOrdersController',
                templateUrl: api_url + 'pages/createorder'
            })
            .state('home.createorder', {
                url: '/createorder',
                controller: 'CreateOrdersController',
                templateUrl: api_url + 'pages/createorder'
            });
    })
.controller('CreateOrdersController', function ($scope, $rootScope, $http, $uibModal, $state, mySocket, myAudio, geocoder) {
        $rootScope.page = 'createorder';
        $rootScope.titlepage = 'Tạo đơn hàng';
        $scope.danhsach = [];
        $scope.selected = {};
        $scope.isSelected = true;
        $scope.onLabel = '<i class="fa fa-magic"></i>';
        $scope.offLabel = '<i class="fa fa-pencil"></i>';
        $scope.orderInfo = {
            id: 0,
            idv: '',
            ordernumber: ''
        };
        $scope.customerInfo = {
            first_name: '',
            last_name: '',
            phone: '',
            gender: 1
        };
        var todayDate = new Date();
        $scope.shippingInfo = {
            floor: '',
            address: '',
            branchid: 0,
            delivery_on: new Date(todayDate.getTime()+(30*60*1000)),
            delivery_on2: new Date(todayDate.getTime()+(60*60*1000)),
            delivery_on_loai: '0',
            note: '',
            discount_code: '',
            ghichuthuonggap: [],
            thuonggapnote: []
        };
        $scope.khuyenmai = {
            description: '',
            data: 0
        };
        $scope.loai = '';
        $scope.chuongtrinhkhuyenmai = [];
        $scope.orders = [];
        $scope.branches = [];
        $scope.schedules = [];
        $scope.init = function () {
            $http.post($scope.myConfig.api_url + 'chuanbi/orders', {}).success(function (response) {
                $scope.danhsach = response.data;
                $scope.branches = response.branches;
                $scope.schedules = response.schedules;
                $scope.chuongtrinhkhuyenmai = response.chuongtrinhkhuyenmai;
                $scope.shippingInfo.ghichuthuonggap = response.ghichuthuonggap;
                $rootScope.appLoaded = true;
            });
        };
        $scope.init();
        $scope.getCustomer = function (val, loai) {
            return $http.post($scope.myConfig.api_url + 'search_customer', {
                data: val,
                loai: loai
            }).then(function(response){
                return response.data.results.map(function(item){
                    return item;
                });
            });
        };
        $scope.choncustomer = function ($item, $model, $label, $event) {
            $scope.customerInfo.first_name = $item.first_name;
            $scope.customerInfo.last_name = $item.last_name;
            $scope.customerInfo.phone = $item.customer_phone;
            $scope.customerInfo.gender = $item.gender;
            $scope.customerInfo.level = $item.level;
            $scope.customerInfo.points = $item.points;
            $scope.shippingInfo.address = $item.shipping_address;
            $scope.shippingInfo.floor = $item.shipping_floor;
            if ($item.shipping_address && $scope.isSelected) {
                geocoder.geocode_by_query($item.shipping_address).then(function (response) {
                    if (response.length) {
                        var results = response[0];
                        /*var cnid = $scope.NearestCity($scope.branches, results.geometry.location.lat(), results.geometry.location.lng());
                        $scope.shippingInfo.branchid = cnid;*/
                        $scope.NearestCity3($scope.branches, results.geometry.location.lat(), results.geometry.location.lng()).then(function(data){
                            console.log(data)
                            $scope.shippingInfo.branchid = data;
                        });
                        var currentPosition = new google.maps.LatLng(results.geometry.location.lat(), results.geometry.location.lng());
                        $scope.calcRoute(currentPosition);
                    }
                });
            }
        };
        /*$scope.getCustomerAddress2 = function (val) {
            var tmp = {
                input: val,
                componentRestrictions: {
                    country: 'vn'
                },
                types: ['geocode'],
                offset: val.length,
                location: new google.maps.LatLng(10.771992000000001, 106.698264),
                radius: 1500
            };
            return geocoder.service_by_query(tmp).then(function (response) {
                return response.map(function(item){
                    console.log(item)
                    return item;
                });
            });
        };*/
        $scope.choncustomeraddress = function ($item, $model, $label, $event) {
            $scope.shippingInfo.address = $item.label + ' ' + $item.description;
            if ($scope.isSelected&&$scope.shippingInfo.address) {
                /*var cnid = $scope.NearestCity($scope.branches, $item.geometry.location.lat(), $item.geometry.location.lng());
                $scope.shippingInfo.branchid = cnid;*/
                $scope.NearestCity3($scope.branches, $item.vitri.lat, $item.vitri.lng).then(function(data){
                    console.log(data)
                    $scope.shippingInfo.branchid = data;
                });
                var currentPosition = new google.maps.LatLng($item.vitri.lat, $item.vitri.lng);
                $scope.calcRoute(currentPosition);
            }
        };
        $scope.$on('g-places-autocomplete:select', function (event, data) {
            console.log(event, data)
            $scope.shippingInfo.address = data.formatted_address;
            if ($scope.isSelected&&$scope.shippingInfo.address) {
                /*var cnid = $scope.NearestCity($scope.branches, $item.geometry.location.lat(), $item.geometry.location.lng());
                 $scope.shippingInfo.branchid = cnid;*/
                $scope.NearestCity3($scope.branches, data.geometry.location.lat(), data.geometry.location.lng()).then(function(data2){
                    console.log(data2)
                    $scope.shippingInfo.branchid = data2;
                });
                var currentPosition = new google.maps.LatLng(data.geometry.location.lat(), data.geometry.location.lng());
                $scope.calcRoute(currentPosition);
            }
        });
        $scope.xacnhanthoat = function () {
            if (confirm('Bạn có chắc chắn không?')) {
                $state.go('home.orders');
            }
        };
        /*var oldValue = '';
        $scope.keyup = function(event, bien) {
            oldValue = bien;

        };
        $scope.keydown = function(event, bien) {
            if (event.keyCode === 9) {
                bien = oldValue;
            }
        };*/
        $scope.danhsachdiachi = [];
        $scope.reqGeoTimeOut;
        $scope.keypress = function() {
            clearTimeout($scope.reqGeoTimeOut);
            $scope.reqGeoTimeOut = setTimeout(function () {
                $scope.danhsachdiachi = $scope.getCustomerAddress($scope.shippingInfo.address);
                //console.log($scope.danhsachdiachi)
            }, 500);
        };
        $scope.calcRoute = function (currentPosition) {
            var directionsService = new google.maps.DirectionsService();
            if ($scope.branches.length) {
                angular.forEach($scope.branches, function (value, index) {
                    var end = new google.maps.LatLng(value.geo_latitude, value.geo_longitude);
                    var request = {
                        origin: currentPosition,
                        destination: end,
                        travelMode: google.maps.TravelMode.DRIVING
                    };
                    directionsService.route(request, function(response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            //console.log(value.name, response)
                            value.distance = response.routes[0].legs[0].distance.text;
                            value.distance2 = response.routes[0].legs[0].distance.value;
                        }
                    });
                });
            }
        };
        $scope.dienkhuyenmai = function (code) {
            $scope.shippingInfo.discount_code = code;
            $scope.apdungkhuyenmai();
        };
        $scope.apdungkhuyenmai = function () {
            $http.post($scope.myConfig.api_url + 'discount_apply', {
                data: $scope.shippingInfo.discount_code,
                items: $scope.orders
            }).then(function (res) {
                var res = res.data;

                $scope.khuyenmai = res;

                if (typeof res.newCartData !== 'undefined') {
                    $scope.orders = res.newCartData;
                }
            });
        };
        $scope.onChange = function () {
            if($scope.isSelected&&$scope.shippingInfo.address){
                geocoder.geocode_by_query($scope.shippingInfo.address).then(function (response) {
                    if (response.length) {
                        var results = response[0];
                        /*var cnid = $scope.NearestCity($scope.branches, results.geometry.location.lat(), results.geometry.location.lng());
                        $scope.shippingInfo.branchid = cnid;*/
                        $scope.NearestCity3($scope.branches, results.geometry.location.lat(), results.geometry.location.lng()).then(function(data){
                            console.log(data)
                            $scope.shippingInfo.branchid = data;
                        });
                        var currentPosition = new google.maps.LatLng(results.geometry.location.lat(), results.geometry.location.lng());
                        $scope.calcRoute(currentPosition);
                    }
                });
            }
        };
        $scope.tongmonhoadon = function () {
            var tmp = 0;
            angular.forEach($scope.orders, function (value) {
                tmp += parseInt(value.qty);
            });
            return tmp;
        };
        $scope.tamtinh = function () {
            var tmp = 0;
            angular.forEach ($scope.orders, function (value) {
                tmp += (value.qty * value.selected_price) + ($scope.thanhtientoppings(value.toppings) * value.qty);
            });
            return tmp / 1000;
        };
        $scope.tongtien = function () {
            var tmp = 0, timthay = -1;
            angular.forEach($scope.orders, function (value) {
                tmp += value.selected_price * value.qty + ($scope.thanhtientoppings(value.toppings) * value.qty);//value.tmpthanhtientoppings;
            });
            tmp -= $scope.khuyenmai.data;
            return tmp;
        };
        $scope.kiemtrakhuyenmai = function ($event) {
            if ($event.keyCode==13) {
                $scope.apdungkhuyenmai();
            }
        };
        $scope.additemtocart = function () {
            if ($scope.selected.selected_size == 'sm') {
                $scope.selected.selected_price = $scope.selected.size.sm.price;
                $scope.selected.selected_label = $scope.selected.size.sm.label;
            } else if ($scope.selected.selected_size == 'md') {
                $scope.selected.selected_price = $scope.selected.size.md.price;
                $scope.selected.selected_label = $scope.selected.size.md.label;
            } else if ($scope.selected.selected_size == 'lg') {
                $scope.selected.selected_price = $scope.selected.size.lg.price;
                $scope.selected.selected_label = $scope.selected.size.lg.label;
            }
            if ($scope.selected.qty <= 0) {
                $scope.selected.qty = 1;
            }
            if ($scope.loai=='additem') {
                /*var timthay = -1;
                angular.forEach ($scope.orders, function (value, index) {
                    if (value.id==$scope.selected.id && $scope.selected.selected_size==value.selected_size) {
                        timthay = index;
                    }
                });
                if (timthay == -1) {
                    $scope.orders.push($scope.selected);
                } else {
                    $scope.orders[timthay].qty = parseInt($scope.orders[timthay].qty) + parseInt($scope.selected.qty);
                }*/
                $scope.orders.push($scope.selected);
            }/* else if ($scope.loai=='edititem') {
                angular.forEach ($scope.orders, function (value, index) {
                    if (value.id==$scope.selected.id && $scope.selected.selected_size==value.selected_size) {
                        $scope.orders[index] = $scope.selected;
                    }
                });
            }*/
            $scope.apdungkhuyenmai();
            $scope.closeModal();
        };
        $scope.toggleSelection = function (id, name) {
            var idx = $scope.selected.thuonggapnote.indexOf(id);
            if (idx > -1) {
                $scope.selected.thuonggapnote.splice(idx, 1);
            } else {
                $scope.selected.thuonggapnote.push(id);
            }
            //$scope.selected.note = name + ($scope.selected.note?(', ' +$scope.selected.note):'');
        };
        $scope.toggleSelection2 = function (id, name) {
            var idx = $scope.shippingInfo.thuonggapnote.indexOf(id);
            if (idx > -1) {
                $scope.shippingInfo.thuonggapnote.splice(idx, 1);
            } else {
                $scope.shippingInfo.thuonggapnote.push(id);
            }
            //$scope.selected.note = name + ($scope.selected.note?(', ' +$scope.selected.note):'');
        };
        $scope.deleteitem = function (item) {
            if (confirm('Are you sure?')) {
                /*angular.forEach($scope.orders, function (value, index) {
                    if (value.id == item.id && item.selected_size==value.selected_size) {
                        $scope.orders.splice(index, 1);
                        $scope.apdungkhuyenmai();
                    }
                });*/
                var tmp = $scope.orders.indexOf(item);
                if (tmp>-1) {
                    $scope.orders.splice(tmp, 1);
                    $scope.apdungkhuyenmai();
                }
            }
        };
        $scope.dangclick = 0;
        $scope.taomoiorder = function () {
            if (!$scope.dangclick) {
                if ($scope.orders.length == 0) {
                    alert('Xin hãy nhập thông tin hàng đặt!');
                } else if ($scope.customerInfo.last_name == '' || $scope.customerInfo.phone == '') {
                    alert('Xin hãy nhập thông tin khách hàng!');
                    $scope.customerorderinfo();
                } else if ($scope.shippingInfo.address == '' || $scope.shippingInfo.branchid == '' || $scope.shippingInfo.branchid == '0' || $scope.shippingInfo.branchid == 0) {
                    alert('Xin hãy nhập thông tin giao đơn hàng!');
                    $scope.customerorderinfo();
                } else {
                    if (confirm('Are you sure?')) {
                        $rootScope.appLoaded = false;
                        $scope.dangclick = 1;
                        $http.post($scope.myConfig.api_url + 'luutru/orders', {
                            items: $scope.orders,
                            customer: $scope.customerInfo,
                            shipping: $scope.shippingInfo
                        }).success(function (response) {
                            $rootScope.appLoaded = true;
                            $scope.dangclick = 0;
                            if (response.status == 'success') {
                                mySocket.emit('sendMessage', {
                                    action: 'newOrder',
                                    data: response.data,
                                    users: response.users,
                                    createuser: response.createuser
                                });
                                // $state.go('home.orders');
                                window.close();
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            }
        };
        $scope.openModal = function (loai, item, size) {
            $scope.loai = loai;
            if (loai=='additem') {
                $scope.selected = angular.copy(item);
            } else {
                $scope.selected = item;
            }
            $scope.selected.selected_size = size;
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'myModal.html',
                windowClass: 'modal-primary',
                size: 'lg',
                scope: $scope
            });
        };
        $scope.closeModal = function () {
            $scope.modalInstance.close();
        };
    })
.controller('EditOrdersController', function ($scope, $rootScope, $http, $uibModal, $state, mySocket, myAudio, $stateParams, geocoder) {
        $scope.id = $stateParams.id;
        $rootScope.page = 'editorder';
        $rootScope.titlepage = 'Chi tiết đơn hàng';
        $scope.danhsach = [];
        $scope.selected = {};
        $scope.isSelected = true;
        $scope.onLabel = '<i class="fa fa-magic"></i>';
        $scope.offLabel = '<i class="fa fa-pencil"></i>';
        $scope.orderInfo = {
            id: 0,
            idv: '',
            ordernumber: '',
            edit_count: 0
        };
        $scope.customerInfo = {
            first_name: '',
            last_name: '',
            phone: '',
            gender: 1
        };
        $scope.shippingInfo = {
            floor: '',
            address: '',
            branchid: 0,
            delivery_on: '',
            delivery_on2: '',
            note: '',
            discount_code: '',
            ghichuthuonggap: [],
            thuonggapnote: []
        };
        $scope.khuyenmai = {
            description: '',
            data: 0
        };
        $scope.loai = '';
        $scope.chuongtrinhkhuyenmai = [];
        $scope.orders = [];
        $scope.branches = [];
        $scope.schedules = [];
        $scope.$on('editorderend', function (event, data) {
            var order = {
                branch_id: $scope.orderInfo.branch_id,
                createuserid: $rootScope.globals.currentUser.userid,
                createuser: $rootScope.globals.currentUser.fullname,
                idv: $scope.orderInfo.idv,
                order_number: $scope.orderInfo.order_number,
                shipping_on: $scope.shippingInfo.delivery_on,
                shipping_on_text: $scope.shippingInfo.shipping_on_text
            };
            mySocket.emit('removeEditLists', {id: $scope.orderInfo.id, user: $rootScope.globals.currentUser.userid, order: order});
        });
        $scope.init = function () {
            $http.post($scope.myConfig.api_url + 'chuanbi/orders', {id: $scope.id}).success(function (response) {
                $scope.danhsach = response.data;
                $scope.branches = response.branches;
                $scope.schedules = response.schedules;
                $scope.orders = response.orders;
                $scope.orderInfo = response.orderInfo;
                $scope.customerInfo = response.customerInfo;
                $scope.shippingInfo = response.shippingInfo;
                $scope.shippingInfo.ghichuthuonggap = response.ghichuthuonggap;
                $scope.shippingInfo.delivery_on = new Date(response.shippingInfo.delivery_on * 1000);
                $scope.shippingInfo.delivery_on2 = new Date(response.shippingInfo.delivery_on2 * 1000);
                $scope.chuongtrinhkhuyenmai = response.chuongtrinhkhuyenmai;
                $scope.apdungkhuyenmai();
                $scope.onChange();
                //addEditLists
                var order = {
                    branch_id: $scope.orderInfo.branch_id,
                    createuserid: $rootScope.globals.currentUser.userid,
                    createuser: $rootScope.globals.currentUser.fullname,
                    idv: $scope.orderInfo.idv,
                    order_number: $scope.orderInfo.order_number,
                    gender: $scope.customerInfo.gender,
                    customer_name: $scope.customerInfo.first_name + ' ' + $scope.customerInfo.last_name,
                    shipping_address: $scope.shippingInfo.address
                };
                mySocket.emit('addEditLists', {id: $scope.orderInfo.id, user: $rootScope.globals.currentUser.userid, order: order});
                $rootScope.appLoaded = true;
            });
        };
        $scope.init();
        $scope.getCustomer = function (val, loai) {
            return $http.post($scope.myConfig.api_url + 'search_customer', {
                data: val,
                loai: loai
            }).then(function(response){
                return response.data.results.map(function(item){
                    return item;
                });
            });
        };
        $scope.choncustomer = function ($item, $model, $label, $event) {
            $scope.customerInfo.first_name = $item.first_name;
            $scope.customerInfo.last_name = $item.last_name;
            $scope.customerInfo.phone = $item.customer_phone;
            $scope.customerInfo.gender = $item.gender;
            $scope.customerInfo.level = $item.level;
            $scope.customerInfo.points = $item.points;
            $scope.shippingInfo.address = $item.shipping_address;
            $scope.shippingInfo.floor = $item.shipping_floor;
            if ($item.shipping_address&&$scope.isSelected) {
                geocoder.geocode_by_query($item.shipping_address).then(function (response) {
                    if (response.length) {
                        var results = response[0];
                        /*var cnid = $scope.NearestCity($scope.branches, results.geometry.location.lat(), results.geometry.location.lng());
                        $scope.shippingInfo.branchid = cnid;*/
                        $scope.NearestCity3($scope.branches, results.geometry.location.lat(), results.geometry.location.lng()).then(function(data){
                            console.log(data)
                            $scope.shippingInfo.branchid = data;
                        });
                        var currentPosition = new google.maps.LatLng(results.geometry.location.lat(), results.geometry.location.lng());
                        $scope.calcRoute(currentPosition);
                    }
                });
            }
        };
        $scope.choncustomeraddress = function ($item, $model, $label, $event) {
            $scope.shippingInfo.address = $item.label + ' ' + $item.description;
            if($scope.isSelected&&$scope.shippingInfo.address) {
                /*var cnid = $scope.NearestCity($scope.branches, $item.geometry.location.lat(), $item.geometry.location.lng());
                $scope.shippingInfo.branchid = cnid;*/
                $scope.NearestCity3($scope.branches, $item.vitri.lat, $item.vitri.lng).then(function(data){
                    console.log(data)
                    $scope.shippingInfo.branchid = data;
                });
                //console.log($item, cnid);
                var currentPosition = new google.maps.LatLng($item.vitri.lat, $item.vitri.lng);
                $scope.calcRoute(currentPosition);
            }
        };
        $scope.$on('g-places-autocomplete:select', function (event, data) {
            console.log(event, data)
            $scope.shippingInfo.address = data.formatted_address;
            if ($scope.isSelected&&$scope.shippingInfo.address) {
                /*var cnid = $scope.NearestCity($scope.branches, $item.geometry.location.lat(), $item.geometry.location.lng());
                 $scope.shippingInfo.branchid = cnid;*/
                $scope.NearestCity3($scope.branches, data.geometry.location.lat(), data.geometry.location.lng()).then(function(data2){
                    console.log(data2)
                    $scope.shippingInfo.branchid = data2;
                });
                var currentPosition = new google.maps.LatLng(data.geometry.location.lat(), data.geometry.location.lng());
                $scope.calcRoute(currentPosition);
            }
        });
        $scope.onChange = function () {
            if($scope.isSelected&&$scope.shippingInfo.address){
                geocoder.geocode_by_query($scope.shippingInfo.address).then(function (response) {
                    if (response.length) {
                        var results = response[0];
                        /*var cnid = $scope.NearestCity($scope.branches, results.geometry.location.lat(), results.geometry.location.lng());
                        $scope.shippingInfo.branchid = cnid;*/
                        $scope.NearestCity3($scope.branches, results.geometry.location.lat(), results.geometry.location.lng()).then(function(data){
                            console.log(data)
                            $scope.shippingInfo.branchid = data;
                        });
                        var currentPosition = new google.maps.LatLng(results.geometry.location.lat(), results.geometry.location.lng());
                        $scope.calcRoute(currentPosition);
                    }
                });
            }
        };
        $scope.danhsachdiachi = [];
        $scope.reqGeoTimeOut;
        $scope.keypress = function() {
            clearTimeout($scope.reqGeoTimeOut);
            $scope.reqGeoTimeOut = setTimeout(function () {
                $scope.danhsachdiachi = $scope.getCustomerAddress($scope.shippingInfo.address);
                console.log($scope.danhsachdiachi)
            }, 500);
        };
        $scope.calcRoute = function (currentPosition) {
            var directionsService = new google.maps.DirectionsService();
            if ($scope.branches.length) {
                angular.forEach($scope.branches, function (value, index) {
                    var end = new google.maps.LatLng(value.geo_latitude, value.geo_longitude);
                    var request = {
                        origin: currentPosition,
                        destination: end,
                        travelMode: google.maps.TravelMode.DRIVING
                    };
                    directionsService.route(request, function(response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            value.distance = response.routes[0].legs[0].distance.text;
                            value.distance2 = response.routes[0].legs[0].distance.value;
                        }
                    });
                });
            }
        };
        $scope.xacnhanthoat = function () {
            if (confirm('Bạn có chắc chắn không?')) {
                $state.go('home.orders');
            }
        };
        $scope.dienkhuyenmai = function (code) {
            $scope.shippingInfo.discount_code = code;
            $scope.apdungkhuyenmai();
        };
        $scope.apdungkhuyenmai = function () {
            $http.post($scope.myConfig.api_url + 'discount_apply', {
                data: $scope.shippingInfo.discount_code,
                items: $scope.orders
            }).then(function (res) {
                var res = res.data;

                $scope.khuyenmai = res;

                if (typeof res.newCartData !== 'undefined') {
                    $scope.orders = res.newCartData;
                }
            });
        };
        $scope.tongmonhoadon = function () {
            var tmp = 0;
            angular.forEach($scope.orders, function (value) {
                tmp += parseInt(value.qty);
            });
            return tmp;
        };
        $scope.tamtinh = function () {
            var tmp = 0;
            angular.forEach ($scope.orders, function (value) {
                tmp += (value.qty * value.selected_price) + ($scope.thanhtientoppings(value.toppings) * value.qty);
            });
            return tmp / 1000;
        };
        $scope.tongtien = function () {
            var tmp = 0, timthay = -1;
            angular.forEach($scope.orders, function (value) {
                tmp += value.selected_price * value.qty + ($scope.thanhtientoppings(value.toppings) * value.qty);//value.tmpthanhtientoppings;
            });
            tmp -= $scope.khuyenmai.data;
            return tmp;
        };
        $scope.kiemtrakhuyenmai = function ($event) {
            if ($event.keyCode==13) {
                $scope.apdungkhuyenmai();
            }
        };
        $scope.additemtocart = function () {
            if ($scope.selected.selected_size == 'sm') {
                $scope.selected.selected_price = $scope.selected.size.sm.price;
                $scope.selected.selected_label = $scope.selected.size.sm.label;
            } else if ($scope.selected.selected_size == 'md') {
                $scope.selected.selected_price = $scope.selected.size.md.price;
                $scope.selected.selected_label = $scope.selected.size.md.label;
            } else if ($scope.selected.selected_size == 'lg') {
                $scope.selected.selected_price = $scope.selected.size.lg.price;
                $scope.selected.selected_label = $scope.selected.size.lg.label;
            }
            if ($scope.selected.qty <= 0) {
                $scope.selected.qty = 1;
            }
            if ($scope.loai=='additem') {
                /*var timthay = -1;
                angular.forEach ($scope.orders, function (value, index) {
                    if (value.id==$scope.selected.id && $scope.selected.selected_size==value.selected_size) {
                        timthay = index;
                    }
                });
                if (timthay == -1) {
                    $scope.orders.push($scope.selected);
                } else {
                    $scope.orders[timthay].qty = parseInt($scope.orders[timthay].qty) + parseInt($scope.selected.qty);
                    //$scope.orders[timthay] = $scope.selected;
                }*/
                $scope.orders.push($scope.selected);
            }/* else if ($scope.loai=='edititem') {
                angular.forEach ($scope.orders, function (value, index) {
                    if (value.id==$scope.selected.id && $scope.selected.selected_size==value.selected_size) {
                        $scope.orders[index] = $scope.selected;
                    }
                });
            }*/
            $scope.apdungkhuyenmai();
            $scope.closeModal();
        };
        $scope.toggleSelection = function (id, name) {
            var idx = $scope.selected.thuonggapnote.indexOf(id);
            if (idx > -1) {
                $scope.selected.thuonggapnote.splice(idx, 1);
            } else {
                $scope.selected.thuonggapnote.push(id);
            }
            //$scope.selected.note = name + ($scope.selected.note?(', ' +$scope.selected.note):'');
        };
        $scope.toggleSelection2 = function (id, name) {
            var idx = $scope.shippingInfo.thuonggapnote.indexOf(id);
            if (idx > -1) {
                $scope.shippingInfo.thuonggapnote.splice(idx, 1);
            } else {
                $scope.shippingInfo.thuonggapnote.push(id);
            }
            //$scope.selected.note = name + ($scope.selected.note?(', ' +$scope.selected.note):'');
        };
        $scope.deleteitem = function (item) {
            if (confirm('Are you sure?')) {
                /*angular.forEach($scope.orders, function (value, index) {
                    if (value.id == item.id && item.selected_size==value.selected_size) {
                        $scope.orders.splice(index, 1);
                        $scope.apdungkhuyenmai();
                    }
                });*/
                var tmp = $scope.orders.indexOf(item);
                if (tmp>-1) {
                    $scope.orders.splice(tmp, 1);
                    $scope.apdungkhuyenmai();
                }
            }
        };
        $scope.dangclick = 0;
        $scope.taomoiorder = function () {
            if (!$scope.dangclick) {
                if ($scope.orders.length == 0) {
                    alert('Xin hãy nhập thông tin hàng đặt!');
                } else if ($scope.customerInfo.last_name == '' || $scope.customerInfo.phone == '') {
                    alert('Xin hãy nhập thông tin khách hàng!');
                    $scope.customerorderinfo();
                } else if ($scope.shippingInfo.address == '' || $scope.shippingInfo.branchid == '' || $scope.shippingInfo.branchid == '0' || $scope.shippingInfo.branchid == 0) {
                    alert('Xin hãy nhập thông tin giao đơn hàng!');
                    $scope.customerorderinfo();
                } else {
                    if (confirm('Are you sure?')) {
                        $scope.dangclick = 1;
                        $rootScope.appLoaded = false;
                        $http.post($scope.myConfig.api_url + 'luutru/orders', {
                            order: $scope.orderInfo,
                            items: $scope.orders,
                            customer: $scope.customerInfo,
                            shipping: $scope.shippingInfo
                        }).success(function (response) {
                            $rootScope.appLoaded = true;
                            $scope.dangclick = 0;
                            if (response.status == 'success') {
                                $scope.shippingInfo.shipping_on_text = response.data.shipping_on_text;
                                mySocket.emit('sendMessage', {
                                    action: 'editOrder',
                                    data: response.data,
                                    users: response.users
                                });
                                // $state.go('home.orders');
                                window.close();
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                    //console.log($scope.customerInfo, $scope.shippingInfo, $scope.orders);
                }
            }
        };
        $scope.openModal = function (loai, item, size) {
            $scope.loai = loai;
            if (loai=='additem') {
                $scope.selected = angular.copy(item);
            } else {
                $scope.selected = item;
            }
            $scope.selected.selected_size = size;
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'myModal.html',
                windowClass: 'modal-primary',
                size: 'lg',
                scope: $scope
            });
        };
        $scope.closeModal = function () {
            $scope.modalInstance.close();
        };
    })
.controller('OrdersController', function ($scope, $rootScope, $http, mySocket, myAudio, $state, $timeout, $uibModal, $stateParams) {
        $scope.page2 = $stateParams.page || 1;
        $rootScope.page = 'orders';
        $rootScope.titlepage = 'Đơn hàng';
        $scope.pagination = {
            currentPage: 1,
            numPerPage: '50',
            totalItems: 0,
            begin: 0,
            end: 0
        };
        $scope.pagination2 = {
            currentPage: 1,
            numPerPage: '20',
            totalItems: 0,
            begin: 0,
            end: 0
        };
        $scope.conditions = {
            loai: 0,
            branch: '',
            idv: '',
            status: '',
            name: '',
            phone: ''
        };
        $scope.chuyenchinhanh = {id: 0};
        $scope.lydohuydonhang = {
            loai: '1',
            noidung: ''
        };
        $scope.danhsachlydohuy = [
            {
                id: '1',
                name: 'Khách yêu cầu hủy'
            },
            {
                id: '2',
                name: 'Tạo đơn hàng sai hoặc trùng'
            },
            {
                id: '3',
                name: 'Không đủ nguyên liệu pha chế'
            },
            {
                id: '4',
                name: 'Không đáp ứng thời điểm giao'
            },
            {
                id: '5',
                name: 'Lý do khác'
            }
        ];
        $scope.orderStatus = [
            {
                id: '0',
                name: 'Đang chờ'
            },
            {
                id: '1',
                name: 'Đang xử lý'
            },
            {
                id: '2',
                name: 'Đang giao'
            },
            {
                id: '3',
                name: 'Đã giao'
            },
            {
                id: '4',
                name: 'Hoàn tất'
            },
            {
                id: '5',
                name: 'Hủy'
            }
        ];
        //$scope.orderEditLists = [];
        $scope.$on('updateEditLists', function (event, data) {
            //console.log(event, data);
            switch (data.action) {
                case 'updateorders':
                    if (data.data.length) {
                        angular.forEach(data.data, function (value) {
                            angular.forEach($scope.danhsach, function (value2, index) {
                                if (value.id == value2.id) {
                                    $scope.danhsach[index].status = -1;
                                    $scope.danhsach[index].status_class = 'dangsua';
                                    $scope.danhsach[index].status_text = 'Đang sửa';
                                    $scope.danhsach[index].chuyenhoadon = false;
                                    $scope.danhsach[index].chuyenlaihoadon = false;
                                }
                            });
                        });
                    }
                    break;
                case 'refreshorder':
                    var isexist = -1;
                    angular.forEach($scope.danhsach, function (value, index) {
                        if (value.id==parseInt(data.id)) {
                            isexist = index;
                        }
                    });
                    if (isexist>=0) {
                        $http.post($scope.myConfig.api_url + 'refresh/orders', {
                            id: data.id
                        }).success(function (response) {
                            if (response.status == 'success') {
                                var item = response.data;
                                angular.forEach($scope.danhsach, function (value, index) {
                                    if (value.id == item.id) {
                                        for (var prop in item) {
                                            if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                            $scope.danhsach[index][prop] = item[prop];
                                        }
                                    }
                                });
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                    break;
                case 'neworder':
                    var isexist = $rootScope.globals.currentUser.branches.indexOf(data.data.branch_id);
                    if (isexist>=0) {
                        var tmp = angular.copy(data.data);
                        var isexist2 = -1;
                        angular.forEach($scope.danhsach, function (value, index) {
                            if (value.id==parseInt(tmp.id)) {
                                isexist2 = index;
                            }
                        });
                        if (isexist2==-1) {
                            $scope.danhsach.unshift(tmp);
                            $scope.filteredOrders = $scope.danhsach;
                            //console.log($scope.danhsach, $scope.filteredOrders, tmp);
                        }
                    }
                    break;
                case 'moveorder':
                    //console.log(data.data);
                    var isexist = $rootScope.globals.currentUser.branches.indexOf(data.data.toBranch);
                    var tmp = angular.copy(data.data.data);
                    if (isexist>=0) {
                        var isexist2 = -1;
                        angular.forEach($scope.danhsach, function (value, index) {
                            if (value.id==parseInt(tmp.id)) {
                                isexist2 = index;
                            }
                        });
                        if (isexist2==-1) {
                            $scope.danhsach.unshift(tmp);
                            $scope.filteredOrders = $scope.danhsach;
                            //console.log($scope.danhsach, $scope.filteredOrders, tmp);
                        } else {
                            $scope.danhsach[isexist2] = tmp;
                            $scope.filteredOrders = $scope.danhsach;
                        }
                    } else {
                        var isexist2 = -1;
                        angular.forEach($scope.danhsach, function (value, index) {
                            if (value.id==parseInt(tmp.id)) {
                                isexist2 = index;
                            }
                        });
                        if (isexist2>=0) {
                            $scope.danhsach.splice(isexist2, 1);
                            $scope.filteredOrders = $scope.danhsach;
                        }
                    }
                    break;
            }
        });
        $scope.danhsach = [];
        $scope.branchList = [];
        $scope.branchList2 = [];
        $scope.filteredOrders = [];
        $scope.init = function () {
            $http.post($scope.myConfig.api_url + 'danhsach/orders', {page: $scope.page2, numPerPage: $scope.pagination.numPerPage}).success(function (response) {
                $scope.branchList = response.branchList;
                $scope.branchList2 = response.branchList2;
                $scope.danhsach = response.data;
                $scope.pagination.totalItems = response.totalItems;//$scope.danhsach.length;
                $scope.pagination.currentPage = response.currentPage;//$scope.page2;
                $scope.pagination.begin = response.begin;
                $scope.pagination.end = response.end;//>$scope.pagination.totalItems?$scope.pagination.totalItems:response.end;
                $scope.filteredOrders = $scope.danhsach;
                //$scope.pageChanged();
                $rootScope.appLoaded = true;
                mySocket.emit('getOrderEditLists', {});
                $timeout($scope.init, 300000);
            });
        };
        //$scope.init();
        $timeout($scope.init, 300000);
        $scope.pageChanged = function () {
            $state.go('home.orders', {page: $scope.pagination.currentPage});
        };
        $scope.pageChanged2 = function () {
            $scope.timkiem($scope.pagination2.currentPage);
        };
        $scope.filter_order = function () {
            $scope.timkiem(1);
        };
        $scope.reset_order = function () {
            $scope.pagination2 = {
                currentPage: 1,
                numPerPage: '20',
                totalItems: 0,
                begin: 0,
                end: 0
            };
            $scope.conditions = {
                loai: 0,
                branch: '',
                idv: '',
                status: '',
                name: '',
                phone: ''
            };
            //$state.go('home.orders');
            $scope.init();
        };
        $scope.myFunct = function(keyEvent) {
            if (keyEvent.which === 13)
                $scope.filter_order();
        };
        $scope.timkiem = function (page) {
            $http.post($scope.myConfig.api_url + 'danhsach/orders', {dieukien: $scope.conditions, page: page, numPerPage: $scope.pagination2.numPerPage}).success(function (response) {
                $scope.danhsach = response.data;
                $scope.pagination2.totalItems = response.totalItems;//$scope.danhsach.length;
                $scope.pagination2.currentPage = response.currentPage;//$scope.page2;
                $scope.pagination2.begin = response.begin;
                $scope.pagination2.end = response.end;//>$scope.pagination.totalItems?$scope.pagination.totalItems:response.end;
                $scope.filteredOrders = $scope.danhsach;
                $scope.conditions.loai = 1;
                mySocket.emit('getOrderEditLists', {});
            });
        };
        $scope.toigio = function (thoigiangiaohang) {
            var hientai = new Date();
            var thoigianchapnhangiaohang = new Date(thoigiangiaohang.year, (thoigiangiaohang.month-1), thoigiangiaohang.day, thoigiangiaohang.hour, thoigiangiaohang.minute, 0, 0);
            return (hientai.getTime()>=thoigianchapnhangiaohang.getTime());
        };
        /*$scope.chuoitoppings = function (item) {
            var tmp = [];
            angular.forEach (item, function (value) {
                if (value.qty > 0) {
                    tmp.push(value.name + ' (' + value.qty + ')');
                }
            });
            return tmp.join(', ');
        };
        $scope.thanhtientoppings = function (item) {
            var tmp = 0;
            angular.forEach (item, function (value) {
                if (value.qty > 0) {
                    tmp += (value.qty * value.price);
                }
            });
            return tmp;
        };*/
        $scope.chuanbiGuiBinhluan = function (item, $event) {
            if ($event.keyCode==13) {
                $scope.sendComment(item);
            }
        };
        $scope.dangclick3 = 0;
        $scope.xacnhanhuydonhang = function () {
            if ($scope.lydohuydonhang.loai=='5'&&$scope.lydohuydonhang.noidung=='') {
                alert('Xin hãy nhập thông tin lý do hủy đơn hàng!');
            } else {
                if (!$scope.dangclick3) {
                    if ($scope.lydohuydonhang.loai != '5') {
                        $scope.lydohuydonhang.noidung = $scope.danhsachlydohuy[parseInt($scope.lydohuydonhang.loai) - 1].name;
                    }
                    if (confirm('Bạn có chắc chắn không?')) {
                        $scope.dangclick3 = 1;
                        $http.post($scope.myConfig.api_url + 'capnhat/orders', {
                            id: $scope.selected.id,
                            status: 5,
                            lydo: $scope.lydohuydonhang.noidung
                        }).success(function (response) {
                            $scope.dangclick3 = 0;
                            if (response.status == 'success') {
                                $scope.closeModal();
                                $scope.lydohuydonhang = {
                                    loai: '1',
                                    noidung: ''
                                };
                                mySocket.emit('sendMessage', {
                                    action: 'cancelOrder',
                                    data: response.data,
                                    users: response.users,
                                    createuser: response.createuser
                                });
                                $scope.selected = response.data;
                                angular.forEach($scope.danhsach, function (value, index) {
                                    if (value.id == $scope.selected.id) {
                                        for (var prop in $scope.selected) {
                                            if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                            $scope.danhsach[index][prop] = $scope.selected[prop];
                                        }
                                    }
                                });
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            }
        };
        $scope.dangclick2 = 0;
        $scope.sendComment = function (item) {
            if (item.comment_text) {
                if (!$scope.dangclick2) {
                    $scope.dangclick2 = 1;
                    $http.post($scope.myConfig.api_url + 'binhluan/orders', {
                        id: item.id,
                        comment: item.comment_text
                    }).success(function (response) {
                        $scope.dangclick2 = 0;
                        if (response.status == 'success') {
                            mySocket.emit('sendMessage', {action: 'commentOrder', data: response.data, users: response.users, createuser: response.createuser, comment: response.comment});
                            item = response.data;
                            angular.forEach($scope.danhsach, function (value, index) {
                                if (value.id == item.id) {
                                    $scope.danhsach[index].comment_text = '';
                                    for (var prop in item) {
                                        if(!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                        $scope.danhsach[index][prop] = item[prop];
                                    }
                                }
                            });
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            } else {
                alert('Xin hãy nhập thông tin bình luận đầy đủ!');
            }
        };
        $scope.dangclick = 0;
        $scope.updatestatus = function (item, giatri) {
            if (!$scope.dangclick) {
                $scope.dangclick = 1;
                if (giatri==1) {
                    myAudio.stop('new');
                }
                $http.post($scope.myConfig.api_url + 'capnhat/orders', {
                    id: item.id,
                    status: giatri
                }).success(function (response) {
                    $scope.dangclick = 0;
                    if (response.status == 'success') {
                        mySocket.emit('sendMessage', {action: 'updateStatus', data: response.data, users: response.users});
                        item = response.data;
                        angular.forEach($scope.danhsach, function (value, index) {
                            if (value.id == item.id) {
                                //if (item.status >= 3) {
                                    //$scope.danhsach.splice(index, 1);
                                //} else {
                                    for (var prop in item) {
                                        if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                        $scope.danhsach[index][prop] = item[prop];
                                    }
                                //}
                            }
                        });
                        if (giatri==1) {
                            $scope.openModal('print', item);
                        }
                    } else {
                        alert(response.msg);
                    }
                });
            }
        };
        $scope.dangclick = 0;
        $scope.baotre = function (item) {
            if (!$scope.dangclick) {
                $scope.dangclick = 1;
                $http.post($scope.myConfig.api_url + 'baotre/orders', {
                    id: item.id
                }).success(function (response) {
                    $scope.dangclick = 0;
                    if (response.status == 'success') {
                        //mySocket.emit('sendMessage', {action: 'updateStatus', data: response.data, users: response.users});
                        item = response.data;
                        angular.forEach($scope.danhsach, function (value, index) {
                            if (value.id == item.id) {
                                //if (item.status >= 3) {
                                    //$scope.danhsach.splice(index, 1);
                                //} else {
                                    for (var prop in item) {
                                        if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                        $scope.danhsach[index][prop] = item[prop];
                                    }
                                //}
                            }
                        });
                        /*if (giatri==1) {
                            $scope.openModal('print', item);
                        }*/
                    } else {
                        alert(response.msg);
                    }
                });
            }
        };
        $scope.updateshipper = function (item) {
            if (!$scope.dangclick) {
                if (parseInt(item.shipper_id)>0) {
                    $scope.dangclick = 1;
                    $http.post($scope.myConfig.api_url + 'capnhat/orders', {
                        id: item.id,
                        status: 2,
                        shipper_id: item.shipper_id
                    }).success(function (response) {
                        $scope.dangclick = 0;
                        if (response.status == 'success') {
                            mySocket.emit('sendMessage', {action: 'updateShipper', data: response.data, users: response.users});
                            item = response.data;
                            angular.forEach($scope.danhsach, function (value, index) {
                                if (value.id == item.id) {
                                    //if (item.status >= 3) {
                                    //$scope.danhsach.splice(index, 1);
                                    //} else {
                                    for (var prop in item) {
                                        if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                        $scope.danhsach[index][prop] = item[prop];
                                    }
                                    //}
                                }
                            });
                        } else {
                            alert(response.msg);
                        }
                    });
                } else {
                    alert('Xin hãy nhập nhân viên giao để thực hiện thao tác!');
                }
            }
        };
        $scope.printDiv = function(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var w = 800;
            var h = 600;
            var left = (screen.width/2)-(w/2);
            var top = (screen.height/2)-(h/2);

            /*var data = '<!DOCTYPE html><html><head><link rel="stylesheet" type="text/css" href="'+$scope.myConfig.asset_url+'global/plugins/bootstrap/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="'+$scope.myConfig.asset_url+'global/css/components.css" /><link rel="stylesheet" type="text/css" href="'+$scope.myConfig.asset_url+'css/myprintstyles.css" /></head><body class="page" onload=""><table><tr><td width="48%">' + printContents + '</td><td width="2%" class="divider-vertical"></td><td width="48%">' + printContents + '</td></tr></table></body></html>';
            var myWindow = window.open("data:text/html," + encodeURIComponent(data),
                "_blank", "width=200,height=100");
            myWindow.focus();*/

            var popupWin = window.open('', '_blank', 'width='+w+',height='+h+',top='+top+',left='+left);
            popupWin.document.open();
            popupWin.document.write('<!DOCTYPE html><html><head><link rel="stylesheet" type="text/css" href="'+$scope.myConfig.asset_url+'global/plugins/bootstrap/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="'+$scope.myConfig.asset_url+'global/css/components.css" /><link rel="stylesheet" type="text/css" href="'+$scope.myConfig.asset_url+'css/myprintstyles.css" /></head><body class="page" onload="window.print();window.close()"><table><tr><td width="48%">' + printContents + '</td><td width="2%" class="divider-vertical"></td><td width="48%">' + printContents + '</td></tr></table></body></html>');
            popupWin.document.close();
            $http.post($scope.myConfig.api_url + 'capnhat/orders', {
                id: $scope.selected.id,
                print_count: $scope.selected.print_count + 1,
                editted: 0
            }).success(function (response) {
                if (response.status == 'success') {
                    mySocket.emit('sendMessage', {action: 'printOrder', data: response.data, users: response.users});
                    $scope.selected = response.data;
                    angular.forEach($scope.danhsach, function (value, index) {
                        if (value.id == $scope.selected.id) {
                            //if ($scope.selected.status >= 3) {
                            //$scope.danhsach.splice(index, 1);
                            //} else {
                            for (var prop in $scope.selected) {
                                if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                $scope.danhsach[index][prop] = $scope.selected[prop];
                            }
                            //}
                        }
                    });
                } else {
                    alert(response.msg);
                }
            });
        };
        $scope.xacnhanchuyenchinhanh = function () {
            if ($scope.chuyenchinhanh.id) {
                if (!$scope.dangclick3) {
                    if (confirm('Are you sure?')) {
                        $scope.dangclick3 = 1;
                        $http.post($scope.myConfig.api_url + 'capnhat/orders', {
                            id: $scope.selected.id,
                            original_branch_id: $scope.selected.branch_id,
                            branch_id: $scope.chuyenchinhanh.id,
                            action: 'move'
                        }).success(function (response) {
                            $scope.dangclick3 = 0;
                            if (response.status == 'success') {
                                mySocket.emit('sendMessage', {
                                    action: 'moveOrder',
                                    data: response.data,
                                    users: response.users,
                                    fromBranch: $scope.selected.branch_id,
                                    toBranch: $scope.chuyenchinhanh.id,
                                    createuser: $rootScope.globals.currentUser.userid
                                });
                                $scope.closeModal();
                                $scope.chuyenchinhanh = {
                                    id: 0
                                };
                                $scope.selected = response.data;
                                angular.forEach($scope.danhsach, function (value, index) {
                                    if (value.id == $scope.selected.id) {
                                        for (var prop in $scope.selected) {
                                            if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                            $scope.danhsach[index][prop] = $scope.selected[prop];
                                        }
                                    }
                                });
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            } else {
                alert('Xin hãy chọn chi nhánh chuyển đơn hàng!');
            }
        };
        $scope.xacnhanchuyenlaihoadon = function (item) {
                if (!$scope.dangclick3) {
                    if (confirm('Are you sure?')) {
                        $scope.dangclick3 = 1;
                        $http.post($scope.myConfig.api_url + 'capnhat/orders', {
                            id: item.id,
                            original_branch_id: -1,
                            branch_id: item.original_branch_id,
                            action: 'moveback'
                        }).success(function (response) {
                            $scope.dangclick3 = 0;
                            if (response.status == 'success') {
                                mySocket.emit('sendMessage', {
                                    action: 'movebackOrder',
                                    data: response.data,
                                    users: response.users,
                                    fromBranch: item.branch_id,
                                    toBranch: item.original_branch_id,
                                    createuser: $rootScope.globals.currentUser.userid
                                });
                                var tmp = response.data;
                                angular.forEach($scope.danhsach, function (value, index) {
                                    if (value.id == tmp.id) {
                                        for (var prop in tmp) {
                                            if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                            $scope.danhsach[index][prop] = tmp[prop];
                                        }
                                    }
                                });
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
        };
        $scope.openModal = function (loai, item) {
            $scope.loai = loai;
            $scope.selected = angular.copy(item);
            if (loai=='chuyenchinhanh') {
                $scope.danhsachchinhanh = [];
                angular.forEach($scope.branchList, function(value) {
                    if (value.id!=$scope.selected.branch_id) {//&&($rootScope.globals.currentUser.branches.indexOf(value.id)>=0)
                        $scope.danhsachchinhanh.push(value);
                    }
                });
            } else if (loai=='print'&&item.status_class=='dachinhsua') {
                /*$http.post($scope.myConfig.api_url + 'capnhat/orders', {
                    id: item.id,
                    editted: 0,
                    //action: 'print2'
                }).success(function (response) {
                    if (response.status == 'success') {
                        var tmp = response.data;
                        angular.forEach($scope.danhsach, function (value, index) {
                            if (value.id == tmp.id) {
                                for (var prop in tmp) {
                                    if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                    $scope.danhsach[index][prop] = tmp[prop];
                                }
                            }
                        });
                    } else {
                        alert(response.msg);
                    }
                });*/
            }
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'myModal.html',
                windowClass: 'modal-primary',
                size: 'md',
                scope: $scope
            });
        };
        $scope.closeModal = function () {
            $scope.modalInstance.close();
        };
        $scope.chinhsua = function (item) {
            if (item.status==-1) {
                alert('Hóa đơn đang chỉnh sửa. Hãy chờ trong giây lát!');
            } else if (item.status==0 || item.status==1) {
                $state.go('home.editorder', {id: item.id});
            }
        };
        $scope.$watch("pagination.numPerPage", function() {
            //$scope.pageChanged();
            $scope.init();
        });
    });