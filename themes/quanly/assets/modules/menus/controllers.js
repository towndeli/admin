'use strict';

angular.module('Menus', ['ui.router'])
    .config(function ($stateProvider) {
        $stateProvider
            // HOME STATES AND NESTED VIEWS ========================================
            .state('home.menus', {
                url: '/menus/:id/:page',
                controller: 'MenusController',
                templateUrl: api_url + 'pages/menus'
            })
            .state('home.createmenu', {
                url: '/createmenu',
                controller: 'CreateMenusController',
                templateUrl: api_url + 'pages/createmenu'
            });
    })
    .controller('MenusController', function ($scope, $rootScope, $http, mySocket, myAudio, $stateParams, $uibModal, Upload) {
        $scope.id = $stateParams.id || 0;
        $rootScope.page = 'menus';
        $rootScope.titlepage = 'Thức uống';
        $scope.pagination = {
            currentPage: 1,
            numPerPage: '20',
            totalItems: 0,
            begin: 0,
            end: 0
        };
        $scope.danhsach = [];
        $scope.filteredOrders = [];
        $scope.toppings = [];
        $scope.categories = [];
        $scope.branchInfo = {};
        $scope.uniqueId = '';
        $scope.init = function () {
            $http.post($scope.myConfig.api_url + 'danhsach/menus', {id: $scope.id}).success(function (response) {
                $scope.danhsach = response.data;
                $scope.categories = response.categories;
                $scope.toppings = response.toppings;
                $scope.uniqueId = response.uniqueId;
                $scope.branchInfo = response.branchInfo;
                $scope.pagination.totalItems = $scope.danhsach.length;
                $scope.pagination.currentPage = $stateParams.page || 1;
                $scope.pageChanged();
                $rootScope.appLoaded = true;
            });
        };
        $scope.init();
        $scope.pageChanged = function () {
            var numPerPage = parseInt($scope.pagination.numPerPage, 10),
                begin = (($scope.pagination.currentPage - 1) * numPerPage),
                end = begin + numPerPage;
            $scope.pagination.begin = begin + 1;
            $scope.pagination.end = end>$scope.pagination.totalItems?$scope.pagination.totalItems:end;
            $scope.filteredOrders = $scope.danhsach.slice(begin, end);
        };
        $scope.dangclick = 0;
        $scope.luutruregion = function () {
            if (!$scope.dangclick) {
                if (confirm('Are you sure?')) {
                    $rootScope.appLoaded = false;
                    $scope.dangclick = 1;
                    $http.post($scope.myConfig.api_url + 'luutru/menus', {
                        thongtin: $scope.branchInfo
                    }).success(function (response) {
                        $rootScope.appLoaded = true;
                        $scope.dangclick = 0;
                        if (response.status == 'success') {
                            $scope.init();
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            }
        };
        $scope.dangclick2 = 0;
        $scope.updatestatus = function (item) {
            if (!$scope.dangclick2) {
                if (confirm('Are you sure?')) {
                    $scope.dangclick2 = 1;
                    $http.post($scope.myConfig.api_url + 'capnhat/menus', {
                        id: item.id,
                        status: !item.status
                    }).success(function (response) {
                        $scope.dangclick2 = 0;
                        if (response.status == 'success') {
                            item = response.data;
                            angular.forEach($scope.danhsach, function (value, index) {
                                if (value.id == item.id) {
                                    for (var prop in item) {
                                        if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                        $scope.danhsach[index][prop] = item[prop];
                                    }
                                }
                            });
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            }
        };
        $scope.capnhattoppingmon = function () {
            var tmp = [];
            angular.forEach($scope.toppings, function (value) {
                if (value.checked) {
                    tmp.push(value.id);
                }
            });
            $scope.branchInfo.toppings = tmp;
            $scope.closeModal();
        };
        $scope.progressPercentage = 0;
        $scope.upload = function (file) {
            Upload.upload({
                url: $scope.myConfig.api_url + 'upload/menus',
                data: {file: file, id: $scope.id, uniqueId: $scope.uniqueId}
            }).then(function (resp) {
                if (resp.data.status=='success') {
                    $scope.branchInfo.uniqueId = resp.data.data;
                } else {
                    alert(resp.data.msg);
                }
                $scope.progressPercentage = 0;
            }, function (resp) {
                $scope.progressPercentage = 0;
            }, function (evt) {
                $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });
        };
        $scope.openModal = function (loai) {
            $scope.loai = loai;
            angular.forEach($scope.toppings, function (value) {
                if ($scope.branchInfo.toppings.indexOf(value.id)>=0) {
                    value.checked = true;
                } else {
                    value.checked = false;
                }
            });
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'myModal.html',
                windowClass: 'modal-primary',
                size: 'lg',
                scope: $scope
            });
        };
        $scope.closeModal = function () {
            $scope.modalInstance.close();
        };
        $scope.$watch("pagination.numPerPage", function() {
            $scope.pageChanged();
        });
    })
    .controller('CreateMenusController', function ($scope, $rootScope, $http, mySocket, myAudio, $state, $uibModal, Upload) {
        $rootScope.page = 'createmenu';
        $rootScope.titlepage = 'Tạo thức uống';
        $scope.branchInfo = {};
        $scope.toppings = [];
        $scope.categories = [];
        $scope.uniqueId = '';
        $scope.init = function () {
            $http.post($scope.myConfig.api_url + 'chuanbi/menus', {}).success(function (response) {
                $scope.branchInfo = response.branchInfo;
                $scope.uniqueId = response.uniqueId;
                $scope.categories = response.categories;
                $scope.toppings = response.toppings;
                $rootScope.appLoaded = true;
            });
        };
        $scope.init();
        $scope.dangclick = 0;
        $scope.luutruregion = function () {
            if (!$scope.dangclick) {
                if (confirm('Are you sure?')) {
                    $rootScope.appLoaded = false;
                    $scope.dangclick = 1;
                    $http.post($scope.myConfig.api_url + 'luutru/menus', {
                        thongtin: $scope.branchInfo
                    }).success(function (response) {
                        $rootScope.appLoaded = true;
                        $scope.dangclick = 0;
                        if (response.status == 'success') {
                            $state.go('home.menus')
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            }
        };
        $scope.capnhattoppingmon = function () {
            var tmp = [];
            angular.forEach($scope.toppings, function (value) {
                if (value.checked) {
                    tmp.push(value.id);
                }
            });
            $scope.branchInfo.toppings = tmp;
            $scope.closeModal();
        };
        $scope.progressPercentage = 0;
        $scope.upload = function (file) {
            Upload.upload({
                url: $scope.myConfig.api_url + 'upload/menus',
                data: {file: file, id: $scope.id, uniqueId: $scope.uniqueId}
            }).then(function (resp) {
                if (resp.data.status=='success') {
                    $scope.branchInfo.uniqueId = resp.data.data;
                } else {
                    alert(resp.data.msg);
                }
                $scope.progressPercentage = 0;
            }, function (resp) {
                $scope.progressPercentage = 0;
            }, function (evt) {
                $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });
        };
        $scope.openModal = function (loai) {
            $scope.loai = loai;
            angular.forEach($scope.toppings, function (value) {
                if ($scope.branchInfo.toppings.indexOf(value.id)>=0) {
                    value.checked = true;
                } else {
                    value.checked = false;
                }
            });
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'myModal.html',
                windowClass: 'modal-primary',
                size: 'lg',
                scope: $scope
            });
        };
        $scope.closeModal = function () {
            $scope.modalInstance.close();
        };
    });