'use strict';

angular.module('Users', ['ui.router'])
    .config(function ($stateProvider) {
        $stateProvider
            // HOME STATES AND NESTED VIEWS ========================================
            .state('home.profile', {
                url: '/profile/:id',
                controller: 'ProfileController',
                templateUrl: api_url + 'pages/profile'
            })
            .state('home.users', {
                url: '/users/:id/:page',
                controller: 'UsersController',
                templateUrl: api_url + 'pages/users'
            })
            .state('home.createuser', {
                url: '/createuser',
                controller: 'CreateUsersController',
                templateUrl: api_url + 'pages/createuser'
            });
    })
    .controller('ProfileController', function ($scope, $rootScope, $http, mySocket, myAudio, $stateParams, Upload) {
        $scope.id = $stateParams.id || 0;
        $rootScope.page = 'profile';
        $rootScope.titlepage = 'My profile';
        $scope.pagination = {
            currentPage: 1,
            numPerPage: '20',
            totalItems: 0,
            begin: 0,
            end: 0
        };
        $scope.danhsach = [];
        $scope.filteredOrders = [];
        $scope.account_types = [];
        $scope.regions = [];
        $scope.branchInfo = {};
        $scope.uniqueId = '';
        $scope.init = function () {
            $http.post($scope.myConfig.api_url + 'danhsach/users', {id: $scope.id, loai: ''}).success(function (response) {
                $scope.danhsach = response.data;
                $scope.regions = response.regions;
                $scope.account_types = response.account_types;
                $scope.uniqueId = response.uniqueId;
                $scope.branchInfo = response.branchInfo;
                $scope.branchInfo.birthday = new Date(response.branchInfo.birthday * 1000);
                $scope.chonRegion($scope.branchInfo.role_tbl);
                $scope.pagination.totalItems = $scope.danhsach.length;
                $scope.pagination.currentPage = $stateParams.page || 1;
                $scope.pageChanged();
                $rootScope.appLoaded = true;
            });
        };
        $scope.init();
        $scope.pageChanged = function () {
            var numPerPage = parseInt($scope.pagination.numPerPage, 10),
                begin = (($scope.pagination.currentPage - 1) * numPerPage),
                end = begin + numPerPage;
            $scope.pagination.begin = begin + 1;
            $scope.pagination.end = end>$scope.pagination.totalItems?$scope.pagination.totalItems:end;
            $scope.filteredOrders = $scope.danhsach.slice(begin, end);
        };
        $scope.tenRegion = function () {
            var tmp = 'Chọn 1 khu vực / chi nhánh...';
            angular.forEach($scope.regions, function (value) {
                if (value.id2==$scope.branchInfo.role_tbl) {
                    tmp = value.name;
                }
            });
            return tmp;
        };
        $scope.chonRegion = function (id) {
            $scope.branchInfo.role_tbl = id;
            angular.forEach($scope.regions, function (value) {
                if (value.id2==id) {
                    $scope.account_types = value.account_types;
                }
            });
        };
        $scope.dangclick = 0;
        $scope.luutruregion = function () {
            if (!$scope.dangclick) {
                if (confirm('Are you sure?')) {
                    $rootScope.appLoaded = false;
                    $scope.dangclick = 1;
                    $http.post($scope.myConfig.api_url + 'luutru2/users', {
                        thongtin: $scope.branchInfo
                    }).success(function (response) {
                        $rootScope.appLoaded = true;
                        $scope.dangclick = 0;
                        if (response.status == 'success') {
                            $scope.init();
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            }
        };
        $scope.dangclick2 = 0;
        $scope.updatestatus = function (item) {
            if (!$scope.dangclick2) {
                if (confirm('Are you sure?')) {
                    $scope.dangclick2 = 1;
                    $http.post($scope.myConfig.api_url + 'capnhat/users', {
                        id: item.id,
                        status: !item.status
                    }).success(function (response) {
                        $scope.dangclick2 = 0;
                        if (response.status == 'success') {
                            item = response.data;
                            angular.forEach($scope.danhsach, function (value, index) {
                                if (value.id == item.id) {
                                    for (var prop in item) {
                                        if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                        $scope.danhsach[index][prop] = item[prop];
                                    }
                                }
                            });
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            }
        };
        $scope.progressPercentage = 0;
        $scope.upload = function (file) {
            Upload.upload({
                url: $scope.myConfig.api_url + 'upload/users',
                data: {file: file, id: $scope.id, uniqueId: $scope.uniqueId}
            }).then(function (resp) {
                console.log(resp.data);
                if (resp.data.status=='success') {
                    $scope.branchInfo.uniqueId = resp.data.data;
                } else {
                    alert(resp.data.msg);
                }
                $scope.progressPercentage = 0;
            }, function (resp) {
                $scope.progressPercentage = 0;
            }, function (evt) {
                $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });
        };
        $scope.$watch("pagination.numPerPage", function() {
            $scope.pageChanged();
        });
    })
    .controller('UsersController', function ($scope, $rootScope, $http, mySocket, myAudio, $stateParams, Upload) {
        $scope.id = $stateParams.id || 0;
        $rootScope.page = 'users';
        $rootScope.titlepage = 'Thành viên';
        $scope.pagination = {
            currentPage: 1,
            numPerPage: '20',
            totalItems: 0,
            begin: 0,
            end: 0
        };
        $scope.danhsach = [];
        $scope.filteredOrders = [];
        $scope.account_types = [];
        $scope.regions = [];
        $scope.branchInfo = {};
        $scope.uniqueId = '';
        $scope.init = function () {
            $http.post($scope.myConfig.api_url + 'danhsach/users', {id: $scope.id, loai: ''}).success(function (response) {
                $scope.danhsach = response.data;
                $scope.regions = response.regions;
                $scope.account_types = response.account_types;
                $scope.uniqueId = response.uniqueId;
                $scope.branchInfo = response.branchInfo;
                $scope.branchInfo.birthday = new Date(response.branchInfo.birthday * 1000);
                $scope.chonRegion($scope.branchInfo.role_tbl);
                $scope.pagination.totalItems = $scope.danhsach.length;
                $scope.pagination.currentPage = $stateParams.page || 1;
                $scope.pageChanged();
                $rootScope.appLoaded = true;
            });
        };
        $scope.init();
        $scope.pageChanged = function () {
            var numPerPage = parseInt($scope.pagination.numPerPage, 10),
                begin = (($scope.pagination.currentPage - 1) * numPerPage),
                end = begin + numPerPage;
            $scope.pagination.begin = begin + 1;
            $scope.pagination.end = end>$scope.pagination.totalItems?$scope.pagination.totalItems:end;
            $scope.filteredOrders = $scope.danhsach.slice(begin, end);
        };
        $scope.tenRegion = function () {
            var tmp = 'Chọn 1 khu vực / chi nhánh...';
            angular.forEach($scope.regions, function (value) {
                if (value.id2==$scope.branchInfo.role_tbl) {
                    tmp = value.name;
                }
            });
            return tmp;
        };
        $scope.chonRegion = function (id) {
            $scope.branchInfo.role_tbl = id;
            angular.forEach($scope.regions, function (value) {
                if (value.id2==id) {
                    $scope.account_types = value.account_types;
                }
            });
        };
        $scope.dangclick = 0;
        $scope.luutruregion = function () {
            if (!$scope.dangclick) {
                if (confirm('Are you sure?')) {
                    $rootScope.appLoaded = false;
                    $scope.dangclick = 1;
                    $http.post($scope.myConfig.api_url + 'luutru/users', {
                        thongtin: $scope.branchInfo
                    }).success(function (response) {
                        $rootScope.appLoaded = true;
                        $scope.dangclick = 0;
                        if (response.status == 'success') {
                            $scope.init();
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            }
        };
        $scope.dangclick2 = 0;
        $scope.updatestatus = function (item) {
            if (!$scope.dangclick2) {
                if (confirm('Are you sure?')) {
                    $scope.dangclick2 = 1;
                    $http.post($scope.myConfig.api_url + 'capnhat/users', {
                        id: item.id,
                        status: !item.status
                    }).success(function (response) {
                        $scope.dangclick2 = 0;
                        if (response.status == 'success') {
                            item = response.data;
                            angular.forEach($scope.danhsach, function (value, index) {
                                if (value.id == item.id) {
                                    for (var prop in item) {
                                        if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                        $scope.danhsach[index][prop] = item[prop];
                                    }
                                }
                            });
                        } else {
                            alert(response.msg);
                        }
                    });
                }
            }
        };
        $scope.progressPercentage = 0;
        $scope.upload = function (file) {
            Upload.upload({
                url: $scope.myConfig.api_url + 'upload/users',
                data: {file: file, id: $scope.id, uniqueId: $scope.uniqueId}
            }).then(function (resp) {
                console.log(resp.data);
                if (resp.data.status=='success') {
                    $scope.branchInfo.uniqueId = resp.data.data;
                } else {
                    alert(resp.data.msg);
                }
                $scope.progressPercentage = 0;
            }, function (resp) {
                $scope.progressPercentage = 0;
            }, function (evt) {
                $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });
        };
        $scope.$watch("pagination.numPerPage", function() {
            $scope.pageChanged();
        });
    })
    .controller('CreateUsersController', function ($scope, $rootScope, $http, mySocket, myAudio, $state, Upload) {
        $rootScope.page = 'createuser';
        $rootScope.titlepage = 'Tạo thành viên';
        $scope.branchInfo = {};
        $scope.account_types = [];
        $scope.regions = [];
        $scope.uniqueId = '';
        $scope.init = function () {
            $http.post($scope.myConfig.api_url + 'chuanbi/users', {}).success(function (response) {
                $scope.regions = response.regions;
                $scope.account_types = response.account_types;
                $scope.uniqueId = response.uniqueId;
                $scope.branchInfo = response.branchInfo;
                $scope.branchInfo.birthday = new Date(response.branchInfo.birthday * 1000);
                $scope.chonRegion($scope.branchInfo.role_tbl);
                $rootScope.appLoaded = true;
            });
        };
        $scope.init();
        $scope.tenRegion = function () {
            var tmp = 'Chọn 1 khu vực / chi nhánh...';
            angular.forEach($scope.regions, function (value) {
                if (value.id2==$scope.branchInfo.role_tbl) {
                    tmp = value.name;
                }
            });
            return tmp;
        };
        $scope.chonRegion = function (id) {
            $scope.branchInfo.role_tbl = id;
            angular.forEach($scope.regions, function (value) {
                if (value.id2==id) {
                    $scope.account_types = value.account_types;
                    $scope.branchInfo.role = $scope.account_types[0].id;
                }
            });
        };
        $scope.dangclick = 0;
        $scope.luutruregion = function () {
            if (!$scope.dangclick) {
                if ($scope.branchInfo.role!=''&&$scope.branchInfo.role_tbl!='') {
                    if (confirm('Are you sure?')) {
                        $rootScope.appLoaded = false;
                        $scope.dangclick = 1;
                        $http.post($scope.myConfig.api_url + 'luutru/users', {
                            thongtin: $scope.branchInfo
                        }).success(function (response) {
                            $rootScope.appLoaded = true;
                            $scope.dangclick = 0;
                            if (response.status == 'success') {
                                $state.go('home.users')
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            }
        };
        $scope.progressPercentage = 0;
        $scope.upload = function (file) {
            Upload.upload({
                url: $scope.myConfig.api_url + 'upload/users',
                data: {file: file, id: $scope.id, uniqueId: $scope.uniqueId}
            }).then(function (resp) {
                if (resp.data.status=='success') {
                    $scope.branchInfo.uniqueId = resp.data.data;
                } else {
                    alert(resp.data.msg);
                }
                $scope.progressPercentage = 0;
            }, function (resp) {
                $scope.progressPercentage = 0;
            }, function (evt) {
                $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            });
        };
    });