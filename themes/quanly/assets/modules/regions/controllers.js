'use strict';

angular.module('Regions', ['ui.router'])
    .config(function ($stateProvider) {
        $stateProvider
            // HOME STATES AND NESTED VIEWS ========================================
            .state('home.regions', {
                url: '/regions/:id',
                controller: 'RegionsController',
                //templateUrl: asset_url + 'modules/home/views/home.html'
                templateUrl: api_url + 'pages/regions'
            })
            .state('home.createregion', {
                url: '/createregion',
                controller: 'CreateRegionsController',
                //templateUrl: asset_url + 'modules/home/views/home.html'
                templateUrl: api_url + 'pages/createregion'
            });
        /*$routeProvider

            .when('/regions', {
                controller: 'RegionsController',
                templateUrl: asset_url + 'modules/home/views/home.html'
            })

            .when('/regions/:id', {
                controller: 'RegionsController',
                templateUrl: asset_url + 'modules/home/views/home.html'
            })

            .when('/createregion', {
                controller: 'CreateRegionsController',
                templateUrl: asset_url + 'modules/home/views/home.html'
            });*/
    })
    .controller('RegionsController', function ($scope, $rootScope, $http, mySocket, myAudio, $stateParams) {
            $scope.id = $stateParams.id || 0;
            $rootScope.page = 'regions';
            $rootScope.titlepage = 'Khu vực';
            $scope.pagination = {
                currentPage: 1,
                numPerPage: '20',
                totalItems: 0,
                begin: 0,
                end: 0
            };
            $scope.danhsach = [];
            $scope.filteredOrders = [];
            $scope.regionInfo = {};
            $scope.rootregions = [];
            $scope.phamviregions = [];
            $scope.init = function () {
                $http.post($scope.myConfig.api_url + 'danhsach/regions', {id: $scope.id}).success(function (response) {
                    $scope.danhsach = response.data;
                    $scope.rootregions = response.regions;
                    $scope.regionInfo = response.regionInfo;
                    $scope.phamviregions = response.phamviregions;
                    //$scope.regionInfo.status = String($scope.regionInfo.status);
                    $scope.pagination.totalItems = $scope.danhsach.length;
                    $scope.pagination.currentPage = 1;
                    $scope.pageChanged();
                    $rootScope.appLoaded = true;
                });
            };
            $scope.init();
            $scope.pageChanged = function () {
                var numPerPage = parseInt($scope.pagination.numPerPage, 10),
                    begin = (($scope.pagination.currentPage - 1) * numPerPage),
                    end = begin + numPerPage;
                $scope.pagination.begin = begin + 1;
                $scope.pagination.end = end>$scope.pagination.totalItems?$scope.pagination.totalItems:end;
                $scope.filteredOrders = $scope.danhsach.slice(begin, end);
            };
            $scope.dangclick = 0;
            $scope.luutruregion = function () {
                //console.log($scope.regionInfo);
                if (!$scope.dangclick) {
                    if (confirm('Are you sure?')) {
                        $rootScope.appLoaded = false;
                        $scope.dangclick = 1;
                        $http.post($scope.myConfig.api_url + 'luutru/regions', {
                            thongtin: $scope.regionInfo
                        }).success(function (response) {
                            $rootScope.appLoaded = true;
                            $scope.dangclick = 0;
                            if (response.status == 'success') {
                                $scope.init();
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            };
            $scope.dangclick2 = 0;
            $scope.updatestatus = function (item) {
                if (!$scope.dangclick2) {
                    if (confirm('Are you sure?')) {
                        $scope.dangclick2 = 1;
                        $http.post($scope.myConfig.api_url + 'capnhat/regions', {
                            id: item.id,
                            status: !item.status
                        }).success(function (response) {
                            $scope.dangclick2 = 0;
                            if (response.status == 'success') {
                                item = response.data;
                                angular.forEach($scope.danhsach, function (value, index) {
                                    if (value.id == item.id) {
                                        for (var prop in item) {
                                            if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                            $scope.danhsach[index][prop] = item[prop];
                                        }
                                    }
                                });
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            };
            $scope.$watch("pagination.numPerPage", function() {
                $scope.pageChanged();
            });
        })
    .controller('CreateRegionsController',
        function ($scope, $rootScope, $http, mySocket, myAudio, $state) {
            $rootScope.page = 'createregion';
            $rootScope.titlepage = 'Tạo khu vực';
            $scope.regionInfo = {
                name: '',
                parent_id: '0',
                children_total: 0,
                phamvi: 'vn',
                status: '1'
            };
            $scope.rootregions = [];
            $scope.phamviregions = [];
            $scope.init = function () {
                $http.post($scope.myConfig.api_url + 'chuanbi/regions', {}).success(function (response) {
                    $scope.rootregions = response.data;
                    $scope.phamviregions = response.phamviregions;
                    $rootScope.appLoaded = true;
                });
            };
            $scope.init();
            $scope.dangclick = 0;
            $scope.luutruregion = function () {
                //console.log($scope.regionInfo);
                if (!$scope.dangclick) {
                    if (confirm('Are you sure?')) {
                        $rootScope.appLoaded = false;
                        $scope.dangclick = 1;
                        $http.post($scope.myConfig.api_url + 'luutru/regions', {
                            thongtin: $scope.regionInfo
                        }).success(function (response) {
                            $rootScope.appLoaded = true;
                            $scope.dangclick = 0;
                            if (response.status == 'success') {
                                $state.go('home.regions');
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            };
        });