'use strict';

angular.module('Categories', ['ui.router'])
    .config(function ($stateProvider) {
        $stateProvider
            // HOME STATES AND NESTED VIEWS ========================================
            .state('home.categories', {
                url: '/categories/:id',
                controller: 'CategoriesController',
                //templateUrl: asset_url + 'modules/home/views/home.html'
                templateUrl: api_url + 'pages/categories'
            })
            .state('home.createcategory', {
                url: '/createcategory',
                controller: 'CreateCategoriesController',
                //templateUrl: asset_url + 'modules/home/views/home.html'
                templateUrl: api_url + 'pages/createcategory'
            });
        /*$routeProvider

            .when('/categories', {
                controller: 'CategoriesController',
                templateUrl: asset_url + 'modules/home/views/home.html'
            })

            .when('/categories/:id', {
                controller: 'CategoriesController',
                templateUrl: asset_url + 'modules/home/views/home.html'
            })

            .when('/createcategory', {
                controller: 'CreateCategoriesController',
                templateUrl: asset_url + 'modules/home/views/home.html'
            });*/
    })
    .controller('CategoriesController',
        function ($scope, $rootScope, $http, mySocket, myAudio, $stateParams, Upload) {
            $scope.id = $stateParams.id || 0;
            $rootScope.page = 'categories';
            $rootScope.titlepage = 'Danh mục';
            $scope.pagination = {
                currentPage: 1,
                numPerPage: '20',
                totalItems: 0,
                begin: 0,
                end: 0
            };
            $scope.danhsach = [];
            $scope.filteredOrders = [];
            $scope.branchInfo = {};
            $scope.uniqueId = '';
            $scope.init = function () {
                $http.post($scope.myConfig.api_url + 'danhsach/categories', {id: $scope.id}).success(function (response) {
                    $scope.danhsach = response.data;
                    $scope.branchInfo = response.branchInfo;
                    $scope.pagination.totalItems = $scope.danhsach.length;
                    $scope.pagination.currentPage = 1;
                    $scope.uniqueId = response.uniqueId;
                    $scope.pageChanged();
                    $rootScope.appLoaded = true;
                });
            };
            $scope.init();
            $scope.pageChanged = function () {
                var numPerPage = parseInt($scope.pagination.numPerPage, 10),
                    begin = (($scope.pagination.currentPage - 1) * numPerPage),
                    end = begin + numPerPage;
                $scope.pagination.begin = begin + 1;
                $scope.pagination.end = end>$scope.pagination.totalItems?$scope.pagination.totalItems:end;
                $scope.filteredOrders = $scope.danhsach.slice(begin, end);
            };
            $scope.dangclick = 0;
            $scope.luutruregion = function () {
                if (!$scope.dangclick) {
                    if (confirm('Are you sure?')) {
                        $rootScope.appLoaded = false;
                        $scope.dangclick = 1;
                        $http.post($scope.myConfig.api_url + 'luutru/categories', {
                            thongtin: $scope.branchInfo
                        }).success(function (response) {
                            $rootScope.appLoaded = true;
                            $scope.dangclick = 0;
                            if (response.status == 'success') {
                                $scope.init();
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            };
            $scope.dangclick2 = 0;
            $scope.updatestatus = function (item) {
                if (!$scope.dangclick2) {
                    if (confirm('Are you sure?')) {
                        $scope.dangclick2 = 1;
                        $http.post($scope.myConfig.api_url + 'capnhat/categories', {
                            id: item.id,
                            status: !item.status
                        }).success(function (response) {
                            $scope.dangclick2 = 0;
                            if (response.status == 'success') {
                                item = response.data;
                                angular.forEach($scope.danhsach, function (value, index) {
                                    if (value.id == item.id) {
                                        for (var prop in item) {
                                            if (!$scope.danhsach[index].hasOwnProperty(prop)) continue;
                                            $scope.danhsach[index][prop] = item[prop];
                                        }
                                    }
                                });
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            };
            $scope.progressPercentage = 0;
            $scope.upload = function (file) {
                Upload.upload({
                    url: $scope.myConfig.api_url + 'upload/categories',
                    data: {file: file, id: $scope.id, uniqueId: $scope.uniqueId}
                }).then(function (resp) {
                    //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    console.log(resp.data);
                    if (resp.data.status=='success') {
                        $scope.branchInfo.uniqueId = resp.data.data;
                    } else {
                        alert(resp.data.msg);
                    }
                    $scope.progressPercentage = 0;
                }, function (resp) {
                    $scope.progressPercentage = 0;
                }, function (evt) {
                    $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            };
            $scope.$watch("pagination.numPerPage", function() {
                $scope.pageChanged();
            });
        })
    .controller('CreateCategoriesController', function ($scope, $rootScope, $http, mySocket, myAudio, $state, Upload) {
            $rootScope.page = 'createcategory';
            $rootScope.titlepage = 'Tạo danh mục';
            $scope.branchInfo = {};
            $scope.uniqueId = '';
            $scope.init = function () {
                $http.post($scope.myConfig.api_url + 'chuanbi/categories', {}).success(function (response) {
                    $scope.branchInfo = response.branchInfo;
                    $scope.uniqueId = response.uniqueId;
                    $rootScope.appLoaded = true;
                });
            };
            $scope.init();
            $scope.dangclick = 0;
            $scope.luutruregion = function () {
                if (!$scope.dangclick) {
                    if (confirm('Are you sure?')) {
                        $rootScope.appLoaded = false;
                        $scope.dangclick = 1;
                        $http.post($scope.myConfig.api_url + 'luutru/categories', {
                            thongtin: $scope.branchInfo
                        }).success(function (response) {
                            $rootScope.appLoaded = true;
                            $scope.dangclick = 0;
                            if (response.status == 'success') {
                                $state.go('home.categories');
                            } else {
                                alert(response.msg);
                            }
                        });
                    }
                }
            };
            $scope.progressPercentage = 0;
            $scope.upload = function (file) {
                Upload.upload({
                    url: $scope.myConfig.api_url + 'upload/categories',
                    data: {file: file, id: $scope.id, uniqueId: $scope.uniqueId}
                }).then(function (resp) {
                    //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    console.log(resp.data);
                    if (resp.data.status=='success') {
                        $scope.branchInfo.uniqueId = resp.data.data;
                    } else {
                        alert(resp.data.msg);
                    }
                    $scope.progressPercentage = 0;
                }, function (resp) {
                    $scope.progressPercentage = 0;
                }, function (evt) {
                    $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            };
        });