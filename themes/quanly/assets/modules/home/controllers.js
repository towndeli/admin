'use strict';
 
angular.module('Home', ['ui.router'])
    .config(function ($stateProvider) {
        $stateProvider
            // HOME STATES AND NESTED VIEWS ========================================
            .state('home.dashboard', {
                url: '/dashboard',
                controller: 'DashBoardController',
                //templateUrl: asset_url + 'modules/home/views/dashboard.html'
                templateUrl: api_url + 'pages/thongke'
            });
    })
.controller('HomeController', function ($scope, $rootScope, myConfig, $http, mySocket, myAudio, $notification, $state, $filter, geocoder, $q) {
        mySocket.forward('messageReceived');
        $scope.myConfig = myConfig;
        $rootScope.thunho = false;
        $scope.thunhosidebar = function () {
            $rootScope.thunho = !$rootScope.thunho;
        };
        $scope.Deg2Rad = function (deg) {
            return deg * Math.PI / 180;
        };
        $scope.PythagorasEquirectangular = function (lat1, lon1, lat2, lon2) {
            lat1 = $scope.Deg2Rad(lat1);
            lat2 = $scope.Deg2Rad(lat2);
            lon1 = $scope.Deg2Rad(lon1);
            lon2 = $scope.Deg2Rad(lon2);
            var R = 6371; // km
            var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
            var y = (lat2 - lat1);
            var d = Math.sqrt(x * x + y * y) * R;
            return d;
        };
        $scope.NearestCity = function (danhsachchinhanh, latitude, longitude) {
            var mindif = 99999;
            var closest = 0;
            angular.forEach(danhsachchinhanh, function(value, key){
                var dif = $scope.PythagorasEquirectangular(latitude, longitude, value.geo_latitude, value.geo_longitude);
                if (dif < mindif) {
                    closest = value.id;
                    mindif = dif;
                }
            });
            //$scope.NearestCity3(danhsachchinhanh, latitude, longitude).then(function(data){console.log(data)});
            return closest;
        };
        $scope.NearestCity3 = function (danhsachchinhanh, latitude, longitude) {
            var deferred2 = $q.defer();
            var promises = [];
            var mindif = 9999999;
            var closest = 0;
            var directionsService = new google.maps.DirectionsService();
            var danhsachchinhanh2 = angular.copy(danhsachchinhanh);
            var currentPosition = new google.maps.LatLng(latitude, longitude);
            if (danhsachchinhanh2.length) {
                angular.forEach(danhsachchinhanh2, function (value, index) {
                    var deferred = $q.defer();
                    var end = new google.maps.LatLng(value.geo_latitude, value.geo_longitude);
                    var request = {
                        origin: currentPosition,
                        destination: end,
                        travelMode: google.maps.TravelMode.DRIVING
                    };
                    directionsService.route(request, function(response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            value.distance = response.routes[0].legs[0].distance.text;
                            value.distance2 = response.routes[0].legs[0].distance.value;
                            deferred.resolve({id: value.id, distance: value.distance2});
                        }

                    });
                    promises.push(deferred.promise);
                });
                $q.all(promises).then(function (promise) {
                    angular.forEach(promise, function (value) {
                        var dif = value.distance;
                        if (dif < mindif) {
                            closest = value.id;
                            mindif = dif;
                        }
                    });
                    deferred2.resolve(closest);
                });
            }
            return deferred2.promise;
        };
        /*$scope.lancuoi = null;
        $scope.kiemtrahoatdong = function () {
            clearInterval($scope.lancuoi);
            $scope.lancuoi = setInterval(function(){
                $state.go('login');
            }, 1800000);
        };*/
        $scope.tangsoluong = function (item) {
            item.qty += 1;
        };
        $scope.giamsoluong = function (item, lonhonmot) {
            item.qty -= 1;
            if (item.qty < 0) {
                item.qty = 0;
            }
            if (lonhonmot!=undefined&&item.qty==0) {
                item.qty = lonhonmot;
            }
        };
        $scope.kiemtrasoluong = function (item, $event) {
            if ($event.keyCode==38) {
                $scope.tangsoluong(item);
            } else if ($event.keyCode==40) {
                $scope.giamsoluong(item, 1);
            }
        };
        $scope.chuoitoppings = function (item) {
            var tmp = [];
            angular.forEach (item, function (value) {
                if (value.qty > 0) {
                    tmp.push(value.name + ' (' + value.qty + ')');
                }
            });
            return tmp.join(', ');
        };
        $scope.thanhtientoppings = function (item) {
            var tmp = 0;
            angular.forEach (item, function (value) {
                if (value.qty > 0) {
                    tmp += (value.qty * value.price);
                }
            });
            return tmp;
        };
        $scope.getCustomerAddress = function (val) {
            /*var tmp = {
                address: val
            };
            if ($rootScope.globals.currentUser.quocgia&&$rootScope.globals.currentUser.role!='admin') {
                tmp.componentRestrictions = {
                    country: $rootScope.globals.currentUser.quocgia
                }
            }
            return geocoder.geocode_by_query_2(tmp).then(function (response) {
                console.log(response)
                return response.map(function(item){
                    return item;
                });
            });*/
            var tmp;
            if ($rootScope.globals.currentUser.quocgia&&$rootScope.globals.currentUser.role!='admin') {
                tmp = $rootScope.globals.currentUser.quocgia;
            }
            return $http.get(myConfig.api_url + 'timkiem/' + tmp + '/' + encodeURIComponent(val)).then(function(response){
                return response.data.results.map(function(item){
                    return item;
                });
            });
        };
        $scope.autocompleteOptions = {
            types: ['geocode']
        };
        if ($rootScope.globals.currentUser.quocgia&&$rootScope.globals.currentUser.role!='admin') {
            $scope.autocompleteOptions.componentRestrictions = {country: $rootScope.globals.currentUser.quocgia};
        };
        $scope.choncustomer2 = function ($event, bien) {
            console.log($event);
            if ($event.type=='keydown'&&$event.keyCode==9) {
                var tmp = angular.element(document.querySelector('#'+bien));
                tmp.click();
                tmp.focus();
                return false;
            } else {
                return true;
            }
        };
        $scope.customerorderinfo = function () {
            angular.element('.orderCustomerInfo').addClass('show');
        };
        $scope.$on('socket:messageReceived', function (ev, data) {
            //console.log(ev, data);
            switch (data.action) {
                case 'orderEditLists':
                    if (data.data.length) {
                        $scope.$broadcast('updateEditLists', {action: 'updateorders', data: data.data});
                    }
                    break;
                case 'addEditLists':
                    $scope.$broadcast('updateEditLists', {action: 'updateorders', data: [{id: data.id}]});
                    break;
                case 'removeEditLists':
                    $scope.$broadcast('updateEditLists', {action: 'refreshorder', id: data.id});
                    break;
                case 'newOrder':
                    $scope.$broadcast('updateEditLists', {action: 'neworder', data: data.data});
                    break;
                case 'editOrder':
                case 'cancelOrder':
                case 'commentOrder':
                case 'updateStatus':
                case 'updateShipper':
                case 'printOrder':
                    $scope.$broadcast('updateEditLists', {action: 'refreshorder', id: data.data.id});
                    break;
                case 'moveOrder':
                case 'movebackOrder':
                    $scope.$broadcast('updateEditLists', {action: 'moveorder', data: data});
                    break;
            }
            var timedelay = 60000;
            switch (data.action) {
                case 'addEditLists':
                    var order = data.order;
                    var isexist = $rootScope.globals.currentUser.branches.indexOf(order.branch_id);
                    if (isexist>=0&&order.createuserid!=$rootScope.globals.currentUser.userid&&(['admin', 'supregion', 'salesman'].indexOf($rootScope.globals.currentUser.role)==-1)) {
                        var title = order.createuser;
                        var description = 'Đơn hàng ' + order.idv.toUpperCase() + ' (SH: ' + $filter('numberFixedLen')(order.order_number, 3) + ') ' +
                            'giao cho ' + (order.gender ? 'Anh' : 'Chị') + ' ' + order.customer_name + ' tại ' + order.shipping_address + ' ' +
                            'ĐANG ĐIỀU CHỈNH bởi ' + order.createuser + '. Vui lòng tạm ngưng xử lý và chờ trong giây lát.';
                        myAudio.play('edit');
                        $notification(title, {
                            body: description,
                            tag: 'my-tag',
                            icon: $scope.myConfig.api_url + 'avatar/users/' + order.createuserid,
                            delay: timedelay, // in ms
                            focusWindowOnClick: true // focus the window on click
                        }).$on('click', function () {
                            console.log('User has clicked.');
                            //$state.go('home.orders');
                            window.open($scope.myConfig.site_url + '#/home/orders/', '_blank');
                        });
                    }
                    break;
                case 'removeEditLists':
                    var order = data.order;
                    var isexist = $rootScope.globals.currentUser.branches.indexOf(order.branch_id);
                    if (isexist>=0&&order.createuserid!=$rootScope.globals.currentUser.userid&&(['admin', 'supregion', 'salesman'].indexOf($rootScope.globals.currentUser.role)==-1)) {
                        var title = order.createuser;
                        var description = 'Đơn hàng ' + order.idv.toUpperCase() + ' (SH: ' + $filter('numberFixedLen')(order.order_number, 3) + ') đã được ĐIỀU CHỈNH XONG. Vui lòng tiếp tục xử lý để giao lúc ' + order.shipping_on_text + '.';
                        myAudio.play('xong');
                        $notification(title, {
                            body: description,
                            tag: 'my-tag',
                            icon: $scope.myConfig.api_url + 'avatar/users/' + data.createuserid,
                            delay: timedelay, // in ms
                            focusWindowOnClick: true // focus the window on click
                        }).$on('click', function () {
                            console.log('User has clicked.');
                            //$state.go('home.orders');
                            window.open($scope.myConfig.site_url + '#/home/orders/', '_blank');
                        });
                    }
                    break;
                case 'newOrder':
                    var isexist = $rootScope.globals.currentUser.branches.indexOf(data.data.branch_id);
                    if (isexist>=0&&data.createuser.id!=$rootScope.globals.currentUser.userid&&(['admin', 'supregion', 'salesman'].indexOf($rootScope.globals.currentUser.role)==-1)) {
                        var title = data.createuser.last_name;//data.createuser.first_name + ' ' +
                        var description = 'Có đơn hàng ' + data.data.idv.toUpperCase() + ' (SH: ' + $filter('numberFixedLen')(data.data.order_number, 3) + ') TẠO MỚI lúc ' + data.data.time_created_text2 + ' bởi ' + title + '. Dự kiến giao lúc ' + data.data.shipping_on_text + '.';
                        myAudio.play('new');
                        $notification(title, {
                            body: description,
                            tag: 'my-tag',
                            icon: $scope.myConfig.api_url + 'avatar/users/' + data.createuser.id,
                            delay: timedelay, // in ms
                            focusWindowOnClick: true // focus the window on click
                        }).$on('click', function () {
                            console.log('User has clicked.');
                            //$state.go('home.orders');
                            window.open($scope.myConfig.site_url + '#/home/orders/', '_blank');
                        });
                    }
                    break;
                //case 'editOrder':
                case 'cancelOrder':
                    var isexist = $rootScope.globals.currentUser.branches.indexOf(data.data.branch_id);
                    if (isexist>=0&&data.createuser.id!=$rootScope.globals.currentUser.userid&&(['admin', 'supregion', 'salesman'].indexOf($rootScope.globals.currentUser.role)==-1)) {
                        var title = data.createuser.last_name;//data.createuser.first_name + ' ' +
                        var description = 'Đơn hàng ' + data.data.idv.toUpperCase() + ' (SH: ' + $filter('numberFixedLen')(data.data.order_number, 3) + ') đã được HỦY bởi ' + title + '. Lý do: ' + data.data.lydo;
                        myAudio.play('cancel');
                        $notification(title, {
                            body: description,
                            tag: 'my-tag',
                            icon: $scope.myConfig.api_url + 'avatar/users/' + data.createuser.id,
                            delay: timedelay, // in ms
                            focusWindowOnClick: true // focus the window on click
                        }).$on('click', function () {
                            console.log('User has clicked.');
                            //$state.go('home.orders');
                            window.open($scope.myConfig.site_url + '#/home/orders/', '_blank');
                        });
                    }
                    break;
                case 'commentOrder':
                    var isexist = $rootScope.globals.currentUser.branches.indexOf(data.data.branch_id);
                    if (isexist>=0&&data.createuser.id!=$rootScope.globals.currentUser.userid) {
                        var title = data.createuser.last_name;//data.createuser.first_name + ' ' +
                        var description = 'Đơn hàng ' + data.data.idv.toUpperCase() + ' (SH: ' + $filter('numberFixedLen')(data.data.order_number, 3) + ') có bình luận mới từ ' + title + ': ' + data.comment;
                        myAudio.play('comment');
                        $notification(title, {
                            body: description,
                            tag: 'my-tag',
                            icon: $scope.myConfig.api_url + 'avatar/users/' + data.createuser.id,
                            delay: timedelay, // in ms
                            focusWindowOnClick: true // focus the window on click
                        }).$on('click', function () {
                            console.log('User has clicked.');
                            //$state.go('home.orders');
                            window.open($scope.myConfig.site_url + '#/home/orders/', '_blank');
                        });
                    }
                    break;
                //case 'updateStatus':
                //case 'updateShipper':
                //case 'printOrder':
                case 'moveOrder':
                    var isexist = $rootScope.globals.currentUser.branches.indexOf(data.data.branch_id);
                    if (isexist>=0&&data.createuser!=$rootScope.globals.currentUser.userid) {
                        $http.post($scope.myConfig.api_url + 'getInfo/branches', {
                            fromBranch: data.fromBranch,
                            toBranch: data.toBranch,
                            userId: data.createuser
                        }).success(function (response) {
                            if (response.status == 'success'&&(['admin', 'supregion', 'salesman'].indexOf($rootScope.globals.currentUser.role)==-1)) {
                                var title = response.createuser.last_name;//response.createuser.first_name + ' ' +
                                var description = 'Có đơn hàng ' + data.data.idv.toUpperCase() + ' (SH: ' + $filter('numberFixedLen')(data.data.order_number, 3) + ') ĐƯỢC CHUYỂN từ CN ' + response.fromBranch + ' đến CN ' + response.toBranch + '. Dự kiến giao lúc ' + data.data.shipping_on_text + '.';
                                myAudio.play('transfer');
                                $notification(title, {
                                    body: description,
                                    tag: 'my-tag',
                                    icon: $scope.myConfig.api_url + 'avatar/users/' + response.createuser.id,
                                    delay: timedelay, // in ms
                                    focusWindowOnClick: true // focus the window on click
                                }).$on('click', function () {
                                    console.log('User has clicked.');
                                    //$state.go('home.orders');
                                    window.open($scope.myConfig.site_url + '#/home/orders/', '_blank');
                                });
                            }
                        });
                    }
                    break;
                case 'movebackOrder':
                    var isexist = $rootScope.globals.currentUser.branches.indexOf(data.data.branch_id);
                    if (isexist>=0&&data.createuser!=$rootScope.globals.currentUser.userid) {
                        $http.post($scope.myConfig.api_url + 'getInfo/branches', {
                            fromBranch: data.fromBranch,
                            toBranch: data.toBranch,
                            userId: data.createuser
                        }).success(function (response) {
                            if (response.status == 'success'&&(['admin', 'supregion', 'salesman'].indexOf($rootScope.globals.currentUser.role)==-1)) {
                                var title = response.createuser.last_name;//response.createuser.first_name + ' ' +
                                var description = 'Có đơn hàng ' + data.data.idv.toUpperCase() + ' (SH: ' + $filter('numberFixedLen')(data.data.order_number, 3) + ') CHUYỂN NGƯỢC VỀ từ CN ' + response.fromBranch + ' đến CN ' + response.toBranch + '. Dự kiến giao lúc ' + data.data.shipping_on_text + '.';
                                myAudio.play('disagree');
                                $notification(title, {
                                    body: description,
                                    tag: 'my-tag',
                                    icon: $scope.myConfig.api_url + 'avatar/users/' + response.createuser.id,
                                    delay: timedelay, // in ms
                                    focusWindowOnClick: true // focus the window on click
                                }).$on('click', function () {
                                    console.log('User has clicked.');
                                    //$state.go('home.orders');
                                    window.open($scope.myConfig.site_url + '#/home/orders/', '_blank');
                                });
                            }
                        });
                    }
                    break;
            }
        });
    })
.controller('DashBoardController', function ($scope, $rootScope, $http, mySocket) {
      //$scope.myConfig = myConfig;
      $rootScope.page = 'thongke';
      $rootScope.titlepage = 'Trang thống kê';
        $scope.thongkehoadon = {
            orders: []
        };
      $http.post($scope.myConfig.api_url + 'index', {}).success(function (response) {
          $scope.ngayhientai = new Date(response.ngayhientai * 1000);
          $scope.thongke = response.thongke;
          $scope.thongketinhtrang('theongay');
          $rootScope.appLoaded = true;
      });
        $scope.thongketinhtrang = function (loai) {
            $http.post($scope.myConfig.api_url + 'thongke', {loai: loai, ngayhientai: $scope.ngayhientai}).success(function (response) {
                $scope.thongkehoadon = response.thongke;
                $scope.danhsach = response.danhsach;
                $scope.danhsach2 = response.danhsach2;
            });
        };
        $scope.getVolumeSum = function(items) {
            return items
                .map(function(x) { return parseInt(x.total); })
                .reduce(function(a, b) { return parseInt(a) + parseInt(b); });
        };
        $scope.dangclick = 0;
        $scope.updateallstatus = function(item) {
            if (!$scope.dangclick) {
                var tmp = [];
                angular.forEach(item.orders, function (value) {
                    if (value.status == 3) {
                        tmp.push(value.id);
                    }
                });
                if (tmp.length) {
                    if (confirm('Are you sure?')) {
                        $scope.dangclick = 1;
                        $http.post($scope.myConfig.api_url + 'capnhattatca/orders', {
                            ids: tmp,
                            shipper: item.employee_id,
                            tongtienthu: item.tongtiengiu,
                            ngaythu: $scope.ngayhientai,
                            status: 4
                        }).success(function (response) {
                            $scope.thongketinhtrang('theongay');
                            $scope.dangclick = 0;
                            angular.forEach(tmp, function (value) {
                                mySocket.emit('sendMessage', {action: 'updateStatus', data: {id: value}, users: []});
                            });
                        });
                    }
                }
            }
        };
        $scope.tongdonhang = function (danhsach) {
            var tmp = 0;
            if (danhsach.length) {
                angular.forEach(danhsach, function (value) {
                    if (value.soluong&&value.loai!=5) {
                        tmp += parseInt(value.soluong);
                    }
                });
            }
            return tmp;
        };
        $scope.tongsomon = function (danhsach) {
            var tmp = 0;
            if (danhsach.length) {
                angular.forEach(danhsach, function (value) {
                    if (value.total_item&&value.loai!=5) {
                        tmp += parseInt(value.total_item);
                    }
                });
            }
            return tmp;
        };
        $scope.tongsotien2 = function (danhsach) {
            var tmp = 0;
            if (danhsach.length) {
                angular.forEach(danhsach, function (value) {
                    if (value.total_item&&value.loai!=5) {
                        tmp += parseInt(value.total);
                    }
                });
            }
            return tmp;
        };
        /*$scope.causeError = function() {

            foo();

        };


        // ---
        // PRIVATE METHODS.
        // ---


        function bar() {

            // NOTE: "y" is undefined.
            var x = y;

        }


        function foo() {

            bar();

        }
        $scope.causeError();*/
    });